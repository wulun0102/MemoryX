/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    RobotAPI::EntityDrawerTestApp
* @author     vahrenkamp
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "EntityDrawerTest.h"
#include <RobotAPI/libraries/core/Pose.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectClass.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>
#include <MemoryX/interface/components/PriorKnowledgeInterface.h>

namespace armarx
{
    void EntityDrawerTest::onInitComponent()
    {
        // need priorknowledge to load data
        usingProxy("PriorKnowledge");

    }


    void EntityDrawerTest::onConnectComponent()
    {
        ARMARX_INFO << "starting Entity drawer test";
        try
        {
            ARMARX_INFO << "Connecting to entoty drawer topic";
            prxDD = getTopic<memoryx::EntityDrawerInterfacePrx>("DebugDrawerUpdates");
            ARMARX_INFO << "Conneting to prior knowledge";
            memoryx::PriorKnowledgeInterfacePrx priorKnowledgePrx = getProxy<memoryx::PriorKnowledgeInterfacePrx>("PriorKnowledge");
            ARMARX_INFO << "clearing all layers";
            prxDD->clearAll();

            std::string objClassName1 = "vitaliscereal";
            ARMARX_INFO << "Getting object class " << objClassName1;
            memoryx::PersistentObjectClassSegmentBasePrx classesSegmentPrx = priorKnowledgePrx->getObjectClassesSegment();
            memoryx::EntityBasePtr classesEntity1 = classesSegmentPrx->getEntityByName(objClassName1);
            memoryx::ObjectClassPtr objectClass1 = memoryx::ObjectClassPtr::dynamicCast(classesEntity1);

            Eigen::Matrix4f p;
            p.setIdentity();
            p(0, 3) = 1000.0f;
            p(2, 3) = 2000.0f;
            PosePtr gp(new Pose(p));
            ARMARX_INFO << "drawing object " << objClassName1 << " at " << p.block(0, 3, 3, 1).transpose();
            prxDD->setObjectVisu("EntityDrawerTest", objClassName1, objectClass1, gp);

            ARMARX_INFO << "sleeping for 5 seconds...";
            usleep(5000000);
            ARMARX_INFO << "moving object for 5 seconds...";
            IceUtil::Time t = IceUtil::Time::now();
            float px = 1000;
            float py = 1000;
            float counter = 0;
            while ((IceUtil::Time::now() - t).toSeconds() < 5)
            {
                counter += 0.001f;
                px = 1000 * sin(counter);
                py = 1000 * cos(counter);
                gp->position->x = px;
                gp->position->y = py;

                prxDD->updateObjectPose("EntityDrawerTest", objClassName1, gp);
                usleep(1000);
            }
            ARMARX_INFO << "setting color of object " << objClassName1;
            DrawColor c;
            c.r = 1.0f;
            c.g = 0.3f;
            c.b = 0.3f;
            c.a = 0.3f;
            prxDD->updateObjectColor("EntityDrawerTest", objClassName1, c);

            std::string objClassName2 = "multivitaminjuice";
            ARMARX_INFO << "Getting object class " << objClassName2;
            memoryx::EntityBasePtr classesEntity2 = classesSegmentPrx->getEntityByName(objClassName2);
            memoryx::ObjectClassPtr objectClass2 = memoryx::ObjectClassPtr::dynamicCast(classesEntity2);

            Eigen::Matrix4f p2;
            p2.setIdentity();
            p2(0, 3) = 1000.0f;
            p2(2, 3) = 3000.0f;
            PosePtr gp2(new Pose(p2));
            ARMARX_INFO << "drawing object " << objClassName2 << " at " << p2.block(0, 3, 3, 1).transpose();
            prxDD->setObjectVisu("EntityDrawerTest", objClassName2, objectClass2, gp2);
            ARMARX_INFO << "sleeping for 5 seconds...";
            usleep(5000000);
            ARMARX_INFO << "clearing layer...";
            prxDD->clearLayer("EntityDrawerTest");
            ARMARX_INFO << "sleeping for 5 seconds...";
            usleep(5000000);
            ARMARX_INFO << "drawing object " << objClassName1 << " at " << gp->toEigen().block(0, 3, 3, 1).transpose();
            prxDD->setObjectVisu("EntityDrawerTest", objClassName1, objectClass1, gp);
            ARMARX_INFO << "drawing object " << objClassName2 << " at " << gp2->toEigen().block(0, 3, 3, 1).transpose();
            prxDD->setObjectVisu("EntityDrawerTest", objClassName2, objectClass2, gp2);


        }
        catch (...)
        {
            ARMARX_IMPORTANT << "TEST FAILED";
            return;
        }
        ARMARX_INFO << "FINISHED test";

    }
}
