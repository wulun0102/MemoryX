export CORE_PATH=../../../Core
export GUI_PATH=../../../Gui
export MEMX_PATH=../..
export CORE_BIN_PATH=$CORE_PATH/build/bin
export GUI_BIN_PATH=$GUI_PATH/build/bin
export MEMX_BIN_PATH=$MEMX_PATH/build/bin
export SCRIPT_PATH=$CORE_PATH/build/bin

echo -e "\nThis script will perform FULL IMPORT (kitchen scenes + kitchen models + object models) into MongoDB.\n"
echo -e "*** Starting common components...\n"

$SCRIPT_PATH/startApplication.sh $MEMX_BIN_PATH/CommonStorageRun --Ice.Config=./configs/ImportArmarKitchen.cfg &
sleep 2
$SCRIPT_PATH/startApplication.sh $MEMX_BIN_PATH/LongtermMemoryRun --Ice.Config=./configs/ImportArmarKitchen.cfg &
sleep 2
$SCRIPT_PATH/startApplication.sh $MEMX_BIN_PATH/WorkingMemoryRun --Ice.Config=./configs/ImportArmarKitchen.cfg &
sleep 2

echo -e "\n*** Importing ArmarKitchen scene...\n"
$SCRIPT_PATH/startApplication.sh $MEMX_BIN_PATH/XMLSceneImporterRun --Ice.Config=./configs/ImportArmarKitchen.cfg

echo -e "\n*** Importing MobileKitchen scene...\n"
$SCRIPT_PATH/startApplication.sh $MEMX_BIN_PATH/XMLSceneImporterRun --Ice.Config=./configs/ImportMobileKitchen.cfg

echo -e "\n*** Importing ArmarKitchen inventor models...\n"
$SCRIPT_PATH/startApplication.sh $MEMX_BIN_PATH/PriorKnowledgeRun --Ice.Config=./configs/ImportArmarKitchenModels.cfg &
sleep 2

$SCRIPT_PATH/startApplication.sh $MEMX_BIN_PATH/PriorKnowledgeImporterRun --Ice.Config=./configs/ImportArmarKitchenModels.cfg

killall PriorKnowledgeRun -SIGINT
sleep 1

echo -e "\n*** Importing MobileKitchen inventor models...\n"
$SCRIPT_PATH/startApplication.sh $MEMX_BIN_PATH/PriorKnowledgeRun --Ice.Config=./configs/ImportMobileKitchenModels.cfg &
sleep 2

$SCRIPT_PATH/startApplication.sh $MEMX_BIN_PATH/PriorKnowledgeImporterRun --Ice.Config=./configs/ImportMobileKitchenModels.cfg

killall PriorKnowledgeRun -SIGINT
sleep 1

echo -e "\n*** Importing object inventor models...\n"
$SCRIPT_PATH/startApplication.sh $MEMX_BIN_PATH/PriorKnowledgeRun --Ice.Config=./configs/ImportObjectModels.cfg &
sleep 2

$SCRIPT_PATH/startApplication.sh $MEMX_BIN_PATH/PriorKnowledgeImporterRun --Ice.Config=./configs/ImportObjectModels.cfg

echo -e "\n*** Killing Ice components...\n"

killall PriorKnowledgeRun -SIGINT
killall WorkingMemoryRun -SIGINT
killall LongtermMemoryRun -SIGINT
killall CommonStorageRun -SIGINT

sleep 1

killall PriorKnowledgeRun -9 2> /dev/null
killall WorkingMemoryRun -9 2> /dev/null
killall LongtermMemoryRun -9 2> /dev/null
killall CommonStorageRun -9 2> /dev/null

echo -e "DONE!"