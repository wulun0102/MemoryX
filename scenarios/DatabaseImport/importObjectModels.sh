export CORE_PATH=../../../Core
export GUI_PATH=../../../Gui
export MEMX_PATH=../..
export CORE_BIN_PATH=$CORE_PATH/build/bin
export GUI_BIN_PATH=$GUI_PATH/build/bin
export MEMX_BIN_PATH=$MEMX_PATH/build/bin
export SCRIPT_PATH=$CORE_PATH/build/bin

$SCRIPT_PATH/startApplication.sh $MEMX_BIN_PATH/CommonStorageRun --Ice.Config=./configs/ImportObjectModels.cfg &
sleep 2
$SCRIPT_PATH/startApplication.sh $MEMX_BIN_PATH/PriorKnowledgeRun --Ice.Config=./configs/ImportObjectModels.cfg &
sleep 2
$SCRIPT_PATH/startApplication.sh $MEMX_BIN_PATH/PriorKnowledgeImporterRun --Ice.Config=./configs/ImportObjectModels.cfg

# Done, now kill all components

killall PriorKnowledgeRun -SIGINT
killall CommonStorageRun -SIGINT

sleep 1

killall PriorKnowledgeRun -9 2> /dev/null
killall CommonStorageRun -9 2> /dev/null
