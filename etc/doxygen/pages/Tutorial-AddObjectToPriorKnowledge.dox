/**

\page MemoryX-Tutorial-AddObjectToPriorKnowledge Tutorial: Add new objects to PriorKnowledge

The following steps are necessary to add new objects to PriorKnowledge:

\li open the PriorMemoryEditor gui plugin in ArmarXGui
\li select the collection to add the new object to (typically  Prior_<SceneName>Objects)
\li click the 'Add' button in the 'Object Classes' section

A dialog opens where the following object information must be entered:

- *Class Name*: the name used to identify this object
- *Recognition Tab* -> *Recognition Method*: this method is used to recognize the object through VisionX perception mechanisms
- *Manipulation Tab* -> *Manipulation Object File*: 
-# *Manipulation object file*: use the filechooser or add a path to a manipulation object description containing grasps, visualization- and collision models, and grasp definitions (uses <a href="https://gitlab.com/Simox/simox/wikis/FileFormat">Simox format</a>). Specify a local file and it will be imported to the prior knowledge database.
-# *Visualization/Collision model*: specify directly a path to a mesh model. Possible formats are .iv (Inventor) and .wrl/.vrml (VRML). Hint: Blender has an optional plugin to export vrml files. After saving these mesh model files, they automatically incorporated into a <a href="https://gitlab.com/Simox/simox/wikis/FileFormat">Simox manipulation object file</a>. Specify local files here and they will be imported to the prior knowledge database.
- *Motion Tab* -> *Motion Model*: the default values is 'Static' and should not be changed (motion models are updated on the fly during execution of robot programs)

The 'Data File' attribute of both TexturedObjectRecognition and SegmentableObjectRecognition must be specified in the IVT format.

\note Either Textured or Segmentable recognition must be specified for use in the dynamic simulation. In this case the 'Data File' attribute can be left empty.

*/
