/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::ArmarXObjects::WorkingMemoryObjectPoseProvider
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "WorkingMemoryObjectPoseProvider.h"

#include <ArmarXCore/core/time/TimeUtil.h>
#include <ArmarXCore/core/time/CycleUtil.h>
#include <RobotAPI/libraries/core/Pose.h>

#include <MemoryX/libraries/memorytypes/entity/ObjectInstance.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>

#include <VirtualRobot/ManipulationObject.h>


namespace memoryx
{
    WorkingMemoryObjectPoseProviderPropertyDefinitions::WorkingMemoryObjectPoseProviderPropertyDefinitions(std::string prefix) :
        armarx::ComponentPropertyDefinitions(prefix)
    {
    }

    armarx::PropertyDefinitionsPtr WorkingMemoryObjectPoseProvider::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr defs(new WorkingMemoryObjectPoseProviderPropertyDefinitions(getConfigIdentifier()));

        defs->component(workingMemory);
        defs->component(robotStateComponent);
        defs->component(priorKnowledge);

        defs->optional(updateFrequency, "UpdateFrequency", "Target number of updates per second.");

        return defs;
    }

    std::string WorkingMemoryObjectPoseProvider::getDefaultName() const
    {
        return "WorkingMemoryObjectPoseProvider";
    }


    void WorkingMemoryObjectPoseProvider::onInitComponent()
    {
    }


    void WorkingMemoryObjectPoseProvider::onConnectComponent()
    {
        ARMARX_CHECK_NOT_NULL(workingMemory);
        ARMARX_CHECK_NOT_NULL(robotStateComponent);
        ARMARX_CHECK_NOT_NULL(priorKnowledge);

        attachments.initFromProxies(workingMemory, robotStateComponent);
        objectClassSegment.initFromProxy(priorKnowledge);

        // A periodic task logs an important info when the cycle time is not met.
        // To avoid this, we use a running task.
        task = new armarx::SimpleRunningTask<>([this]()
        {
            armarx::CycleUtil cycle(int(1000 / updateFrequency));

            while (task && !task->isStopped())
            {
                provideObjectInstancesPoses();
                cycle.waitForCycleDuration();
            }
        });

        task->start();
    }


    void WorkingMemoryObjectPoseProvider::onDisconnectComponent()
    {
        if (task)
        {
            task->stop();
        }
    }


    void WorkingMemoryObjectPoseProvider::onExitComponent()
    {
    }


    void WorkingMemoryObjectPoseProvider::provideObjectInstancesPoses()
    {
        std::scoped_lock lock(mutex);
        std::vector<ObjectInstancePtr> instances = attachments.queryObjects();
        provideObjectInstancesPoses(instances);
    }

    void WorkingMemoryObjectPoseProvider::provideObjectInstancesPoses(const std::vector<ObjectInstancePtr>& objectInstances)
    {
        armarx::objpose::data::ProvidedObjectPoseSeq objectPoses;

        for (const auto& instance : objectInstances)
        {
            objectPoses.push_back(toProvidedObjectPose(instance));
        }

        objectPoseTopic->reportObjectPoses(getName(), objectPoses);
    }


    armarx::objpose::data::ProvidedObjectPose WorkingMemoryObjectPoseProvider::toProvidedObjectPose(
        const ObjectInstancePtr& instance)
    {
        armarx::objpose::data::ProvidedObjectPose pose;

        pose.objectType = armarx::objpose::KnownObject;

        pose.objectID.dataset = "";
        pose.objectID.className = instance->getMostProbableClass();
        pose.objectID.instanceName = instance->getId();

        pose.objectPose = new armarx::Pose(instance->getPose()->toEigen());
        pose.objectPoseFrame = instance->getPose()->getFrame();

        pose.confidence = instance->getExistenceCertainty();
        if (instance->hasLocalizationTimestamp())
        {
            pose.timestampMicroSeconds = instance->getLocalizationTimestamp().toMicroSeconds();
        }
        else
        {
            pose.timestampMicroSeconds = armarx::TimeUtil::GetTime().toMicroSeconds();
        }

        std::string className = instance->getMostProbableClass();
        std::optional<ObjectClassWrapper> objectClass = objectClassSegment.getClass(className);
        //ARMARX_IMPORTANT << "Looking for class: " << className;
        if (objectClass)
        {
            VirtualRobot::CollisionModelPtr collisionModel = objectClass->manipulationObject->getCollisionModel();
            VirtualRobot::BoundingBox bb = collisionModel->getBoundingBox(false);
            Eigen::Vector3f bbMin = bb.getMin();
            Eigen::Vector3f bbMax = bb.getMax();
            Eigen::Vector3f extents = bbMax - bbMin;
            //ARMARX_IMPORTANT << "Bounding box: " << extents.transpose();

            pose.localOOBB = new armarx::objpose::Box();
            pose.localOOBB->position = new armarx::Vector3((0.5 * (bbMin + bbMax)).eval());
            pose.localOOBB->orientation = new armarx::Quaternion(Eigen::Quaternionf::Identity());
            pose.localOOBB->extents = new armarx::Vector3(extents);
        }

        pose.providerName = getName();

        return pose;
    }



    armarx::objpose::ProviderInfo WorkingMemoryObjectPoseProvider::getProviderInfo(const Ice::Current&)
    {
        armarx::objpose::ProviderInfo info;

        return info;
    }

    void WorkingMemoryObjectPoseProvider::attachObjectToRobotNode(const AttachObjectToRobotNodeInput& attachment, const Ice::Current&)
    {
        std::scoped_lock lock(mutex);
        attachments.attachObjectToRobotNode(attachment);
    }

    void WorkingMemoryObjectPoseProvider::detachObjectFromRobotNode(const DetachObjectFromRobotNodeInput& detachment, const Ice::Current&)
    {
        std::scoped_lock lock(mutex);
        attachments.detachObjectFromRobotNode(detachment);
    }

}
