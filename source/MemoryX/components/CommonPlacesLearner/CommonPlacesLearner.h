/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::CommonPlacesLearner
* @author     Alexey Kozlov ( kozlov at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <MemoryX/interface/components/CommonPlacesLearnerInterface.h>
#include <MemoryX/interface/components/LongtermMemoryInterface.h>
#include <MemoryX/interface/components/PriorKnowledgeInterface.h>

#include "GaussianMixturePositionFusion.h"
#include "GaussianMixtureAssociationMethod.h"

// memoryx helpers
#include <MemoryX/libraries/helpers/GaussianMixtureHelpers/GMMReducer.h>


namespace memoryx
{

    class CommonPlacesLearnerPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        CommonPlacesLearnerPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("LTMSegment", "objects", "Name of segment in LTM to store learned CommonPlaces / to run queries on");
            defineOptionalProperty<float>("AgingFactor", 0.8, "Discount factor to make older observations less significant");
            defineOptionalProperty<float>("PruningThreshold", 0.0, "Maximum weight of Gauss component to be pruned");
            defineOptionalProperty<std::string>("MergingDistanceType", "KL", "Distance type to be used for association/merging: Mahalanobis, ISD, KL");
            defineOptionalProperty<float>("MergingThreshold", 0.0, "Maximum distance value for association (meaning depends upon measure used)");
            defineOptionalProperty<std::string>("GMMReducerAlgorithm", "West", "A GMM reducing algorithm to be used for queries: West, Runnalls, Williams");
        }
    };

    class ARMARXCOMPONENT_IMPORT_EXPORT CommonPlacesLearner :
        virtual public CommonPlacesLearnerInterface,
        virtual public armarx::Component
    {
    public:
        // inherited from Component
        std::string getDefaultName() const override
        {
            return "CommonPlacesLearner";
        };
        void onInitComponent() override;
        void onConnectComponent() override;
        void onExitComponent() override;

        // inherited from CommonPlacesLearnerInterface
        void setLTMSegmentName(const ::std::string& segmentName, const ::Ice::Current& = Ice::emptyCurrent) override;
        void setAgingFactor(float factor, const ::Ice::Current& = Ice::emptyCurrent) override;
        void setMergingThreshold(float threshold, const ::Ice::Current& = Ice::emptyCurrent) override;

        void learnFromSnapshot(const ::std::string& snapshotName, const ::Ice::Current& = Ice::emptyCurrent) override;
        void learnFromObject(const ObjectInstanceBasePtr& newObject, const ::Ice::Current& = Ice::emptyCurrent) override;
        void learnFromObjectMCA(const ::std::string& objectName, const MultivariateNormalDistributionBasePtr& posDist,
                                const ::Ice::Current& = Ice::emptyCurrent) override;

        // inherited from CommonPlacesQueryInterface
        GaussianMixtureDistributionBasePtr getPositionFull(const ::std::string& objectName, const ::Ice::Current& = Ice::emptyCurrent) override;
        GaussianMixtureDistributionBasePtr getPositionReducedByComponentCount(const ::std::string& objectName, Ice::Int compCount, const ::Ice::Current& = Ice::emptyCurrent) override;
        GaussianMixtureDistributionBasePtr getPositionReducedByMaxDeviation(const ::std::string& objectName, Ice::Float maxDeviation, DeviationType devType, const ::Ice::Current& = Ice::emptyCurrent) override;
        NormalDistributionBasePtr getPositionAsGaussian(const ::std::string& objectName, const ::Ice::Current& = Ice::emptyCurrent) override;

        Cluster3DList getPositionClustersByComponentCount(const ::std::string& objectName, Ice::Int compCount, const ::Ice::Current& = Ice::emptyCurrent) override;
        Cluster3DList getPositionClustersByMaxDeviation(const ::std::string& objectName, Ice::Float maxDeviation, DeviationType devType, const ::Ice::Current& = Ice::emptyCurrent) override;

    private:
        PriorKnowledgeInterfacePrx priorKnowledgePrx;

        LongtermMemoryInterfacePrx longtermMemoryPrx;
        PersistentObjectInstanceSegmentBasePrx ltmInstancesSegmentPrx;

        GaussianMixtureAssociationMethodPtr assMethod;
        GaussianMixturePositionFusionPtr fusionMethod;
        GMMReducerPtr gmmReducer;

        //
        DeviationMeasure convertDeviationType(DeviationType devType);
        Cluster3DList gmmToClusterList(const GaussianMixtureDistributionBasePtr& gmm);
        void getChildClasses(std::string className, NameList& result);

    public:
        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return armarx::PropertyDefinitionsPtr(
                       new CommonPlacesLearnerPropertyDefinitions(
                           getConfigIdentifier()));
        }

    };

    using CommonPlacesLearnerPtr = IceUtil::Handle<CommonPlacesLearner>;
}

