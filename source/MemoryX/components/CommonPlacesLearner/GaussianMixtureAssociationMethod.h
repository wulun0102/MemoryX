/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::CommonPlacesLearner
* @author     Alexey Kozlov <kozlov@kit.edu>
* @copyright  2013 Alexey Kozlov
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <string>
#include <ArmarXCore/core/system/ImportExport.h>
#include <MemoryX/interface/core/FusionMethods.h>
#include <MemoryX/core/entity/ProbabilityMeasures.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectInstance.h>
#include <MemoryX/libraries/helpers/GaussianMixtureHelpers/GMMDistance.h>

namespace memoryx
{
    /**
     * @class MahalanobisAssociationMethod
     * @ingroup CommonPlacesLearner
     */
    class ARMARXCORE_IMPORT_EXPORT GaussianMixtureAssociationMethod :
        virtual public GaussianMixtureAssociationMethodBase
    {
    public:
        /**
         * Creates a new GaussianMixtureAssociationMethod
         */
        GaussianMixtureAssociationMethod(float threshold, GMMDistancePtr gmmDistance) :
            threshold(threshold), gmmDistance(gmmDistance)
        {
        }

        void setThreshold(float threshold)
        {
            this->threshold = threshold;
        }

        /**
         * Returns an index of GM component, which is "closest" to the specified gaussian
         *
         * @param gmDistr GaussianMixture to associate (e.g. current belief state)
         * @param newComp new gaussian (e.g. new observation)
         *
         * @return index of associated GM component, or -1 if none found
         */
        Ice::Int getAssociatedComponentIndex(const GaussianMixtureDistributionBasePtr& gmDistr, const GaussianMixtureComponent& newComp, const ::Ice::Current& = Ice::emptyCurrent) override
        {
            if (threshold < 0.)
            {
                return -1;
            }

            gmDistr->addComponent(newComp);
            gmDistr->normalize();

            float minDist = threshold + 100.;
            int result = -1;

            for (int i = 0; i < gmDistr->size() - 1; ++i)
            {
                float curDist = gmmDistance->getDistance(gmDistr->getComponent(i), newComp);
                std::cout << "Cur dist: "  << curDist << std::endl;

                if (curDist < minDist && curDist < threshold)
                {
                    minDist = curDist;
                    result = i;
                }
            }

            std::cout << "Min dist: "  << minDist << " , threshold: " << threshold << std::endl;

            return result;
        };

    private:
        float threshold;
        GMMDistancePtr gmmDistance;

    };

    using GaussianMixtureAssociationMethodPtr = IceInternal::Handle<GaussianMixtureAssociationMethod>;

}

