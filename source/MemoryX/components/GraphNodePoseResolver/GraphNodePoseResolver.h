/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::ArmarXObjects::GraphNodePoseResolver
 * @author     Valerij Wittenbeck ( valerij.wittenbeck at student dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/ManagedIceObject.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>
#include <RobotAPI/interface/core/RobotState.h>
#include <RobotAPI/libraries/core/FramedPose.h>
#include <MemoryX/components/PriorKnowledge/PriorKnowledge.h>
#include <MemoryX/components/WorkingMemory/WorkingMemory.h>
#include <MemoryX/interface/components/GraphNodePoseResolverInterface.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>
#include <MemoryX/libraries/memorytypes/variants/GraphNode/GraphNode.h>

#include <mutex>

namespace memoryx
{
    class GraphNodePoseResolverPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        GraphNodePoseResolverPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("WorkingMemoryName", "WorkingMemory", "Name of the WorkingMemory that should be used");
            defineOptionalProperty<std::string>("RobotStateComponentName", "RobotStateComponent", "Name of the RobotStateComponent that should be used");

        }
    };
    /**
     * @class GraphNodePoseResolver
     * @brief A brief description
     *
     * Detailed Description
     */
    class GraphNodePoseResolver :
        virtual public GraphNodePoseResolverInterface,
        virtual public armarx::Component
    {
    public:
        GraphNodePoseResolver();
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "GraphNodePoseResolver";
        }

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return armarx::PropertyDefinitionsPtr(new GraphNodePoseResolverPropertyDefinitions(
                    getConfigIdentifier()));
        }

        armarx::FramedPoseBasePtr resolveToGlobalPose(const GraphNodeBasePtr& node, const ::Ice::Current& = Ice::emptyCurrent) override;
        std::string getNearestNodeIdToPosition(const std::string& sceneName, const std::string& nodeParentName, ::Ice::Float x, ::Ice::Float y, const ::Ice::Current& = Ice::emptyCurrent) override;
        GraphNodeBasePtr getNearestNodeToPosition(const std::string& sceneName, const std::string& nodeParentName, ::Ice::Float x, ::Ice::Float y, const ::Ice::Current& = Ice::emptyCurrent) override;
        std::string getNearestNodeIdToPose(const std::string& sceneName, const std::string& nodeParentName, const armarx::FramedPoseBasePtr& pose, const ::Ice::Current& = Ice::emptyCurrent) override;
        GraphNodeBasePtr getNearestNodeToPose(const std::string& sceneName, const std::string& nodeParentName, const armarx::FramedPoseBasePtr& pose, const ::Ice::Current& = Ice::emptyCurrent) override;
        std::string getNearestRobotLocationNodeId(const GraphNodeBasePtr& node, const ::Ice::Current& = Ice::emptyCurrent) override;
        GraphNodeBasePtr getNearestRobotLocationNode(const GraphNodeBasePtr& node, const ::Ice::Current& = Ice::emptyCurrent) override;

        void forceRefetch(const std::string& nodeId, const ::Ice::Current& = Ice::emptyCurrent) override;
        void forceRefetchForAll(const ::Ice::Current& = Ice::emptyCurrent) override;

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        void onConnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        void onDisconnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        void onExitComponent() override;

        armarx::FramedPosePtr getRelativeNodePositionForObject(const std::string& objectClassName, const GraphNodeBasePtr& node);

    private:
        struct CachedNodeInfo
        {
            GraphNodePtr node;
            armarx::FramedPosePtr globalPose;
            std::vector<std::string> parents;
        };

        CachedNodeInfo getCacheEntry(const std::string& nodeId);
        GraphNodeBasePtr getNearestNodeToPoseImpl(const std::string& sceneName, const std::string& nodeParentName, const Eigen::Matrix4f& pose,
                bool ignoreOrientation = false, bool ignoreParent = false);

        memoryx::PriorKnowledgeInterfacePrx prior;
        memoryx::WorkingMemoryInterfacePrx wm;
        armarx::RobotStateComponentInterfacePrx rsc;

        memoryx::ObjectInstanceMemorySegmentBasePrx objectInstances;
        memoryx::GraphMemorySegmentBasePrx graphSegment;

        std::map<std::string, std::map<std::string, armarx::FramedPositionPtr>> relativePlacesettingPositions;
        std::map<std::string, CachedNodeInfo> nodeInfoCache;
        std::map<std::string, GraphNodeBaseList> sceneNodesCache;

        std::recursive_mutex cacheMutex;
    };

    using GraphNodePoseResolverPtr = IceInternal::Handle<GraphNodePoseResolver>;
}

