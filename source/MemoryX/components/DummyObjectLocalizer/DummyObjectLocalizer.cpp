/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::ArmarXObjects::DummyObjectLocalizer
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "DummyObjectLocalizer.h"

#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/observers/variant/TimestampVariant.h>
#include <ArmarXCore/util/json/JSONObject.h>
#include <MemoryX/core/entity/ProbabilityMeasures.h>
#include <fstream>
#include <sstream>

namespace armarx
{
    void DummyObjectLocalizer::onInitComponent()
    {
        std::setlocale(LC_ALL, "En_US");

        std::string objectPoseFile = getProperty<std::string>("ObjectPoseFile").getValue();
        if (!armarx::ArmarXDataPath::getAbsolutePath(objectPoseFile, objectPoseFile))
        {
            ARMARX_ERROR << "Could not find object pose file file in ArmarXDataPath: " << objectPoseFile;
            return;
        }

        std::stringstream fileContent;
        try
        {
            std::ifstream input;
            input.exceptions(std::ifstream::failbit | std::ifstream::badbit);
            input.open(objectPoseFile, std::ios::binary);
            fileContent << input.rdbuf();
        }
        catch (std::exception const& ex)
        {
            ARMARX_WARNING << "Could not open or read from file: " << objectPoseFile;
        }

        ARMARX_INFO << "File content: " << fileContent.str();

        armarx::JSONObject o;
        o.fromString(fileContent.str());
        const Json::Value& j = o.getJsonValue();

        std::vector<std::string> objectNames = j.getMemberNames();

        for (std::string const& objectName : objectNames)
        {
            ARMARX_INFO << "Object name: " << objectName;
            DummyObject object;
            object.name = objectName;

            Json::Value map = j.get(objectName, Json::Value::null);
            std::string encodedPose = map["pose"].asString();
            std::string frame = map["frame"].asString();
            std::string agent = map["agent"].asString();

            std::istringstream poseInput(encodedPose);
            Eigen::Matrix4f pose;
            for (int row = 0; row < 4; ++row)
            {
                for (int column = 0; column < 4; ++column)
                {
                    float value;
                    if (poseInput >> value)
                    {
                        pose.col(column)[row] = value;
                    }
                    else
                    {
                        ARMARX_WARNING << "Failed to read float: " << poseInput.str();
                    }
                }
            }

            object.pose = new armarx::FramedPose(pose, frame, agent);

            dummyObjects.push_back(object);
        }


        usingProxy(getProperty<std::string>("RobotStateComponentName").getValue());
    }


    void DummyObjectLocalizer::onConnectComponent()
    {
        for (DummyObject const& object : dummyObjects)
        {
            ARMARX_INFO << "Object '" << object.name << "': " << *object.pose;
        }


        robotStateComponent = getProxy<armarx::RobotStateComponentInterfacePrx>(getProperty<std::string>("RobotStateComponentName").getValue());
    }


    void DummyObjectLocalizer::onDisconnectComponent()
    {

    }


    void DummyObjectLocalizer::onExitComponent()
    {

    }

    armarx::PropertyDefinitionsPtr DummyObjectLocalizer::createPropertyDefinitions()
    {
        return armarx::PropertyDefinitionsPtr(new DummyObjectLocalizerPropertyDefinitions(
                getConfigIdentifier()));
    }



    memoryx::ObjectLocalizationResultList armarx::DummyObjectLocalizer::localizeObjectClasses(const memoryx::ObjectClassNameList& classes, const Ice::Current&)
    {
        memoryx::ObjectLocalizationResultList result;
        result.reserve(dummyObjects.size());


        armarx::SharedRobotInterfacePrx robot = robotStateComponent->getSynchronizedRobot();
        armarx::TimestampVariantPtr now = armarx::TimestampVariant::nowPtr();
        for (DummyObject const& object : dummyObjects)
        {
            if (std::find(classes.begin(), classes.end(), object.name) == classes.end())
            {
                continue;
            }


            armarx::FramedPosePtr poseInRootFrame = object.pose->toRootFrame(robot);
            memoryx::ObjectLocalizationResult localizedObject;

            localizedObject.objectClassName = object.name;
            ARMARX_INFO << "Reporting: " << object.name << " at " << poseInRootFrame->output();
            localizedObject.position = poseInRootFrame->getPosition();
            localizedObject.orientation = poseInRootFrame->getOrientation();

            localizedObject.recognitionCertainty = 1.0f;

            localizedObject.positionNoise = memoryx::MultivariateNormalDistribution::CreateDefaultDistribution();

            localizedObject.timeStamp = now;


            result.push_back(localizedObject);
        }

        return result;
    }
}
