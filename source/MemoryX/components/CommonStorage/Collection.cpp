/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::CommonStorage
* @author     Alexey Kozlov ( kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "Collection.h"

#include <IceUtil/UUID.h>
#include <mongo/client/dbclient.h>


namespace memoryx
{
    // Collection implementation
    Collection::Collection(const CommonStoragePtr& dbConn, const std::string& collNS) :
        dbConn(dbConn)
    {
        ns = collNS;
        iceId.name = IceUtil::generateUUID();
    }

    Ice::Identity Collection::getIceId() const
    {
        return iceId;
    }

    // inherited from Collection
    Ice::Int Collection::count(const ::Ice::Current& c)
    {
        std::unique_lock lock(dbConnectionMutex);
        return dbConn->count(ns);
    }

    DBStorableData Collection::findByMongoId(const std::string& id, const ::Ice::Current& c)
    {
        std::unique_lock lock(dbConnectionMutex);
        return dbConn->findByMongoId(ns, id);
    }

    DBStorableDataList Collection::findByFieldValue(
        const std::string& fieldName, const std::string& fieldValue,
        const Ice::Current& c)
    {
        std::unique_lock lock(dbConnectionMutex);
        return dbConn->findByFieldValue(ns, fieldName, fieldValue);
    }

    DBStorableData Collection::findOneByFieldValue(
        const std::string& fieldName, const std::string& fieldValue,
        const Ice::Current& c)
    {
        std::unique_lock lock(dbConnectionMutex);
        return dbConn->findOneByFieldValue(ns, fieldName, fieldValue);
    }

    DBStorableDataList Collection::findByFieldValueList(
        const std::string& fieldName, const NameList& fieldValueList,
        const Ice::Current& c)
    {
        std::unique_lock lock(dbConnectionMutex);
        return dbConn->findByFieldValueList(ns, fieldName, fieldValueList);
    }


    DBStorableData Collection::findOneByQuery(const std::string& query,
            const Ice::Current& c)
    {
        std::unique_lock lock(dbConnectionMutex);
        return dbConn->findOneByQuery(ns, query);
    }

    DBStorableDataList Collection::findByQuery(const std::string& query, const ::Ice::Current& c)
    {
        std::unique_lock lock(dbConnectionMutex);
        return dbConn->findByQuery(ns, query);
    }

    DBStorableDataList Collection::findByConstraintQuery(const std::string& query, const std::string& where, const Ice::Current&)
    {
        std::unique_lock lock(dbConnectionMutex);
        return dbConn->findByQuery(ns, query, where);
    }

    DBStorableDataList Collection::findAll(const Ice::Current& c)
    {
        std::unique_lock lock(dbConnectionMutex);
        return dbConn->findAll(ns);
    }

    DBStorableData Collection::findAllUniqueByFieldName(const std::string& fieldName, const ::Ice::Current&)
    {
        std::unique_lock lock(dbConnectionMutex);
        return dbConn->findAllUniqueByFieldName(ns, fieldName);
    }

    EntityIdList Collection::findAllIds(const Ice::Current& c)
    {
        std::unique_lock lock(dbConnectionMutex);
        return dbConn->findAllIds(ns);
    }

    NameList Collection::findAllFieldValues(const std::string& fieldName, const ::Ice::Current&)
    {
        std::unique_lock lock(dbConnectionMutex);
        return dbConn->findAllFieldValues(ns, fieldName);
    }

    std::string Collection::insert(const DBStorableData& obj, const ::Ice::Current& c)
    {
        std::unique_lock lock(dbConnectionMutex);
        return dbConn->insert(ns, obj);
    }

    std::vector<std::string> Collection::insertList(const DBStorableDataList& objectList, const Ice::Current&)
    {
        std::unique_lock lock(dbConnectionMutex);
        return dbConn->insertList(ns, objectList);
    }

    bool Collection::update(const DBStorableData& obj, const ::Ice::Current& c)
    {
        std::unique_lock lock(dbConnectionMutex);
        return dbConn->update(ns, obj, "_id");
    }

    bool Collection::updateWithUserKey(const DBStorableData& obj, const std::string& keyField, const ::Ice::Current& c)
    {
        std::unique_lock lock(dbConnectionMutex);
        return dbConn->update(ns, obj, keyField);
    }

    bool Collection::updateByQuery(const std::string& query, const DBStorableData& obj, const ::Ice::Current& c)
    {
        return dbConn->updateByQuery(ns, query, mongo::fromjson(obj.JSON));
    }

    std::string Collection::save(const DBStorableData& obj,
                                 const Ice::Current& c)
    {
        std::unique_lock lock(dbConnectionMutex);
        return dbConn->insert(ns, obj, true);
    }

    bool Collection::saveWithUserKey(const DBStorableData& obj,
                                     const std::string& keyField, const Ice::Current& c)
    {
        std::unique_lock lock(dbConnectionMutex);
        return dbConn->update(ns, obj, keyField, true);
    }

    bool Collection::removeByMongoId(const std::string& id, const ::Ice::Current& c)
    {
        std::unique_lock lock(dbConnectionMutex);
        return dbConn->removeByMongoId(ns, id);
    }

    bool Collection::removeByFieldValue(const std::string& fieldName,
                                        const std::string& fieldValue, const Ice::Current& c)
    {
        std::unique_lock lock(dbConnectionMutex);
        return dbConn->removeByFieldValue(ns, fieldName, fieldValue);
    }

    bool Collection::removeByQuery(const std::string& query, const ::Ice::Current& c)
    {
        std::unique_lock lock(dbConnectionMutex);
        return dbConn->removeByQuery(ns, query);
    }

    bool Collection::clear(const ::Ice::Current& c)
    {
        std::unique_lock lock(dbConnectionMutex);
        return dbConn->clearCollection(ns);
    }

    bool Collection::ensureIndex(const std::string& fieldName, bool unique, const ::Ice::Current& c)
    {
        std::unique_lock lock(dbConnectionMutex);
        return dbConn->ensureIndex(ns, fieldName, unique);
    }

    std::string Collection::getNS(const ::Ice::Current& c)
    {
        return ns;
    }
}
