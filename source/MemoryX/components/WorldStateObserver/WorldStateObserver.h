/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::WorldStateObserver
* @author     David Schiebener (schiebener at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXCore/observers/variant/ChannelRef.h>

#include <MemoryX/interface/observers/WorldStateObserver.h>
#include <MemoryX/interface/memorytypes/MemorySegments.h>
#include <MemoryX/components/WorkingMemory/WorkingMemory.h>
#include <MemoryX/core/entity/EntityRef.h>

#include <RobotAPI/interface/units/HandUnitInterface.h>

#include <mutex>


namespace memoryx
{

    class WorldStateObserverPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        WorldStateObserverPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
        }
    };


    /*!
     * \brief The WorldStateObserver class
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT WorldStateObserver :
        virtual public WorldStateObserverInterface,
        virtual public armarx::Component
    {
    public:
        // inherited from Component
        std::string getDefaultName() const override
        {
            return "WorldStateObserver";
        }
        void onInitComponent() override;
        void onConnectComponent() override;

        //        virtual Ice::StringSeq getWorldState(const ::Ice::Current&);
        memoryx::PredicateInstanceList getWorldState(const ::Ice::Current&) override;
        bool isObservable(const ::std::string& predicateName, const ::Ice::Current&) override;
        bool updatePredicateValue(const PredicateInstance& pi, bool removePredicate, const ::Ice::Current&) override;
        void setPredicateArgumentWhitelist(const EntityBaseList& argumentWhitelist, const Ice::Current&) override;
        void resetPredicateArgumentWhitelist(const ::Ice::Current&) override;

    private:
        bool areAllowed(const std::vector<memoryx::EntityRefBasePtr>& entityRefs);
        void addListToList(PredicateInstanceList& target, const PredicateInstanceList& source);
        PredicateInstanceList getHandEmptyPredicates();

        PredicateInstanceList getNonobservableRelationsAndPredicates();

        std::mutex updaterMutex;
        std::map<std::string, WorldStateUpdaterInterfacePrx> updaters;

        EntityBaseList argumentWhitelist;
        WorkingMemoryInterfacePrx wm;
        PriorKnowledgeInterfacePrx prior;

        ObjectInstanceMemorySegmentBasePrx objectInstances;
        PersistentObjectClassSegmentBasePrx objectClasses;
        RelationMemorySegmentBasePrx objectRelations;

        PredicateInstanceList observablePredicateInstances;

        // WorldStateObserverInterface interface
    public:
        void addObservablePredicateInstances(const PredicateInstanceList& predicates, const Ice::Current&) override;
        void registerAsUpdater(const std::string& name, const WorldStateUpdaterInterfacePrx& updater, const Ice::Current&) override;
        WorldStateUpdaterInterfaceList getRegisteredUpdaters(const Ice::Current&) override;
    };
}

