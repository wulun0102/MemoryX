/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Valerij Wittenbeck ( valerij.wittenbeck at  student dot kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <MemoryX/components/WorldStateObserver/WorldStateUpdaterBase.h>
#include <MemoryX/interface/memorytypes/MemorySegments.h>
#include <MemoryX/interface/components/WorkingMemoryInterface.h>
#include <MemoryX/components/GraphNodePoseResolver/GraphNodePoseResolver.h>

namespace memoryx
{

    class ObjectAtPredicateProviderPropertyDefinitions:
        public WorldStateUpdaterPropertyDefinitions
    {
    public:
        ObjectAtPredicateProviderPropertyDefinitions(std::string prefix):
            WorldStateUpdaterPropertyDefinitions(prefix)
        {
            defineOptionalProperty<float>("DistanceThreshold", 1500.f, "Threshold below which an object is considered to be close to a node");
            defineOptionalProperty<float>("HandDistanceThreshold", 150.f, "Threshold below which an object is considered to grasped, and no predicate instance will be generated");
            defineOptionalProperty<std::string>("PlatformGraphSceneName", "XperienceDemoKitchenRM", "Name of the scene in the graph segment");
        }
    };

    class ObjectAtPredicateProvider : public WorldStateUpdaterBase
    {
    public:
        ObjectAtPredicateProvider();

        // ManagedIceObject interface
    protected:
        void onInitWorldStateUpdater() override;
        void onConnectWorldStateUpdater() override;
        std::string getDefaultName() const override;

        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return armarx::PropertyDefinitionsPtr(
                       new ObjectAtPredicateProviderPropertyDefinitions(
                           getConfigIdentifier()));
        }

        // WorldStateUpdaterInterface interface
    public:
        PredicateInfoList getPredicateInfos(const Ice::Current& = Ice::emptyCurrent) override;
        PredicateInstanceList calcPredicates(const Ice::Current& = Ice::emptyCurrent) override;

    private:
        struct CachedNodeInfo
        {
            GraphNodePtr node;
            EntityRefBasePtr nodeRef;
            std::string originalFrame;
            Ice::StringSeq parents;
            armarx::FramedPosePtr globalPose;
        };

        CachedNodeInfo getCacheEntry(const std::string& nodeId);

        WorkingMemoryInterfacePrx wm;
        PriorKnowledgeInterfacePrx prior;
        GraphNodePoseResolverInterfacePrx psr;

        AgentInstancesSegmentBasePrx agentInstances;
        ObjectInstanceMemorySegmentBasePrx objectInstances;
        PersistentObjectClassSegmentBasePrx objectClasses;
        GraphMemorySegmentBasePrx graphs;

        std::string sceneName;
        float distanceThreshold;
        float handDistanceThreshold;

        std::map<std::string, CachedNodeInfo> nodeInfoMap;
        GraphNodeBaseList graphNodes;
        std::map<std::string, std::vector<std::string>> classNameParentsMap;
    };

} // namespace memoryx

