/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Valerij Wittenbeck ( valerij.wittenbeck at  student dot kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <MemoryX/components/WorldStateObserver/WorldStateUpdaterBase.h>
#include <MemoryX/interface/memorytypes/MemorySegments.h>
#include <MemoryX/interface/components/WorkingMemoryInterface.h>
#include <ArmarXCore/core/Component.h>
#include <MemoryX/components/GraphNodePoseResolver/GraphNodePoseResolver.h>

namespace memoryx
{
    class AgentAtPredicateProviderPropertyDefinitions:
        public WorldStateUpdaterPropertyDefinitions
    {
    public:
        AgentAtPredicateProviderPropertyDefinitions(std::string prefix):
            WorldStateUpdaterPropertyDefinitions(prefix)
        {
            defineOptionalProperty<float>("DistanceThreshold", 1000.f, "Threshold below which the agent is considered to be close to a node");
            defineOptionalProperty<float>("HumanDistanceThreshold", 2000.f, "Threshold for the human below which the agent is considered to be close to a node");
            defineOptionalProperty<std::string>("PlatformGraphSceneName", "XperienceDemoKitchenRM", "Name of the scene in the graph segment");
        }
    };

    class AgentAtPredicateProvider : public WorldStateUpdaterBase
    {
    public:
        AgentAtPredicateProvider();

        // ManagedIceObject interface
    protected:
        void onInitWorldStateUpdater() override;
        void onConnectWorldStateUpdater() override;
        std::string getDefaultName() const override;

        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return armarx::PropertyDefinitionsPtr(
                       new AgentAtPredicateProviderPropertyDefinitions(
                           getConfigIdentifier()));
        }

        // WorldStateUpdaterInterface interface
    public:
        PredicateInfoList getPredicateInfos(const Ice::Current& c = Ice::emptyCurrent) override;
        PredicateInstanceList calcPredicates(const Ice::Current&) override;

    private:
        struct CachedNodeInfo
        {
            std::string nodeId;
            std::vector<memoryx::EntityRefBasePtr> agentAtNodeRefs;
        };

        std::map<std::string, CachedNodeInfo> nodeInfoCache;
        std::map<std::string, std::vector<std::string>> nodeParentsCache;

        CachedNodeInfo getCacheEntry(const std::string& nodeId);

        float distanceThreshold, humanDistanceThreshold;
        std::string sceneName;

        WorkingMemoryInterfacePrx wm;
        PriorKnowledgeInterfacePrx prior;
        GraphNodePoseResolverInterfacePrx psr;

        AgentInstancesSegmentBasePrx agentInstances;
        GraphMemorySegmentBasePrx graphSegment;
        std::vector<GraphNodeBasePtr> graphNodes;

        PredicateInfo agentAt;
    };

} // namespace memoryx

