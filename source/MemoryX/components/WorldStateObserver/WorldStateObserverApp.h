/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::WorldStateObserver
* @author     David Schiebener ( schiebener at kit dot edu)
* @date
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/


#include "WorldStateObserver.h"
#include <MemoryX/core/MemoryXApplication.h>
#include <MemoryX/components/WorldStateObserver/predicateProviders/HandPredicateProvider.h>
#include <MemoryX/components/WorldStateObserver/predicateProviders/AgentAtPredicateProvider.h>
#include <MemoryX/components/WorldStateObserver/predicateProviders/ObjectAtPredicateProvider.h>
#include <MemoryX/components/WorldStateObserver/predicateProviders/GraspablePredicateProvider.h>
#include <MemoryX/components/WorldStateObserver/predicateProviders/PutAwayLocationPredicateProvider.h>

namespace memoryx
{
    class WorldStateObserverApp :
        virtual public armarx::Application
    {
        void setup(const armarx::ManagedIceObjectRegistryInterfacePtr& registry, Ice::PropertiesPtr properties) override
        {
            registry->addObject(armarx::Component::create<memoryx::WorldStateObserver>(properties, "WorldStateObserver", "MemoryX"));
            registry->addObject(armarx::Component::create<memoryx::HandPredicateProvider>(properties, "HandPredicateProvider", "MemoryX"));
            registry->addObject(armarx::Component::create<memoryx::AgentAtPredicateProvider>(properties, "AgentAtPredicateProvider", "MemoryX"));
            registry->addObject(armarx::Component::create<memoryx::ObjectAtPredicateProvider>(properties, "ObjectAtPredicateProvider", "MemoryX"));
            registry->addObject(armarx::Component::create<memoryx::GraspablePredicateProvider>(properties, "GraspablePredicateProvider", "MemoryX"));
            registry->addObject(armarx::Component::create<memoryx::PutAwayLocationPredicateProvider>(properties, "PutAwayLocationPredicateProvider", "MemoryX"));
        }

    };
}
