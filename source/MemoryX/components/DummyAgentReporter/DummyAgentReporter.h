/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::ArmarXObjects::DummyAgentReporter
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>

#include <RobotAPI/interface/units/PlatformUnitInterface.h>
#include <RobotAPI/interface/core/RobotState.h>

#include <MemoryX/interface/memorytypes/MemorySegments.h>
#include <MemoryX/interface/components/WorkingMemoryInterface.h>
#include <MemoryX/libraries/memorytypes/entity/AgentInstance.h>

namespace armarx
{
    /**
     * @class DummyAgentReporterPropertyDefinitions
     * @brief
     */
    class DummyAgentReporterPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        DummyAgentReporterPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("RobotStateComponentName", "RobotStateComponent", "Name of the RobotStateComponent");
            defineOptionalProperty<std::string>("WorkingMemoryName", "WorkingMemory", "Name of the WorkingMemory");
            defineOptionalProperty<std::string>("PlatformStateTopicName", "PlatformState", "Name of the PlatformState topic");
        }
    };

    /**
     * @defgroup Component-DummyAgentReporter DummyAgentReporter
     * @ingroup MemoryX-Components
     * A description of the component DummyAgentReporter.
     *
     * @class DummyAgentReporter
     * @ingroup Component-DummyAgentReporter
     * @brief Brief description of class DummyAgentReporter.
     *
     * Detailed description of class DummyAgentReporter.
     */
    class DummyAgentReporter :
        virtual public armarx::Component,
        virtual public armarx::PlatformUnitListener
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "DummyAgentReporter";
        }

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        void onConnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        void onDisconnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        void onExitComponent() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

    protected:
        void reportRobotPose();

        armarx::RobotStateComponentInterfacePrx robotStateComponentPrx;

        memoryx::WorkingMemoryInterfacePrx memoryPrx;
        memoryx::AgentInstancesSegmentBasePrx agentsMemoryPrx;
        memoryx::AgentInstancePtr robotAgent;

        std::string robotAgentId;
        std::string robotName;
        armarx::FramedPositionPtr robotPos;
        armarx::FramedOrientationPtr robotOri;

        std::mutex dataMutex;
        std::vector<Eigen::Vector4d> poseBuffer;
        Eigen::Vector4d currentVelocity;
        IceUtil::Time lastUpdateTime;

        PlatformUnitListenerPrx platformTopic;

        float velocityDeltaSum;
        float velocityDeltaUpdateCount;

    public:
        void reportPlatformPose(PlatformPose const& currentPose, const Ice::Current&) override;
        void reportNewTargetPose(Ice::Float x, Ice::Float y, Ice::Float alpha, const Ice::Current&) override;
        void reportPlatformVelocity(Ice::Float x, Ice::Float y, Ice::Float alpha, const Ice::Current&) override;
        void reportPlatformOdometryPose(Ice::Float x, Ice::Float y, Ice::Float alpha, const Ice::Current&) override;
    };
}

