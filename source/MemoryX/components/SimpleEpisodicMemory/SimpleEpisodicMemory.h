/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::ArmarXObjects::SimpleEpisodicMemory
 * @author     fabian.peller-konrad@kit.edu ( fabian dot peller-konrad at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <memory>
#include <mutex>
#include <string>
#include <vector>

// Ice
#include <Ice/Current.h>
#include <IceUtil/Time.h>

// ArmarX
#include <ArmarXCore/core/Component.h>
#include <MemoryX/interface/components/SimpleEpisodicMemoryInterface.h>


namespace memoryx
{

    class SimpleEpisodicMemory :
        virtual public armarx::Component,
        virtual public SimpleEpisodicMemoryInterface
    {
    public:
        std::string getDefaultName() const override;

        static const std::map<EpisodeStatus, std::string> episode_status_descriptor;
        static const std::map<ActionStatus, std::string> action_status_descriptor;
        static const std::map<ObjectPoseEventType, std::string> object_type_descriptor;


    protected:

        void onInitComponent() override;

        void onConnectComponent() override;

        void onDisconnectComponent() override;

        void onExitComponent() override;

        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void registerEpisodeEvent(const EpisodeEvent&, const Ice::Current& = Ice::emptyCurrent) override;
        void registerImageEvent(const ImageEvent&, const Ice::Current& = Ice::emptyCurrent) override;
        void registerObjectPoseEvent(const ObjectPoseEvent&, const Ice::Current& = Ice::emptyCurrent) override;
        void registerActionEvent(const ActionEvent&, const Ice::Current& = Ice::emptyCurrent) override;
        void registerHumanPoseEvent(const Body25HumanPoseEvent&, const Ice::Current& = Ice::emptyCurrent) override;
        void registerSpeechEvent(const SpeechEvent&, const Ice::Current& = Ice::emptyCurrent) override;
        void registerKinematicUnitEvent(const KinematicUnitEvent&, const Ice::Current& = Ice::emptyCurrent) override;
        void registerPlatformUnitEvent(const PlatformUnitEvent&, const Ice::Current& = Ice::emptyCurrent) override;
        void registerPlatformUnitTargetEvent(const PlatformUnitTargetEvent&, const Ice::Current& = Ice::emptyCurrent) override;
        void notifyKeyframe(const Ice::Current& = Ice::emptyCurrent) override;


    private:
        void export_episode() const;
        void clearAll();

        static const std::string NO_EPISODE;

        bool m_enable_export;
        std::string m_export_folder;

        Episode m_current_episode;

        std::mutex episodeEventMutex;
        std::mutex imageEventMutex;
        std::mutex objectPoseEventMutex;
        std::mutex actionEventMutex;
        std::mutex humanPoseEventMutex;
        std::mutex speechEventMutex;
        std::mutex kinematicUnitEventMutex;
        std::mutex platformUnitEventMutex;
        std::mutex platformUnitTargetEventMutex;

    };
}
