/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::ArmarXObjects::DummyWorrldStateObserver
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_MemoryX_DummyWorldStateObserver_H
#define _ARMARX_COMPONENT_MemoryX_DummyWorldStateObserver_H

#include <ArmarXCore/core/Component.h>
#include <MemoryX/interface/observers/WorldStateObserver.h>

namespace armarx
{
    /**
     * @class DummyWorldStateObserverPropertyDefinitions
     * @brief
     */
    class DummyWorldStateObserverPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        DummyWorldStateObserverPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            //defineRequiredProperty<std::string>("PropertyName", "Description");
            //defineOptionalProperty<std::string>("PropertyName", "DefaultValue", "Description");
        }
    };

    /**
     * @defgroup Component-DummyWorldStateObserver DummyWorldStateObserver
     * @ingroup MemoryX-Components
     * A description of the component DummyWorldStateObserver.
     *
     * @class DummyWorldStateObserver
     * @ingroup Component-DummyWorldStateObserver
     * @brief Brief description of class DummyWorldStateObserver.
     *
     * Detailed description of class DummyWorldStateObserver.
     */
    class DummyWorldStateObserver :
        virtual public memoryx::WorldStateObserverInterface,
        virtual public armarx::Component
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        virtual std::string getDefaultName() const override
        {
            return "DummyWorldStateObserver";
        }

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        virtual void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        virtual void onConnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        virtual void onDisconnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        virtual void onExitComponent() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

    public:
        virtual memoryx::PredicateInstanceList getWorldState(const ::Ice::Current&) override;
        virtual bool isObservable(const ::std::string& predicateName, const ::Ice::Current&) override;
        virtual bool updatePredicateValue(const memoryx::PredicateInstance& pi, bool removePredicate, const ::Ice::Current&) override;
        virtual void setPredicateArgumentWhitelist(const memoryx::EntityBaseList& argumentWhitelist, const Ice::Current&) override;
        virtual void resetPredicateArgumentWhitelist(const ::Ice::Current&) override;
        void addObservablePredicateInstances(const memoryx::PredicateInstanceList& predicates, const Ice::Current&) override;
        void registerAsUpdater(const std::string& name, const memoryx::WorldStateUpdaterInterfacePrx& updater, const Ice::Current&) override;
        memoryx::WorldStateUpdaterInterfaceList getRegisteredUpdaters(const Ice::Current&) override;
    };
}

#endif
