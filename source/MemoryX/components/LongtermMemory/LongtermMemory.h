/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::LongtermMemory
* @author     Alexey Kozlov ( kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <MemoryX/interface/components/LongtermMemoryInterface.h>
#include <MemoryX/interface/components/CommonStorageInterface.h>
#include <MemoryX/interface/components/PriorKnowledgeInterface.h>

#include <MemoryX/core/MongoSerializer.h>
#include <MemoryX/core/memory/SegmentedMemory.h>
#include <MemoryX/libraries/longtermmemory/AbstractLongtermMemory.h>

namespace memoryx
{

    class LongtermMemoryPropertyDefinitions:
        public AbstractLongtermMemoryPropertyDefinitions
    {
    public:
        LongtermMemoryPropertyDefinitions(std::string prefix) :
            AbstractLongtermMemoryPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("SnapshotListCollection", LTM::SegmentNames::SNAPSHOTS, "Mongo collection holding a list of snapshots with corresponding metadata");
            defineOptionalProperty<std::string>("OacCollection", LTM::SegmentNames::OACS, "Mongo collection holding all OACs");
            defineOptionalProperty<std::string>("DmpCollection", LTM::SegmentNames::DMP, "Mongo collection holding all DMPs");
            defineOptionalProperty<std::string>("KbmCollection", LTM::SegmentNames::KBM, "Mongo collection holding all KBM instances");
            defineOptionalProperty<std::string>("ProfilerDataCollection", "ltm_" + LTM::SegmentNames::PROFILER, "Mongo collection for storing Profiler data");
            defineOptionalProperty<std::string>("ResourceProfileCollection", "ltm_" + LTM::SegmentNames::RESOURCE_PROFILES, "Mongo collection for storing ResourceProfile");
            defineOptionalProperty<std::string>("PredictionDataCollection", "ltm_" + LTM::SegmentNames::PREDICTION_DATA, "Mongo collection for storing ResourceProfile");
            defineOptionalProperty<std::string>("SelfLocalisationCollection", "ltm_" + LTM::SegmentNames::SELF_LOCALISATION, "Mongo collection for storing self localisation results");
            defineOptionalProperty<std::string>("ClassCollections", "memdb.Longterm_Objects", "Comma separated list of MongoDB collections (<db>.<collection>) which store known object classes. First collection will be used for writing.");
        }
    };

    /*!
     * \brief The LongtermMemory class provides persistent data that has been learned or copied as a snapshot from working memory.
     *
     *  The LongtermMemory is a segmented memory and provides methods to store the current WorkingMemory into a snapshot and to fill the WorkingMemory with
     *  previously stored snapshots.
     *
     *  See \ref longtermmemory "memoryx::LongtermMemory" - "offline" memory, built mostly from earlier experience
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT LongtermMemory :
        virtual public LongtermMemoryInterface,
        virtual public AbstractLongtermMemory
    {
    public:
        // inherited from AbstractLongtermMemory
        std::string getDefaultName() const override;
        void onInitLongtermMemory() override;
        void onConnectLongtermMemory() override;

        std::string getMemoryName(const Ice::Current& = Ice::emptyCurrent) const override;

        // inherited from LongtermMemoryInterface
        WorkingMemorySnapshotListSegmentBasePrx getWorkingMemorySnapshotListSegment(const ::Ice::Current& = Ice::emptyCurrent) override;

        PersistentObjectInstanceSegmentBasePrx getCustomInstancesSegment(const std::string& segmentName, bool createIfMissing, const ::Ice::Current& c = Ice::emptyCurrent) override;
        OacMemorySegmentBasePrx getOacSegment(const ::Ice::Current& = Ice::emptyCurrent) override;
        KBMSegmentBasePrx getKBMSegment(const Ice::Current&) override;
        PersistentDMPDataSegmentBasePrx getDMPSegment(const ::Ice::Current& = Ice::emptyCurrent) override;
        PersistentProfilerDataSegmentBasePrx getProfilerDataSegment(const Ice::Current& c = Ice::emptyCurrent) override;
        PersistentPredictionDataSegmentBasePrx getPredictionDataSegment(const Ice::Current& c = Ice::emptyCurrent) override;
        PersistentResourceProfileSegmentBasePrx getResourceProfileSegment(const Ice::Current& c = Ice::emptyCurrent) override;
        PersistentEntitySegmentBasePrx getSelfLocalisationSegment(const Ice::Current& c = Ice::emptyCurrent) override;

        PersistentObjectClassSegmentBasePrx getObjectClassesSegment(const ::Ice::Current& c = Ice::emptyCurrent) const override;
        CommonStorageInterfacePrx getCommonStorage(const ::Ice::Current& c = Ice::emptyCurrent) const override;

        // snapshot management
        void loadWorkingMemorySnapshot(const std::string& snapshotName, const AbstractWorkingMemoryInterfacePrx& workingMemory, const ::Ice::Current& = Ice::emptyCurrent) override;
        bool saveWorkingMemorySnapshot(const std::string& snapshotName, const AbstractWorkingMemoryInterfacePrx& workingMemory, const ::Ice::Current& = Ice::emptyCurrent) override;
        bool removeWorkingMemorySnapshot(const std::string& snapshotName, const ::Ice::Current& = Ice::emptyCurrent) override;
        WorkingMemorySnapshotInterfacePrx openWorkingMemorySnapshot(const std::string& snapshotName, const ::Ice::Current& = Ice::emptyCurrent) override;
        NameList getSnapshotNames(const ::Ice::Current& = Ice::emptyCurrent) override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return armarx::PropertyDefinitionsPtr(new LongtermMemoryPropertyDefinitions(getConfigIdentifier()));
        }
        AbstractMemorySegmentPrx addGenericSegment(const std::string& segmentName, const Ice::Current&) override;

    private:
        DatabaseInterfacePrx databaseInterfacePrx;
        PriorKnowledgeInterfacePrx priorKnowledgePrx;
        NameList classCollNames;
    };

    using LongtermMemoryPtr = IceUtil::Handle<LongtermMemory>;
}

