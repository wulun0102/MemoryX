/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::LongtermMemory
* @author     Alexey Kozlov ( kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

// core
#include "LongtermMemory.h"

#include "../../libraries/longtermmemory/WorkingMemorySnapshotListSegment.h"
#include "../../libraries/memorytypes/segment/PersistentObjectInstanceSegment.h"
#include "../../libraries/memorytypes/segment/KBMSegment.h"
#include "../../libraries/memorytypes/segment/OacMemorySegment.h"
#include "../../libraries/memorytypes/segment/PersistentProfilerDataSegment.h"
#include "../../libraries/memorytypes/segment/PersistentResourceProfileSegment.h"
#include "../../libraries/memorytypes/segment/PersistentPredictionDataSegment.h"
#include "../../libraries/memorytypes/segment/PersistentObjectClassSegment.h"
#include "../../libraries/memorytypes/segment/PersistentDMPDataSegment.h"

#include <MemoryX/interface/core/EntityBase.h>
#include <MemoryX/interface/memorytypes/MemoryEntities.h>

#include <ArmarXCore/core/util/StringHelpers.h>

namespace memoryx
{
    std::string LongtermMemory::getDefaultName() const
    {
        return "LongtermMemory";
    }

    void LongtermMemory::onInitLongtermMemory()
    {
        usingProxy("PriorKnowledge");

        const std::string clsCollNamesStr = getProperty<std::string>("ClassCollections").getValue();
        classCollNames = armarx::Split(clsCollNamesStr, ",");
    }


    void LongtermMemory::onConnectLongtermMemory()
    {
        ARMARX_INFO << "connecting long term memory";
        priorKnowledgePrx = getProxy<PriorKnowledgeInterfacePrx>("PriorKnowledge");
        const std::string snaplistCollectionName = getProperty<std::string>("SnapshotListCollection").getValue();
        const std::string oacCollectionName = getProperty<std::string>("OacCollection").getValue();
        const std::string kbmCollectionName = getProperty<std::string>("KbmCollection").getValue();
        const std::string dmpCollectionName = getProperty<std::string>("DmpCollection").getValue();
        const std::string profilerCollectionName = getProperty<std::string>("ProfilerDataCollection").getValue();
        const std::string resourceProfileCollectionName = getProperty<std::string>("ResourceProfileCollection").getValue();
        const std::string predictionDataCollectionName = getProperty<std::string>("PredictionDataCollection").getValue();
        const std::string selfLocalisationCollectionName = getProperty<std::string>("SelfLocalisationCollection").getValue();

        databaseInterfacePrx = storagePrx->requestDatabase(dbName);

        CollectionInterfacePrx snapshotListCollection = databaseInterfacePrx->requestCollection(snaplistCollectionName);
        CollectionInterfacePrx oacCollection = databaseInterfacePrx->requestCollection(oacCollectionName);
        CollectionInterfacePrx kbmCollection = databaseInterfacePrx->requestCollection(kbmCollectionName);
        CollectionInterfacePrx dmpCollection = databaseInterfacePrx->requestCollection(dmpCollectionName);

        WorkingMemorySnapshotListSegmentPtr snapshotListSegment = new WorkingMemorySnapshotListSegment(databaseInterfacePrx, snapshotListCollection, ic);
        addSegment(LTM::SegmentNames::SNAPSHOTS, snapshotListSegment);

        OacMemorySegmentPtr oacSegment = new OacMemorySegment(priorKnowledgePrx->getObjectClassesSegment(), oacCollection, ic);
        addSegment(LTM::SegmentNames::OACS, oacSegment);

        PersistentEntitySegmentBasePtr kbmSegment = new KBMSegment(kbmCollection, ic);
        addSegment(LTM::SegmentNames::KBM, kbmSegment);

        CollectionInterfacePrx profilerCollection = databaseInterfacePrx->requestCollection(profilerCollectionName);
        addSegment(LTM::SegmentNames::PROFILER, new PersistentProfilerDataSegment(profilerCollection, ic));

        CollectionInterfacePrx resourceProfileCollection = databaseInterfacePrx->requestCollection(resourceProfileCollectionName);
        addSegment(LTM::SegmentNames::RESOURCE_PROFILES, new PersistentResourceProfileSegment(resourceProfileCollection, ic));

        CollectionInterfacePrx predictionDataCollection = databaseInterfacePrx->requestCollection(predictionDataCollectionName);
        addSegment(LTM::SegmentNames::PREDICTION_DATA, new PersistentPredictionDataSegment(predictionDataCollection, ic));

        CollectionInterfacePrx classColl = storagePrx->requestCollection(classCollNames[0]);
        PersistentObjectClassSegmentPtr classesSegment = new PersistentObjectClassSegment(classColl, ic);
        addSegment(LTM::SegmentNames::OBJECTCLASSES, classesSegment);

        PersistentDMPDataSegmentPtr dmpSegment = new PersistentDMPDataSegment(dmpCollection, ic);
        addSegment(LTM::SegmentNames::DMP, dmpSegment);

        CollectionInterfacePrx selfLocalisationCollection = databaseInterfacePrx->requestCollection(selfLocalisationCollectionName);
        PersistentEntitySegmentPtr selfLocalisationSegment = new PersistentEntitySegment(selfLocalisationCollection, ic);
        addSegment(LTM::SegmentNames::SELF_LOCALISATION, selfLocalisationSegment);

        ARMARX_INFO << "successfully connected long term memory";
    }


    WorkingMemorySnapshotListSegmentBasePrx LongtermMemory::getWorkingMemorySnapshotListSegment(const ::Ice::Current& c)
    {
        return WorkingMemorySnapshotListSegmentBasePrx::uncheckedCast(getSegment(LTM::SegmentNames::SNAPSHOTS, c));
    }


    PersistentObjectInstanceSegmentBasePrx LongtermMemory::getCustomInstancesSegment(const std::string& segmentName, bool createIfMissing, const ::Ice::Current& c)
    {
        if (hasSegment(segmentName))
        {
            return PersistentObjectInstanceSegmentBasePrx::uncheckedCast(getSegment(segmentName, c));
        }
        else if (createIfMissing)
        {
            PersistentObjectInstanceSegmentPtr customSegment = new PersistentObjectInstanceSegment(databaseInterfacePrx->requestCollection(segmentName), ic);
            return PersistentObjectInstanceSegmentBasePrx::uncheckedCast(addSegment(segmentName, customSegment, c));
        }
        else
        {
            return PersistentObjectInstanceSegmentBasePrx();
        }
    }


    void LongtermMemory::loadWorkingMemorySnapshot(const std::string& snapshotName, const AbstractWorkingMemoryInterfacePrx& workingMemory, const ::Ice::Current&)
    {
        WorkingMemorySnapshotListSegmentBasePrx snapshotListSegment = getWorkingMemorySnapshotListSegment();

        if (snapshotListSegment)
        {
            snapshotListSegment->loadSnapshot(snapshotName, workingMemory);
        }
    }


    bool LongtermMemory::saveWorkingMemorySnapshot(const std::string& snapshotName, const AbstractWorkingMemoryInterfacePrx& workingMemory, const ::Ice::Current&)
    {
        WorkingMemorySnapshotListSegmentBasePrx snapshotListSegment = getWorkingMemorySnapshotListSegment();

        if (snapshotListSegment)
        {
            return snapshotListSegment->createSnapshot(snapshotName, workingMemory);
        }
        else
        {
            return false;
        }
    }


    WorkingMemorySnapshotInterfacePrx LongtermMemory::openWorkingMemorySnapshot(const std::string& snapshotName, const ::Ice::Current&)
    {
        WorkingMemorySnapshotListSegmentBasePrx snapshotListSegment = getWorkingMemorySnapshotListSegment();

        if (snapshotListSegment)
        {
            return snapshotListSegment->openSnapshot(snapshotName);
        }
        else
        {
            return WorkingMemorySnapshotInterfacePrx();
        }
    }


    bool LongtermMemory::removeWorkingMemorySnapshot(const std::string& snapshotName, const ::Ice::Current&)
    {
        WorkingMemorySnapshotListSegmentBasePrx snapshotListSegment = getWorkingMemorySnapshotListSegment();

        if (snapshotListSegment)
        {
            return snapshotListSegment->removeSnapshot(snapshotName);
        }
        else
        {
            return false;
        }
    }


    NameList LongtermMemory::getSnapshotNames(const ::Ice::Current& c)
    {
        WorkingMemorySnapshotListSegmentBasePrx snapshotListSegment = getWorkingMemorySnapshotListSegment();

        if (snapshotListSegment)
        {
            return snapshotListSegment->getSnapshotNames();
        }
        else
        {
            throw SnapshotNotFoundException("Snapshot segment not found!", "");
        }
    }


    std::string LongtermMemory::getMemoryName(const Ice::Current&) const
    {
        return getName();
    }


    PersistentProfilerDataSegmentBasePrx LongtermMemory::getProfilerDataSegment(const Ice::Current& c)
    {
        return PersistentProfilerDataSegmentBasePrx::uncheckedCast(getSegment(LTM::SegmentNames::PROFILER, c));
    }

    PersistentPredictionDataSegmentBasePrx LongtermMemory::getPredictionDataSegment(const Ice::Current& c)
    {
        return PersistentPredictionDataSegmentBasePrx::uncheckedCast(getSegment(LTM::SegmentNames::PREDICTION_DATA, c));
    }


    PersistentResourceProfileSegmentBasePrx LongtermMemory::getResourceProfileSegment(const Ice::Current& c)
    {
        return PersistentResourceProfileSegmentBasePrx::uncheckedCast(getSegment(LTM::SegmentNames::RESOURCE_PROFILES, c));
    }


    OacMemorySegmentBasePrx LongtermMemory::getOacSegment(const ::Ice::Current& c)
    {
        return OacMemorySegmentBasePrx::uncheckedCast(getSegment(LTM::SegmentNames::OACS, c));
    }


    KBMSegmentBasePrx LongtermMemory::getKBMSegment(const Ice::Current& c)
    {
        return KBMSegmentBasePrx::uncheckedCast(getSegment(LTM::SegmentNames::KBM, c));
    }


    CommonStorageInterfacePrx LongtermMemory::getCommonStorage(const ::Ice::Current&) const
    {
        return storagePrx;
    }


    PersistentObjectClassSegmentBasePrx LongtermMemory::getObjectClassesSegment(const ::Ice::Current& c) const
    {
        return PersistentObjectClassSegmentBasePrx::uncheckedCast(getSegment(LTM::SegmentNames::OBJECTCLASSES, c));
    }


    PersistentDMPDataSegmentBasePrx LongtermMemory::getDMPSegment(const Ice::Current& c)
    {
        return PersistentDMPDataSegmentBasePrx::uncheckedCast(getSegment(LTM::SegmentNames::DMP, c));
    }


    PersistentEntitySegmentBasePrx memoryx::LongtermMemory::getSelfLocalisationSegment(const Ice::Current& c)
    {
        return PersistentEntitySegmentBasePrx::uncheckedCast(getSegment(LTM::SegmentNames::SELF_LOCALISATION, c));
    }


    AbstractMemorySegmentPrx memoryx::LongtermMemory::addGenericSegment(const std::string& segmentName, const Ice::Current&)
    {
        PersistentEntitySegmentPtr segment = new PersistentEntitySegment(databaseInterfacePrx->requestCollection(segmentName), getIceManager()->getCommunicator());
        return addSegment(segmentName, segment);
    }
}
