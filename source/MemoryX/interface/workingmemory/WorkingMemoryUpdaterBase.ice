/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::WorkingMemory
* @author     Kai Welke ( welke at kit dot edu), Alexey Kozlov ( kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <MemoryX/interface/components/MemoryException.ice>
#include <MemoryX/interface/components/CommonStorageInterface.ice>
#include <MemoryX/interface/core/EntityBase.ice>
#include <MemoryX/interface/core/GridFileInterface.ice>
#include <MemoryX/interface/workingmemory/WorkingMemoryListenerInterface.ice>

#include <RobotAPI/interface/core/FramedPoseBase.ice>
#include <RobotAPI/interface/core/RobotState.ice>

#include <ArmarXCore/interface/observers/Serialization.ice>
#include <ArmarXCore/interface/observers/VariantBase.ice>

#include <ArmarXCore/interface/observers/Timestamp.ice>



module memoryx
{
    /*
      * Generic WorkingMemoryUpdater
      * 
      */    

    ["cpp:virtual"]
    class WorkingMemoryUpdaterBase implements memoryx::WorkingMemoryListenerInterface
    {
    };
    
   ["cpp:virtual"]
   class ObjectLocalizationMemoryUpdaterBase extends WorkingMemoryUpdaterBase
    {
    };
    
    
    struct ObjectLocalizationResult
    {
        string objectClassName;
        string instanceName; //! optional instance name of the localization result. If empty, the objectClassName will be used.

        armarx::TimestampBase timeStamp;
        
        armarx::FramedPositionBase position;
        MultivariateNormalDistributionBase positionNoise;
        
        armarx::FramedOrientationBase orientation;
        
        float recognitionCertainty;
    };
    
    sequence<ObjectLocalizationResult> ObjectLocalizationResultList;

    sequence<string> ObjectClassNameList;
    
    interface ObjectLocalizerInterface
    {
        ObjectLocalizationResultList localizeObjectClasses(ObjectClassNameList objectClassNames);
    };

    dictionary<string,bool> JobStateMap;
    
    class LocalizationQueryBase extends armarx::VariantDataClass
    {
        string queryName;
        string className;
        int cycleTimeMS;
        int priority;
        
        JobStateMap jobsFinished;
    };
    
    sequence<LocalizationQueryBase> LocalizationQueryList;
};

