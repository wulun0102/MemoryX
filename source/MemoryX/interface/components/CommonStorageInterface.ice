/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::CommonStorage
* @author     Kai Welke ( welke at kit dot edu), Alexey Kozlov ( kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <MemoryX/interface/components/MemoryException.ice>
#include <MemoryX/interface/core/EntityBase.ice>
#include <MemoryX/interface/core/GridFileInterface.ice>

#include <ArmarXCore/interface/observers/Serialization.ice>

module memoryx
{
    struct DBStorableData
    {
        string JSON;
    };

    sequence<DBStorableData> DBStorableDataList;

    class DBSerializerBase
    {
        DBStorableData serialize(armarx::Serializable obj);
        void deserialize(DBStorableData objData, armarx::Serializable obj);
    };

    interface CollectionInterface
    {
        // get mongo namespace(=dbname.collectioname) for this collection
        idempotent string getNS();

        // get number of documents in collection
        idempotent int count();

        // get document by mongo id
        idempotent DBStorableData findByMongoId(string mongoId)
            throws NotConnectedException, InvalidMongoIdException;

        // find documents with fieldName = fieldValue
        idempotent DBStorableDataList findByFieldValue(string fieldName, string fieldValue)
            throws NotConnectedException;

        // find documents with fieldName IN fieldValueList (Mongo $in operator)
        idempotent DBStorableDataList findByFieldValueList(string fieldName, NameList fieldValueList)
            throws NotConnectedException;

        // find single document with fieldName = fieldValue
        idempotent DBStorableData findOneByFieldValue(string query, string fieldValue)
            throws NotConnectedException;

        // find documents by mongo query
        idempotent DBStorableDataList findByQuery(string query)
            throws NotConnectedException;
        // find documents by mongo query with $where constraint
        idempotent DBStorableDataList findByConstraintQuery(string query, string where)
            throws NotConnectedException;

        // find single document by mongo query
        idempotent DBStorableData findOneByQuery(string query)
            throws NotConnectedException;

        // return all documents in collection
        idempotent DBStorableDataList findAll()
            throws NotConnectedException;

        idempotent DBStorableData findAllUniqueByFieldName(string fieldName)
            throws NotConnectedException;

        // return ids of all documents (i.e. values od "_id" field) in collection
        idempotent EntityIdList findAllIds()
            throws NotConnectedException;

        // return value of a specific field (e.g. "name") for all documents in collection
        idempotent NameList findAllFieldValues(string fieldName)
            throws NotConnectedException;

        // insert new document.
        string insert(DBStorableData obj)
            throws NotConnectedException;

        // insert new documents.
        Ice::StringSeq insertList(DBStorableDataList objectList)
            throws NotConnectedException;

        // update existing document, match by _id field
        bool update(DBStorableData obj)
            throws NotConnectedException;

        bool updateByQuery(string query, DBStorableData obj)
            throws NotConnectedException;

        // update document, use keyField to find its existing counterpart
        bool updateWithUserKey(DBStorableData obj, string keyField)
            throws NotConnectedException, FieldNotFoundException;

        // insert (if new) or update (if exists) document, match by _id field
        string save(DBStorableData obj)
            throws NotConnectedException;

        // insert (if new) or update (if exists) document, match by keyField
        bool saveWithUserKey(DBStorableData obj, string keyField)
            throws NotConnectedException, FieldNotFoundException;

        // remove a document with _id = mongoId
        bool removeByMongoId(string mongoId)
            throws NotConnectedException, InvalidMongoIdException;

        // remove all documents which match query
        bool removeByQuery(string query)
            throws NotConnectedException;

        // remove all documents with fieldName = fieldValue
        bool removeByFieldValue(string fieldName, string fieldValue)
            throws NotConnectedException;

        // remove all documents from collection
        bool clear()
            throws NotConnectedException;

        // create index on fieldName if it does not already exists
        bool ensureIndex(string fieldName, bool unique)
            throws NotConnectedException;
    };

    sequence<CollectionInterface> CollectionList;
    sequence<CollectionInterface*> CollectionPrxList;

    // this interface represents MongoDB database
    interface DatabaseInterface
    {
        CollectionInterface* requestCollection(string collectionName);
        void releaseCollection(CollectionInterface* coll);
        void dropCollection(string collectionName);

        // get the name of this DB
        idempotent string getName();
    };

    // this interface represents MongoDB connection
    interface CommonStorageInterface
    {
        // get mongo connection string ("host:port", port is omitted if default)
        string getMongoHostAndPort();
        NameList getDBNames();
        NameList getCollectionNames(string dbName);
        bool isConnected();

        bool reconnect(string hostAndPort, string userName, string password);

        // authenticate for specific mongo database with user/password
        bool authDB(string dbName, string userName, string password);

        // database management
        DatabaseInterface* requestDatabase(string dbName)
            throws MongoAuthenticationException;
        void releaseDatabase(DatabaseInterface* db);

        // collection management
        CollectionInterface* requestCollection(string collectionNS)
            throws DBNotSpecifiedException, MongoAuthenticationException;
        void releaseCollection(CollectionInterface* coll);
        void dropCollection(string collectionNS) throws MongoAuthenticationException;

        // store local file as in mongo GridFS
        // return _id in fs.file collection or empty string on error
        string storeFile(string dbName, string fileName, string gridFSName) throws FileNotFoundException;

        // store string buffer content as text file in mongo GridFS
        // return _id in fs.file collection or empty string on error (eg file not found)
        string storeTextFile(string dbName, string bufferToStore, string gridFSName);

        // store string buffer content as text file in mongo GridFS
        // return _id in fs.file collection or empty string on error (eg file not found)
        string storeBinaryFile(string dbName, Blob bufferToStore, string gridFSName);

        // return file proxy, given _id in fs.file collection
        GridFileInterface* getFileProxyById(string dbName, string fileId);

        // return file proxy, given filename in gridFS
        GridFileInterface* getFileProxyByName(string dbName, string gridFSName);

        // call this method when the proxy isn't needed any more
        void releaseFileProxy(GridFileInterface* fileProxy);

        // return text file contents in buffer, given _id in fs.file collection
        bool getTextFileById(string dbName, string fileId, out string buffer);

        // return binary file contents in buffer, given _id in fs.file collection
        bool getBinaryFileById(string dbName, string fileId, out Blob buffer);

        // return text file contents in buffer, given filename in gridFS
        bool getTextFileByName(string dbName, string gridFSName, out string buffer);

        // return binary file contents in buffer, given filename in gridFS
        bool getBinaryFileByName(string dbName, string gridFSName, out Blob buffer);

        // remove file from mongo gridFS, given _id in fs.file collection
        bool removeFileById(string dbName, string fileId);

        // remove file from mongo gridFS, given filename
        bool removeFileByName(string dbName, string gridFSName);

        NameList getFileNameList(string dbName);
        NameList getFileIdList(string dbName);
    };
};

