/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::GraphNodePoseResolver
* @author     Valerij Wittenbeck ( valerij.wittenbeck at student dot kit dot edu)
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <MemoryX/interface/memorytypes/MemoryEntities.ice>
#include <MemoryX/interface/components/MemoryException.ice>
#include <RobotAPI/interface/core/FramedPoseBase.ice>

module memoryx
{
    exception GraphNodePoseResolverException extends MemoryXException {};
    exception NodeNotResolveableException extends GraphNodePoseResolverException {};

    interface GraphNodePoseResolverInterface
    {

        armarx::FramedPoseBase resolveToGlobalPose(GraphNodeBase node) throws NodeNotResolveableException;

        string getNearestNodeIdToPosition(string sceneName, string nodeParentName, float x, float y);
        GraphNodeBase getNearestNodeToPosition(string sceneName, string nodeParentName, float x, float y);

        string getNearestNodeIdToPose(string sceneName, string nodeParentName, armarx::FramedPoseBase pose);
        GraphNodeBase getNearestNodeToPose(string sceneName, string nodeParentName, armarx::FramedPoseBase pose);

        string getNearestRobotLocationNodeId(GraphNodeBase node);
        GraphNodeBase getNearestRobotLocationNode(GraphNodeBase node);

        void forceRefetch(string nodeId);
        void forceRefetchForAll();
    };
    
};

