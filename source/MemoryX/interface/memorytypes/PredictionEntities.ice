/**
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::Core
 * @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <MemoryX/interface/core/EntityBase.ice>
#include <MemoryX/interface/memorytypes/MemoryEntities.ice>
#include <MemoryX/interface/memorytypes/ProfilerEntities.ice>


module memoryx
{
    ["cpp:virtual"]
    class PredictionEntityBase extends EntityBase
    {
        ["cpp:const"]
        EntityRefBase getProfilerMemorySnapshotRef();
        void setProfilerMemorySnapshotRef(EntityRefBase memorySnapshotRef);

        ["cpp:const"]
        EntityRefBase getSourceProfilerMemorySnapshotRef();
        void setSourceProfilerMemorySnapshotRef(EntityRefBase sourceSnapshotRef);

        ["cpp:const"]
        EntityRefBase getPredictedProfilerMemorySnapshotRef();
        void setPredictedProfilerMemorySnapshotRef(EntityRefBase predictedSnapshotRef);

        ["cpp:const"]
        EntityRefBaseList getPredictedProfilerMemorySnapshotRefList();
        void setPredictedProfilerMemorySnapshotRefList(EntityRefBaseList predictedSnapshotRefList);

        ["cpp:const"]
        string getPredictionMethodName();
        void setPredictionMethodName(string predictionMethodName);

        ["cpp:const"]
        int getTaskCount();
        void setTaskCount(int taskCount);

        ["cpp:const"]
        float getPredictionScore();
        void setPredictionScore(float predictionScore);
    };

    sequence<PredictionEntityBase> PredictionEntityBaseList;

    ["cpp:virtual"]
    class PredictionTaskEntityBase extends EntityBase
    {
        ["cpp:const"]
        string getPredictionTaskName();
        void setPredictionTaskName(string predictionTaskName);

        ["cpp:const"]
        EntityRefBaseList getPredictionEntityRefList();
        void setPredictionEntityRefList(EntityRefBaseList predictionEntityRefList);
    };

    sequence<PredictionTaskEntityBase> PredictionTaskEntityBaseList;
};

