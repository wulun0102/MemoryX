/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Core
* @author     Alexey Kozlov ( kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <MemoryX/interface/components/MemoryException.ice>
#include <MemoryX/interface/core/EntityBase.ice>
#include <MemoryX/interface/core/MemoryInterface.ice>

#include <ArmarXCore/interface/observers/VariantBase.ice>
#include <RobotAPI/interface/core/PoseBase.ice>
#include <RobotAPI/interface/core/FramedPoseBase.ice>
#include <RobotAPI/interface/core/RobotState.ice>
#include <ArmarXCore/interface/observers/Matrix.ice>
#include <ArmarXCore/interface/core/UserException.ice>
#include <ArmarXCore/interface/observers/Timestamp.ice>
#include <MemoryX/interface/memorytypes/ObjectClass.ice>

module memoryx
{
    ["cpp:virtual"]
    class AgentInstanceBase extends EntityBase
    {
        ["cpp:const"]
        armarx::FramedPositionBase getPositionBase();
        void setPosition(armarx::FramedPositionBase position);

        ["cpp:const"]
        armarx::FramedOrientationBase getOrientationBase();
        void setOrientation(armarx::FramedOrientationBase orientation);

        ["cpp:const"]
        armarx::FramedPoseBase getPoseBase();

        ["cpp:const"]
        string getStringifiedSharedRobotInterfaceProxy();
        void setStringifiedSharedRobotInterfaceProxy(string stringifiedSharedRobotInterfaceProxy);

        ["cpp:const"]
        string getAgentFilePath();
        void setAgentFilePath(string agentFilePath);

        ["cpp:const"]
        idempotent armarx::SharedRobotInterface* getSharedRobot();
        void setSharedRobot(armarx::SharedRobotInterface* robot);
        ["protected"]
        armarx::SharedRobotInterface* robot;
    };

    dictionary<string, double> ClassProbabilityMap;

    sequence<AgentInstanceBase> AgentInstanceList;

    ["cpp:virtual"]
    class ObjectInstanceBase extends EntityBase
    {
        ["cpp:const"]
        float getExistenceCertainty();
        void setExistenceCertainty(float existenceCertainty);

        ["cpp:const"]
        armarx::FramedPositionBase getPositionBase();
        void setPosition(armarx::FramedPositionBase position);

        ["cpp:const"]
        MultivariateNormalDistributionBase getPositionUncertainty();
        void setPositionUncertainty(MultivariateNormalDistributionBase uncertainty);

        ["cpp:const"]
        armarx::FramedOrientationBase getOrientationBase();
        void setOrientation(armarx::FramedOrientationBase orientation);

        ["cpp:const"]
        int getLocalizationPriority();
        void setLocalizationPriority(int priority);

        ["cpp:const"]
        ClassProbabilityMap getClasses();
        void setClass(string className, float probability);
        void addClass(string className, float probability);
        void clearClasses();

        ["cpp:const"]
        string getMostProbableClass();
        ["cpp:const"]
        float getClassProbability(string className);
    };

    sequence<ObjectInstanceBase> ObjectInstanceList;


    sequence<ObjectClassBase> ObjectClassList;
    dictionary<string, ObjectClassBase > ObjectClassMap;

    ["cpp:virtual"]
    class RelationBase extends EntityBase
    {
        ["cpp:const"]
        EntityRefList getEntities();
        void setEntities(EntityRefList entities);

        ["cpp:const"]
        bool getSign();
        void setSign(bool sign);

        ["cpp:const"]
        float getProb();
        void setProb(float prob);
    };

    sequence<RelationBase> RelationList;

    /*
     * Semantic Event Chain related definitions
     */

    ["cpp:virtual"]
    class SECRelationBase extends armarx::VariantDataClass
    {
        ["protected"]
        string name;
        ObjectClassList objects1;
        ObjectClassList objects2;
        void setName(string newName);
        ["cpp:const"]
        string getName();
        ["cpp:const"]
        bool isEqual(SECRelationBase other);
        bool hasEqualObjects(SECRelationBase other);
    };
    sequence<SECRelationBase> SECRelationList;

    ["cpp:virtual"]
    class SECObjectRelationsBase extends armarx::VariantDataClass
    {
        SECRelationList relations;
        /**
         * @brief contains checks if the given relations are contained in this instance
         * @param other
         * @return returns a value between 0 and 1. 1 if other is completely in this instance
         */
        ["cpp:const"]
        float contains(SECObjectRelationsBase other);
        ["cpp:const"]
        bool containsRelation(SECRelationBase relation);
        ["cpp:const"]
        SECRelationBase containsRelationBetweenObjects(ObjectClassList objects1, ObjectClassList objects2);

        int addRelations(SECRelationList newRelations);
        bool addRelation(SECRelationBase newRelation);
    };

    class SECKeyFrameBase
    {
        SECObjectRelationsBase worldState;
        int index = -1;
    };


    dictionary<int, SECKeyFrameBase> SECKeyFrameMap;

    enum RelevantObject
    {
        eObject1,
        eObject2
    };


    struct SECRelationPair
    {
        SECRelationBase relation1;
        SECRelationBase relation2;
//        RelevantObject relation1RelevantObj;
//        RelevantObject relation2RelevantObj;
    };
    sequence<SECRelationPair> SECRelationPairList;

    /*
     * OAC related definitions
     */

    ["cpp:virtual"]
    class OacParameterListBase extends armarx::VariantDataClass
    {
        ["cpp:const"]
        NameList getNames();
        void setNames(NameList names);
        Ice::StringSeq parameterNames;
    };

    ["cpp:virtual"]
    class OacPredictionFunctionBase extends armarx::VariantDataClass
    {
        ["cpp:const"]
        string getPreconditionExpression();
        void setPreconditionExpression(string precondition);

        ["cpp:const"]
        SECObjectRelationsBase getSECPreconditions();
        void setSECPreconditions(SECObjectRelationsBase preconditions);


        ["cpp:const"]
        string getEffectExpression();
        void setEffectExpression(string effect);

        ["cpp:const"]
        SECObjectRelationsBase getSECEffects();
        void setSECEffects(SECObjectRelationsBase effects);


        ["cpp:const"]
        SECRelationPairList getSECSideConstraints();
        void setSECSideConstraints(SECRelationPairList SECSideConstraints);



        ["protected"]
        string effect;
        ["protected"]
        string precondition;
        ["protected"]
        SECObjectRelationsBase SECPreconditions;
        ["protected"]
        SECObjectRelationsBase SECEffects;
        ["protected"]
        SECRelationPairList SECSideConstraints;
    };

    ["cpp:virtual"]
    class OacStatisticalMeasureBase extends armarx::VariantDataClass
    {
        ["cpp:const"]
        int getExperimentCount();
        void setExperimentCount(int count);

        ["cpp:const"]
        int getFailedExperimentCount();
        void setFailedExperimentCount(int countFailed);

        ["protected"]
        int count;
        ["protected"]
        int countFailed;
    };

    ["cpp:virtual"]
    class OacBase extends EntityBase
    {
        ["cpp:const"]
        NameList getParameters();
        void setParameters(NameList parameters);

        ["cpp:const"]
        string getStatechartProxyName();
        void setStatechartProxyName(string statechartProxyName);

        ["cpp:const"]
        string getStateUUID();
        void setStateUUID(string stateUUID);

        ["cpp:const"]
        bool isPlannable();
        void setPlannable(bool isPlannable);

        ["cpp:const"]
        OacPredictionFunctionBase getPredictionFunction();
        void setPredictionFunction(OacPredictionFunctionBase prediction);

        ["cpp:const"]
        OacStatisticalMeasureBase getStatisticalMeasure();
        void setStatisticalMeasure(OacStatisticalMeasureBase measure);
    };

    sequence<OacBase> OacBaseList;

    enum OacExecState
    {
        eIncomplete,
        eSuccess,
        eFailure
    };

    ["cpp:virtual"]
    class ActiveOacBase extends EntityBase
    {
        ["cpp:const"]
        NameList getArgumentInstanceIds();
        void setArgumentInstanceIds(NameList ids);

        ["cpp:const"]
        EntityRefBase getOacRef();
        void setOacRef(EntityRefBase oac);

        ["cpp:const"]
        string getOacName();

        ["cpp:const"]
        OacExecState getState();
        void setState(OacExecState state);
    };

    class ActiveOacFull
    {
        OacBase oac;
        ObjectInstanceList parameters;

        ActiveOacBase entity;
    };

    struct SECMotionSegment
    {
        SECKeyFrameBase prevKeyFrame;
        SECKeyFrameBase nextKeyFrame;
        OacBaseList oacs;
        ObjectClassList involvedObjects;
    };

    sequence<SECMotionSegment> SECMotionSegmentList;



    enum AffordanceType
    {
        eAffordanceTypeGraspPlatform,
        eAffordanceTypeGraspPrismatic,
        eAffordanceTypeGrasp,

        eAffordanceTypeSupport,
        eAffordanceTypeLean,
        eAffordanceTypeHold,
        eAffordanceTypePush,
        eAffordanceTypePull,
        eAffordanceTypeLift,
        eAffordanceTypeTurn,

        eAffordanceTypeBimanualGraspPlatform,
        eAffordanceTypeBimanualGraspPrismatic,
        eAffordanceTypeBimanualGrasp,

        eAffordanceTypeBimanualGraspAligned,
        eAffordanceTypeBimanualGraspOpposed,
        eAffordanceTypeBimanualOpposedGraspPlatform,
        eAffordanceTypeBimanualOpposedGraspPrismatic,
        eAffordanceTypeBimanualAlignedGraspPlatform,
        eAffordanceTypeBimanualAlignedGraspPrismatic,

        eAffordanceTypeBimanualSupport,
        eAffordanceTypeBimanualLean,
        eAffordanceTypeBimanualHold,
        eAffordanceTypeBimanualPush,
        eAffordanceTypeBimanualPull,
        eAffordanceTypeBimanualLift,
        eAffordanceTypeBimanualTurn,
    };

    enum AffordanceValidationStatus
    {
        eAffordanceNotValidated,
        eAffordanceVerified,
        eAffordanceFalsified
    };

    sequence<armarx::MatrixFloatBase> AffordanceObservationList;

    ["cpp:virtual"]
    class AffordanceBase extends EntityBase
    {
        ["cpp:const"]
        AffordanceType getType();
        void setType(AffordanceType type);

        ["cpp:const"]
        armarx::Vector3Base getPosition();
        void setPosition(armarx::Vector3Base position);

        ["cpp:const"]
        string getPrimitiveId();
        void setPrimitiveId(string id);

        ["cpp:const"]
        idempotent float getProbability();
        idempotent void setProbability(float probability);

        ["cpp:const"]
        idempotent armarx::TimestampBase getTime();
        idempotent void setTime(armarx::TimestampBase time);

        ["cpp:const"]
        AffordanceValidationStatus getValidationStatus();
        void setValidationStatus(AffordanceValidationStatus status);

        ["cpp:const"]
        armarx::MatrixFloatBase getCertaintyFunction();
        void setCertaintyFunction(armarx::MatrixFloatBase certainties);

        ["cpp:const"]
        AffordanceObservationList getObservations();
        void setObservations(AffordanceObservationList observations);
        void addObservation(armarx::MatrixFloatBase observation);
    };

    sequence<AffordanceBase> AffordanceBaseList;


    sequence<armarx::Vector3Base> PointList;

    struct EnvironmentalPrimitiveOBBPlane
    {
        armarx::FramedPoseBase pose;
        float dimX;
        float dimY;
    };

    ["cpp:virtual"]
    class EnvironmentalSegmentBase extends EntityBase
    {
        ["cpp:const"]
        idempotent Ice::StringSeq getBagOfWords();
        idempotent void setBagOfWords(Ice::StringSeq bow);

        ["cpp:const"]
        idempotent memoryx::EntityRefList getPrimitives();
        idempotent void setPrimitives(memoryx::EntityRefList primitives);
    };


    ["cpp:virtual"]
    class EnvironmentalPrimitiveBase extends EntityBase
    {
        ["cpp:const"]
        idempotent string getShape();

        ["cpp:protected"]
        idempotent void setShape(string shape);

        ["cpp:const"]
        idempotent armarx::Vector3Base getOBBDimensions();
        idempotent void setOBBDimensions(armarx::Vector3Base dimensions);

        ["cpp:const"]
        idempotent armarx::FramedPoseBase getPose();
        idempotent void setPose(armarx::FramedPoseBase pose);

        ["cpp:const"]
        idempotent PointList getGraspPoints();
        idempotent void setGraspPoints(PointList  graspPoints);

        ["cpp:const"]
        idempotent PointList getInliers();
        idempotent void setInliers(PointList inliers);

        ["cpp:const"]
        idempotent float getProbability();
        idempotent void setProbability(float probability);

        ["cpp:const"]
        idempotent float getLength();
        idempotent void setLength(float length);

        ["cpp:const"]
        idempotent armarx::TimestampBase getTime();
        idempotent void setTime(armarx::TimestampBase time);

        ["cpp:const"]
        idempotent int getLabel();
        idempotent void setLabel(int label);

        ["cpp:const"]
        idempotent float getCircularityProbability();
        idempotent void setCircularityProbability(float probability);

        ["cpp:const"]
        idempotent armarx::MatrixFloatBase getSampling();
        idempotent void setSampling(armarx::MatrixFloatBase sampling);
    };

    sequence<EnvironmentalPrimitiveBase> EnvironmentalPrimitiveBaseList;

    ["cpp:virtual"]
    class PlanePrimitiveBase extends EnvironmentalPrimitiveBase
    {
        ["cpp:const"]
        idempotent armarx::Vector3Base getPlaneNormal();
        idempotent void setPlaneNormal(armarx::Vector3Base normal);

        ["cpp:const"]
        idempotent float getPlaneDistance();
        idempotent void setPlaneDistance(float distance);
    };

    sequence<PlanePrimitiveBase> PlanePrimitiveBaseList;

    ["cpp:virtual"]
    class SpherePrimitiveBase extends EnvironmentalPrimitiveBase
    {
        ["cpp:const"]
        idempotent armarx::Vector3Base getSphereCenter();
        idempotent void setSphereCenter(armarx::Vector3Base center);

        ["cpp:const"]
        idempotent float getSphereRadius();
        idempotent void setSphereRadius(float radius);
    };

    sequence<SpherePrimitiveBase> SpherePrimitiveBaseList;

    ["cpp:virtual"]
    class CylinderPrimitiveBase extends EnvironmentalPrimitiveBase
    {
        ["cpp:const"]
         idempotent armarx::Vector3Base getCylinderPoint();
         idempotent void setCylinderPoint(armarx::Vector3Base point);

         ["cpp:const"]
         idempotent armarx::Vector3Base getCylinderAxisDirection();
         idempotent void setCylinderAxisDirection(armarx::Vector3Base axis);

         ["cpp:const"]
         idempotent float getCylinderRadius();
         idempotent void setCylinderRadius(float radius);
    };

    sequence<CylinderPrimitiveBase> CylinderPrimitiveBaseList;



    ["cpp:virtual"]
    class ConePrimitiveBase extends EnvironmentalPrimitiveBase
    {
        ["cpp:const"]
        idempotent armarx::Vector3Base getConeApex();
        idempotent void setConeApex(armarx::Vector3Base apex);

        ["cpp:const"]
        idempotent armarx::Vector3Base getConeAxisDirection();
        idempotent void setConeAxisDirection(armarx::Vector3Base axis);

        ["cpp:const"]
        idempotent float getConeOpeningAngle();
        idempotent void setConeOpeningAngle(float angle);
    };

    sequence<ConePrimitiveBase> ConePrimitiveBaseList;


    ["cpp:virtual"]
    class BoxPrimitiveBase extends EnvironmentalPrimitiveBase
    {
        ["cpp:const"]
        idempotent memoryx::EntityRefList getBoxSides();
        idempotent void setBoxSides(memoryx::EntityRefList sides);
    };

    sequence<BoxPrimitiveBase> BoxPrimitiveBaseList;

    ["cpp:virtual"]
    class HigherSemanticStructureBase extends EnvironmentalPrimitiveBase
    {
        ["cpp:const"]
        idempotent Ice::StringSeq getLabels();
        idempotent void setLabels(Ice::StringSeq bow);

        ["cpp:const"]
        idempotent memoryx::EntityRefList getPrimitives();
        idempotent void setPrimitives(memoryx::EntityRefList primitives);
    };

    sequence<HigherSemanticStructureBase> HigherSemanticStructureBaseList;


    ["cpp:virtual"]
    class KBMDataBase extends EntityBase
    {

        ["cpp:const"]
        idempotent armarx::MatrixDoubleBase getControlNet();

        void setControlNet(armarx::MatrixDoubleBase controlNet);
        ["cpp:const"]
        string getReferenceFrameName();

        ["cpp:const"]
        string getNodeSetName();

        ["cpp:const"]
        string getRobotName();

        void setNodeSetName(string nodeSetName);

        void setReferenceFrameName(string referenceFrameName);

        void setRobotName(string robotName);
    };

    class GraphNodeBase;

    sequence<GraphNodeBase> GraphNodeBaseList;

    ["cpp:virtual"]
    /**
     * @brief Represents a directed multigraph's node.
     * The node contains a list of adjacent nodes.
     * A node is assoicated to a scene.
     * If a node can be reached by traversing an outgowing edge it is adjacent.
     */
    class GraphNodeBase extends EntityBase
    {
        /**
         * @brief Returns the pose of the node.
         * @return The pose of the node.
         */
        ["cpp:const"]
        armarx::FramedPoseBase getPose();

        /**
         * @brief Sets the position.
         * @param position The new position.
         */
        void setPose(armarx::FramedPoseBase pose);

        /**
         * @brief Returns the scene's name.
         * @return The scene's name.
         */
        ["cpp:const"]
        string getScene();

        /**
         * @brief Sets the scene.
         * @param scene The new scene.
         */
        void setScene(string scene);

        /**
         * @brief Returns an adjacent node.
         * @param number The adjacent node to return.
         * @return An adjacent node.
         */
        EntityRefBase getAdjacentNode(int number) throws armarx::IndexOutOfBoundsException;
        EntityRefBase getAdjacentNodeById(string nodeId);
        GraphNodeBaseList getAdjacentNodes();

        /**
         * @brief Returns the outdegree.
         * @return The outdegree.
         */
        ["cpp:const"]
        int getOutdegree();

        /**
         * @brief Adds a new adjacent node.
         * @param newAdjacentNode The node to add. (It is supposed to reference a GraphNodeBase)
         *
         * @note If this function is called n times with the same parameter p the graph contains n edges (this,p).
         *
         */
        void addAdjacentNode(EntityRefBase newAdjacentNode) throws armarx::InvalidTypeException;
        bool removeAdjacentNode(string nodeId) throws armarx::InvalidTypeException;


        /**
         * @brief Removes all adjacent nodes.
         */
        void clearAdjacentNodes();

    };


    ["cpp:virtual"]

    class DMPEntityBase extends EntityBase
    {
        ["cpp:const"]
        idempotent string getDMPName();
        void setDMPName(string name);

        ["cpp:const"]
        idempotent int getDMPType();
        void setDMPType(string type);

        ["cpp:const"]
        idempotent bool get3rdOrder();
        void set3rdOrder(bool is3rdOrder);

        ["cpp:const"]
        idempotent string getDMPtextStr();
        void setDMPtextStr(string dmptext);

        ["cpp:const"]
        idempotent int getTrajDim();
        void setTrajDim(int trajDim);
    };
};

