/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX
* @author     Mirko Waechter <waechter at kit dot edu>
* @copyright  2014 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <MemoryX/interface/workingmemory/AbstractWorkingMemoryInterface.ice>

#include <ArmarXCore/interface/observers/ObserverInterface.ice>
#include <ArmarXCore/interface/observers/VariantBase.ice>
#include <ArmarXCore/interface/observers/ChannelRefBase.ice>

module memoryx
{
    struct PredicateInstance
    {
        string name;
        memoryx::EntityRefList argValues;
        bool sign = true;
    };
    sequence<PredicateInstance> PredicateInstanceList;
    
    interface WorldStateUpdaterInterface;
    sequence<WorldStateUpdaterInterface*> WorldStateUpdaterInterfaceList;

    interface WorldStateObserverInterface  /*extends armarx::ObserverInterface, WorkingMemoryListenerInterface*/
    {

        /**
            Returns the current world state as perceived by the robot including
            non-observable predicates that were inferred from actions.
        */
        PredicateInstanceList getWorldState();

        /**
            Checks wether the given predicate is observable by the robot or only
            inferrable.
            @param predicateName Name of the predicate to check
        */
        bool isObservable(string predicateName);

        /**
            Updates the predicate instance according to second parameter.
            @param pi the predicate instance to add/remove
            @param removePredicate flag that signals whether the predicate
            should be added or removed (i.e. "add()" and "del()" in PKS)
            @return returns true if the world state was really changed or remained the same
                    (i.e. if the status of the predicate was before the same as after)
        */
        bool updatePredicateValue(PredicateInstance pi, bool removePredicate);

        /**
         * @brief Add the given predicates to the WorldState.
         * Does not check if the predicate already exists.
         * @param predicates
         */
        void addObservablePredicateInstances(PredicateInstanceList predicates);

        /**
         * @brief setPredicateArgumentWhitelist only allow getWorldState to consider these entities
         * @param argumentWhitelist the allowed entities; all are allowed if empty
         */
        void setPredicateArgumentWhitelist(EntityBaseList argumentWhitelist);
        void resetPredicateArgumentWhitelist();
        void registerAsUpdater(string name, WorldStateUpdaterInterface* updater);
        WorldStateUpdaterInterfaceList getRegisteredUpdaters();
    };

    class PredicateBase extends armarx::VariantDataClass
    {
        string predicateName;
        memoryx::EntityRefList arguments;
        bool sign = true;
    };

    struct PredicateInfo {
        string name;
        int arity;    
    };

    sequence<PredicateInfo> PredicateInfoList;

    interface WorldStateUpdaterInterface
    {
        void setObserver(WorldStateObserverInterface* worldStateObserver);
        void requestUpdateOnRegisteredObserver();
        void requestUpdate(WorldStateObserverInterface* worldStateObserver);
        PredicateInfoList getPredicateInfos();
        PredicateInstanceList calcPredicates();
    };
    sequence<WorldStateUpdaterInterface*> WorldStateUpdaterList;
};

