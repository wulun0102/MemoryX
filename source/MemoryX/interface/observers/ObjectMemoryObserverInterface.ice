/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     David Schiebener <david dot schiebener at kit dot edu>
* @copyright  2012 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <MemoryX/interface/workingmemory/AbstractWorkingMemoryInterface.ice>

#include <ArmarXCore/interface/observers/ObserverInterface.ice>
#include <ArmarXCore/interface/observers/ChannelRefBase.ice>

module memoryx
{
    sequence<armarx::ChannelRefBase> ChannelRefBaseSequence;

    interface ObjectMemoryObserverInterface extends armarx::ObserverInterface, WorkingMemoryListenerInterface
    {
        // object class and instance handling
        armarx::ChannelRefBase requestObjectClassOnce(string objectClassName, optional(1) int priority);
        armarx::ChannelRefBase requestObjectClassRepeated(string objectClassName, int cycleTimeMS, optional(1) int priority);
        void releaseObjectClass(armarx::ChannelRefBase objectClassChannel);

        ChannelRefBaseSequence getObjectInstances(armarx::ChannelRefBase channel);
        ChannelRefBaseSequence getObjectInstancesByClass(string objectClassName);

        /**
     * @brief Convenience for function getObjectInstances(). Directly returns the first instance.
     * @param channel Channel of the object class
     * @return Returns the first instance of the list getObjectInstances() returns. Null if list is empty.
     */
        armarx::ChannelRefBase getFirstObjectInstance(armarx::ChannelRefBase channel);
        armarx::ChannelRefBase getObjectInstanceById(string id);

        /**
     * @brief Convenience for function getFirstObjectInstanceByClass(). Directly returns the first instance.
     * @param objectClassName Name of the object class
     * @return Returns the first instance of the list getFirstObjectInstanceByClass() returns. Null if list is empty.
     */
        armarx::ChannelRefBase getFirstObjectInstanceByClass(string objectClassName);

        ["cpp:const"]
        idempotent string getInstanceChannelName(EntityBase objectInstanceEntity);
    };
};

