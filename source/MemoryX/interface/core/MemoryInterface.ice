/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::LongtermMemory
* @author     Alexey Kozlov ( kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <MemoryX/interface/components/MemoryException.ice>
#include <MemoryX/interface/components/CommonStorageInterface.ice>
#include <MemoryX/interface/core/EntityBase.ice>

module memoryx
{
    class EntityRefBase;
    sequence<EntityRefBase> EntityRefList;
    sequence<EntityRefBase> EntityRefBaseList;


    interface MemoryInterface;

    ["cpp:virtual"]
    class AbstractMemorySegment
    {

        ["cpp:const"]
        idempotent Ice::Identity getIceId();

        ["cpp:const"]
        idempotent int size();

        ["cpp:const"]
        idempotent void print();

        idempotent void clear();

        ["protected"]
        void setSegmentName(string segmentName);
        ["protected"]
        void setParentMemory(MemoryInterface memory);
    };

    ["cpp:virtual"]
    class PersistentMemorySegment extends AbstractMemorySegment
    {
    };

    dictionary<string, EntityBase> IdEntityMap;

    class SegmentLockBase;

    ["cpp:virtual"]
    interface EntityMemorySegmentInterface
    {
        ["cpp:const"]
        idempotent string getObjectTypeId();

        // entities
        string addEntity(EntityBase entity) throws InvalidEntityException;

        EntityIdList addEntityList(EntityBaseList entityList);

        void updateEntity(string entityId, EntityBase update) throws InvalidEntityException, EntityNotFoundException;
        /**
          * Updates or insert entity. If not found with given id, entity is inserted and id is returned.
          *
          **/
        string upsertEntity(string entityId, EntityBase newEntity) throws InvalidEntityException;
        string upsertEntityByName(string entityName, EntityBase newEntity) throws InvalidEntityException;

        EntityIdList upsertEntityList(EntityBaseList entityList);

        void removeEntity(string entityId) throws EntityNotFoundException;

        void removeAllEntities();

        /**
          set attribute on entity with ID entityId
         */
        void setEntityAttribute(string entityId, EntityAttributeBase attribute);

        /**
          set all attributes in attributeList on entity with ID entityId
         */
        void setEntityAttributes(string entityId, EntityAttributeList attributeList);

        ["cpp:const"]
        idempotent bool hasEntityById(string entityId);

        ["cpp:const"]
        idempotent bool hasEntityByName(string entityName);

        ["cpp:const"]
        idempotent EntityBase getEntityById(string entityId);
        ["cpp:const"]
        idempotent string getJSONEntityById(string entityId);

        ["cpp:const"]
        idempotent EntityRefBase getEntityRefById(string entityId);

        ["cpp:const"]
        idempotent EntityBase getEntityByName(string entityName);

        ["cpp:const"]
        idempotent EntityRefBase getEntityRefByName(string entityName);

        ["cpp:const"]
        idempotent EntityBaseList getEntitiesByAttrValue(string attrName, string attrValue);

        ["cpp:const"]
        idempotent EntityBaseList getEntitiesByAttrValueList(string attrName, NameList attrValueList);

        ["cpp:const"]
        idempotent EntityBaseList getEntityWithChildrenById(string entityId, bool includeMetaEntities);
        ["cpp:const"]
        idempotent EntityBaseList getEntityWithChildrenByName(string entityName, bool includeMetaEntities);


        ["cpp:const"]
        idempotent EntityIdList getAllEntityIds();

        ["cpp:const"]
        idempotent EntityBaseList getAllEntities();

        ["cpp:const"]
        idempotent IdEntityMap getIdEntityMap();

        ["cpp:const"]
        idempotent string getSegmentName();

        SegmentLockBase lockSegment();
        bool keepLockAlive(string token)  throws InvalidLockTokenException;
        bool unlockSegment(SegmentLockBase lock) throws InvalidLockTokenException;
        bool unlockSegmentWithToken(string token) throws InvalidLockTokenException;
    };

    class SegmentLockBase
    {
        void unlock();
        string token;
        int keepAliveIntervalMs = 500;
        EntityMemorySegmentInterface* segment;
    };


    ["cpp:virtual"]
    class PersistentEntitySegmentBase extends PersistentMemorySegment
        implements EntityMemorySegmentInterface
    {
        ["protected"]
        string objectTypeId;

        ["cpp:const"]
        idempotent NameList getReadCollectionsNS();

        void clearReadCollections();

        void addReadCollection(CollectionInterface* collNS);

        ["cpp:const"]
        idempotent string getWriteCollectionNS();

        void setWriteCollection(CollectionInterface* collNS);

        /*!
            Be aware that you can change the collection setup of the PriorKnowledge Component.
            This may result in missing collections for other components.
        */
        void setSingleRWCollection(CollectionInterface* collNS);

        EntityRefList findRefsByQuery(string bsonQuery);
    };

    interface MemoryInterface
    {
        AbstractMemorySegment* addSegment(string segmentName, AbstractMemorySegment s)
            throws SegmentAlreadyExistsException;
        AbstractMemorySegment* addGenericSegment(string segmentName)
            throws SegmentAlreadyExistsException;


        ["cpp:const"]
        bool hasSegment(string segmentName);

        ["cpp:const"]
        AbstractMemorySegment* getSegment(string segmentName);

        void removeSegment(string segmentName);

        ["cpp:const"]
        idempotent NameList getSegmentNames();

        EntityBase findEntityById(string entityId);
        EntityRefBase findEntityRefById(string entityId);

        idempotent void clear();
        ["cpp:const"]
        idempotent string getMemoryName();
    };


    class EntityRefBase extends armarx::VariantDataClass
    {
        ["cpp:const"]
        idempotent EntityBase getEntity();
        ["cpp:const"]
        idempotent bool equals(EntityRefBase other);
        string entityId;
        string entityName;
        string segmentName;
        string memoryName;
        MemoryInterface* memory;
        EntityMemorySegmentInterface* segment;
    };


};

