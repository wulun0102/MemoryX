/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * @package    ArmarX::RobotAPI
 * @author     Raphael Grimm <raphael dot grimm at kit dot edu>
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <RobotAPI/interface/core/FramedPoseBase.ice>
#include <MemoryX/interface/memorytypes/MemoryEntities.ice>

module memoryx
{
    interface GraphVisualizerInterface
    {
        /**
         * @brief returns whether an edge is in the graph.
         * @param node1 The edge's first node id.
         * @param node2 The edge's first node id.
         * @return Whether an edge is in the graph.
         */
        bool hasEdge(string node1Id, string node2Id);

        /**
         * @brief Returns whether a node is in the graph.
         * @param name The node's name.
         * @return Whether a node is in the graph.
         */
        bool hasNode(string name);

        /**
         * @brief Adds an edge to the graph.
         * @param node1Id The edge's first node id.
         * @param node2Id The edge's first node id.
         */
        void addEdge(string node1Id ,string node2Id);

        /**
         * @brief Adds a node to the graph.
         * @param node The node.
         */
        void addNode(GraphNodeBase node);

        /**
         * @brief Highlights a edge according to highlighted.
         * @param node1 The edge's first node id.
         * @param node2 The edge's first node id.
         * @param highlighted Whether the highlight will be set or reset.
         */
        void highlightEdge(string node1Id, string node2Id, bool highlighted);
        /**
         * @brief Highlights a node according to highlighted.
         * @param nodeId The node's nodeId.
         * @param highlighted Whether the highlight will be set or reset.
         */
        void highlightNode(string nodeId, bool highlighted);

        /**
         * @brief Draws the whole graph to the debug layer.
         */
        void redraw();

        /**
         * @brief Removes all nodes and edges.
         */
        void clearGraph();

        /**
         * @brief Removes all edges.
         */
        void clearEdges();

        /**
         * @brief Resets the highlighted edges and nodes.
         */
        void resetHighlight();


    };
};

