/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * @package    ArmarX::RobotAPI
 * @author     Nikolaus Vahrenkamp
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <RobotAPI/interface/core/FramedPoseBase.ice>
#include <RobotAPI/interface/visualization/DebugDrawerInterface.ice>
#include <MemoryX/interface/memorytypes/ObjectClass.ice>

module memoryx
{
    interface EntityDrawerInterface extends armarx::DebugDrawerInterfaceAndListener
    {
        /*!
         * \brief setObject Adds an object visualization to the scene
         * \param layerName The layer
         * \param objectName The name of the object (is used as identitfier, e.g. to update the pose later on). An identically named object in this layer is silently replaced.
         * \param objectClassBase The prior knowledge object class. Is used to get the 3D model.
         * \param globalPose The initial pose
         */
        void setObjectVisu(string layerName, string objectName, memoryx::ObjectClassBase objectClassBase, armarx::PoseBase globalPose);
        void setObjectVisuByName(string layerName, string customObjectName, string objectClassName, armarx::PoseBase globalPose);
        void updateObjectPose(string layerName, string objectName, armarx::PoseBase globalPose);
        /*!
         * \brief updateRobotColor Colorizes the object visualization
         * \param layerName The layer
         * \param robotName The object identifyer
         * \param c The draw color, if all is set to 0, the colorization is disabled (i.e. the original vizualization shows up)
         */
        void updateObjectColor(string layerName, string objectName, armarx::DrawColor c);
        /*!
         * \brief updateObjectTransparency Sets the transparency value of an object visualization
         * \param layerName The layer
         * \param robotName The object identifyer
         * \param alpha The alpha value (0 = original, 1 = completely transparent)
         */
        void updateObjectTransparency(string layerName, string objectName, float alpha);
        void removeObjectVisu(string layerName, string objectName);
    };
};

