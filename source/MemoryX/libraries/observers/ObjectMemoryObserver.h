/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Observers
* @author     Kai Welke (welke at kit dot edu), David Schiebener (david dot schiebener at kit dot edu)
* @copyright  2012 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#pragma once

// core
#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <ArmarXCore/core/system/ImportExport.h>
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/interface/observers/VariantBase.h>
#include <ArmarXCore/observers/ConditionCheck.h>
#include <ArmarXCore/observers/Observer.h>
#include <ArmarXCore/observers/variant/ChannelRef.h>

// memory interface
#include <MemoryX/interface/workingmemory/AbstractWorkingMemoryInterface.h>
#include <MemoryX/interface/observers/ObjectMemoryObserverInterface.h>
#include <MemoryX/interface/memorytypes/MemorySegments.h>
#include <MemoryX/interface/components/PriorKnowledgeInterface.h>

// memoryx
#include <MemoryX/libraries/memorytypes/entity/ObjectClass.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectInstance.h>
#include <MemoryX/libraries/updater/ObjectLocalization/LocalizationQuery.h>

#include <string>
#include <mutex>


namespace memoryx
{


    class ObjectMemoryObserverPropertyDefinitions:
        public armarx::ObserverPropertyDefinitions
    {
    public:
        ObjectMemoryObserverPropertyDefinitions(std::string prefix):
            ObserverPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("WorkingMemoryListenerTopic", "WorkingMemoryUpdates", "Topic for working memory updates");
            defineOptionalProperty<std::string>("WorkingMemoryProxy", "WorkingMemory", "Proxy of working memory");
            defineOptionalProperty<std::string>("PriorKnowledgeProxy", "PriorKnowledge", "Proxy of PriorKnowledge");
        }
    };

    /**
     * VisionX ObjectMemoryObserver.
     *
     * The ObjectMemoryObserver allows installing conditions on all known objects.
     * It receives reports from the ObjectMemory about those objects which are being localized.
     *
     * It offers a channel for every known object that contains information about the object
     * pose, if it has been found and how certain the localization is.
     *
     * It also offers a channel that contains the names of all objects which have been localized
     * successfully.
     *
     */
    class ObjectMemoryObserver :
        virtual public armarx::Observer,
        virtual public ObjectMemoryObserverInterface
    {
    public:
        // framework hooks
        void onInitObserver() override;
        void onConnectObserver() override;

        // implementation of ObjectMemoryObserverInterface
        armarx::ChannelRefBasePtr requestObjectClassOnce(const std::string& objectClassName, const IceUtil::Optional<Ice::Int>& priority, const ::Ice::Current& c = Ice::emptyCurrent) override;
        armarx::ChannelRefBasePtr requestObjectClassRepeated(const std::string& objectClassName, int cycleTimeMS, const IceUtil::Optional<Ice::Int>& priority, const ::Ice::Current& c = Ice::emptyCurrent) override;
        void releaseObjectClass(const armarx::ChannelRefBasePtr& objectClassChannel, const ::Ice::Current& c = Ice::emptyCurrent) override;

        ChannelRefBaseSequence getObjectInstances(const armarx::ChannelRefBasePtr& objectClassChannel, const ::Ice::Current& c = Ice::emptyCurrent) override;
        ChannelRefBaseSequence getObjectInstancesByClass(const std::string& objectClassName, const ::Ice::Current& c = Ice::emptyCurrent) override;
        armarx::ChannelRefBasePtr getFirstObjectInstance(const armarx::ChannelRefBasePtr& objectClassChannel, const Ice::Current& c = Ice::emptyCurrent) override;
        armarx::ChannelRefBasePtr getFirstObjectInstanceByClass(const std::string& objectClassName, const Ice::Current& c = Ice::emptyCurrent) override;
        // ObjectMemoryObserverInterface interface
        armarx::ChannelRefBasePtr getObjectInstanceById(const std::string&, const ::Ice::Current& c = ::Ice::Current()) override;
        // TODO: implement user frames
        armarx::ChannelRefBasePtr addFrame(const std::string& frameName, const ::Ice::Current& c = Ice::emptyCurrent);
        void removeFrame(const armarx::ChannelRefBasePtr& frameChannel, const ::Ice::Current& c = Ice::emptyCurrent);

        // implementation of WorkingMemoryListenerInterface
        void reportEntityCreated(const std::string& segmentName, const EntityBasePtr& entity, const ::Ice::Current& c = Ice::emptyCurrent) override;
        void reportEntityUpdated(const std::string& segmentName, const EntityBasePtr& entityOld, const EntityBasePtr& entityNew, const ::Ice::Current& c = Ice::emptyCurrent) override;
        void reportEntityRemoved(const std::string& segmentName, const EntityBasePtr& entity, const ::Ice::Current& c = Ice::emptyCurrent) override;
        void reportSnapshotLoaded(const std::string& segmentName, const ::Ice::Current& c = Ice::emptyCurrent) override;
        void reportSnapshotCompletelyLoaded(const Ice::Current& c = Ice::emptyCurrent) override { }
        void reportMemoryCleared(const std::string& segmentName, const ::Ice::Current& c = Ice::emptyCurrent) override;

        // Implementation of ManagedIceObject interface
        std::string getDefaultName() const override
        {
            return "ObjectMemoryObserver";
        }

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return armarx::PropertyDefinitionsPtr(
                       new ObjectMemoryObserverPropertyDefinitions(
                           getConfigIdentifier()));
        }
    private:
        // utility methods
        armarx::ChannelRefPtr createObjectLocalizationQueryChannel(const LocalizationQueryPtr& query);
        void updateObjectLocalizationQueryChannel(const LocalizationQueryPtr& query);

        std::vector<armarx::ChannelRefPtr> getObjectClassQueries(const std::string& objectClassName);

        void createObjectInstanceChannel(const ObjectInstancePtr& instance);
        void updateObjectInstanceChannel(const ObjectInstancePtr& instance);
        std::string getInstanceChannelName(const EntityBasePtr& instance, const ::Ice::Current& c = Ice::emptyCurrent) const override;

        // proxies
        AbstractWorkingMemoryInterfacePrx proxyWorkingMemory;
        ObjectInstanceMemorySegmentBasePrx objectInstancesSegment;
        ObjectClassMemorySegmentBasePrx objectClassesSegment;
        PriorKnowledgeInterfacePrx priorKnowledge;
        PersistentObjectClassSegmentBasePrx priorObjectClassesSegment;

        // id handling
        unsigned int uniqueId;
        std::mutex idMutex;
        unsigned int getUniqueId()
        {
            std::unique_lock lock(idMutex);
            return uniqueId++;
        }
    };
}

