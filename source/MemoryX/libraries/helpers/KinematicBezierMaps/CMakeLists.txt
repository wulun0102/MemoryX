armarx_set_target("MemoryXKinematicBezierMaps")

find_package(Simox ${RobotAPI_Simox_VERSION} QUIET)
armarx_build_if(Simox_FOUND "Simox not available")

set(LIB_NAME       MemoryXKinematicBezierMaps)
set(LIBS MemoryXCore VirtualRobot)
set(LIB_FILES kbm.cpp pls.cpp inverse.cpp)
set(LIB_HEADERS kbm.h)

armarx_add_library("${LIB_NAME}"  "${LIB_FILES}" "${LIB_HEADERS}" "${LIBS}" )

