#include "ObjectClassSegmentWrapper.h"


#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <MemoryX/libraries/helpers/VirtualRobotHelpers/SimoxObjectWrapper.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectClass.h>
#include <MemoryX/core/GridFileManager.h>

#include <VirtualRobot/ManipulationObject.h>


namespace memoryx
{

    void ObjectClassSegmentWrapper::initFromProxy(PriorKnowledgeInterfacePrx const& priorKnowledge)
    {
        ARMARX_TRACE;
        ARMARX_CHECK_NOT_NULL(priorKnowledge);

        memoryx::PersistentObjectClassSegmentBasePrx objectClassesSegment = priorKnowledge->getObjectClassesSegment();
        memoryx::GridFileManagerPtr fileManager(new memoryx::GridFileManager(priorKnowledge->getCommonStorage()));

        memoryx::EntityBaseList classEntities = objectClassesSegment->getAllEntities();
        for (auto& classEntity : classEntities)
        {
            ARMARX_TRACE;
            ARMARX_CHECK_NOT_NULL(classEntity);
            memoryx::ObjectClassPtr objectClass = memoryx::ObjectClassPtr::dynamicCast(classEntity);
            if (objectClass)
            {
                std::string objectClassName = objectClass->getName();
                ARMARX_VERBOSE << "Getting files for object class '" << objectClassName << "'";
                memoryx::EntityWrappers::SimoxObjectWrapperPtr sw = objectClass->addWrapper(new memoryx::EntityWrappers::SimoxObjectWrapper(fileManager));
                VirtualRobot::ManipulationObjectPtr manipulationObject = sw->getManipulationObject();

                ObjectClassWrapper classData = {objectClass, manipulationObject};
                classToWrapper.emplace(objectClassName, classData);
            }
        }
    }

    std::optional<ObjectClassWrapper> ObjectClassSegmentWrapper::getClass(std::string const& className) const
    {
        auto found = classToWrapper.find(className);
        if (found == classToWrapper.end())
        {
            return std::nullopt;
        }
        else
        {
            return found->second;
        }
    }

}
