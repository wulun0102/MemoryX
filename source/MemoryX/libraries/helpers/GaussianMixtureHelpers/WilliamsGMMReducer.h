/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::GaussianMixtureHelpers
* @author     Alexey Kozlov <kozlov@kit.edu>
* @copyright  2013 Alexey Kozlov
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <string>
#include <cfloat>
#include <ArmarXCore/core/system/ImportExport.h>

#include <MemoryX/core/entity/ProbabilityMeasures.h>
#include "GMMReducer.h"
#include "ISDDistance.h"

namespace memoryx
{
    /**
    * @class WilliamsGMMReducer
    * @ingroup GaussianMixtureHelpers
    */
    class ARMARXCORE_IMPORT_EXPORT WilliamsGMMReducer:
        public GMMReducer
    {
    public:
        WilliamsGMMReducer();

    protected:
        float getMergingCost(const GaussianMixtureDistributionBasePtr& gmm, int c1, int c2) override;
        void fillMergingCostVector(const GaussianMixtureDistributionBasePtr& gmm, GMMCompPairDistanceVector& costVec) override;

    private:
        ISDDistancePtr isdDistance;
    };
}

