/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::GaussianMixtureHelpers
* @author     Alexey Kozlov <kozlov@kit.edu>
* @copyright  2013 Alexey Kozlov
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "WilliamsGMMReducer.h"
#include "ISDDistance.h"

namespace memoryx
{
    WilliamsGMMReducer::WilliamsGMMReducer(): GMMReducer()
    {
        isdDistance.reset(new ISDDistance());
        //    gmmDistance.reset(isdDistance.get());
    }

    float WilliamsGMMReducer::getMergingCost(const GaussianMixtureDistributionBasePtr& gmm, int c1, int c2)
    {
        // TODO this could be calculated a way more efficient
        GaussianMixtureDistributionPtr reducedGMM = GaussianMixtureDistributionPtr::dynamicCast(gmm->clone());
        mergeGMMComponents(reducedGMM, c1, c2);
        return gmmDistance->getDistance(gmm, reducedGMM);
    }

    void WilliamsGMMReducer::fillMergingCostVector(const GaussianMixtureDistributionBasePtr& gmm, GMMCompPairDistanceVector& costVec)
    {
        GaussianMixtureDistributionPtr reducedGMM = GaussianMixtureDistributionPtr::dynamicCast(gmm->clone());
        float d11 = isdDistance->calcSelfLikeness(gmm);
        const int GMM_SIZE = gmm->size();

        for (int i = 0; i < GMM_SIZE - 1; ++i)
            for (int j = i + 1; j < GMM_SIZE; ++j)
            {
                //            GaussianMixtureDistributionPtr reducedGMM = GaussianMixtureDistributionPtr::dynamicCast(gmm->clone());
                GaussianMixtureComponent c1 = reducedGMM->getComponent(i);
                GaussianMixtureComponent c2 = reducedGMM->getComponent(j);
                mergeGMMComponents(reducedGMM, i, j);
                const float cost = isdDistance->getDistance(gmm, reducedGMM, d11);
                const int index = i * GMM_SIZE + j;
                costVec.push_back(GMMCompPairDistance(index, cost));
                reducedGMM->setComponent(i, c1);
                reducedGMM->addComponent(c2);
            }
    }
}
