/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::GaussianMixtureHelpers
* @author     Alexey Kozlov <kozlov@kit.edu>
* @copyright  2013 Alexey Kozlov
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

// boost
#include <memory>

#include <string>
#include <ArmarXCore/core/system/ImportExport.h>

#include <MemoryX/core/entity/ProbabilityMeasures.h>
#include "GMMDistance.h"

namespace memoryx
{

    enum DeviationMeasure
    {
        eAABB,
        eOrientedBBox,
        eEqualSphere
    };

    using GMMCompPairDistance = std::pair<int, float>;
    using GMMCompPairDistanceVector = std::vector<GMMCompPairDistance>;

    /**
    * @class GMMReducer
    * @ingroup CommonPlacesLearner
    */
    class ARMARXCORE_IMPORT_EXPORT GMMReducer
    {
    public:
        /**
         * Creates a new GMMReducer
         */
        GMMReducer()
        {
        }

        /**
         *
         *
         */
        virtual GaussianMixtureDistributionBasePtr reduceByComponentCount(const GaussianMixtureDistributionBasePtr& fullGMM, int countComp);

        virtual GaussianMixtureDistributionBasePtr reduceByMaxAABB(const GaussianMixtureDistributionBasePtr& fullGMM, float maxSideLength);

        virtual GaussianMixtureDistributionBasePtr reduceByMaxOrientedBBox(const GaussianMixtureDistributionBasePtr& fullGMM, float maxSideLength);

        virtual GaussianMixtureDistributionBasePtr reduceByMaxEqualSphere(const GaussianMixtureDistributionBasePtr& fullGMM, float maxSphereRadius);

        virtual GaussianMixtureDistributionBasePtr reduceByMaxDeviation(const GaussianMixtureDistributionBasePtr& fullGMM, float maxDeviation,
                DeviationMeasure devMeasure);

    protected:
        GMMDistancePtr gmmDistance;

        virtual float getMergingCost(const GaussianMixtureDistributionBasePtr& gmm, int c1, int c2);

        virtual void fillMergingCostVector(const GaussianMixtureDistributionBasePtr& gmm, GMMCompPairDistanceVector& costVec);

        void mergeGMMComponents(const GaussianMixtureComponent& comp1, const GaussianMixtureComponent& comp2, GaussianMixtureComponent& mergedComp);

        void replaceComponentsWithMerged(GaussianMixtureDistributionPtr& gmm, int index1, int index2, const GaussianMixtureComponent& mergedComp);

        void mergeGMMComponents(GaussianMixtureDistributionPtr& gmm, int index1, int index2);
    };

    using GMMReducerPtr = std::shared_ptr<GMMReducer>;

}

