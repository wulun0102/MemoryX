// *****************************************************************
// Filename:    KalmanFilter.h
// Copyright:   Kai Welke, Chair Prof. Dillmann (IAIM),
//              Institute for Computer Science and Engineering (CSE),
//              University of Karlsruhe. All rights reserved.
// Author:      Kai Welke
// Date:        27.05.2009
// *****************************************************************
#pragma once

// *****************************************************************
// includes
// *****************************************************************
#include <Eigen/Eigen>

#include "Gaussian.h"
#include "KalmanFilter.h"

// Kalman filter for linear system with the following control model:
//      xt = xt-1
// and following measurment model
//      zt = Ix + e + gaussian error

// *****************************************************************
// declaration of CNoMotionFilter
// *****************************************************************
class NoMotionFilter : public KalmanFilter
{
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    // construction / destruction
    NoMotionFilter() {};

    // filtering (if result == NULL, believe is updated)
    Gaussian filter(const Gaussian& believe, const Eigen::VectorXd& perceptZ);
};

