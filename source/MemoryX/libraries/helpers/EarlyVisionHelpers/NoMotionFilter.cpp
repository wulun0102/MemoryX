// *****************************************************************
// Filename:    NoMotionFilter.cpp
// Copyright:   Kai Welke, Chair Prof. Dillmann (IAIM),
//              Institute for Computer Science and Engineering (CSE),
//              University of Karlsruhe. All rights reserved.
// Author:      Kai Welke
// Date:        27.05.2009
// *****************************************************************

// *****************************************************************
// includes
// *****************************************************************
#include "NoMotionFilter.h"

// *****************************************************************
// prediction / update
// *****************************************************************
Gaussian NoMotionFilter::filter(const Gaussian& believe, const Eigen::VectorXd& perceptZ)
{
    // only update
    Gaussian result = update(believe, perceptZ);

    return result;
}
