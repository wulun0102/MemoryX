/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Core
* @author     Kai Welke <welke@kit.edu>
* @copyright  2012 Kai Welke
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <string>
#include <ArmarXCore/core/system/ImportExport.h>
#include <MemoryX/libraries/workingmemory/fusion/EntityFusionMethod.h>

namespace memoryx
{
    /**
     * @class AttributeReplacementFusion
     * @ingroup WorkingMemory
     */
    class ARMARXCORE_IMPORT_EXPORT AttributeReplacementFusion :
        virtual public EntityFusionMethod
    {
    public:
        /**
         * Creates a new AttributeEnrichementFusion
         */
        AttributeReplacementFusion() :
            EntityFusionMethod("AttributeReplacementFusion")
        {
        }

        /**
         * Copies attributes from the update entity to the stored entity. Replaces existing attributes and adds those which have not been present so far.
         *
         * @param baseEntity base entity for fusion
         * @param updateEntity entity to be fused with base entity
         *
         * @return fused entity
         */
        EntityBasePtr fuseEntity(const EntityBasePtr& baseEntity, const EntityBasePtr& updateEntity, const ::Ice::Current&) override
        {
            ARMARX_DEBUG_S << "AttributeReplacementFusion::fuseEntity() called";

            // do we really need to clone ??
            EntityBasePtr fusedEntity = EntityBasePtr::dynamicCast(baseEntity->ice_clone());
            addAttributesWithReplacement(updateEntity, fusedEntity);

            return fusedEntity;
        }

    private:
        void addAttributesWithReplacement(const EntityBasePtr& srcEntity, const EntityBasePtr& destEntity) const
        {
            NameList attrNames = srcEntity->getAttributeNames();

            for (NameList::const_iterator it = attrNames.begin(); it != attrNames.end(); ++it)
            {
                destEntity->putAttribute(srcEntity->getAttribute(*it));
            }
        }
    };
}

