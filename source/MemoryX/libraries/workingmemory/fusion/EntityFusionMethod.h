/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Core
* @author     Kai Welke <welke@kit.edu>
* @copyright  2012 Kai Welke
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <string>
#include <ArmarXCore/core/system/ImportExport.h>
#include <IceUtil/Handle.h>
#include <MemoryX/interface/core/FusionMethods.h>

namespace memoryx
{
    class EntityFusionMethod;
    using EntityFusionMethodPtr = IceInternal::Handle<EntityFusionMethod>;

    /**
     * @class EntityFusionMethod
     * @brief Interface for fusion methods used for entities in working memory
     * @ingroup WorkingMemory
     *
     * Define fusion methods by subclassing EntityFusionMethod. The pure virtual method fuse()
     * needs to be implemented.
     */
    class ARMARXCORE_IMPORT_EXPORT EntityFusionMethod :
        virtual public EntityFusionMethodBase
    {
    public:
        /**
        * Constructs a new fusion method
        *
        * @param name of the fusion method
        */
        EntityFusionMethod(std::string methodName)
        {
            this->methodName = methodName;
        }

        /**
        * Initialization of an entity. Implement this method in order to add attributes which are fusion method
        * specific, if you need it. The default implementation just returns the updateEntity.
        *
        * @param updateEntity entity to be initilialized
        * @return initialized entity
        */
        EntityBasePtr initEntity(const EntityBasePtr& updateEntity, const ::Ice::Current& = Ice::emptyCurrent) override
        {
            return updateEntity;
        }

        /**
        * Fusion method. Fuses two entities with the same key. Implement this in a concrete EntityFusionMethod.
        * @param baseEntity base entity for fusion
        * @param updateEntity entity to be fused with base entity
        *
        * @return fused entity
        */
        EntityBasePtr fuseEntity(const EntityBasePtr& baseEntity, const EntityBasePtr& updateEntity, const ::Ice::Current& = Ice::emptyCurrent) override = 0;

        std::string getMethodName(const ::Ice::Current& = Ice::emptyCurrent) const override
        {
            return methodName;
        }
    private:
        std::string methodName;
    };
}

