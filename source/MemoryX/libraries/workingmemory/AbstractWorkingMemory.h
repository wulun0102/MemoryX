/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::WorkingMemory
* @author     ALexey Kozlov ( kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include "WorkingMemoryEntitySegment.h"

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <MemoryX/interface/workingmemory/AbstractWorkingMemoryInterface.h>

#include <MemoryX/interface/components/CommonStorageInterface.h>
#include <MemoryX/interface/components/PriorKnowledgeInterface.h>
#include <MemoryX/interface/components/LongtermMemoryInterface.h>

#include <MemoryX/core/memory/SegmentedMemory.h>
#include <MemoryX/core/MongoSerializer.h>
#include <MemoryX/core/MongoDBRef.h>

namespace memoryx
{

    class AbstractWorkingMemoryPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        AbstractWorkingMemoryPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<bool>("UsePriorMemory", true, "Switch prior knowledge on/off.");

            defineOptionalProperty<std::string>("PriorMemoryName", "PriorKnowledge", "Name of PriorKnowledge Ice component");

            defineOptionalProperty<bool>("UseLongtermMemory", true, "Switch longterm memory on/off.");

            defineOptionalProperty<std::string>("LongtermMemoryName", "LongtermMemory", "Name of LongtermMemory Ice component");

            defineOptionalProperty<bool>("PublishUpdates", true, "Publish scene updates (ObjectCreated/Updated/Removed) on IceStrom topic");

            defineOptionalProperty<std::string>("UpdatesTopicName", "WorkingMemoryUpdates", "Name of IceStrom topic to publish scene updates on");

            defineOptionalProperty<std::string>("SnapshotToLoad", "", "Name of Snapshot that should be loaded on start up. Leave empty for none.");
        }
    };

    struct MemoryUpdaterEntry
    {
        WorkingMemoryUpdaterBasePrx proxy;
        WorkingMemoryUpdaterBasePtr pointer;
    };

    using MemoryUpdaterMap = std::map<std::string, MemoryUpdaterEntry>;

    class ARMARXCOMPONENT_IMPORT_EXPORT AbstractWorkingMemory :
        virtual public AbstractWorkingMemoryInterface,
        virtual public SegmentedMemory,
        virtual public armarx::Component
    {
    public:
        // inherited from Component
        std::string getDefaultName() const override
        {
            return "AbstractWorkingMemory";
        }
        void onInitComponent() override;
        void onConnectComponent() override;

        // TODO: exit or destructor: clean all segments and updaters in order to resolve cyclic pointer dependencies

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return armarx::PropertyDefinitionsPtr(
                       new AbstractWorkingMemoryPropertyDefinitions(
                           getConfigIdentifier()));
        }

        Ice::ObjectAdapterPtr getObjectAdapter() const override
        {
            return Component::getObjectAdapter();
        }

        // implementation of WorkingMemoryInterface
        AbstractMemorySegmentPrx addSegment(const std::string& segmentName, const AbstractMemorySegmentPtr& segment, const ::Ice::Current& = Ice::emptyCurrent) override;

        WorkingMemoryUpdaterBasePrx registerUpdater(const std::string& updaterName, const WorkingMemoryUpdaterBasePtr& updater, const ::Ice::Current& = Ice::emptyCurrent) override;
        WorkingMemoryUpdaterBasePrx getUpdater(const std::string& updaterName, const ::Ice::Current& = Ice::emptyCurrent) override;
        void unregisterUpdater(const std::string& updaterName, const ::Ice::Current& = Ice::emptyCurrent) override;

        /*!
         * \brief getListenerTopicName The topic name.
         * \return
         */
        std::string getListenerTopicName()
        {
            return updatesTopicName;
        }

        AbstractMemorySegmentPrx addGenericSegment(const std::string& segmentName, const Ice::Current&) override
        {
            WorkingMemoryEntitySegmentBasePtr segment = new WorkingMemoryEntitySegment<Entity>();
            return addSegment(segmentName, segment);
        }

        // misc
        void clear(const ::Ice::Current& = Ice::emptyCurrent) override;
        void print(const ::Ice::Current& = Ice::emptyCurrent) const override;

    protected:
        // subclass hooks
        virtual void onInitWorkingMemory() = 0;
        virtual void onConnectWorkingMemory() = 0;

        CommonStorageInterfacePrx dataBasePrx;
        PriorKnowledgeInterfacePrx priorKnowledgePrx;
        LongtermMemoryInterfacePrx longtermMemoryPrx;
        WorkingMemoryListenerInterfacePrx listenerPrx;
        MongoSerializerPtr dbSerializer;

        MemoryUpdaterMap updaters;
        mutable std::shared_mutex updaterMutex;

        bool usePriorMemory;
        bool useLongtermMemory;
        bool useCommonStorage;
        bool publishUpdates;

        std::string priorMemoryName;
        std::string longtermMemoryName;
        std::string updatesTopicName;
    };

    using AbstractWorkingMemoryPtr = IceUtil::Handle<AbstractWorkingMemory>;

}
