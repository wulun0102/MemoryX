/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::CommonStorage
* @author     Alexey Kozlov ( kozlov at kit dot edu), Kai Welke (welke at kit dot edu)
* @date
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "AbstractWorkingMemory.h"
#include "WorkingMemoryEntitySegment.h"
#include <MemoryX/libraries/workingmemory/updater/WorkingMemoryUpdater.h>
#include <ArmarXCore/core/ArmarXManager.h>
#include <ArmarXCore/interface/core/Log.h>

#include <IceUtil/UUID.h>


#define COMMONSTORAGE_NAME "CommonStorage"

namespace memoryx
{
    void AbstractWorkingMemory::onInitComponent()
    {
        usePriorMemory = getProperty<bool>("UsePriorMemory").getValue();
        priorMemoryName = getProperty<std::string>("PriorMemoryName").getValue();

        if (usePriorMemory)
        {
            usingProxy(priorMemoryName);
        }

        useLongtermMemory = getProperty<bool>("UseLongtermMemory").getValue();
        longtermMemoryName = getProperty<std::string>("LongtermMemoryName").getValue();

        if (useLongtermMemory)
        {
            usingProxy(longtermMemoryName);
        }

        useCommonStorage = usePriorMemory || useLongtermMemory;

        if (useCommonStorage)
        {
            usingProxy("CommonStorage");
        }

        publishUpdates = getProperty<bool>("PublishUpdates").getValue();
        updatesTopicName = getProperty<std::string>("UpdatesTopicName").getValue();

        if (publishUpdates)
        {
            offeringTopic(updatesTopicName);
        }

        // call subclass hook
        onInitWorkingMemory();
    }


    void AbstractWorkingMemory::onConnectComponent()
    {
        ARMARX_INFO << "Starting MemoryX::AbstractWorkingMemory";

        const Ice::CommunicatorPtr ic = getIceManager()->getCommunicator();


        dbSerializer = new MongoSerializer(ic);

        if (usePriorMemory)
        {
            priorKnowledgePrx = getProxy<PriorKnowledgeInterfacePrx>(priorMemoryName);
        }

        if (useLongtermMemory)
        {
            longtermMemoryPrx = getProxy<LongtermMemoryInterfacePrx>(longtermMemoryName);
        }

        if (useCommonStorage)
        {
            dataBasePrx = getProxy<CommonStorageInterfacePrx>(COMMONSTORAGE_NAME);
        }

        if (publishUpdates)
        {
            listenerPrx = getTopic<WorkingMemoryListenerInterfacePrx>(updatesTopicName);
        }

        // call subclass hook
        onConnectWorkingMemory();

        if (longtermMemoryPrx && !getProperty<std::string>("SnapshotToLoad").getValue().empty())
        {
            longtermMemoryPrx->loadWorkingMemorySnapshot(getProperty<std::string>("SnapshotToLoad").getValue(), AbstractWorkingMemoryInterfacePrx::uncheckedCast(getProxy()));
        }
    }


    AbstractMemorySegmentPrx AbstractWorkingMemory::addSegment(const std::string& segmentName, const AbstractMemorySegmentPtr& segment, const ::Ice::Current& c)
    {
        AbstractWorkingMemorySegmentPtr wmSegment = AbstractWorkingMemorySegmentPtr::dynamicCast(segment);

        if (wmSegment && listenerPrx)
        {
            // set observer proxy
            wmSegment->setListenerProxy(listenerPrx);
        }

        AbstractMemorySegmentPrx segmentProxy = SegmentedMemory::addSegment(segmentName, segment);

        ARMARX_INFO << "Added AbstractWorkingMemorySegment: " << segmentName;

        return segmentProxy;
    }

    WorkingMemoryUpdaterBasePrx AbstractWorkingMemory::registerUpdater(const std::string& updaterName, const WorkingMemoryUpdaterBasePtr& updater, const ::Ice::Current&)
    {
        // init updater
        WorkingMemoryUpdaterPtr updaterImpl = WorkingMemoryUpdaterPtr::dynamicCast(updater);
        updaterImpl->setWorkingMemory(this);
        getArmarXManager()->addObject(updaterImpl, false);

        // insert into known updaters
        std::pair<std::string, MemoryUpdaterEntry> updaterEntry;
        updaterEntry.first = updaterName;
        updaterEntry.second.proxy = WorkingMemoryUpdaterBasePrx::uncheckedCast(updaterImpl->getProxy());
        updaterEntry.second.pointer = updaterImpl;

        std::unique_lock lock(updaterMutex);
        updaters.insert(updaterEntry);

        ARMARX_INFO << "Registered AbstractWorkingMemoryUpdater: " << updaterName;

        return updaterEntry.second.proxy;
    }

    WorkingMemoryUpdaterBasePrx AbstractWorkingMemory::getUpdater(const std::string& updaterName, const ::Ice::Current&)
    {
        std::unique_lock lock(updaterMutex);

        // find segment
        MemoryUpdaterMap::iterator iter =  updaters.find(updaterName);

        if (iter == updaters.end())
        {
            throw InvalidEntityException();
        }

        return iter->second.proxy;
    }

    void AbstractWorkingMemory::unregisterUpdater(const std::string& updaterName, const ::Ice::Current&)
    {
        std::unique_lock lock(updaterMutex);

        MemoryUpdaterMap::iterator iter = updaters.find(updaterName);

        if (iter != updaters.end())
        {
            WorkingMemoryUpdaterPtr updater = WorkingMemoryUpdaterPtr::dynamicCast(iter->second.pointer);

            // remove from manager
            getArmarXManager()->removeObjectBlocking(updater);

            // remove from known updaters
            updaters.erase(iter);

            ARMARX_INFO << "Unregistered  AbstractWorkingMemoryUpdater: " << updaterName;
        }
    }

    void AbstractWorkingMemory::clear(const ::Ice::Current&)
    {
        std::unique_lock lock(segmentsMutex);

        for (MemorySegmentMap::iterator it = segments.begin(); it != segments.end(); ++it)
        {
            it->second.pointer->clear();
        }
    }

    void AbstractWorkingMemory::print(const ::Ice::Current&) const
    {
        std::unique_lock lock(segmentsMutex);

        std::cout << "Memory contains " << segments.size() << " segments" << std::endl;

        for (MemorySegmentMap::const_iterator it = segments.begin(); it != segments.end(); ++it)
        {
            std::cout << "Memory segment " << it->first << " contains " << it->second.pointer->size() << " entities" << std::endl;
            it->second.pointer->print();
        }
    }
}
