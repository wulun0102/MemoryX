/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::WorkingMemory
 * @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "PersistentProfilerDataSegment.h"


namespace memoryx
{
    PersistentProfilerDataSegment::PersistentProfilerDataSegment(CollectionInterfacePrx entityCollection, Ice::CommunicatorPtr ic, bool useMongoIds) :
        PersistentEntitySegment(entityCollection, ic, useMongoIds),
        PersistentProfilerDataSegmentBase()
    {
    }


    ProfilerEntityBaseList PersistentProfilerDataSegment::getProfilerDataEntities(const Ice::Current&) const
    {
        ProfilerEntityBaseList entities;

        for (std::string& id : getAllEntityIds())
        {
            EntityBasePtr entity = getEntityById(id);

            if (entity->ice_isA(ProfilerEntityBase::ice_staticId()))
            {
                entities.push_back(ProfilerEntityBasePtr::dynamicCast(entity));
            }
        }

        return entities;
    }


    ProfilerEventBaseList memoryx::PersistentProfilerDataSegment::getProfilerEventEntities(const Ice::Current&) const
    {
        ProfilerEventBaseList entities;

        for (std::string& id : getAllEntityIds())
        {
            EntityBasePtr entity = getEntityById(id);

            if (entity->ice_isA(ProfilerEventBase::ice_staticId()))
            {
                entities.push_back(ProfilerEventBasePtr::dynamicCast(entity));
            }
        }

        return entities;
    }

    ProfilerTransitionBaseList memoryx::PersistentProfilerDataSegment::getProfilerTransitionEntities(const Ice::Current&) const
    {
        ProfilerTransitionBaseList entities;

        for (std::string& id : getAllEntityIds())
        {
            EntityBasePtr entity = getEntityById(id);

            if (entity->ice_isA(ProfilerTransitionBase::ice_staticId()))
            {
                entities.push_back(ProfilerTransitionBasePtr::dynamicCast(entity));
            }
        }

        return entities;
    }
}
