/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::Core
 * @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE MemoryX::PersistentProfilerSegmentTests
#define ARMARX_BOOST_TEST
#include <MemoryX/Test.h>

#include "../PersistentProfilerDataSegment.h"
#include "../../entity/profiler/ProfilerMemorySnapshot.h"
#include "../../entity/profiler/ProfilerTransition.h"
#include "../../../../core/entity/EntityRef.h"


BOOST_AUTO_TEST_SUITE(PersistentProfilerSegmentTest)

/*
 * MemorySnapshot Tests
 */


class ProfilerMemorySnapshotFixture
{
public:
    ProfilerMemorySnapshotFixture() :
        segment(nullptr, nullptr),
        STATE_NAME("StateA"),
        STATE_ID("IdStateA")
    {
        snapshotEmptyContext = new memoryx::ProfilerMemorySnapshot(STATE_NAME, {});

        entityListEmptyContext.push_back(new memoryx::ProfilerMemorySnapshot("StateC", {}));
        entityListEmptyContext.push_back(new memoryx::ProfilerMemorySnapshot("StateB", {}));
        entityListEmptyContext.push_back(new memoryx::ProfilerMemorySnapshot(STATE_NAME, {}));
        entityListEmptyContext.back()->setId(STATE_ID);

        snapshotWithContext = new memoryx::ProfilerMemorySnapshot(STATE_NAME, {{"param1", "value5"}, {"param2", "value6"}});

        entityListDifferentContexts.push_back(new memoryx::ProfilerMemorySnapshot(STATE_NAME, {{"param1", "value1"}, {"param2", "value2"}}));
        entityListDifferentContexts.push_back(new memoryx::ProfilerMemorySnapshot(STATE_NAME, {{"param1", "value3"}, {"param2", "value4"}}));
        entityListDifferentContexts.push_back(new memoryx::ProfilerMemorySnapshot(STATE_NAME, {{"param1", "value5"}, {"param2", "value6"}}));
        entityListDifferentContexts.front()->setId("IdState1");
        entityListDifferentContexts.back()->setId(STATE_ID);
    }
    ~ProfilerMemorySnapshotFixture()
    {
    }
    memoryx::PersistentProfilerDataSegment segment;

    memoryx::EntityBaseList emptyEntityList;
    memoryx::EntityBaseList entityListEmptyContext;
    memoryx::EntityBaseList entityListDifferentContexts;

    memoryx::ProfilerMemorySnapshotPtr snapshotEmptyContext;
    memoryx::ProfilerMemorySnapshotPtr snapshotWithContext;

    const std::string STATE_NAME;
    const std::string STATE_ID;
};


BOOST_FIXTURE_TEST_CASE(snapshotInEmptyEntityList, ProfilerMemorySnapshotFixture)
{
    BOOST_CHECK_EQUAL(memoryx::PersistentProfilerDataSegment::GetEntityIdFromList<memoryx::EntityBasePtr>(snapshotEmptyContext, emptyEntityList), "");
}

BOOST_FIXTURE_TEST_CASE(snapshotInEntityListEmptyContext, ProfilerMemorySnapshotFixture)
{
    BOOST_CHECK_EQUAL(memoryx::PersistentProfilerDataSegment::GetEntityIdFromList<memoryx::EntityBasePtr>(snapshotEmptyContext, entityListEmptyContext), STATE_ID);
    BOOST_CHECK_EQUAL(memoryx::PersistentProfilerDataSegment::GetEntityIdFromList<memoryx::EntityBasePtr>(snapshotEmptyContext, entityListDifferentContexts), "");
}

BOOST_FIXTURE_TEST_CASE(snapshotWithContextInEntityListDifferentContext, ProfilerMemorySnapshotFixture)
{
    BOOST_CHECK_EQUAL(memoryx::PersistentProfilerDataSegment::GetEntityIdFromList<memoryx::EntityBasePtr>(snapshotWithContext, entityListEmptyContext), "");
    BOOST_CHECK_EQUAL(memoryx::PersistentProfilerDataSegment::GetEntityIdFromList<memoryx::EntityBasePtr>(snapshotWithContext, entityListDifferentContexts), STATE_ID);
}



/*
 * Transition Tests
 */


class ProfilerTransitionFixture
{
public:
    ProfilerTransitionFixture() :
        segment(nullptr, nullptr),
        PARENT_STATE_NAME("ParentState"),
        SOURCE_STATE_NAME("SourceState"),
        TARGET_STATE_NAME("TargetState"),
        STATE_ID("StateId")
    {
        transitionEmptySnapshots = new memoryx::ProfilerTransition(PARENT_STATE_NAME, SOURCE_STATE_NAME, TARGET_STATE_NAME, new memoryx::EntityRef(), new memoryx::EntityRef(), 1);

        transitionListDifferentStateNames.push_back(new memoryx::ProfilerTransition("ParentState2", SOURCE_STATE_NAME, TARGET_STATE_NAME, new memoryx::EntityRef(), new memoryx::EntityRef(), 1));
        transitionListDifferentStateNames.push_back(new memoryx::ProfilerTransition(PARENT_STATE_NAME, "SourceState2", TARGET_STATE_NAME, new memoryx::EntityRef(), new memoryx::EntityRef(), 1));
        transitionListDifferentStateNames.push_back(new memoryx::ProfilerTransition(PARENT_STATE_NAME, SOURCE_STATE_NAME, "TargetState2", new memoryx::EntityRef(), new memoryx::EntityRef(), 1));
        transitionListDifferentStateNames.push_back(new memoryx::ProfilerTransition(PARENT_STATE_NAME, SOURCE_STATE_NAME, TARGET_STATE_NAME, new memoryx::EntityRef(), new memoryx::EntityRef(), 1));
        transitionListDifferentStateNames.back()->setId(STATE_ID);

        memoryx::EntityRefBasePtr sourceEntityRef = new memoryx::EntityRef();
        sourceEntityRef->entityId = "entityIdSource";
        sourceEntityRef->entityName = "entitySource";
        sourceEntityRef->segment = nullptr;
        sourceEntityRef->segmentName = "segment";
        sourceEntityRef->memory = nullptr;
        sourceEntityRef->memoryName = "memory";
        memoryx::EntityRefBasePtr targetEntityRef = new memoryx::EntityRef();
        targetEntityRef->entityId = "entityIdTarget";
        targetEntityRef->entityName = "entityTarget";
        targetEntityRef->segment = nullptr;
        targetEntityRef->segmentName = "segment";
        targetEntityRef->memory = nullptr;
        targetEntityRef->memoryName = "memory";

        transitionWithSnapshots = new memoryx::ProfilerTransition(PARENT_STATE_NAME, SOURCE_STATE_NAME, TARGET_STATE_NAME, sourceEntityRef, targetEntityRef, 1);

        memoryx::EntityRefBasePtr sourceEntityRef2 = new memoryx::EntityRef();
        sourceEntityRef2->entityId = "entityIdSource2";
        sourceEntityRef2->entityName = "entitySource2";
        sourceEntityRef2->segment = nullptr;
        sourceEntityRef2->segmentName = "segment";
        sourceEntityRef2->memory = nullptr;
        sourceEntityRef2->memoryName = "memory";
        memoryx::EntityRefBasePtr targetEntityRef2 = new memoryx::EntityRef();
        targetEntityRef2->entityId = "entityIdTarget2";
        targetEntityRef2->entityName = "entityTarget2";
        targetEntityRef2->segment = nullptr;
        targetEntityRef2->segmentName = "segment";
        targetEntityRef2->memory = nullptr;
        targetEntityRef2->memoryName = "memory";

        transitionListDifferentSnapshots.push_back(new memoryx::ProfilerTransition(PARENT_STATE_NAME, SOURCE_STATE_NAME, TARGET_STATE_NAME, new memoryx::EntityRef(), new memoryx::EntityRef(), 1));
        transitionListDifferentSnapshots.push_back(new memoryx::ProfilerTransition(PARENT_STATE_NAME, SOURCE_STATE_NAME, TARGET_STATE_NAME, sourceEntityRef2, targetEntityRef, 1));
        transitionListDifferentSnapshots.push_back(new memoryx::ProfilerTransition(PARENT_STATE_NAME, SOURCE_STATE_NAME, TARGET_STATE_NAME, sourceEntityRef, targetEntityRef2, 1));
        transitionListDifferentSnapshots.push_back(new memoryx::ProfilerTransition(PARENT_STATE_NAME, SOURCE_STATE_NAME, TARGET_STATE_NAME, sourceEntityRef2, targetEntityRef2, 1));
        transitionListDifferentSnapshots.push_back(new memoryx::ProfilerTransition(PARENT_STATE_NAME, SOURCE_STATE_NAME, TARGET_STATE_NAME, sourceEntityRef, targetEntityRef, 1));
        transitionListDifferentSnapshots.back()->setId(STATE_ID);
    }
    ~ProfilerTransitionFixture()
    {
    }
    memoryx::PersistentProfilerDataSegment segment;

    memoryx::ProfilerTransitionBaseList emptyTransitionList;
    memoryx::ProfilerTransitionBaseList transitionListDifferentStateNames;
    memoryx::ProfilerTransitionBaseList transitionListDifferentSnapshots;

    memoryx::ProfilerTransitionPtr transitionEmptySnapshots;
    memoryx::ProfilerTransitionPtr transitionWithSnapshots;

    const std::string PARENT_STATE_NAME;
    const std::string SOURCE_STATE_NAME;
    const std::string TARGET_STATE_NAME;
    const std::string STATE_ID;
};

BOOST_FIXTURE_TEST_CASE(transitionInEmptyEntityList, ProfilerTransitionFixture)
{
    BOOST_CHECK_EQUAL(memoryx::PersistentProfilerDataSegment::GetEntityIdFromList<memoryx::ProfilerTransitionBasePtr>(transitionEmptySnapshots, emptyTransitionList), "");
}

BOOST_FIXTURE_TEST_CASE(transitionInEntityListWithoutSnapshots, ProfilerTransitionFixture)
{
    BOOST_CHECK_EQUAL(memoryx::PersistentProfilerDataSegment::GetEntityIdFromList<memoryx::ProfilerTransitionBasePtr>(transitionEmptySnapshots, transitionListDifferentStateNames), STATE_ID);
}

BOOST_FIXTURE_TEST_CASE(transitionInEmptyEntityWithSnapshots, ProfilerTransitionFixture)
{
    BOOST_CHECK_EQUAL(memoryx::PersistentProfilerDataSegment::GetEntityIdFromList<memoryx::ProfilerTransitionBasePtr>(transitionWithSnapshots, transitionListDifferentSnapshots), STATE_ID);
}

BOOST_AUTO_TEST_SUITE_END()
