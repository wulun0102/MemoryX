/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::WorkingMemory
* @author     Thomas von der Heyde (tvh242 at hotmail dot com)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "WorldStateSegment.h"

namespace memoryx
{
    WorldStateSegment::WorldStateSegment() :
        WorkingMemoryEntitySegment<ObjectInstance>::WorkingMemoryEntitySegment(),
        WorldStateSegmentBase::WorldStateSegmentBase()
    {

    }

    ObjectInstanceBasePtr WorldStateSegment::getWorldInstanceById(const ::std::string& id, const ::Ice::Current&) const
    {
        ObjectInstanceBasePtr res = ObjectInstanceBasePtr::dynamicCast(getEntityById(id));

        if (!res)
        {
            ARMARX_WARNING_S << "Entity with id " << id << " is not of type ObjectInstance!" << std::endl;
        }

        return res;
    }

    ObjectInstanceBasePtr WorldStateSegment::getWorldInstanceByName(const ::std::string& name, const ::Ice::Current&) const
    {
        ObjectInstanceBasePtr res = ObjectInstanceBasePtr::dynamicCast(getEntityByName(name));

        if (!res)
        {
            ARMARX_WARNING_S << "Entity with name " << name << " is not of type ObjectInstance!" << std::endl;
        }

        return res;
    }

    ObjectInstanceList WorldStateSegment::getWorldInstancesByClass(const ::std::string& className, const ::Ice::Current& c) const
    {
        NameList classList;
        classList.push_back(className);
        return getWorldInstancesByClassList(classList, c);
    }

    ObjectInstanceList WorldStateSegment::getWorldInstancesByClassList(const NameList& classList, const ::Ice::Current& c) const
    {
        ObjectInstanceList result;
        ScopedSharedLockPtr lock(getReadLock(c));

        for (IdEntityMap::const_iterator it = entityMap.begin(); it != entityMap.end(); ++it)
        {
            ObjectInstanceBasePtr inst = ObjectInstanceBasePtr::dynamicCast(it->second);
            ClassProbabilityMap instClasses = inst->getClasses();

            for (NameList::const_iterator itCls = classList.begin(); itCls != classList.end(); ++itCls)
            {
                if (instClasses.count(*itCls))
                {
                    result.push_back(inst);
                    break;
                }
            }
        }

        return result;
    }
}
