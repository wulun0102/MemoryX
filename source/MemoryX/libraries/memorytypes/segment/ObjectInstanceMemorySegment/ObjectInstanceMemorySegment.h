/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::WorkingMemory
* @author     Kai Welke ( welke at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <MemoryX/libraries/workingmemory/WorkingMemoryEntitySegment.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectClass.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectInstance.h>
#include <MemoryX/libraries/motionmodels/MotionModelObjectFactories.h>

#include <MemoryX/interface/core/EntityBase.h>
#include <MemoryX/interface/memorytypes/MemoryEntities.h>
#include <MemoryX/interface/memorytypes/MemorySegments.h>


namespace memoryx
{
    /*!
      The ObjectInstanceMemorySegment is a specialization of the WorkingMemoryEntitySegment template.
        \see WorkingMemoryEntitySegment
      */
    class ARMARXCOMPONENT_IMPORT_EXPORT ObjectInstanceMemorySegment :
        virtual public WorkingMemoryEntitySegment<ObjectInstance>,
        virtual public ObjectInstanceMemorySegmentBase
    {
    public:
        ObjectInstanceMemorySegment(float matchThreshold, bool matchByClass);


        ObjectInstanceBasePtr getObjectInstanceById(const ::std::string& id, const ::Ice::Current& = Ice::emptyCurrent) const override;


        ObjectInstanceBasePtr getObjectInstanceByName(const ::std::string& name, const ::Ice::Current& = Ice::emptyCurrent) const override;


        ObjectInstanceList getObjectInstancesByClass(const ::std::string& className, const ::Ice::Current& c = Ice::emptyCurrent) const override;


        ObjectInstanceList getObjectInstancesByClassList(const NameList& classList, const ::Ice::Current& = Ice::emptyCurrent) const override;


        ObjectInstanceBasePtr getCorrespondingObjectInstance(const ObjectInstanceBasePtr& objectInstance, const ::Ice::Current& = Ice::emptyCurrent) const override;


        void setNewMotionModel(const ::std::string& entityId, const ::memoryx::MotionModelInterfacePtr& newMotionModel, const ::Ice::Current& = Ice::emptyCurrent) override;


        void setObjectPose(const std::string& entityId, const armarx::LinkedPoseBasePtr& objectPose, const ::Ice::Current& = Ice::emptyCurrent) override;
        void setObjectPoseWithoutMotionModel(const std::string& entityId, const armarx::FramedPoseBasePtr& objectPose, const ::Ice::Current& = Ice::emptyCurrent) override;


        std::string addObjectInstance(const std::string& instanceName, const std::string& className, const armarx::LinkedPoseBasePtr& objectPose,
                                      const ::memoryx::MotionModelInterfacePtr& motionModel, const ::Ice::Current& = Ice::emptyCurrent) override;


        void updateEntity(const std::string& entityId, const EntityBasePtr& update, const ::Ice::Current& = Ice::emptyCurrent) override;


        void updateEntityInternal(const std::string& entityId, const EntityBasePtr& update, const ::Ice::Current& c = Ice::emptyCurrent)
        {
            WorkingMemoryEntitySegment::updateEntity(entityId, update, c);
        }


        void updateEntityInternal(const std::string& entityId, const EntityBasePtr& update, const Ice::StringSeq& deactivatedFusionMethods)
        {
            WorkingMemoryEntitySegment::updateEntity(entityId, update, deactivatedFusionMethods);
        }


    private:
        double matchThreshold;
        bool matchByClass;
    };

    using ObjectInstanceMemorySegmentPtr = IceInternal::Handle<ObjectInstanceMemorySegment>;

}

