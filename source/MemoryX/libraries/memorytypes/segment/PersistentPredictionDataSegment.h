/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX
 * @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#pragma once

#include <MemoryX/core/memory/PersistentEntitySegment.h>
#include <MemoryX/interface/memorytypes/MemorySegments.h>

#include <ArmarXCore/core/system/ImportExportComponent.h>

namespace memoryx
{
    class ARMARXCOMPONENT_IMPORT_EXPORT PersistentPredictionDataSegment :
        virtual public PersistentEntitySegment,
        virtual public PersistentPredictionDataSegmentBase
    {
    public:
        PersistentPredictionDataSegment(CollectionInterfacePrx entityCollection, Ice::CommunicatorPtr ic, bool useMongoIds = true);

        // PersistentPredictionDataSegmentBase interface
        PredictionEntityBaseList getPredictionEntities(const Ice::Current&) const override;

        PredictionTaskEntityBaseList getPredictionTaskEntities(const Ice::Current&) const override;
    };

    using PersistentPredictionDataSegmentPtr = IceInternal::Handle<PersistentPredictionDataSegment>;
}

