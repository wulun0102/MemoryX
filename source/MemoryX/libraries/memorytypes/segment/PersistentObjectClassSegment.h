/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::WorkingMemory
* @author     Kai Welke ( welke at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <MemoryX/core/memory/PersistentEntitySegment.h>

#include <MemoryX/interface/core/EntityBase.h>
#include <MemoryX/interface/memorytypes/MemoryEntities.h>
#include <MemoryX/interface/memorytypes/MemorySegments.h>
#include "MemoryX/libraries/memorytypes/entity/ObjectClass.h"

#include <vector>

namespace memoryx
{
    /**
     * The persistent object class segment is a specialized segment of the SegmentedMemory.
     * It keeps object classes which are held in prior knowledge (MongoDB) and thus are persistent.
     * This segment is usually part of the prior knowledge memory.
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT PersistentObjectClassSegment :
        virtual public PersistentEntitySegment,
        virtual public PersistentObjectClassSegmentBase
    {
    public:
        /**
         * Creates a new peristent object class segment.
         *
         * @param entityCollection a proxy to the collection from commonstorage
         * @param ic ice communicator
         * @param useMongoIds whether to use entity IDs stored in MongoDB or internal ids
         */
        PersistentObjectClassSegment(CollectionInterfacePrx entityCollection, Ice::CommunicatorPtr ic, bool useMongoIds = true) :
            PersistentEntitySegment(entityCollection, ic, useMongoIds),
            PersistentObjectClassSegmentBase()
        {
        }

        /**
         * Retrieve a persistent object class by id
         *
         * @param id either mongoId (if useMongIds) or internal id
         * @return pointer to object class on success
         */
        ObjectClassBasePtr getObjectClassById(const ::std::string& id, const ::Ice::Current& = Ice::emptyCurrent) const override
        {
            return ObjectClassBasePtr::dynamicCast(getEntityById(id));
        }

        /**
         * Retrieve a persistent object class by name
         *
         * @param name name of object class
         * @return pointer to object class on success
         */
        ObjectClassBasePtr getObjectClassByName(const ::std::string& name, const ::Ice::Current& = Ice::emptyCurrent) const override
        {
            return ObjectClassBasePtr::dynamicCast(getEntityByName(name));
        }

        /**
         * @brief Retrieve a object class with all the parent classes,
         * that means also the parent classes of the direct parent classes and so on.
         * @param name Name of the object class to retrieve
         * @return pointer to the object class
         */
        ObjectClassBasePtr getObjectClassByNameWithAllParents(const ::std::string& name, const ::Ice::Current& = Ice::emptyCurrent) const override
        {
            ObjectClassBasePtr objClass = ObjectClassBasePtr::dynamicCast(getEntityByName(name));

            if (!objClass)
            {
                ARMARX_ERROR_S << "No object class with name '" << name << "'";
                return objClass;
            }

            objClass->clearParentClasses();
            ObjectClassList parents = getAllParentClasses(objClass->getName());

            if (objClass)
            {
                for (size_t i = 0; i < parents.size(); i++)
                {
                    objClass->addParentClass(parents.at(i)->getName());
                }
            }

            return objClass;
        }

        /**
         * Retrieve child classes of a class
         *
         * @param parentClassName name of the parent
         * @return list of child object classes
         */
        ObjectClassList getChildClasses(const std::string& parentClassName, const ::Ice::Current& = Ice::emptyCurrent) const override
        {
            ObjectClassList result;
            EntityBaseList children = getEntitiesByAttrValue("parentClasses", parentClassName);

            for (EntityBaseList::const_iterator it = children.begin(); it != children.end(); ++it)
            {
                result.push_back(ObjectClassBasePtr::dynamicCast(*it));
            }

            return result;
        }

        /**
         * Retrieve direct parent classes of a class
         *
         * @param parentClassName name of the parent
         * @return list of child object classes
         */
        ObjectClassList getParentClasses(const std::string& className, const ::Ice::Current& = Ice::emptyCurrent) const override
        {

            EntityBasePtr entity = getEntityByName(className);

            if (!entity)
            {
                throw armarx::LocalException("Could not find class with name ") << className;
            }

            ObjectClassBasePtr objClass = ObjectClassBasePtr::dynamicCast(entity);

            if (!objClass)
            {
                throw armarx::LocalException("Could not cast class with name ") << className;
            }

            NameList parentClassNameList = objClass->getParentClasses();
            ObjectClassList parentClassList;

            for (NameList::const_iterator it = parentClassNameList.begin(); it != parentClassNameList.end(); ++it)
            {
                if (it->empty())
                {
                    continue;
                }

                ObjectClassBasePtr ptr = ObjectClassBasePtr::dynamicCast(getEntityByName(*it));

                if (!ptr)
                {
                    ARMARX_INFO_S << "Could not cast parent " << *it << " of class " << className;
                }
                else
                {
                    parentClassList.push_back(ptr);
                }
            }

            return parentClassList;
        }

        /**
         * Retrieve class and all subclasses. Retrieves a complete ontological subtree from the class prior knowledge.
         *
         * @param rootClassName name of the root class
         * @return list of object classes in the subtree
         */
        ObjectClassList getClassWithSubclasses(const std::string& rootClassName, const ::Ice::Current& = Ice::emptyCurrent) const override
        {
            // iterate through classes
            ObjectClassList relevantClasses;
            ObjectClassPtr root = ObjectClassPtr::dynamicCast(getEntityByName(rootClassName));

            if (!root)
            {
                ARMARX_WARNING_S << "Parent Object Class '" << rootClassName << "' does not exist in segment!";
                return ObjectClassList();
            }

            relevantClasses.push_back(root);

            int index = 0;

            while (index != int(relevantClasses.size()))
            {
                ObjectClassBasePtr current = relevantClasses.at(index);

                // add children of this class
                ObjectClassList childs = getChildClasses(current->getName());

                if (childs.size() != 0)
                {
                    std::copy(childs.begin(), childs.end(), std::back_inserter(relevantClasses));
                }

                index++;
            }

            unique(relevantClasses);

            return relevantClasses;
        }

        /**
         * @brief Retrieves recursively all parent classes for an object class from the database.
         * @param className Name of object class for which the parents should be calculated.
         * @return List of parent objects
         */
        ObjectClassList getAllParentClasses(const std::string& className, const ::Ice::Current& = Ice::emptyCurrent) const override
        {
            ObjectClassList relevantClasses, classesLeftToCheck;

            ObjectClassBasePtr current = ObjectClassBasePtr::dynamicCast(getEntityByName(className));

            if (!current)
            {
                return relevantClasses;
            }

            do
            {
                // add children of this class
                if (!current)
                {
                    throw armarx::LocalException("current is NULL!");
                }

                //                ARMARX_INFO_S << index << " Getting parents of " << current->getName();
                ObjectClassList parents = getParentClasses(current->getName());

                if (parents.size() != 0)
                {
                    std::copy(parents.begin(), parents.end(), std::back_inserter(relevantClasses));
                    std::copy(parents.begin(), parents.end(), std::back_inserter(classesLeftToCheck));
                }
                unique(classesLeftToCheck);
                if (classesLeftToCheck.size() == 0)
                {
                    break;
                }
                current = *classesLeftToCheck.rbegin();
                classesLeftToCheck.pop_back();
            }
            while (true);

            unique(relevantClasses);
            return relevantClasses;
        }

        /**
         * @brief Checks whether an object class is equal to another or if they contain an equal parent class
         * @param objClass
         * @param other
         * @return
         */
        ObjectComparisonResult compare(const ObjectClassBasePtr& objClass, const ObjectClassBasePtr& other, const Ice::Current& = Ice::emptyCurrent) const override
        {
            if (other->getName() == objClass->getName())
            {
                return eEqualClass;
            }

            ObjectClassList otherParentClasses = getAllParentClasses(other->getName());
            ObjectClassList parentClasses = getAllParentClasses(objClass->getName());

            // check parents names
            for (ObjectClassList::iterator itOther = otherParentClasses.begin(); itOther != otherParentClasses.end(); itOther++)
            {
                if (objClass->getName() == (*itOther)->getName()) // other is a subclass of this -> so its equal
                {
                    return eEqualClass;
                }

                for (ObjectClassList::iterator it = parentClasses.begin(); it != parentClasses.end(); it++)
                {
                    if ((*it)->getName() == (*itOther)->getName()) // parent of this is equal to parent of other, so only equal parent class
                    {
                        return eEqualParentClass;
                    }
                }
            }

            for (ObjectClassList::iterator it = parentClasses.begin(); it != parentClasses.end(); it++)
            {
                if ((*it)->getName() == other->getName()) // other is parent class of objClass -> so only equal parent class
                {
                    return eEqualParentClass;
                }
            }

            return eNotEqualClass;
        }

        bool isParentClass(const ::std::string& className, const ::std::string& parentCandidate, const ::Ice::Current& = Ice::emptyCurrent) const override
        {
            ObjectClassList parentClasses = getAllParentClasses(className);

            for (const auto& parent : parentClasses)
            {
                if (parent->getName() == parentCandidate)
                {
                    return true;
                }
            }

            return false;
        }

        static void unique(ObjectClassList& classes)
        {
            auto objectClassPtrCompareLess = [](const ObjectClassBasePtr & lhs, const ObjectClassBasePtr & rhs)
            {
                return lhs->getName() < rhs->getName();
            };
            auto objectClassPtrCompareEq = [](const ObjectClassBasePtr & lhs, const ObjectClassBasePtr & rhs)
            {
                return lhs->getName() == rhs->getName();
            };
            std::sort(classes.begin(), classes.end(), objectClassPtrCompareLess);
            auto newEnd = std::unique(classes.begin(), classes.end(), objectClassPtrCompareEq);
            classes.erase(newEnd, classes.end());
        }

    };

    using PersistentObjectClassSegmentPtr = IceInternal::Handle<PersistentObjectClassSegment>;

}

