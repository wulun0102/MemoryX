/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include <MemoryX/libraries/memorytypes/segment/SemanticEventChainSegment.h>

#include <ArmarXCore/core/exceptions/Exception.h>
#include <ArmarXCore/core/logging/Logging.h>

namespace memoryx
{

    SemanticEventChainSegment::SemanticEventChainSegment(PersistentObjectClassSegmentBasePrx objClassSegment, CollectionInterfacePrx entityCollection, Ice::CommunicatorPtr ic, bool useMongoIds) :
        PersistentEntitySegment(entityCollection, ic, useMongoIds),
        objClassSegment(objClassSegment)
    {
    }

    void SemanticEventChainSegment::ReplaceObjectDummies(SECObjectRelationsBasePtr objRelations, PersistentObjectClassSegmentBasePrx objClassSegment)
    {
        if (!objClassSegment)
        {
            throw armarx::LocalException("PersistentObjectClassSegmentBaseProxy is NULL");
        }

        for (unsigned int i = 0; i < objRelations->relations.size(); i++)
        {
            memoryx::SECRelationBasePtr relation = objRelations->relations.at(i);

            for (unsigned int j = 0; j < relation->objects1.size(); j++)
            {
                ObjectClassBasePtr obj = relation->objects1.at(j);

                if (obj->getId().empty())
                {
                    relation->objects1.at(j) = objClassSegment->getObjectClassByName(obj->getName());
                }

                if (!relation->objects1.at(j))
                {
                    ARMARX_ERROR_S << "Could not find object with name " << obj->getName();
                }
            }

            for (unsigned int j = 0; j < relation->objects2.size(); j++)
            {
                ObjectClassBasePtr obj = relation->objects2.at(j);

                if (obj->getId().empty())
                {
                    relation->objects2.at(j) = objClassSegment->getObjectClassByName(obj->getName());
                }

                if (!relation->objects2.at(j))
                {
                    ARMARX_ERROR_S << "Could not find object with name " << obj->getName();
                }
            }
        }
    }

}
