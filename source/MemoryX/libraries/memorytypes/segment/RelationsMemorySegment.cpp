#include "RelationsMemorySegment.h"

namespace memoryx
{

    RelationBasePtr RelationMemorySegment::getRelationById(const std::string& id, const Ice::Current&) const
    {
        return RelationBasePtr::dynamicCast(getEntityById(id));
    }

    RelationList RelationMemorySegment::getRelationsByName(const std::string& name, const Ice::Current&) const
    {
        RelationList result;
        // Relations do not have an attribute 'name'. The name is stored as a std::string member
        // Therefore, the method getEntitiesByName cannot be used...
        EntityBaseList allEntities = getAllEntities();
        for (EntityBasePtr const& entity : allEntities)
        {
            if (entity->getName() == name)
            {
                result.push_back(RelationBasePtr::dynamicCast(entity));
            }
        }

        return result;
    }

    RelationList RelationMemorySegment::getRelationsBySign(bool sign, const Ice::Current&) const
    {
        RelationList result;

        for (const auto& id : getAllEntityIds())
        {
            RelationBasePtr rel = RelationBasePtr::dynamicCast(getEntityById(id));

            if (rel->getSign() == sign)
            {
                result.push_back(rel);
            }
        }

        return result;
    }

    RelationList RelationMemorySegment::getRelationsByEntityId(const std::string& entityId, const Ice::Current&) const
    {
        RelationList result;

        for (const auto& id : getAllEntityIds())
        {
            RelationBasePtr rel = RelationBasePtr::dynamicCast(getEntityById(id));
            EntityRefList relEntities = rel->getEntities();

            for (EntityRefBasePtr& relEntity : relEntities)
            {
                if (relEntity->entityId == entityId)
                {
                    result.push_back(rel);
                    break;
                }
            }
        }

        return result;
    }

    RelationList RelationMemorySegment::getRelationsByEntityRef(const EntityRefBasePtr& entityRef, const Ice::Current&) const
    {
        return getRelationsByEntityId(entityRef->entityId);
    }

    RelationList RelationMemorySegment::getRelationsByEntityRefs(const EntityRefList& entities, const Ice::Current&) const
    {
        return getRelationsByAttrValues(entities);
    }

    RelationBasePtr RelationMemorySegment::getRelationByAttrValues(const std::string& name, const EntityRefList& entities, bool sign, const Ice::Current&) const
    {
        RelationBasePtr result;
        RelationList resultList = getRelationsByAttrValues(entities, name, true, sign);

        if (!resultList.empty())
        {
            result = resultList[0];
        }

        return result;
    }

    RelationList RelationMemorySegment::getRelationsByAttrValues(const EntityRefList& entities, const std::string& name, bool considerSign, bool sign, const Ice::Current&) const
    {
        RelationList result;

        ARMARX_INFO_S << "Checking for relation: " << name << " sign: " << sign;
        for (const auto& entity : getAllEntities())
        {
            RelationBasePtr rel = RelationBasePtr::dynamicCast(entity);

            if (!name.empty() && (rel->getName() != name))
            {
                continue;
            }

            if (considerSign && (sign != rel->getSign()))
            {
                continue;
            }

            EntityRefList relEntities = rel->getEntities();

            bool foundArgs = true;

            for (const auto& entityRef : entities)
            {
                foundArgs &= std::find_if(relEntities.cbegin(), relEntities.cend(), [&](const memoryx::EntityRefBasePtr & e)
                {
                    return entityRef->equals(e);
                }) != relEntities.cend();
            }

            if (foundArgs)
            {
                result.push_back(rel);
            }
        }

        return result;
    }

    std::string RelationMemorySegment::addEntity(const EntityBasePtr& entity, const Ice::Current&)
    {
        RelationPtr rel = RelationPtr::dynamicCast(entity);
        ARMARX_INFO_S << "Adding relation " << rel->getName() << " with sign " << rel->getSign();
        return WorkingMemoryEntitySegment::addEntity(entity);
    }

    void RelationMemorySegment::updateEntity(const std::string& id, const EntityBasePtr& entity, const Ice::Current&)
    {
        RelationPtr rel = RelationPtr::dynamicCast(entity);
        ARMARX_INFO_S << "updating relation " << rel->getName() << " with sign " << rel->getSign();
        WorkingMemoryEntitySegment::updateEntity(id, entity);
    }

    void RelationMemorySegment::removeRelations(const std::string& name, const Ice::Current&)
    {
        RelationList relations = getRelationsByName(name);
        for (auto& relation : relations)
        {
            removeEntity(relation->getId());
        }
    }

    void RelationMemorySegment::replaceRelations(const RelationList& newRelations, const Ice::Current& c)
    {
        std::set<std::string> names;
        for (auto& relation : newRelations)
        {
            names.insert(relation->getName());
        }
        for (std::string const& name : names)
        {
            removeRelations(name, c);
        }
        for (auto& relation : newRelations)
        {
            addEntity(relation, c);
        }
    }

}
