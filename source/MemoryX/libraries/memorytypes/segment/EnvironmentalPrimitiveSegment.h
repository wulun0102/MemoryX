/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::WorkingMemory
* @author     Peter Kaiser (peter dot kaiser at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <MemoryX/libraries/workingmemory/WorkingMemoryEntitySegment.h>
#include <MemoryX/libraries/memorytypes/entity/EnvironmentalPrimitive.h>

#include <MemoryX/interface/core/EntityBase.h>
#include <MemoryX/interface/memorytypes/MemoryEntities.h>
#include <MemoryX/interface/memorytypes/MemorySegments.h>

namespace memoryx
{
    class ARMARXCOMPONENT_IMPORT_EXPORT EnvironmentalPrimitiveSegment :
        virtual public WorkingMemoryEntitySegment<EnvironmentalPrimitive>,
        virtual public EnvironmentalPrimitiveSegmentBase
    {
    public:
        EnvironmentalPrimitiveSegment();

        EnvironmentalPrimitiveBaseList getEnvironmentalPrimitives(const ::Ice::Current& c = Ice::emptyCurrent) const override;
        PlanePrimitiveBaseList getPlanes(const ::Ice::Current& c = Ice::emptyCurrent) const override;
        SpherePrimitiveBaseList getSpheres(const ::Ice::Current& c = Ice::emptyCurrent) const override;
        CylinderPrimitiveBaseList getCylinders(const ::Ice::Current& c = Ice::emptyCurrent) const override;
        BoxPrimitiveBaseList getBoxes(const ::Ice::Current& c = Ice::emptyCurrent) const override;

        EnvironmentalPrimitiveBasePtr getEnvironmentalPrimitiveById(const std::string& id, const ::Ice::Current& c = Ice::emptyCurrent) const override;
        PlanePrimitiveBasePtr getPlaneById(const std::string& id, const ::Ice::Current& c = Ice::emptyCurrent) const override;
        SpherePrimitiveBasePtr getSphereById(const std::string& id, const ::Ice::Current& c = Ice::emptyCurrent) const override;
        CylinderPrimitiveBasePtr getCylinderById(const std::string& id, const ::Ice::Current& c = Ice::emptyCurrent) const override;
        BoxPrimitiveBasePtr getBoxById(const std::string& id, const ::Ice::Current& c = Ice::emptyCurrent) const override;

        EnvironmentalPrimitiveBaseList getEnvironmentalPrimitivesByTimestamp(const armarx::TimestampBasePtr& timestamp, const ::Ice::Current& c = Ice::emptyCurrent) const override;
        PlanePrimitiveBaseList getPlanesByTimestamp(const armarx::TimestampBasePtr& timestamp, const ::Ice::Current& c = Ice::emptyCurrent) const override;
        SpherePrimitiveBaseList getSpheresByTimestamp(const armarx::TimestampBasePtr& timestamp, const ::Ice::Current& c = Ice::emptyCurrent) const override;
        CylinderPrimitiveBaseList getCylindersByTimestamp(const armarx::TimestampBasePtr& timestamp, const ::Ice::Current& c = Ice::emptyCurrent) const override;
        BoxPrimitiveBaseList getBoxesByTimestamp(const armarx::TimestampBasePtr& timestamp, const ::Ice::Current& c = Ice::emptyCurrent) const override;

        EnvironmentalPrimitiveBaseList getMostRecentEnvironmentalPrimitives(const ::Ice::Current& c = Ice::emptyCurrent) const override;
        PlanePrimitiveBaseList getMostRecentPlanes(const ::Ice::Current& c = Ice::emptyCurrent) const override;
        SpherePrimitiveBaseList getMostRecentSpheres(const ::Ice::Current& c = Ice::emptyCurrent) const override;
        CylinderPrimitiveBaseList getMostRecentCylinders(const ::Ice::Current& c = Ice::emptyCurrent) const override;
        BoxPrimitiveBaseList getMostRecentBoxes(const ::Ice::Current& c = Ice::emptyCurrent) const override;

        void removePrimitivesByTimestamp(const armarx::TimestampBasePtr& timestamp, const ::Ice::Current& c = Ice::emptyCurrent) override;
        void removeOlderPrimitives(const armarx::TimestampBasePtr& olderThan, const ::Ice::Current& c = Ice::emptyCurrent) override;

    protected:
        template<typename EntityPtrType> Ice::Long getMostRecentTimestamp(const std::vector<EntityPtrType>& primitives, const ::Ice::Current& c = Ice::emptyCurrent) const
        {
            Ice::Long mostRecentTimestamp = 0;

            for (auto& p : primitives)
            {
                Ice::Long t = p->getTime(c)->timestamp;
                if (t > mostRecentTimestamp)
                {
                    mostRecentTimestamp = t;
                }
            }

            return mostRecentTimestamp;
        }
    };

    using EnvironmentalPrimitiveSegmentPtr = IceInternal::Handle<EnvironmentalPrimitiveSegment>;
}

