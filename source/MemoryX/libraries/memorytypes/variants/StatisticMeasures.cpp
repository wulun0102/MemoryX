/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::Core::MemoryTypes
 * @author     Adil Orhan (ubdnw at student dot kit dot edu)
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include "StatisticMeasures.h"

#include <ArmarXCore/observers/AbstractObjectSerializer.h>

namespace memoryx
{

    StatisticMeasures::StatisticMeasures()
    {
        mean = 0;
        standardDeviation = 0;
        relativeStandardDeviation = 0;
    }

    StatisticMeasures::StatisticMeasures(double mean, double standardDeviation, double relativeStandardDeviation)
    {
        this->mean = mean;
        this->standardDeviation = standardDeviation;
        this->relativeStandardDeviation = relativeStandardDeviation;
    }

    StatisticMeasures::~StatisticMeasures()
    {
    }

    double StatisticMeasures::getMean()
    {
        return mean;
    }

    double StatisticMeasures::getStandardDeviation()
    {
        return standardDeviation;
    }

    double StatisticMeasures::getRelativeStandardDeviation()
    {
        return relativeStandardDeviation;
    }

    Ice::ObjectPtr StatisticMeasures::ice_clone() const
    {
        return this->clone();
    }

    armarx::VariantDataClassPtr StatisticMeasures::clone(const Ice::Current& c) const
    {
        return new StatisticMeasures(*this);
    }

    std::string StatisticMeasures::output(const Ice::Current&) const
    {
        std::stringstream ss;
        ss << "mean " << mean << " standardDeviation " << standardDeviation << " relativeStandardDeviation " << relativeStandardDeviation;
        return ss.str();
    }

    armarx::VariantTypeId StatisticMeasures::getType(const Ice::Current& c) const
    {
        return memoryx::VariantType::StatisticMeasures;
    }

    bool StatisticMeasures::validate(const Ice::Current& c)
    {
        return true;
    }

    void StatisticMeasures::serialize(const armarx::ObjectSerializerBasePtr& serializer, const Ice::Current&) const
    {
        armarx::AbstractObjectSerializerPtr obj = armarx::AbstractObjectSerializerPtr::dynamicCast(serializer);

        obj->setDouble("mean", mean);
        obj->setDouble("standardDeviation", standardDeviation);
        obj->setDouble("relativeStandardDeviation", relativeStandardDeviation);
    }

    void StatisticMeasures::deserialize(const armarx::ObjectSerializerBasePtr& serializer, const Ice::Current&)
    {
        armarx::AbstractObjectSerializerPtr obj = armarx::AbstractObjectSerializerPtr::dynamicCast(serializer);

        mean = obj->getDouble("mean");
        standardDeviation = obj->getDouble("standardDeviation");
        relativeStandardDeviation = obj->getDouble("relativeStandardDeviation");
    }

}
