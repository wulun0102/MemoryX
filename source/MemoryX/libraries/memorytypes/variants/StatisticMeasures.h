/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::Core::MemoryTypes
 * @author     Adil Orhan (ubdnw at student dot kit dot edu)
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/observers/variant/Variant.h>
#include <MemoryX/interface/memorytypes/StatisticMeasures.h>

namespace memoryx::VariantType
{
    const armarx::VariantTypeId StatisticMeasures = armarx::Variant::addTypeName("::memoryx::StatisticMeasuresBase");
}

namespace memoryx
{
    class StatisticMeasures;
    using StatisticMeasuresPtr = IceInternal::Handle<StatisticMeasures>;

    class StatisticMeasures :
        virtual public StatisticMeasuresBase
    {
    public:
        StatisticMeasures();
        StatisticMeasures(double mean, double standardDeviation, double relativeStandardDeviation);
        ~StatisticMeasures() override;

        double getMean();
        double getStandardDeviation();
        double getRelativeStandardDeviation();

        Ice::ObjectPtr ice_clone() const override;

        armarx::VariantDataClassPtr clone(const Ice::Current& c = Ice::emptyCurrent) const override;

        std::string output(const Ice::Current& = Ice::emptyCurrent) const override;

        armarx::VariantTypeId getType(const Ice::Current& c = Ice::emptyCurrent) const override;

        bool validate(const Ice::Current& c = Ice::emptyCurrent) override;

    public:
        void serialize(const armarx::ObjectSerializerBasePtr& serializer, const ::Ice::Current& = Ice::emptyCurrent) const override;
        void deserialize(const armarx::ObjectSerializerBasePtr& serializer, const ::Ice::Current& = Ice::emptyCurrent) override;
    };
}

