/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "MemoryXTypesObjectFactories.h"
#include "entity/AgentInstance.h"
#include "entity/Relation.h"
#include "entity/ActiveOac.h"
#include "entity/Oac.h"
#include "entity/OacParameterList.h"
#include "entity/OacPredictionFunction.h"
#include "entity/OacStatisticalMeasure.h"
#include "entity/SEC/SECObjectRelations.h"
#include "entity/SEC/SECRelation.h"
#include "entity/SEC/SECKeyFrame.h"
#include "entity/Affordance.h"
#include "entity/EnvironmentalPrimitive.h"
#include "entity/KBMData.h"
#include "entity/profiler/ProfilerTransition.h"
#include "entity/profiler/ProfilerEvent.h"
#include "entity/profiler/ProfilerMemorySnapshot.h"
#include "entity/profiler/ResourceProfileEntity.h"
#include "entity/profiler/ProfilerProcess.h"
#include "entity/profiler/ProfilerMemoryUsage.h"
#include "entity/prediction/PredictionEntity.h"
#include "entity/prediction/PredictionTaskEntity.h"
#include "entity/DMPEntity.h"

#include "variants/StatisticMeasures.h"
#include "variants/GraphNode/GraphNode.h"


armarx::ObjectFactoryMap memoryx::ObjectFactories::MemoryXTypesObjectFactories::getFactories()
{
    armarx::ObjectFactoryMap map;

    add<AgentInstanceBase, AgentInstance>(map);
    add<memoryx::ObjectInstanceBase, memoryx::ObjectInstance>(map);
    add<ObjectClassBase, ObjectClass>(map);
    add<RelationBase, Relation>(map);
    add<ActiveOacBase, ActiveOac>(map);
    add<OacBase, Oac>(map);
    add<OacParameterListBase, OacParameterList>(map);
    add<OacPredictionFunctionBase, OacPredictionFunction>(map);
    add<OacStatisticalMeasureBase, OacStatisticalMeasure>(map);
    add<memoryx::SECRelationBase, memoryx::SECRelation>(map);
    add<SECObjectRelationsBase, SECObjectRelations>(map);
    add<SECKeyFrameBase, SECKeyFrame>(map);
    add<memoryx::GraphNodeBase, memoryx::GraphNode>(map);
    add<StatisticMeasuresBase, StatisticMeasures>(map);

    add<AffordanceBase, Affordance>(map);
    add<KBMDataBase, KBMData>(map);
    add<EnvironmentalPrimitiveBase, EnvironmentalPrimitive>(map);
    add<PlanePrimitiveBase, PlanePrimitive>(map);
    add<ConePrimitiveBase, ConePrimitive>(map);
    add<CylinderPrimitiveBase, CylinderPrimitive>(map);
    add<SpherePrimitiveBase, SpherePrimitive>(map);
    add<BoxPrimitiveBase, BoxPrimitive>(map);
    add<ProfilerTransitionBase, ProfilerTransition>(map);
    add<ProfilerEventBase, ProfilerEvent>(map);
    add<ProfilerMemorySnapshotBase, ProfilerMemorySnapshot>(map);
    add<ResourceProfileEntityBase, ResourceProfileEntity>(map);
    add<ProfilerProcessBase, ProfilerProcess>(map);
    add<ProfilerMemoryUsageBase, ProfilerMemoryUsage>(map);
    add<PredictionEntityBase, PredictionEntity>(map);
    add<PredictionTaskEntityBase, PredictionTaskEntity>(map);
    add<DMPEntityBase, DMPEntity>(map);

    return map;
}

#include "MemoryXTypesObjectFactories.h"
