/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::Core::MemoryTypes
 * @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <MemoryX/interface/memorytypes/PredictionEntities.h>
#include <MemoryX/core/entity/Entity.h>

namespace memoryx
{
    class PredictionEntity;
    using PredictionEntityPtr = IceInternal::Handle<PredictionEntity>;

    class PredictionEntity :
        virtual public memoryx::PredictionEntityBase,
        virtual public memoryx::Entity
    {
    public:
        PredictionEntity();
        PredictionEntity(const EntityRefBasePtr& memorySnapshotRef, const EntityRefBasePtr& sourceMemorySnapshotRef, const EntityRefBasePtr& predictedMemorySnapshotRef, const EntityRefBaseList& predictedMemorySnapshotRefList, const std::string& predictionMethodName, Ice::Int taskCount, Ice::Float predictionScore);
        PredictionEntity(const PredictionEntity& source);
        ~PredictionEntity() override;

        void initializeAttributes();


        // cloning
        Ice::ObjectPtr ice_clone() const override;
        PredictionEntityPtr clone(const Ice::Current& c = Ice::emptyCurrent) const;

        // PredictionEntityBase interface
        /**
         * @brief getProfilerMemorySnapshotRef situation (memory snapshot) that actually occured (may differ from pediction)
         */
        EntityRefBasePtr getProfilerMemorySnapshotRef(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setProfilerMemorySnapshotRef(const EntityRefBasePtr& memorySnapshotRef, const Ice::Current& c = Ice::emptyCurrent) override;

        /**
         * @brief getSourceProfilerMemorySnapshotRef situation (memory snapshot) before the prediction
         */
        EntityRefBasePtr getSourceProfilerMemorySnapshotRef(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setSourceProfilerMemorySnapshotRef(const EntityRefBasePtr& sourceMemorySnapshotRef, const Ice::Current& c = Ice::emptyCurrent) override;

        /**
         * @brief getPredictedProfilerMemorySnapshotRef situation (memory snapshot) which was predicted on the basis of getSourceProfilerMemorySnapshotRef()
         */
        EntityRefBasePtr getPredictedProfilerMemorySnapshotRef(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setPredictedProfilerMemorySnapshotRef(const EntityRefBasePtr& predictedMemorySnapshotRef, const Ice::Current& c = Ice::emptyCurrent) override;

        EntityRefBaseList getPredictedProfilerMemorySnapshotRefList(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setPredictedProfilerMemorySnapshotRefList(const EntityRefBaseList& predictedMemorySnapshotRefList, const Ice::Current& c = Ice::emptyCurrent) override;

        std::string getPredictionMethodName(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setPredictionMethodName(const std::string& predictionMethodName, const Ice::Current& c = Ice::emptyCurrent) override;

        Ice::Int getTaskCount(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setTaskCount(Ice::Int taskCount, const Ice::Current& c = Ice::emptyCurrent) override;

        Ice::Float getPredictionScore(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setPredictionScore(Ice::Float predictionScore, const Ice::Current& c = Ice::emptyCurrent) override;

        // streaming operator
        friend std::ostream& operator<<(std::ostream& stream, const PredictionEntity& rhs)
        {
            rhs.output(stream);
            return stream;
        }

        friend std::ostream& operator<<(std::ostream& stream, const PredictionEntityPtr& rhs)
        {
            rhs->output(stream);
            return stream;
        }

        friend std::ostream& operator<<(std::ostream& stream, const PredictionEntityBasePtr& rhs)
        {
            stream << PredictionEntityPtr::dynamicCast(rhs);
            return stream;
        }
    private:
        void output(std::ostream& stream) const;

        static const std::string SNAPSHOT_ATTRIBUTE;
        static const std::string SOURCE_SNAPSHOT_ATTRIBUTE;
        static const std::string PREDICTION_ATTRIBUTE;
        static const std::string PREDICTION_LIST_ATTRIBUTE;
        static const std::string PREDICTION_METHOD_NAME_ATTRIBUTE;
        static const std::string TASK_COUNT_ATTRIBUTE;
        static const std::string PREDICTION_SCORE_ATTRIBUTE;
    };

}

