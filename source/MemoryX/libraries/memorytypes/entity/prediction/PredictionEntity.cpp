/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::Core
 * @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "PredictionEntity.h"
#include "../../../../core/entity/EntityRef.h"


namespace memoryx
{
    const std::string PredictionEntity::SNAPSHOT_ATTRIBUTE("memorySnapshot");
    const std::string PredictionEntity::SOURCE_SNAPSHOT_ATTRIBUTE("sourceMemorySnapshot");
    const std::string PredictionEntity::PREDICTION_ATTRIBUTE("predictedMemorySnapshot");
    const std::string PredictionEntity::PREDICTION_LIST_ATTRIBUTE("predictedMemorySnapshotList");
    const std::string PredictionEntity::PREDICTION_METHOD_NAME_ATTRIBUTE("predictionMethodName");
    const std::string PredictionEntity::TASK_COUNT_ATTRIBUTE("taskCount");
    const std::string PredictionEntity::PREDICTION_SCORE_ATTRIBUTE("predictionScore");

    PredictionEntity::PredictionEntity() :
        Entity()
    {
        initializeAttributes();
    }

    PredictionEntity::PredictionEntity(const EntityRefBasePtr& memorySnapshotRef, const EntityRefBasePtr& sourceMemorySnapshotRef, const EntityRefBasePtr& predictedMemorySnapshotRef, const EntityRefBaseList& predictedMemorySnapshotRefList, const std::string& predictionMethodName, Ice::Int taskCount, Ice::Float predictionScore) :
        Entity()
    {
        initializeAttributes();
        setProfilerMemorySnapshotRef(memorySnapshotRef);
        setSourceProfilerMemorySnapshotRef(sourceMemorySnapshotRef);
        setPredictedProfilerMemorySnapshotRef(predictedMemorySnapshotRef);
        setPredictedProfilerMemorySnapshotRefList(predictedMemorySnapshotRefList);
        setPredictionMethodName(predictionMethodName);
        setTaskCount(taskCount);
        setPredictionScore(predictionScore);
    }


    PredictionEntity::PredictionEntity(const PredictionEntity& source):
        IceUtil::Shared(source),
        ::armarx::Serializable(source),
        EntityBase(),// dont copy
        PredictionEntityBase(source),
        Entity(source)
    {
    }


    PredictionEntity::~PredictionEntity()
    {
    }

    void PredictionEntity::initializeAttributes()
    {
        putAttribute(new EntityAttribute(PredictionEntity::SNAPSHOT_ATTRIBUTE));
        putAttribute(new EntityAttribute(PredictionEntity::SOURCE_SNAPSHOT_ATTRIBUTE));
        putAttribute(new EntityAttribute(PredictionEntity::PREDICTION_ATTRIBUTE));
        putAttribute(new EntityAttribute(PredictionEntity::PREDICTION_LIST_ATTRIBUTE));
        putAttribute(new EntityAttribute(PredictionEntity::PREDICTION_METHOD_NAME_ATTRIBUTE));
        putAttribute(new EntityAttribute(PredictionEntity::TASK_COUNT_ATTRIBUTE));
        putAttribute(new EntityAttribute(PredictionEntity::PREDICTION_SCORE_ATTRIBUTE));
    }


    void PredictionEntity::output(std::ostream& stream) const
    {
        Entity::output(stream);
    }


    Ice::ObjectPtr PredictionEntity::ice_clone() const
    {
        return this->clone();
    }


    PredictionEntityPtr PredictionEntity::clone(const Ice::Current& context) const
    {
        std::shared_lock entityLock(entityMutex);
        std::scoped_lock attributesLock(attributesMutex);
        std::scoped_lock wrappersLock(wrappersMutex);
        return new PredictionEntity(*this);
    }

    EntityRefBasePtr PredictionEntity::getProfilerMemorySnapshotRef(const Ice::Current&) const
    {
        return getAttributeValue(PredictionEntity::SNAPSHOT_ATTRIBUTE)->get<EntityRef>();
    }

    void PredictionEntity::setProfilerMemorySnapshotRef(const EntityRefBasePtr& memorySnapshotRef, const Ice::Current&)
    {
        getAttribute(PredictionEntity::SNAPSHOT_ATTRIBUTE)->setValue(new armarx::Variant(memorySnapshotRef));
    }

    EntityRefBasePtr PredictionEntity::getSourceProfilerMemorySnapshotRef(const Ice::Current&) const
    {
        return getAttributeValue(PredictionEntity::SOURCE_SNAPSHOT_ATTRIBUTE)->get<EntityRef>();
    }

    void PredictionEntity::setSourceProfilerMemorySnapshotRef(const EntityRefBasePtr& sourceMemorySnapshotRef, const Ice::Current&)
    {
        getAttribute(PredictionEntity::SOURCE_SNAPSHOT_ATTRIBUTE)->setValue(new armarx::Variant(sourceMemorySnapshotRef));
    }

    EntityRefBasePtr PredictionEntity::getPredictedProfilerMemorySnapshotRef(const Ice::Current&) const
    {
        return getAttributeValue(PredictionEntity::PREDICTION_ATTRIBUTE)->get<EntityRef>();
    }

    void PredictionEntity::setPredictedProfilerMemorySnapshotRef(const EntityRefBasePtr& predictedMemorySnapshotRef, const Ice::Current&)
    {
        getAttribute(PredictionEntity::PREDICTION_ATTRIBUTE)->setValue(new armarx::Variant(predictedMemorySnapshotRef));
    }

    EntityRefBaseList PredictionEntity::getPredictedProfilerMemorySnapshotRefList(const Ice::Current&) const
    {
        EntityAttributeBasePtr attribute = getAttribute(PredictionEntity::PREDICTION_LIST_ATTRIBUTE);
        EntityRefBaseList entityList;
        for (int i = 0; i < attribute->size(); i++)
        {
            armarx::VariantPtr attributeValue = armarx::VariantPtr::dynamicCast(attribute->getValueAt(i));
            entityList.push_back(attributeValue->get<EntityRef>());
        }
        return entityList;
    }

    void PredictionEntity::setPredictedProfilerMemorySnapshotRefList(const EntityRefBaseList& predictedMemorySnapshotRefList, const Ice::Current&)
    {
        EntityAttributeBasePtr attribute = getAttribute(PredictionEntity::PREDICTION_LIST_ATTRIBUTE);
        attribute->clear();
        for (const EntityRefBasePtr& entityRef : predictedMemorySnapshotRefList)
        {
            attribute->addValue(new armarx::Variant(entityRef));
        }
    }

    std::string PredictionEntity::getPredictionMethodName(const Ice::Current&) const
    {
        return getAttribute(PredictionEntity::PREDICTION_METHOD_NAME_ATTRIBUTE)->getValue()->getString();
    }

    void PredictionEntity::setPredictionMethodName(const std::string& predictionMethodName, const Ice::Current&)
    {
        getAttribute(PredictionEntity::PREDICTION_METHOD_NAME_ATTRIBUTE)->setValue(new armarx::Variant(predictionMethodName));
    }

    Ice::Int PredictionEntity::getTaskCount(const Ice::Current&) const
    {
        return getAttributeValue(PredictionEntity::TASK_COUNT_ATTRIBUTE)->getInt();
    }

    void PredictionEntity::setTaskCount(Ice::Int taskCount, const Ice::Current&)
    {
        getAttribute(PredictionEntity::TASK_COUNT_ATTRIBUTE)->setValue(new armarx::Variant(taskCount));
    }

    Ice::Float PredictionEntity::getPredictionScore(const Ice::Current&) const
    {
        return getAttributeValue(PredictionEntity::PREDICTION_SCORE_ATTRIBUTE)->getFloat();
    }

    void PredictionEntity::setPredictionScore(Ice::Float predictionScore, const Ice::Current&)
    {
        getAttribute(PredictionEntity::PREDICTION_SCORE_ATTRIBUTE)->setValue(new armarx::Variant(predictionScore));
    }
}
