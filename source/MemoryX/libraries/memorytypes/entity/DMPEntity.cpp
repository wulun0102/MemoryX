/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Core
* @author     Alexey Kozlov ( kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "DMPEntity.h"

#include <ArmarXCore/observers/AbstractObjectSerializer.h>

namespace memoryx
{

    DMPEntity::DMPEntity(): Entity()
    {
        //        EntityAttributePtr parentClassesAttr = new EntityAttribute("parentClasses");
        //        putAttribute(parentClassesAttr);
        setName("");
        setId("");

    }

    DMPEntity::DMPEntity(const DMPEntity& source):
        IceUtil::Shared(source),
        ::armarx::Serializable(source),
        EntityBase(),// dont copy
        DMPEntityBase(source),
        Entity(source)
    {

    }

    DMPEntity::DMPEntity(const std::string& name, const std::string& id)
    {
        setName(name);
        setId(id);
    }

    DMPEntity::~DMPEntity()
    {
        // TODO Auto-generated destructor stub
    }

    void DMPEntity::setDMPName(const std::string& dmpName, const Ice::Current&)
    {
        putAttribute("DMPName", dmpName);
    }

    void DMPEntity::setDMPType(const std::string& dmpType, const Ice::Current&)
    {
        putAttribute("DMPType", dmpType);
    }

    void DMPEntity::setDMPtextStr(const std::string& textdmp, const Ice::Current&)
    {
        putAttribute("DMPtextStr", textdmp);
    }

    void DMPEntity::set3rdOrder(const bool is3rdOrder, const Ice::Current&)
    {
        putAttribute("3rdOrder", is3rdOrder);
    }

    void DMPEntity::setTrajDim(const int trajdim, const ::Ice::Current&)
    {
        putAttribute("TrajDim", trajdim);
    }


    std::string DMPEntity::getDMPName(const Ice::Current&) const
    {
        return getAttributeValue("DMPName")->getString();
    }

    int DMPEntity::getDMPType(const Ice::Current&) const
    {
        return getAttributeValue("DMPType")->getInt();
    }

    std::string DMPEntity::getDMPtextStr(const Ice::Current&) const
    {
        return getAttributeValue("DMPtextStr")->getString();
    }

    bool DMPEntity::get3rdOrder(const Ice::Current&) const
    {
        return getAttributeValue("3rdOrder")->getBool();
    }

    int DMPEntity::getTrajDim(const Ice::Current&) const
    {
        return getAttributeValue("TrajDim")->getInt();
    }

    void DMPEntity::output(std::ostream& stream) const
    {
        Entity::output(stream);
    }

    Ice::ObjectPtr DMPEntity::ice_clone() const
    {
        return this->clone();
    }

    DMPEntityPtr DMPEntity::clone(const Ice::Current& c) const
    {
        std::shared_lock lock1(entityMutex);
        std::scoped_lock lock2(attributesMutex);
        std::scoped_lock lock3(wrappersMutex);
        DMPEntityPtr ret = new DMPEntity(*this);
        return ret;
    }


}


