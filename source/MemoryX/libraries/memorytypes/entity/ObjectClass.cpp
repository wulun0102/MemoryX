/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Core
* @author     Alexey Kozlov ( kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "ObjectClass.h"

#include <ArmarXCore/observers/AbstractObjectSerializer.h>

namespace memoryx
{

    ObjectClass::ObjectClass(): Entity()
    {
        EntityAttributePtr parentClassesAttr = new EntityAttribute("parentClasses");
        putAttribute(parentClassesAttr);
        EntityAttributePtr isInstanceableAttr = new EntityAttribute("isInstanceable");
        putAttribute(isInstanceableAttr);
    }

    ObjectClass::ObjectClass(const ObjectClass& source):
        IceUtil::Shared(source),
        ::armarx::Serializable(source),
        EntityBase(),// dont copy
        ObjectClassBase(source),
        Entity(source)
    {

    }

    ObjectClass::~ObjectClass()
    {
        // TODO Auto-generated destructor stub
    }

    memoryx::NameList ObjectClass::getParentClasses(const Ice::Current&) const
    {
        NameList result;

        const EntityAttributeBasePtr parentAttr = getParentClassesAttr();

        for (int i = 0; i < parentAttr->size(); ++i)
        {
            result.push_back(parentAttr->getValueAt(i)->getString());
        }

        return result;
    }

    void ObjectClass::addParentClass(const std::string& className,
                                     const Ice::Current&)
    {
        getParentClassesAttr()->addValue(armarx::VariantPtr(new armarx::Variant(className)));
    }

    void ObjectClass::setParentClass(const std::string& className,
                                     const Ice::Current&)
    {
        clearParentClasses();
        addParentClass(className);
    }

    void ObjectClass::clearParentClasses(const Ice::Current&)
    {
        getParentClassesAttr()->clear();
    }

    void ObjectClass::setInstanceable(bool isInstanceable, const Ice::Current&)
    {
        auto attr = getAttribute("isInstanceable");
        if (attr)
        {
            attr->clear();
        }
        else
        {
            attr = new EntityAttribute("isInstanceable");
        }
        attr->addValue(armarx::VariantPtr(new armarx::Variant(isInstanceable)));
    }

    bool ObjectClass::isInstanceable(const Ice::Current&) const
    {
        auto attr = getAttribute("isInstanceable");

        if (attr && attr->size() != 0)
        {
            return attr->getValue()->getBool();
        }

        return true;
    }

    void ObjectClass::output(std::ostream& stream) const
    {
        Entity::output(stream);
    }

    EntityAttributeBasePtr ObjectClass::getParentClassesAttr() const
    {
        return getAttribute("parentClasses");
    }

    Ice::ObjectPtr ObjectClass::ice_clone() const
    {
        return this->clone();
    }

    ObjectClassPtr ObjectClass::clone(const Ice::Current& c) const
    {
        std::shared_lock lock1(entityMutex);
        std::scoped_lock lock2(attributesMutex);
        std::scoped_lock lock3(wrappersMutex);
        ObjectClassPtr ret = new ObjectClass(*this);
        //        ret->deepCopy(*this);
        return ret;
    }

    ObjectComparisonResult ObjectClass::compare(const ObjectClassBasePtr& other, const Ice::Current&) const
    {
        if (other->getName() == getName())
        {
            return eEqualClass;
        }

        NameList otherParentClasses = other->getParentClasses();
        NameList parentClasses = getParentClasses();
        ObjectComparisonResult result = eNotEqualClass;

        // check parents names
        for (NameList::iterator itOther = otherParentClasses.begin(); itOther != otherParentClasses.end(); itOther++)
        {
            if (getName() == *itOther) // other is a subclass of this -> so its equal
            {
                return eEqualClass;
            }

            for (NameList::iterator it = parentClasses.begin(); it != parentClasses.end(); it++)
            {
                if ((*it) == (*itOther)) // parent of this is equal to parent of other, so only equal class
                {
                    result =  eEqualParentClass;
                }
            }
        }

        if (result == eEqualParentClass)
        {
            return result;
        }

        for (NameList::iterator it = parentClasses.begin(); it != parentClasses.end(); it++)
        {
            if (*it == other->getName()) // other is parent class of this -> so only equal class
            {
                return eEqualParentClass;
            }
        }

        return result;
    }

}


