/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::Core
 * @author     Adil Orhan (ubdnw at student dot kit dot edu)
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include "ProfilerProcess.h"


namespace memoryx
{

    ProfilerProcess::ProfilerProcess():
        Entity()
    {
        initializeAttributes();
    }

    ProfilerProcess::ProfilerProcess(const armarx::ProfilerProcessCpuUsage& process)
    {
        initializeAttributes();
        setProfilerProcess(process);
    }

    ProfilerProcess::ProfilerProcess(const ProfilerProcess& source):
        IceUtil::Shared(source),
        ::armarx::Serializable(source),
        EntityBase(),
        ProfilerEntityBase(source),
        ProfilerProcessBase(source),
        Entity(source),
        ProfilerEntity(source)
    {
    }

    ProfilerProcess::~ProfilerProcess()
    {
    }

    void ProfilerProcess::initializeAttributes()
    {
        putAttribute(new EntityAttribute("pid"));
        putAttribute(new EntityAttribute("processName"));
        putAttribute(new EntityAttribute("timestamp"));
        putAttribute(new EntityAttribute("processCpuUsage"));
    }

    Ice::ObjectPtr ProfilerProcess::ice_clone() const
    {
        return this->clone();
    }

    ProfilerEntityPtr ProfilerProcess::clone(const Ice::Current& c) const
    {
        std::shared_lock entityLock(entityMutex);
        std::scoped_lock attributesLock(attributesMutex);
        std::scoped_lock wrappersLock(wrappersMutex);
        ProfilerProcessPtr ret = new ProfilerProcess(*this);
        return ret;
    }

    void ProfilerProcess::setProfilerProcess(const armarx::ProfilerProcessCpuUsage& process)
    {
        setPid(process.processId);
        setProcessName(process.processName);
        setTimestamp(process.timestamp);
        setProcessCpuUsage(process.cpuUsage);
    }

    Ice::Int ProfilerProcess::getPid(const Ice::Current& c) const
    {
        return getAttribute("pid")->getValue()->getInt();
    }

    void ProfilerProcess::setPid(Ice::Int pid, const Ice::Current&)
    {
        getAttribute("pid")->setValue(new armarx::Variant(pid));
    }

    std::string ProfilerProcess::getProcessName(const Ice::Current&) const
    {
        return getAttribute("processName")->getValue()->getString();
    }

    void ProfilerProcess::setProcessName(const std::string& processName, const Ice::Current&)
    {
        getAttribute("processName")->setValue(new armarx::Variant(processName));
    }

    Ice::Int ProfilerProcess::getTimestamp(const Ice::Current&) const
    {
        return getAttribute("timestamp")->getValue()->getInt();
    }

    void ProfilerProcess::setTimestamp(Ice::Int timestamp, const Ice::Current&)
    {
        getAttribute("timestamp")->setValue(new armarx::Variant(timestamp));
    }

    Ice::Double ProfilerProcess::getProcessCpuUsage(const Ice::Current&) const
    {
        return getAttribute("processCpuUsage")->getValue()->getDouble();
    }

    void ProfilerProcess::setProcessCpuUsage(Ice::Double processCpuUsage, const Ice::Current&)
    {
        getAttribute("processCpuUsage")->setValue(new armarx::Variant(processCpuUsage));
    }
}
