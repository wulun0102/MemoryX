/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::Core
 * @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ProfilerMemorySnapshot.h"

#include <ArmarXCore/observers/variant/StringValueMap.h>


namespace memoryx
{

    const std::string ProfilerMemorySnapshot::ATTRIBUTE_STATENAME = "stateName";
    const std::string ProfilerMemorySnapshot::ATTRIBUTE_MEMORYPARAMETERS = "memoryParameters";


    ProfilerMemorySnapshot::ProfilerMemorySnapshot()
    {
        initializeAttributes();
    }

    ProfilerMemorySnapshot::ProfilerMemorySnapshot(const std::string& stateName, const Ice::Context& parameters)
    {
        initializeAttributes();
        setStateName(stateName);
        setMemoryParameterMap(parameters);
    }

    ProfilerMemorySnapshot::ProfilerMemorySnapshot(const ProfilerMemorySnapshot& source):
        IceUtil::Shared(source),
        ::armarx::Serializable(source),
        EntityBase(),// dont copy
        ProfilerEntityBase(source),
        ProfilerMemorySnapshotBase(source),
        Entity(source),
        ProfilerEntity(source)
    {
    }

    ProfilerMemorySnapshot::~ProfilerMemorySnapshot()
    {
    }


    void ProfilerMemorySnapshot::output(std::ostream& stream) const
    {
        Entity::output(stream);
    }

    void ProfilerMemorySnapshot::initializeAttributes()
    {
        putAttribute(new EntityAttribute(ATTRIBUTE_STATENAME));
        putAttribute(new EntityAttribute(ATTRIBUTE_MEMORYPARAMETERS));
    }


    Ice::ObjectPtr ProfilerMemorySnapshot::ice_clone() const
    {
        return this->clone();
    }

    ProfilerMemorySnapshotPtr ProfilerMemorySnapshot::clone(const Ice::Current& c) const
    {
        std::shared_lock entityLock(entityMutex);
        std::scoped_lock attributesLock(attributesMutex);
        std::scoped_lock wrappersLock(wrappersMutex);
        return new ProfilerMemorySnapshot(*this);
    }

    std::string ProfilerMemorySnapshot::getStateName(const Ice::Current& context) const
    {
        return getAttribute(ATTRIBUTE_STATENAME)->getValue()->getString();
    }

    void ProfilerMemorySnapshot::setStateName(const std::string& statename, const Ice::Current& context)
    {
        getAttribute(ATTRIBUTE_STATENAME)->setValue(new armarx::Variant(statename));
        setName(statename);
    }

    Ice::Context ProfilerMemorySnapshot::getMemoryParameterMap(const Ice::Current& context) const
    {
        Ice::Context memoryParameters;
        armarx::VariantPtr parameters = armarx::VariantPtr::dynamicCast(getAttribute(ATTRIBUTE_MEMORYPARAMETERS)->getValue());
        if (!parameters)
        {
            return memoryParameters;
        }
        armarx::StringValueMapPtr parameterMap = parameters->get<armarx::StringValueMap>();
        if (!parameterMap)
        {
            return memoryParameters;
        }
        return parameterMap->toStdMap<std::string>();
    }

    void ProfilerMemorySnapshot::setMemoryParameterMap(const Ice::Context& parameters, const Ice::Current& context)
    {
        armarx::StringValueMapPtr memoryParameters = armarx::StringValueMap::FromStdMap<std::string>(parameters);
        getAttribute(ATTRIBUTE_MEMORYPARAMETERS)->setValue(new armarx::Variant(memoryParameters));
    }

}
