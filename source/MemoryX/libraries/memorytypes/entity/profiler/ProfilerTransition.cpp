/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::Core
 * @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ProfilerTransition.h"

#include "../../../../core/entity/EntityRef.h"


namespace memoryx
{

    ProfilerTransition::ProfilerTransition()
    {
        initializeAttributes();
        setParentStateName("");
        setSourceStateName("");
        setTargetStateName("");
        setSourceStateMemorySnapshotRef(new EntityRef());
        setTargetStateMemorySnapshotRef(new EntityRef());
        setCount(0);
    }

    ProfilerTransition::ProfilerTransition(const std::string& parentStateName, const std::string& sourceStateName, const std::string& targetStateName, const EntityRefBasePtr& sourceStateMemorySnapshotRef, const EntityRefBasePtr& targetStateMemorySnapshotRef, Ice::Int count)
    {
        initializeAttributes();
        setParentStateName(parentStateName);
        setSourceStateName(sourceStateName);
        setTargetStateName(targetStateName);
        setSourceStateMemorySnapshotRef(sourceStateMemorySnapshotRef);
        setTargetStateMemorySnapshotRef(targetStateMemorySnapshotRef);
        setCount(count);
    }

    ProfilerTransition::ProfilerTransition(const ProfilerTransition& source):
        IceUtil::Shared(source),
        ::armarx::Serializable(source),
        EntityBase(),// dont copy
        ProfilerEntityBase(source),
        ProfilerTransitionBase(source),
        Entity(source),
        ProfilerEntity(source)
    {
    }

    ProfilerTransition::~ProfilerTransition()
    {
    }


    void ProfilerTransition::output(std::ostream& stream) const
    {
        Entity::output(stream);
    }

    void ProfilerTransition::initializeAttributes()
    {
        putAttribute(new EntityAttribute("parentState"));
        putAttribute(new EntityAttribute("sourceState"));
        putAttribute(new EntityAttribute("targetState"));
        putAttribute(new EntityAttribute("sourceMemorySnapshot"));
        putAttribute(new EntityAttribute("targetMemorySnapshot"));
        putAttribute(new EntityAttribute("transitionCount"));
    }


    Ice::ObjectPtr ProfilerTransition::ice_clone() const
    {
        return this->clone();
    }

    ProfilerTransitionPtr ProfilerTransition::clone(const Ice::Current& c) const
    {
        std::shared_lock entityLock(entityMutex);
        std::scoped_lock attributesLock(attributesMutex);
        std::scoped_lock wrappersLock(wrappersMutex);
        return new ProfilerTransition(*this);
    }

    std::string ProfilerTransition::getParentStateName(const Ice::Current& context) const
    {
        return getAttribute("parentState")->getValue()->getString();
    }

    void ProfilerTransition::setParentStateName(const std::string& statename, const Ice::Current& context)
    {
        getAttribute("parentState")->setValue(new armarx::Variant(statename));
        setName(statename);
    }


    std::string ProfilerTransition::getSourceStateName(const Ice::Current& context) const
    {
        return getAttributeValue("sourceState")->getString();
    }

    void ProfilerTransition::setSourceStateName(const std::string& statename, const Ice::Current& context)
    {
        getAttribute("sourceState")->setValue(new armarx::Variant(statename));
    }

    std::string ProfilerTransition::getTargetStateName(const Ice::Current& context) const
    {
        return getAttributeValue("targetState")->getString();
    }

    void ProfilerTransition::setTargetStateName(const std::string& statename, const Ice::Current& context)
    {
        getAttribute("targetState")->setValue(new armarx::Variant(statename));
    }


    EntityRefBasePtr ProfilerTransition::getSourceStateMemorySnapshotRef(const Ice::Current& context) const
    {
        return getAttributeValue("sourceMemorySnapshot")->get<EntityRef>();
    }

    void ProfilerTransition::setSourceStateMemorySnapshotRef(const EntityRefBasePtr& memorySnapshotRef, const Ice::Current& context)
    {
        getAttribute("sourceMemorySnapshot")->setValue(new armarx::Variant(memorySnapshotRef));
    }

    EntityRefBasePtr ProfilerTransition::getTargetStateMemorySnapshotRef(const Ice::Current& context) const
    {
        return getAttributeValue("targetMemorySnapshot")->get<EntityRef>();
    }

    void ProfilerTransition::setTargetStateMemorySnapshotRef(const EntityRefBasePtr& memorySnapshotRef, const Ice::Current& context)
    {
        getAttribute("targetMemorySnapshot")->setValue(new armarx::Variant(memorySnapshotRef));
    }


    Ice::Int ProfilerTransition::getCount(const Ice::Current& context) const
    {
        return getAttributeValue("transitionCount")->getInt();
    }

    void ProfilerTransition::setCount(Ice::Int count, const Ice::Current& context)
    {
        getAttribute("transitionCount")->setValue(new armarx::Variant(count));
    }

    bool memoryx::ProfilerTransition::memorySnapshotIdsEqual(const std::string& sourceSnapshotID, const std::string& targetSnapshotID)
    {
        auto sourceStateMemorySnapshotRef = this->getSourceStateMemorySnapshotRef();
        if (!sourceStateMemorySnapshotRef->getEntity())
        {
            return false;
        }
        auto targetStateMemorySnapshotRef = this->getTargetStateMemorySnapshotRef();
        if (!targetStateMemorySnapshotRef->getEntity())
        {
            return false;
        }
        return ((sourceStateMemorySnapshotRef->entityId == sourceSnapshotID)
                && (targetStateMemorySnapshotRef->entityId == targetSnapshotID));

    }

}
