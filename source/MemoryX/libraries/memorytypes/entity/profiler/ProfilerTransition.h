/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::Core::MemoryTypes
 * @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "ProfilerEntity.h"

#include <MemoryX/interface/core/EntityBase.h>
#include <MemoryX/interface/memorytypes/ProfilerEntities.h>
#include <MemoryX/core/entity/Entity.h>

namespace memoryx
{

    class ProfilerTransition;
    using ProfilerTransitionPtr = IceInternal::Handle<ProfilerTransition>;

    class ProfilerTransition :
        virtual public memoryx::ProfilerTransitionBase,
        virtual public memoryx::ProfilerEntity
    {
    public:
        ProfilerTransition();
        ProfilerTransition(const std::string& parentStateName, const std::string& sourceStateName, const std::string& targetStateName, const EntityRefBasePtr& sourceStateMemorySnapshotRef, const EntityRefBasePtr& targetStateMemorySnapshotRef, Ice::Int count);
        ProfilerTransition(const ProfilerTransition& source);
        ~ProfilerTransition() override;


        // cloning
        Ice::ObjectPtr ice_clone() const override;
        ProfilerTransitionPtr clone(const Ice::Current& c = Ice::emptyCurrent) const;

        // interface
        std::string getParentStateName(const Ice::Current& context = Ice::emptyCurrent) const override;
        void setParentStateName(const std::string& statename, const Ice::Current& context = Ice::emptyCurrent) override;

        std::string getSourceStateName(const Ice::Current& context = Ice::emptyCurrent) const override;
        void setSourceStateName(const std::string& statename, const Ice::Current& context = Ice::emptyCurrent) override;
        std::string getTargetStateName(const Ice::Current& context = Ice::emptyCurrent) const override;
        void setTargetStateName(const std::string& statename, const Ice::Current& context = Ice::emptyCurrent) override;

        EntityRefBasePtr getSourceStateMemorySnapshotRef(const Ice::Current& context = Ice::emptyCurrent) const override;
        void setSourceStateMemorySnapshotRef(const EntityRefBasePtr& memorySnapshotRef, const Ice::Current& context = Ice::emptyCurrent) override;
        EntityRefBasePtr getTargetStateMemorySnapshotRef(const Ice::Current& context = Ice::emptyCurrent) const override;
        void setTargetStateMemorySnapshotRef(const EntityRefBasePtr& memorySnapshotRef, const Ice::Current& context = Ice::emptyCurrent) override;

        Ice::Int getCount(const Ice::Current& context = Ice::emptyCurrent) const override;
        void setCount(Ice::Int count, const Ice::Current& context = Ice::emptyCurrent) override;

        /**
         * @brief equalsMemorySnapshotIDs returns true if the ProfilerMemorySnapshot IDs asssocited with this transition are equal to the passed in parameters.
         * @param sourceSnapshotID
         * @param targetSnapshotID
         * @return
         */
        bool memorySnapshotIdsEqual(const std::string& sourceSnapshotID, const std::string& targetSnapshotID);

    private:
        void output(std::ostream& stream) const;
        void initializeAttributes();

    public:
        // streaming operator
        friend std::ostream& operator<<(std::ostream& stream, const ProfilerTransition& rhs)
        {
            rhs.output(stream);
            return stream;
        }

        friend std::ostream& operator<<(std::ostream& stream, const ProfilerTransitionPtr& rhs)
        {
            rhs->output(stream);
            return stream;
        }

        friend std::ostream& operator<<(std::ostream& stream, const ProfilerTransitionBasePtr& rhs)
        {
            stream << ProfilerTransitionPtr::dynamicCast(rhs);
            return stream;
        }
    };

}

