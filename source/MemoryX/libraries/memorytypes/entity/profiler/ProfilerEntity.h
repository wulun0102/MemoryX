/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::Core::MemoryTypes
 * @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <MemoryX/interface/core/EntityBase.h>
#include <MemoryX/interface/memorytypes/ProfilerEntities.h>
#include <MemoryX/core/entity/Entity.h>

namespace memoryx
{
    class ProfilerEntity;
    using ProfilerEntityPtr = IceInternal::Handle<ProfilerEntity>;

    class ProfilerEntity :
        virtual public memoryx::ProfilerEntityBase,
        virtual public memoryx::Entity
    {
    public:
        ProfilerEntity();
        ProfilerEntity(const ProfilerEntity& source);
        ~ProfilerEntity() override;


        // cloning
        Ice::ObjectPtr ice_clone() const override;
        ProfilerEntityPtr clone(const Ice::Current& c = Ice::emptyCurrent) const;

        // interface
        Ice::StringSeq getTags(const Ice::Current& context = Ice::emptyCurrent) const override;
        void setTags(const Ice::StringSeq& tags, const Ice::Current& context = Ice::emptyCurrent) override;
        void addTag(const std::string& tag, const Ice::Current& context = Ice::emptyCurrent) override;

    private:
        void output(std::ostream& stream) const;

    public:
        // streaming operator
        friend std::ostream& operator<<(std::ostream& stream, const ProfilerEntity& rhs)
        {
            rhs.output(stream);
            return stream;
        }

        friend std::ostream& operator<<(std::ostream& stream, const ProfilerEntityPtr& rhs)
        {
            rhs->output(stream);
            return stream;
        }

        friend std::ostream& operator<<(std::ostream& stream, const ProfilerEntityBasePtr& rhs)
        {
            stream << ProfilerEntityPtr::dynamicCast(rhs);
            return stream;
        }
    };

}

