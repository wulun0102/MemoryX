/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::Core::MemoryTypes
 * @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "ProfilerEntity.h"

#include <MemoryX/interface/core/EntityBase.h>
#include <MemoryX/interface/memorytypes/ProfilerEntities.h>
#include <MemoryX/core/entity/Entity.h>


namespace memoryx
{

    class ProfilerEvent;
    using ProfilerEventPtr = IceInternal::Handle<ProfilerEvent>;

    class ProfilerEvent :
        virtual public memoryx::ProfilerEventBase,
        virtual public memoryx::ProfilerEntity
    {
    public:
        ProfilerEvent();
        ProfilerEvent(const armarx::ProfilerEvent& event);
        ProfilerEvent(const memoryx::ProfilerEvent& source);
        ~ProfilerEvent() override;


        // cloning
        Ice::ObjectPtr ice_clone() const override;
        ProfilerEntityPtr clone(const Ice::Current& c = Ice::emptyCurrent) const;

        // interface
        void setProfilerEvent(const armarx::ProfilerEvent& event);

        Ice::Int getPid(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setPid(Ice::Int pid, const Ice::Current& c = Ice::emptyCurrent) override;
        std::string getExecutableName(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setExecutableName(const std::string& executableName, const Ice::Current& c = Ice::emptyCurrent) override;
        Ice::Int getTimestamp(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setTimestamp(Ice::Int timestamp, const Ice::Current& c = Ice::emptyCurrent) override;
        std::string getTimestampUnit(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setTimestampUnit(const std::string& unit, const Ice::Current& c = Ice::emptyCurrent) override;
        std::string getEventName(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setEventName(const std::string& eventName, const Ice::Current& c = Ice::emptyCurrent) override;
        std::string getParentName(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setParentName(const std::string& parentName, const Ice::Current& c = Ice::emptyCurrent) override;
        std::string getFunctionName(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setFunctionName(const std::string& functionName, const Ice::Current& c = Ice::emptyCurrent) override;
    private:
        void output(std::ostream& stream) const;

        void initializeAttributes();

    public:
        // streaming operator
        friend std::ostream& operator<<(std::ostream& stream, const ProfilerEvent& rhs)
        {
            rhs.output(stream);
            return stream;
        }

        friend std::ostream& operator<<(std::ostream& stream, const ProfilerEventPtr& rhs)
        {
            rhs->output(stream);
            return stream;
        }

        friend std::ostream& operator<<(std::ostream& stream, const ProfilerEventBasePtr& rhs)
        {
            stream << ProfilerEventPtr::dynamicCast(rhs);
            return stream;
        }
    };

}

