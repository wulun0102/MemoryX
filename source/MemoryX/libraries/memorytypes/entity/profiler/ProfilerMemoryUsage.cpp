/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::Core::MemoryTypes
 * @author     Adil Orhan (ubdnw at student dot kit dot edu)
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include "ProfilerMemoryUsage.h"


namespace memoryx
{

    ProfilerMemoryUsage::ProfilerMemoryUsage():
        Entity()
    {
        initializeAttributes();
    }

    ProfilerMemoryUsage::ProfilerMemoryUsage(const armarx::ProfilerProcessMemoryUsage& memoryUsage)
    {
        initializeAttributes();
        setProfilerMemoryUsage(memoryUsage);
    }

    ProfilerMemoryUsage::ProfilerMemoryUsage(const ProfilerMemoryUsage& source):
        IceUtil::Shared(source),
        ::armarx::Serializable(source),
        EntityBase(),
        ProfilerEntityBase(source),
        ProfilerMemoryUsageBase(source),
        Entity(source),
        ProfilerEntity(source)
    {
    }

    ProfilerMemoryUsage::~ProfilerMemoryUsage()
    {
    }

    void ProfilerMemoryUsage::initializeAttributes()
    {
        putAttribute(new EntityAttribute("pid"));
        putAttribute(new EntityAttribute("processName"));
        putAttribute(new EntityAttribute("timestamp"));
        putAttribute(new EntityAttribute("memoryUsage"));
    }

    Ice::ObjectPtr ProfilerMemoryUsage::ice_clone() const
    {
        return this->clone();
    }

    ProfilerEntityPtr ProfilerMemoryUsage::clone(const Ice::Current& c) const
    {
        std::shared_lock entityLock(entityMutex);
        std::scoped_lock attributesLock(attributesMutex);
        std::scoped_lock wrappersLock(wrappersMutex);
        ProfilerMemoryUsagePtr ret = new ProfilerMemoryUsage(*this);
        return ret;
    }

    void ProfilerMemoryUsage::setProfilerMemoryUsage(const armarx::ProfilerProcessMemoryUsage& memoryUsage)
    {
        setPid(memoryUsage.processId);
        setProcessName(memoryUsage.processName);
        setTimestamp(memoryUsage.timestamp);
        setMemoryUsage(memoryUsage.memoryUsage);
    }

    Ice::Int ProfilerMemoryUsage::getPid(const Ice::Current& c) const
    {
        return getAttribute("pid")->getValue()->getInt();
    }

    void ProfilerMemoryUsage::setPid(Ice::Int pid, const Ice::Current&)
    {
        getAttribute("pid")->setValue(new armarx::Variant(pid));
    }

    std::string ProfilerMemoryUsage::getProcessName(const Ice::Current&) const
    {
        return getAttribute("processName")->getValue()->getString();
    }

    void ProfilerMemoryUsage::setProcessName(const std::string& processName, const Ice::Current&)
    {
        getAttribute("processName")->setValue(new armarx::Variant(processName));
    }

    Ice::Int ProfilerMemoryUsage::getTimestamp(const Ice::Current&) const
    {
        return getAttribute("timestamp")->getValue()->getInt();
    }

    void ProfilerMemoryUsage::setTimestamp(Ice::Int timestamp, const Ice::Current&)
    {
        getAttribute("timestamp")->setValue(new armarx::Variant(timestamp));
    }

    Ice::Int ProfilerMemoryUsage::getMemoryUsage(const Ice::Current& c) const
    {
        return getAttribute("memoryUsage")->getValue()->getInt();
    }

    void ProfilerMemoryUsage::setMemoryUsage(Ice::Int memoryUsage, const Ice::Current& c)
    {
        getAttribute("memoryUsage")->setValue(new armarx::Variant(memoryUsage));
    }
}
