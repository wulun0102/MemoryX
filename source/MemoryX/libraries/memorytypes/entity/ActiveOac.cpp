/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::WorkingMemory
* @author     ALexey Kozlov ( kozlov at kit dot edu), Kai Welke (welke at kit got edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "ActiveOac.h"
#include "Oac.h"
#include "OacParameterList.h"
#include "OacPredictionFunction.h"
#include "OacStatisticalMeasure.h"

#include <ArmarXCore/observers/AbstractObjectSerializer.h>
#include <ArmarXCore/observers/variant/TimestampVariant.h>

#include <MemoryX/core/entity/EntityRef.h>

namespace memoryx
{
    ActiveOac::ActiveOac(const std::string& id)
        : Entity()
    {
        setName(id);
        setId(id);

        setArgumentInstanceIds(std::vector<std::string>());
        setOacRef(NULL);
        setState(eIncomplete);
    }

    NameList ActiveOac::getArgumentInstanceIds(const ::Ice::Current&) const
    {
        return OacParameterListPtr::dynamicCast(
                   getAttributeValue("parameterIds")->getClass<OacParameterListBase>()
               )->getNames();
    }

    void ActiveOac::setArgumentInstanceIds(const NameList& ids, const ::Ice::Current&)
    {
        OacParameterListPtr paramObj = new OacParameterList();
        paramObj->setNames(ids);
        putAttribute("parameterIds", paramObj);
    }


    OacExecState ActiveOac::getState(const ::Ice::Current&) const
    {
        return static_cast<OacExecState>(getAttributeValue("state")->getInt());
    }

    void ActiveOac::setState(OacExecState state, const ::Ice::Current&)
    {
        putAttribute("state", static_cast<int>(state));
    }

    Ice::ObjectPtr ActiveOac::ice_clone() const
    {
        return this->clone();
    }

    ActiveOacPtr ActiveOac::clone(const Ice::Current& c) const
    {
        ActiveOacPtr ret = new ActiveOac(*this);
        //    ret->deepCopy(*this);
        return ret;
    }

    EntityRefBasePtr ActiveOac::getOacRef(const Ice::Current&) const
    {
        return getAttributeValue("oacId")->get<EntityRef>();
    }

    void ActiveOac::setOacRef(const EntityRefBasePtr& oacRef, const Ice::Current&)
    {
        putAttribute("oacRef", oacRef);
    }

    std::string ActiveOac::getOacName(const Ice::Current&) const
    {
        if (EntityRefBasePtr ref = getOacRef())
        {
            return ref->entityName;
        }

        return "";

    }

    void ActiveOac::setStartTime(const IceUtil::Time& time)
    {
        putAttribute("startTime", new armarx::TimestampVariant(time));
    }

    IceUtil::Time ActiveOac::getStartTime() const
    {
        armarx::TimestampVariantPtr timeVar = getAttributeValue("startTime")->get<armarx::TimestampVariant>();
        return timeVar->toTime();
    }
}
