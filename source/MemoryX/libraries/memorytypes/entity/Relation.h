/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Core
* @author     Alexey Kozlov ( kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <MemoryX/interface/core/EntityBase.h>
#include <MemoryX/interface/memorytypes/MemoryEntities.h>

#include <MemoryX/core/entity/Entity.h>


namespace memoryx
{

    class Relation;
    using RelationPtr = IceInternal::Handle<Relation>;

    /**
     * Relation class represents a directed relation between entities.
     * It allows to specify the uncertainty of this relation as DoB probability.
     *
     */
    class Relation: public memoryx::RelationBase,
        public memoryx::Entity
    {
    public:
        /**
         * Constucts a new Relation
         *
         * @param name name of the relation (and actually its type, i.e. "isOn")
         * @param entities the list of entity refs between which this relation holds
         * @param sign whether the relation is known to be true or false
         * @param prob existence probability of relation
         */
        Relation(const std::string& name = "", const EntityRefList& entities = EntityRefList {}, bool sign = true, float prob = 1.f);

        /**
         * Retrieve the entities involved in this relation
         *
         * @return list of entity refs
         */
        EntityRefList getEntities(const ::Ice::Current& = Ice::emptyCurrent) const override;

        /**
         * set the entities involved in this relation
         *
         * @param entities list of entity refs
         */
        void setEntities(const EntityRefList& entities, const ::Ice::Current& = Ice::emptyCurrent) override;

        /**
         * Retrieve whether this relation is true or false
         *
         * @return the sign
         */
        bool getSign(const ::Ice::Current& = Ice::emptyCurrent) const override;

        /**
         * Set whether this relation is true or false
         *
         * @param sign the sign
         */
        void setSign(bool sign, const ::Ice::Current& = Ice::emptyCurrent) override;

        /**
         * Retrieve probability that this relation actually exists
         *
         * @return existence probability
         */
        ::Ice::Float getProb(const ::Ice::Current& = Ice::emptyCurrent) const override;

        /**
         * Set probability that this relation actually exists
         *
         * @prob existence probability
         */
        void setProb(::Ice::Float prob, const ::Ice::Current& = Ice::emptyCurrent) override;

        std::string getAttributes() const;

        void setAttributes(std::string const& attributes);

        std::string getSourceAttributes() const;

        void setSourceAttributes(std::string const& attributes);

        std::string getTargetAttributes() const;

        void setTargetAttributes(std::string const& attributes);

        // cloning
        Ice::ObjectPtr ice_clone() const override;
        RelationPtr clone(const Ice::Current& c = Ice::emptyCurrent) const;

    private:
        void output(std::ostream& stream) const;

    public:    // streaming operator
        friend std::ostream& operator<<(std::ostream& stream, const Relation& rhs)
        {
            rhs.output(stream);
            return stream;
        }

        friend std::ostream& operator<<(std::ostream& stream, const RelationPtr& rhs)
        {
            rhs->output(stream);
            return stream;
        }

        friend std::ostream& operator<<(std::ostream& stream, const RelationBasePtr& rhs)
        {
            stream << RelationPtr::dynamicCast(rhs);
            return stream;
        }
    };

}

