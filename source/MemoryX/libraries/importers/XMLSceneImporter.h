/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::XMLSceneImporter
* @author     (kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXCore/core/rapidxml/rapidxml.hpp>
#include <RobotAPI/libraries/core/Pose.h>

#include <MemoryX/interface/components/WorkingMemoryInterface.h>
#include <MemoryX/interface/components/LongtermMemoryInterface.h>

namespace memoryx
{
    enum LengthUnit
    {
        eMETER,
        eCENTIMETER,
        eMILLIMETER
    };

    class XMLSceneImporterPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        XMLSceneImporterPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineRequiredProperty<std::string>("SceneFile", "Name of scene XML file to import")
            .setCaseInsensitive(true);
            defineRequiredProperty<std::string>("SnapshotName", "Name of snapshot (=MongoDB collection) to load objects to")
            .setCaseInsensitive(true);
            defineOptionalProperty<LengthUnit>("TargetLengthUnit", eMETER, "Length unit to convert into (M, CM or MM)")
            .setCaseInsensitive(false)
            .map("M",  eMETER)
            .map("CM", eCENTIMETER)
            .map("MM", eMILLIMETER);
        }
    };

    /**
     * @brief The XMLSceneImporter class reads scene specification files and creates a snapshot with the specified name.

       An ObjectInstance is created for every <node> tag found in the XML description
       and added to the objectInstances memory segment.
       The contents of the objectInstances memory segment is then stored as a snapshot in the LongtermMemory under the
       identifier given by the SnapshotName property.

       The XML scene format is described next.
       Note that the unit attribute can be omitted from <position> tags in which case the default value "METER" is assumed.

    \code
    <scene formatVersion="1.0.0">
    <nodes>
    <node name="object_name">
      <position x="0.000000" y="0.000000" z="0.000000" unit"METER">
      </position>
      <?rotation qx="-0.707107" qy="0.707107" qz="-0.000000" qw="0.000000"/>
      <?scale x="0.001000" y="0.001000" z="0.001000"/>
      <?AABB local="0">
        <min x="-2102.074219" y="-654.052673" z="-1337.000122" />
        <max x="0.000000" y="0.000000" z="0.000000" />
      </AABB>
      <?entity name="object_name" meshFile="object_name.wrl"/>
    </node>
    ...
    <node name="object_name">
    </node>
    </nodes>
    </scene>
    \endcode

     */
    class ARMARXCOMPONENT_IMPORT_EXPORT XMLSceneImporter :
        virtual public armarx::Component
    {
    public:
        // inherited from Component
        std::string getDefaultName() const override
        {
            return "XMLSceneImporter";
        }
        void onInitComponent() override;
        void onConnectComponent() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return armarx::PropertyDefinitionsPtr(new XMLSceneImporterPropertyDefinitions(getConfigIdentifier()));
        }

    private:
        WorkingMemoryInterfacePrx memoryPrx;
        ObjectInstanceMemorySegmentBasePrx objectInstancesMemoryPrx;
        LongtermMemoryInterfacePrx longtermMemoryPrx;

        LengthUnit targetUnit;

        /**
         * @brief importXMLSnapshot reads the given scene description and creates entities for the objects to be stored in the snapshot
         * @param fileName the name/path of the scene.xml file to import
         */
        void importXMLSnapshot(const std::string& fileName);
        /**
         * @brief createObjectInstanceFromXML
         * @param xmlNode the current <node> tag representing an entity
         * @return ObjectInstanceBase
         */
        ObjectInstanceBasePtr createObjectInstanceFromXML(rapidxml::xml_node<>* xmlNode);

        /**
         * @brief positionFromXML extracts the <position> tag from the given \p xmlNode an turns it into a FramedPosition.
         * @param xmlNode extract the <position> tag from this node
         * @return FramedPosition instance containing the object rotation or a NullPointer if no rotation was specified
         */
        armarx::FramedPositionBasePtr positionFromXML(rapidxml::xml_node<>* xmlNode);

        /**
         * @brief scaleFactorFromPositionXML extracts the <unit> tag from the \p positionNode and calculates a scale factor
         * based on the value of the TargetLengthUnit property.
         * @param positionNode the XML node from which to extract the <unit> tag
         * @return scale factor which needs to be applied to the position node
         */
        float scaleFactorFromPositionXML(rapidxml::xml_node<>* positionNode);

        /**
         * @brief orientationFromXML extracts the <rotation> tag from the given \p xmlNode an turns it into a FramedOrientation.
         * @param xmlNode extract the <rotation> tag from this node
         * @return FramedOrientation instance containing the object rotation or a NullPointer if no rotation was specified
         */
        armarx::FramedOrientationBasePtr orientationFromXML(rapidxml::xml_node<>* xmlNode);
    };

}

