/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::PriorKnowledgeEditor
* @author     Alexey Kozlov (kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef ARMARX_COMPONENT_PriorKnowledgeImporter_PriorKnowledgeImporter_H

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <MemoryX/interface/components/PriorKnowledgeInterface.h>
#include <MemoryX/interface/components/CommonStorageInterface.h>

#include <MemoryX/core/entity/EntityAttribute.h>
#include <MemoryX/core/MongoSerializer.h>
#include <MemoryX/core/GridFileManager.h>

namespace memoryx
{

    class PriorKnowledgeImporterPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        PriorKnowledgeImporterPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("TaskName", "ImportFiles", "Task to perform: ImportFiles, AddParents, ConvertFormat, AddRelations")
            .setCaseInsensitive(true);
            defineRequiredProperty<std::string>("FilesDirectory", "Name of directory to load files from")
            .setCaseInsensitive(true);
            defineRequiredProperty<std::string>("FilesDbName", "Name of snapshot Mongo database to load files into")
            .setCaseInsensitive(true);
        }
    };


    /**
     * @brief The PriorKnowledgeImporter class is used to add object descriptions to the PriorKnowledge database.
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT PriorKnowledgeImporter :
        virtual public armarx::Component
    {
    public:

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return armarx::PropertyDefinitionsPtr(
                       new PriorKnowledgeImporterPropertyDefinitions(
                           getConfigIdentifier()));
        }

        // inherited from Component
        std::string getDefaultName() const override
        {
            return "PriorKnowledgeImporter";
        }
        void onInitComponent() override;
        void onConnectComponent() override;

        void importObjectClass(const std::string& ivFile, const std::string& className);
    private:
        PriorKnowledgeInterfacePrx memoryPrx;
        PersistentObjectClassSegmentBasePrx classesSegmentPrx;
        CommonStorageInterfacePrx dataBasePrx;

        MongoSerializerPtr dbSerializer;
        GridFileManagerPtr fileManager;

        std::string filesDBName;
        std::string filesDir;

        void importFiles();
        void addParents();
        void convertToNewFormat();
        void addRelations();
    };

}

#endif
