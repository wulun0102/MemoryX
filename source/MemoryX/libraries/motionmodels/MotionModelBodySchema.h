/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::MotionModelBodySchema
* @author     David Schiebener ( Schiebener at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once


#include "AbstractMotionModel.h"
#include <MemoryX/libraries/helpers/KinematicBezierMaps/kbm.h>
#include <MemoryX/interface/components/LongtermMemoryInterface.h>



namespace memoryx
{
    class MotionModelBodySchema: virtual public AbstractMotionModel
    {
    public:

        MotionModelBodySchema(armarx::RobotStateComponentInterfacePrx robotStateProxy, std::string handNodeName, LongtermMemoryInterfacePrx longtermMemory);
        ~MotionModelBodySchema();

        virtual AbstractMotionModel::EMotionModelType GetMotionModelType()
        {
            return AbstractMotionModel::eMotionModelBodySchema;
        }
        virtual void SetPoseAtLastLocalisation(const armarx::LinkedPoseBasePtr& poseAtLastLocalization, const armarx::PoseBasePtr& globalRobotPoseAtLastLocalization, const Ice::Current& c = Ice::emptyCurrent);

    protected:
        std::shared_ptr<memoryx::KBM> model;

        virtual armarx::LinkedPosePtr GetPredictedPoseInternal();
        virtual MultivariateNormalDistributionBasePtr getUncertaintyInternal();

        std::string handNodeName;
        unsigned int inputDim, outputDim;
        armarx::NameList parentNodeNames;
        LongtermMemoryInterfacePrx longtermMemory;
    };
    using MotionModelBodySchemaPtr = IceInternal::Handle<MotionModelBodySchema>;
}

