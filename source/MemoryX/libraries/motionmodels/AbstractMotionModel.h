/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::AbstractMotionModel
* @author     David Schiebener ( Schiebener at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/


#pragma once


#include <MemoryX/interface/workingmemory/MotionModel.h>
#include <MemoryX/core/entity/ProbabilityMeasures.h>

#include <RobotAPI/libraries/core/LinkedPose.h>

#include <ArmarXCore/observers/variant/TimestampVariant.h>

#include <mutex>

namespace memoryx
{

    class AbstractMotionModel : virtual public MotionModelInterface
    {
    public:
        enum EMotionModelType
        {
            eMotionModelStaticObject,
            eMotionModelRobotHand,
            eMotionModelAttachedToOtherObject,
            eMotionModelKBM
        };


        AbstractMotionModel(armarx::RobotStateComponentInterfacePrx robotStateProxy);


        armarx::LinkedPoseBasePtr getPredictedPose(const Ice::Current& = Ice::emptyCurrent) override;


        MultivariateNormalDistributionBasePtr getUncertainty(const Ice::Current& = Ice::emptyCurrent) override;


        void setPoseAtLastLocalisation(const armarx::LinkedPoseBasePtr& poseAtLastLocalization,
                                       const armarx::PoseBasePtr& globalRobotPoseAtLastLocalization,
                                       const MultivariateNormalDistributionBasePtr& uncertaintyAtLastLocalization,
                                       const Ice::Current& = Ice::emptyCurrent) override;


        armarx::LinkedPoseBasePtr getPoseAtLastLocalisation(const Ice::Current& = Ice::emptyCurrent) override;


        void savePredictedPoseAtStartOfCurrentLocalization(const Ice::Current& = Ice::emptyCurrent) override;


        armarx::LinkedPoseBasePtr getPredictedPoseAtStartOfCurrentLocalization(const Ice::Current& = Ice::emptyCurrent) override;


        MultivariateNormalDistributionBasePtr getUncertaintyAtStartOfCurrentLocalization(const Ice::Current& = Ice::emptyCurrent) override;


        // implement this method in your motion model!
        virtual EMotionModelType getMotionModelType() = 0;

        virtual std::string getMotionModelName();


        // returns the enum value of the motion model for a model name given as a string (e.g. coming from the PriorKnowledgeEditor)
        static EMotionModelType getMotionModelTypeByName(std::string motionModelName);


    protected:

        // implement these two methods in your motion model!
        virtual armarx::LinkedPosePtr getPredictedPoseInternal() = 0;
        virtual MultivariateNormalDistributionBasePtr getUncertaintyInternal() = 0;


        std::recursive_mutex motionPredictionLock;


        // for the object factory
        AbstractMotionModel();
        template <class BaseClass, class VariantClass>
        friend class GenericFactory;
    };
    using AbstractMotionModelPtr = IceInternal::Handle<AbstractMotionModel>;
}
