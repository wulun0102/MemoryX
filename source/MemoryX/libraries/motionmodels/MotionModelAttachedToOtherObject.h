/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::MotionModelAttachedToOtherObject
* @author     David Schiebener ( Schiebener at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/system/FactoryCollectionBase.h>
#include <ArmarXCore/observers/variant/ChannelRef.h>

#include "AbstractMotionModel.h"



namespace memoryx
{
    class MotionModelAttachedToOtherObject :
        virtual public AbstractMotionModel,
        virtual public MotionModelAttachedToOtherObjectBase
    {
    public:

        MotionModelAttachedToOtherObject(armarx::RobotStateComponentInterfacePrx robotStateProxy, armarx::ChannelRefPtr channelRefToObjectToWhichThisIsAttached);

        AbstractMotionModel::EMotionModelType getMotionModelType() override
        {
            return AbstractMotionModel::eMotionModelAttachedToOtherObject;
        }

        void setPoseAtLastLocalisation(const armarx::LinkedPoseBasePtr& poseAtLastLocalization, const armarx::PoseBasePtr& globalRobotPoseAtLastLocalization, const MultivariateNormalDistributionBasePtr& uncertaintyAtLastLocalization, const Ice::Current& c = Ice::emptyCurrent) override;

        void savePredictedPoseAtStartOfCurrentLocalization(const Ice::Current& c = Ice::emptyCurrent) override;
    protected:

        armarx::LinkedPosePtr getPredictedPoseInternal() override;
        MultivariateNormalDistributionBasePtr getUncertaintyInternal() override;

        // for the object factory
        template <class IceBaseClass, class DerivedClass>
        friend class armarx::GenericFactory;
        MotionModelAttachedToOtherObject() { }
    };
    using MotionModelAttachedToOtherObjectPtr = IceInternal::Handle<MotionModelAttachedToOtherObject>;

}

