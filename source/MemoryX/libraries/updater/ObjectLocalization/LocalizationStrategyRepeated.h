/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Core
* @author     Kai Welke <welke@kit.edu>
* @copyright  2012 Kai Welke
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once


#include <MemoryX/libraries/updater/ObjectLocalization/LocalizationJob.h>


#include <IceUtil/Time.h>

#include <ArmarXCore/core/time/TimeUtil.h>

namespace memoryx
{
    class LocalizationStrategyRepeated : public LocalizationJob
    {
    public:
        LocalizationStrategyRepeated(unsigned int cycleTimeMS)
        {
            started = false;
            this->cycleTimeMS = cycleTimeMS;
        }

        void start() override
        {
            startTime = armarx::TimeUtil::GetTime();
            started = true;
        }

        bool waiting() override
        {
            if (!started)
            {
                return true;
            }
            else
            {
                IceUtil::Time currentTime = armarx::TimeUtil::GetTime();
                IceUtil::Time diff = currentTime - startTime;

                if (diff.toMilliSeconds() > cycleTimeMS)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

    private:
        bool started;
        IceUtil::Time startTime;
        unsigned int cycleTimeMS;
    };

    using LocalizationStrategyRepeatedPtr = IceUtil::Handle<LocalizationStrategyRepeated>;
}

