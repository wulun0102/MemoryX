/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::WorkingMemory
* @author     ALexey Kozlov ( kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <Ice/Ice.h>
#include <ArmarXCore/core/system/FactoryCollectionBase.h>

#include <MemoryX/libraries/updater/ObjectLocalization/LocalizationQuery.h>

namespace memoryx::ObjectFactories
{
    class MemoryXUpdaterObjectFactories : public armarx::FactoryCollectionBase
    {
    public:
        armarx::ObjectFactoryMap getFactories() override
        {
            armarx::ObjectFactoryMap map;
            add<LocalizationQueryBase, LocalizationQuery>(map);
            return map;
        }
        static bool FactoryRegistrationDummyVar;
    };
    const armarx::FactoryCollectionBaseCleanUp MemoryXUpdaterObjectFactoriesVar = armarx::FactoryCollectionBase::addToPreregistration(new MemoryXUpdaterObjectFactories());
}
