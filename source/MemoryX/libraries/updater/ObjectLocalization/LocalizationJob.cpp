/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Core
* @author     Kai Welke <welke@kit.edu>
* @copyright  2012 Kai Welke
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "LocalizationJob.h"
#include "LocalizationQuery.h"
#include <MemoryX/libraries/helpers/ObjectRecognitionHelpers/ObjectRecognitionWrapper.h>

namespace memoryx
{
    void LocalizationJob::init(IceInternal::Handle<LocalizationQuery> query, std::string recognitionMethod, const std::vector<std::string>& classNames)
    {
        this->localizationQuery = LocalizationQueryPtr::dynamicCast(query->ice_clone());
        this->classNames = classNames;
        this->classNamesToBeLocalized = classNames;
        this->recognitionMethod = recognitionMethod;
    }

    std::string LocalizationJob::getRecognitionMethod()
    {
        return recognitionMethod;
    }

    std::vector<std::string> LocalizationJob::getAllClassNames()
    {
        return classNames;
    }

    std::vector<std::string> LocalizationJob::getClassNamesToBeLocalized()
    {
        return classNamesToBeLocalized;
    }

    void LocalizationJob::setClassNamesToBeLocalized(const std::vector<std::string>& classNamesToBeLocalized)
    {
        this->classNamesToBeLocalized = classNamesToBeLocalized;
    }

    std::string LocalizationJob::getQueryName()
    {
        return localizationQuery->queryName;
    }

    IceInternal::Handle<LocalizationQuery> LocalizationJob::getQuery()
    {
        return localizationQuery;
    }

    void LocalizationJob::setFinished()
    {
        localizationQuery->setJobFinished(recognitionMethod);
    }
}
