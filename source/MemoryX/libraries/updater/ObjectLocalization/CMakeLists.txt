armarx_set_target("Memory Core Library: MemoryXUpdater")

set(LIB_NAME       MemoryXUpdater)

set(LIBS
        MemoryXWorkingMemory MemoryXMemoryTypes
        MemoryXObjectRecognitionHelpers MemoryXMotionModels MemoryXObjectInstanceMemorySegment
)

set(LIB_FILES
    ObjectLocalizationMemoryUpdater.cpp
    LocalizationQuery.cpp
    LocalizationJob.cpp)

set(LIB_HEADERS
    ObjectLocalizationMemoryUpdater.h
    LocalizationQuery.h
    LocalizationJob.h
    LocalizationJobContainer.h
    LocalizationStrategyOnce.h
    LocalizationStrategyRepeated.h
    MemoryXUpdaterObjectFactories.h)

armarx_add_library("${LIB_NAME}"  "${LIB_FILES}" "${LIB_HEADERS}" "${LIBS}")

target_include_directories("${LIB_NAME}" PUBLIC ${IVT_INCLUDE_DIRS})
