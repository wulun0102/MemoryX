/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Core
* @author     Kai Welke <welke@kit.edu>
* @copyright  2012 Kai Welke
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <string>
#include <ArmarXCore/core/system/ImportExport.h>
#include <Ice/Ice.h>
#include <IceUtil/Handle.h>

#include <MemoryX/libraries/memorytypes/entity/ObjectClass.h>
#include <MemoryX/interface/observers/ObjectMemoryObserverInterface.h>

namespace memoryx
{
    class LocalizationQuery;

    /**
     * @brief The LocalizationJob class is the description of the basic workload executed by ObjectLocalizationMemoryUpdater
     *
     * It describes which object classes to localize, which method to use, and which LocalizationQuery instance the job belongs to.
     */
    class LocalizationJob :
        public Ice::LocalObject
    {
    public:
        void init(IceInternal::Handle<LocalizationQuery> query, std::string recognitionMethod, const std::vector<std::string>& classNames);

        virtual void start() {}
        virtual bool waiting() = 0;

        std::string getRecognitionMethod();
        std::vector<std::string> getAllClassNames();
        std::vector<std::string> getClassNamesToBeLocalized();
        void setClassNamesToBeLocalized(const std::vector<std::string>& classNamesToBeLocalized);
        std::string getQueryName();
        IceInternal::Handle<LocalizationQuery> getQuery();

        void setFinished();

    private:
        IceInternal::Handle<LocalizationQuery> localizationQuery;
        std::vector<std::string> classNames;
        std::vector<std::string> classNamesToBeLocalized;
        std::string recognitionMethod;
    };

    using LocalizationJobPtr = IceUtil::Handle<LocalizationJob>;
    using LocalizationJobList = std::vector<LocalizationJobPtr>;
}

