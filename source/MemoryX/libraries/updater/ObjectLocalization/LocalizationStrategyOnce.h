/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Core
* @author     Kai Welke <welke@kit.edu>
* @copyright  2012 Kai Welke
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once


#include <MemoryX/libraries/memorytypes/entity/ObjectClass.h>
#include <MemoryX/libraries/updater/ObjectLocalization/LocalizationJob.h>

namespace memoryx
{
    class LocalizationStrategyOnce : public LocalizationJob
    {
    public:
        LocalizationStrategyOnce()
        {
            localized = false;
        }

        void start() override
        {
            localized = true;
        }

        bool waiting() override
        {
            return !localized;
        }

    private:
        bool localized;
    };

    using LocalizationStrategyOncePtr = IceUtil::Handle<LocalizationStrategyOnce>;
}

