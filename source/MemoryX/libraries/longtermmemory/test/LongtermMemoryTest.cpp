/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::LongtermMemory
* @author     Mirko Waechter <waechter at kit dot edu>
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE MemoryX::SEC
#define ARMARX_BOOST_TEST
#include <MemoryX/Test.h>



#include <MemoryX/core/MongoTestHelper.h>
#include <ArmarXCore/util/json/JSONObject.h>
#include <MemoryX/libraries/memorytypes/entity/Oac.h>
#include <MemoryX/libraries/memorytypes/entity/Relation.h>

#include <ArmarXCore/core/CoreObjectFactories.h>
#include <ArmarXCore/core/ArmarXManager.h>
#include <ArmarXCore/core/exceptions/Exception.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>

#include "LongtermMemoryEnvironment.h"

using namespace memoryx;


BOOST_AUTO_TEST_CASE(testObjectClassCompare)
{
    LongtermMemoryEnvironmentPtr env(new LongtermMemoryEnvironment("LTMtestenv"));

    EntityRefPtr cupRef = new EntityRef("cup");
    EntityRefPtr tableRef = new EntityRef("table");

    RelationPtr relation =  new Relation("ontop", {cupRef, tableRef});
    auto id = env->wm->getRelationsSegment()->addEntity(relation);
    auto queryResult = env->wm->getRelationsSegment()->getRelationByAttrValues(relation->getName(), relation->getEntities(), relation->getSign());
    BOOST_CHECK(queryResult);
    ARMARX_IMPORTANT_S << VAROUT(id);
    auto snapshot = env->ltm->getWorkingMemorySnapshotListSegment()->createSubsetSnapshot("testSnapshot", env->wm,
    {id});
    ARMARX_IMPORTANT_S << "segments: " << snapshot->getSegmentNames();
    ARMARX_IMPORTANT_S << "relations: " << snapshot->getSegment("objectRelations")->getAllEntityIds();
    BOOST_CHECK_EQUAL(id, *snapshot->getSegment("objectRelations")->getAllEntityIds().begin());
    snapshot = env->ltm->getWorkingMemorySnapshotListSegment()->createSubsetSnapshot("testSnapshot2", env->wm,
    {"5"});
    ARMARX_IMPORTANT_S << "relations: " << snapshot->getSegment("objectRelations")->getAllEntityIds();
    BOOST_CHECK_EQUAL(0, snapshot->getSegment("objectRelations")->getAllEntityIds().size());
}


