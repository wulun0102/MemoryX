/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::LongtermMemory
* @author     Alexey Kozlov ( kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

// core
#include <ArmarXCore/interface/core/Log.h>
#include <ArmarXCore/core/CoreObjectFactories.h>
#include <ArmarXCore/observers/ObserverObjectFactories.h>

// memoryx interface
#include <MemoryX/interface/core/EntityBase.h>
#include <MemoryX/interface/memorytypes/MemoryEntities.h>

#include <MemoryX/core/MemoryXCoreObjectFactories.h>
#include <MemoryX/core/memory/PersistentEntitySegment.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>

#include "AbstractLongtermMemory.h"

namespace memoryx
{
    void AbstractLongtermMemory::onInitComponent()
    {
        usingProxy("CommonStorage");
        onInitLongtermMemory();
    }


    void AbstractLongtermMemory::onConnectComponent()
    {
        ARMARX_INFO << "Starting MemoryX::LongtermMemory";

        ic = getIceManager()->getCommunicator();


        storagePrx = getProxy<CommonStorageInterfacePrx>("CommonStorage");
        dbName = getProperty<std::string>("DatabaseName").getValue();

        onConnectLongtermMemory();
    }

    void AbstractLongtermMemory::clear(const ::Ice::Current&)
    {
        ARMARX_ERROR << "LongtermMemory::clear() not implemented yet!";
    }
}
