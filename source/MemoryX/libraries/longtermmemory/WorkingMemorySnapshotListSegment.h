/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::LongtermMemory
* @author     Alexey Kozlov ( kozlov at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <MemoryX/interface/components/LongtermMemoryInterface.h>

#include <MemoryX/core/MongoSerializer.h>

namespace memoryx
{

    using SnapshotMap = std::map<Ice::Identity, WorkingMemorySnapshotInterfacePtr>;

    class WorkingMemorySnapshot;

    using WorkingMemorySnapshotPtr = IceUtil::Handle<WorkingMemorySnapshot>;

    class WorkingMemorySnapshotListSegment:
        virtual public WorkingMemorySnapshotListSegmentBase
    {
    public:
        WorkingMemorySnapshotListSegment(const DatabaseInterfacePrx& databasePrx, const CollectionInterfacePrx& collection, Ice::CommunicatorPtr ic);
        ~WorkingMemorySnapshotListSegment() override;

        WorkingMemorySnapshotInterfacePrx createSnapshot(const ::std::string& name, const AbstractWorkingMemoryInterfacePrx& workingMemory,
                const ::Ice::Current& = Ice::emptyCurrent) override;
        WorkingMemorySnapshotInterfacePrx createSubsetSnapshot(const ::std::string& name, const AbstractWorkingMemoryInterfacePrx& workingMemory, const Ice::StringSeq& entityIdList,
                const ::Ice::Current& c = Ice::emptyCurrent) override;
        WorkingMemorySnapshotInterfacePrx openSnapshot(const ::std::string& name, const ::Ice::Current& = Ice::emptyCurrent) override;
        void closeSnapshot(const WorkingMemorySnapshotInterfacePrx& snapshot, const ::Ice::Current& = Ice::emptyCurrent) override;
        void loadSnapshot(const ::std::string& name, const AbstractWorkingMemoryInterfacePrx& workingMemory,
                          const ::Ice::Current& = Ice::emptyCurrent) override;
        bool removeSnapshot(const ::std::string& name, const ::Ice::Current& = Ice::emptyCurrent) override;
        NameList getSnapshotNames(const ::Ice::Current& = Ice::emptyCurrent) const override;

        Ice::Int size(const ::Ice::Current& = Ice::emptyCurrent) const override;
        void clear(const ::Ice::Current& = Ice::emptyCurrent) override;
        void print(const ::Ice::Current& = Ice::emptyCurrent) const override;

    protected:
        // set segment name
        void setSegmentName(const std::string& segmentName, const ::Ice::Current& = Ice::emptyCurrent) override
        {
            this->segmentName = segmentName;
        }

        void setParentMemory(const MemoryInterfacePtr& memory, const Ice::Current&) override
        {
            this->parentMemory = memory;
        }



    private:
        MemoryInterfacePtr parentMemory;
        DatabaseInterfacePrx databasePrx;
        CollectionInterfacePrx snapshotListCollection;
        SnapshotMap openedSnapshots;
        MongoSerializerPtr dbSerializer;

        Ice::CommunicatorPtr ic;

        WorkingMemorySnapshotInterfacePrx createSnapshotProxy(const WorkingMemorySnapshotPtr& snapshot, const Ice::Current& c);
        WorkingMemorySnapshotPtr findSnapshot(const std::string& name);
        std::string segmentName;

        // AbstractMemorySegment interface
    public:
        Ice::Identity getIceId(const Ice::Current&) const override;
    };

    using WorkingMemorySnapshotListSegmentPtr = IceUtil::Handle<WorkingMemorySnapshotListSegment>;

}

