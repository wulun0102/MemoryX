/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * @package    ArmarX::RobotAPI
 * @author     Raphael Grimm <raphael dot grimm at kit dot edu>
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// ArmarX
#include <ArmarXCore/core/Component.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>
#include <RobotAPI/libraries/core/FramedPose.h>

#include <MemoryX/components/PriorKnowledge/PriorKnowledge.h>
#include <MemoryX/interface/gui/GraphVisualizerInterface.h>

//qt
#include <QMainWindow>
#include <QDialog>
#include <QGraphicsScene>
#include <QGraphicsLineItem>
#include <QGraphicsEllipseItem>
#include <QMouseEvent>
#include <MemoryX/interface/components/GraphNodePoseResolverInterface.h>
//std
#include <string>
#include <map>
#include <vector>
#include <tuple>

//boost



#include <MemoryX/gui-plugins/GraphVisualizerPlugin/ui_GraphVisualizerGuiPlugin.h>
#include "GraphVisualizerConfigDialog.h"

namespace memoryx
{
    class GraphVisualizerGraphicsEllipseItem;
    class GraphVisualizerGraphicsLineItem;

    class GraphVisualizerConfigDialog;
    class GraphVisualizerWidget;

    class MouseEventProcessor;

    /**
     * @class GraphVisualizerGuiPlugin
     * @brief This plugin provides a widget used to visualize an undirected graph and draw it to a debug layer.
     * @see GraphVisualizerWidget
     */
    class GraphVisualizerGuiPlugin :
        public ::armarx::ArmarXGuiPlugin
    {
        Q_OBJECT
        Q_INTERFACES(ArmarXGuiInterface)
        Q_PLUGIN_METADATA(IID "ArmarXGuiInterface/1.00")
    public:
        GraphVisualizerGuiPlugin();
        QString getPluginName() override
        {
            return "GraphVisualizerGuiPlugin";
        }
    };


    /**
      \page MemoryX-GuiPlugins-GraphVisualizer GraphVisualizer
     \brief A widget used to visualize an undirected graph and draw it to a debug layer.

     This widget implements the ice interface GraphVisualizerInterface and therefore
     can be provided with an undirected graph.

     The graph is drawn to a debug layer and a scene located in the widget.
     The widget has tables containing information about the nodes and edges.
     Nodes on the debug layer are visualized as coordinate systems.
     Nodes on the scene are visualized as circles.
     Edges on the debug layer and scene are visualized with lines.

     Nodes can be added with
     @code{.cpp}
     addNode(const ::armarx::FramedPoseBasePtr& p)
     @endcode
     and are identified with the string stored in p.

     Edges can be added with
     @code{.cpp}
     addEdge(const ::std::string& fst, const ::std::string& snd)
     @endcode
     and are identified with {fst,snd}.
     fst and snd have to be the names of already existing nodes.
     The order of fst and snd does not matter.

     The existence can be checked with the following methods:
     @code{.cpp}
     hasNode(const ::std::string& name)
     hasEdge(const ::std::string& fst, const ::std::string& snd)
     @endcode

     All edges (the whole graph) can be deleted with:
     @code{.cpp}
     clearEdges()
     clearGraph()
     @endcode

     If the graph has to be redrawn to the debug layer, use
     @code{.cpp}
     redraw()
     @endcode

     Edges and nodes have four states: {selected, not selected}X{highlighted, not highlighted}

     Selection affects the width of lines and the size of nodes. Selected lines are thicker and nodes have an increased size.
     Selection can be toggled by double clicking the element in the table or the scene.

     Highlighting affects the color. If not highlighted, elements are blue. If highlighted, elements are green.
     (nodes drawn on a debug layer can't change color)
     The highlight can be set and cleared with the functions:
     @code{.cpp}
     highlightNode(const ::std::string& name, bool highlighted)
     highlightEdge(const ::std::string& fst, const ::std::string& snd, bool highlighted)
     resetHilight()
     @endcode


     The graph used in the following examples can be created with the following code:
     @code{.cpp}
     static ::armarx::FramedPosePtr table  {new ::armarx::FramedPose{Eigen::Vector3f{3400.f,7300.f,1000.f}, "table"  }};
     static ::armarx::FramedPosePtr fridge {new ::armarx::FramedPose{Eigen::Vector3f{2150.f,7750.f,1000.f}, "fridge" }};
     static ::armarx::FramedPosePtr sink   {new ::armarx::FramedPose{Eigen::Vector3f{2500.f,9700.f,1000.f}, "sink"   }};
     static ::armarx::FramedPosePtr hub2   {new ::armarx::FramedPose{Eigen::Vector3f{3750.f,5150.f,5000.f}, "hub2"   }};
     static ::armarx::FramedPosePtr hub1   {new ::armarx::FramedPose{Eigen::Vector3f{2900.f,8000.f,1000.f}, "hub1"   }};
     static ::armarx::FramedPosePtr hub3   {new ::armarx::FramedPose{Eigen::Vector3f{3400.f,2200.f,1000.f}, "hub3"   }};
     static ::armarx::FramedPosePtr hub4   {new ::armarx::FramedPose{Eigen::Vector3f{1900.f,3000.f,1000.f}, "hub4"   }};
     static ::armarx::FramedPosePtr counter{new ::armarx::FramedPose{Eigen::Vector3f{1890.f,4050.f,1000.f}, "counter"}};

     //prx is a proxy passing the commands to the plugin
     //add nodes
     prx->addNode(table);
     prx->addNode(fridge);
     prx->addNode(sink);
     prx->addNode(hub2);
     prx->addNode(hub1);
     prx->addNode(hub3);
     prx->addNode(hub4);
     prx->addNode(counter);

     //add edges
     prx->addEdge("hub1","hub2");
     prx->addEdge("hub1","table");
     prx->addEdge("hub1","sink");
     prx->addEdge("hub1","fridge");
     prx->addEdge("hub2","hub3");
     prx->addEdge("hub3","hub4");
     prx->addEdge("hub4","counter");

     //highlight a node and an edge
     prx->highlightEdge("hub2","hub3");
     prx->highlightNode("table");
     @endcode

     @image html GraphVisualizerGuiPlugin_ConfigDialog.png "The config dialog for the plugin." width=300px
     You can set the topic of the used debug drawer and the used debug layer.

     @image html GraphVisualizerGuiPlugin_Simulation_800.png "The graph drawn to the debug layer." width=300px
     @image html GraphVisualizerGuiPlugin_Widget_800.png "The plugin's ui." width=300px

     The ui has 5 sections
          -# Display options for the graph.
              - a. Rotate the graph clockwise
              - b. Rotate the graph counter clockwise
              - c. Zoom factor for the graph
              - d. Rotate and zoom the graph to display most of it. (The rotation is a multiple of pi/4)
          -# Is the scene containing the graph
          -# The table of nodes.
          -# The table of edges.
          -# Triggers a repaint for the debug layer.

          - A) Shows a highlighted and selected node
          - B) Shows a selected edge
          - C) Shows a highlighted edge
          - D) Shows a node. (no highlight or selection)
          - E) Shows the tool tip of an edge.

     @see GraphVisualizerGuiPlugin
    */

    /**
     * @brief The GraphVisualizerWidget class
     */
    class GraphVisualizerWidget :
        public armarx::ArmarXComponentWidgetControllerTemplate<GraphVisualizerWidget>,
        public GraphVisualizerInterface
    {
        Q_OBJECT
        friend class MouseEventProcessor;
    public:

        /**
         * @brief The type of node ids. (This type implies the node exists)
         */
        using NodeId = const std::string;

        /**
         * @brief The type of edge ids. (This type implies the edge exists)
         */
        using EdgeId = const std::pair<const std::string, const std::string>;


        GraphVisualizerWidget();
        ~GraphVisualizerWidget() override;

        // inherited from Component
        void onInitComponent() override;
        void onConnectComponent() override;
        void onExitComponent() override;

        // inherited of ArmarXWidget
        static QString GetWidgetName()
        {
            return "MemoryX.GraphVisualizerGUI";
        }
        static QIcon GetWidgetIcon()
        {
            return QIcon {"://icons/graph_visu.svg"};
        }

        QPointer<QDialog> getConfigDialog(QWidget* parent = 0) override;
        void loadSettings(QSettings* settings) override;
        void saveSettings(QSettings* settings) override;

        //because the above load/save functions are *obviously* for "Save/Load Gui Config." :/
        virtual void loadAutomaticSettings();
        virtual void saveAutomaticSettings();

        void configured() override;

        // slice interface implementation
        bool hasEdge(const std::string& node1, const std::string& node2, const Ice::Current&  = Ice::emptyCurrent) override
        {
            return (edges.find(toEdge(node1, node2)) != edges.end());
        }

        bool hasNode(const std::string& id, const Ice::Current&  = Ice::emptyCurrent) override
        {
            return (nodes.find(id) != nodes.end());
        }

    public slots:
        // slice interface implementation
        void addEdge(const std::string& node1Id, const std::string& node2Id, const Ice::Current&  = Ice::emptyCurrent) override;
        void addNode(const GraphNodeBasePtr& node, const Ice::Current&  = Ice::emptyCurrent) override;

        void highlightEdge(const std::string& node1Id, const std::string& node2Id, bool highlighted = true, const Ice::Current&  = Ice::emptyCurrent) override;
        void highlightNode(const std::string& nodeId, bool highlighted = true, const Ice::Current&  = Ice::emptyCurrent) override;

        void clearEdges(const Ice::Current&  = Ice::emptyCurrent) override;
        void clearGraph(const Ice::Current&  = Ice::emptyCurrent) override;

        void resetHighlight(const Ice::Current&  = Ice::emptyCurrent) override;

        void redraw(const Ice::Current&  = Ice::emptyCurrent) override;
        void refreshGraph();

        void tableWidgetNodesCustomContextMenu(QPoint pos);
        void tableWidgetEdgesCustomContextMenu(QPoint pos);

    protected:
        /**
         * @brief Contains the ui.
         */
        Ui::GraphVisualizerGuiPlugin ui;

    private slots:
        /**
         * @brief add kitchen graph (H2T Armar3a robot kitchen)
         */
        void addKitchenGraph();

        void selectedSceneChanged(int i);
        void updateSceneList();

        void drawScene();

        /**
         * @brief Toggles the double clicked node's selection state.
         * @param row Identifies the node.
         */
        void nodeTableDoubleClicked(int row, int);
        /**
         * @brief Toggles the double clicked edge's selection state.
         * @param row Identifies the edge.
         */
        void edgeTableDoubleClicked(int row, int);

        /**
         * @brief Toggles the double clicked node's selection state.
         * @param id Identifies the node.
         */
        void nodeDoubleClicked(NodeId id);

        /**
         * @brief Toggles the double clicked edge's selection state.
         * @param id Identifies the edge.
         */
        void edgeDoubleClicked(EdgeId id);

        /**
         * @brief Rotates the view clockwise.
         *
         * Stepsize set by VIEW_ROTATE_STEP_SIZE_CC in GraphVisualizerGuiPlugin.cpp
         */
        void viewRotatedClock();

        /**
         * @brief Rotates the view counter clockwise.
         *
         * Stepsize set by VIEW_ROTATE_STEP_SIZE_CC in GraphVisualizerGuiPlugin.cpp
         */
        void viewRotatedCounterClock();

        /**
         * @brief Applies the current transforamtion to the view.
         */
        void transformView();

        /**
         * @brief Adjusts the view's zoom and rotation to display most of the graph.
         */
        void adjustView();

        bool addNewEdge(const std::string& from, const std::string& to);
        void addNewEdgeBoth();
        void addNewEdgeStartEnd();
        void addNewEdgeEndStart();

        void addNewGraphNode();
        void editGraphNode();

    private:
        /**
         * @brief The NodeData struct holds data required for the node.
         * The name is stored in the key used in the map nodes.
         */
        struct NodeData
        {
            /**
             * @brief The Entity of the graph segment this struct represents
             */
            GraphNodeBasePtr node;

            /**
             * @brief The pose drawn to debugDrawer.
             */
            armarx::FramedPosePtr pose;

            /**
             * @brief The ellipse in the scene.
             */
            QGraphicsEllipseItem* graphicsItem;

            /**
             * @brief The row in the table tableWidgetNodes.
             */
            int tableWidgetNodesIndex;

            /**
             * @brief Whether the node is highlighted.
             */
            bool highlighted;
        };

        /**
         * @brief The EdgeData struct holds data required for the edge.
         * The name is stored in the key used in the map edges.
         */
        struct EdgeData
        {
            /**
             * @brief The line in the scene.
             */
            QGraphicsLineItem* graphicsItem;
            /**
             * @brief The row in the table tableWidgetEdges.
             */
            int tableWidgetEdgesIndex;

            /**
             * @brief Whether the edge is highlighted.
             */
            bool highlighted;
        };


        /**
         * @brief Returns the EdgeId corresponding to two nodes.
         * @param node1 First node id.
         * @param node2 Second node id.
         * @return The EdgeId corresponding to two nodes.
         */
        static EdgeId toEdge(const std::string& node1, const std::string& node2)
        {
            return EdgeId {node1, node2};
        }

        /**
         * @brief Updates an edge.
         * @param The edge to update.
         */
        void updateEdge(const EdgeId& id);

        /**
         * @brief Updates a node.
         * @param The node to update.
         */
        void updateNode(const NodeId& id);

        void setEditFields(const NodeData& node);

        /**
         * @brief The topic name used by debugDrawer.
         */
        std::string debugDrawerTopicName;

        /**
         * @brief Used to draw onto debug layers.
         */
        ::armarx::DebugDrawerInterfacePrx debugDrawer;

        /**
         * @brief The config dialog.
         */
        QPointer<GraphVisualizerConfigDialog> dialog;

        float getYawAngle(const armarx::PoseBasePtr& pose) const;

        /**
         * @brief The scene displayed in the widget.
         *
         * For y coordinates -pos->y is used to mirror the scene on the y axis.
         * If pos->y would be used the graph displayed in the scene would not
         * match the graph drawn to the debug layer.
         */
        QPointer<QGraphicsScene> scene;

        /**
         * @brief The nodes.
         */
        std::map<std::string, NodeData> nodes;

        /**
         * @brief The edges.
         */
        std::map<EdgeId, EdgeData> edges;

        /**
         * @brief The view's rotation angle.
         */
        qreal viewAngle;

        /**
         * @brief The layer to draw on.
         */
        std::string debugDrawerLayerName;

        bool editStartNodeNext;

        std::string priorKnowledgeProxyName;
        memoryx::PriorKnowledgeInterfacePrx priorKnowledgePrx;
        memoryx::GraphNodePoseResolverInterfacePrx gnpr;
        memoryx::GraphMemorySegmentBasePrx graphSeg;
        QSettings settings;
        QString lastSelectedSceneName;

        /**
         * selectScene(): private function called when button load is pushed, and calls the function loadScene()
         */
        void selectScene();

        /**
         * @brief loadScene Private function that parses XML file to load a scene
         * @param xmlFile
         */
        void loadScene(const std::string& xmlFile);


        friend class GraphVisualizerGraphicsEllipseItem;
        friend class GraphVisualizerGraphicsLineItem;
    };
    /**
     * @brief Boost shared pointer to a GraphVisualizerWidget.
     */
    using GraphVisualizerGuiPluginPtr = std::shared_ptr<GraphVisualizerWidget>;


    /**
     * @brief Required to override the double click event. This is required to toggle the select state.
     */
    class GraphVisualizerGraphicsEllipseItem: public QGraphicsEllipseItem
    {
    public:
        using NodeId = GraphVisualizerWidget::NodeId;

        GraphVisualizerGraphicsEllipseItem(GraphVisualizerWidget& visuWidget, NodeId name, qreal x, qreal y, qreal width, qreal height, QGraphicsItem* parent = nullptr):
            QGraphicsEllipseItem {x, y, width, height, parent},
            id {name},
            parentVisuWidget(visuWidget)
        {
        }

        ~GraphVisualizerGraphicsEllipseItem() override
        {
        }
    protected:
        void mouseDoubleClickEvent(QGraphicsSceneMouseEvent*) override
        {
            parentVisuWidget.nodeDoubleClicked(id);
        }
    private:
        /**
         * @brief Required to identify the element.
         */
        const NodeId id;

        /**
         * @brief Required to call nodeDoubleClicked on it. (This class is no QObject so it does not support signals)
         */
        GraphVisualizerWidget& parentVisuWidget;
    };

    /**
     * @brief Required to override the double click event. This is required to toggle the select state.
     */
    class GraphVisualizerGraphicsLineItem: public QGraphicsLineItem
    {
    public:
        using EdgeId = GraphVisualizerWidget::EdgeId;

        GraphVisualizerGraphicsLineItem(GraphVisualizerWidget& visuWidget, EdgeId name, qreal x1, qreal y1, qreal x2, qreal y2, QGraphicsItem* parent = 0):
            QGraphicsLineItem {x1, y1, x2, y2, parent},
            id {name},
            parentVisuWidget(visuWidget)
        {
        }

        ~GraphVisualizerGraphicsLineItem() override
        {
        }
    signals:
    protected:
        void mouseDoubleClickEvent(QGraphicsSceneMouseEvent*) override
        {
            parentVisuWidget.edgeDoubleClicked(id);
        }
    private:
        /**
         * @brief Required to identify the element.
         */
        const EdgeId id;

        /**
         * @brief Required to call edgeDoubleClicked on it. (This class is no QObject so it does not support signals)
         */
        GraphVisualizerWidget& parentVisuWidget;
    };

    class MouseEventProcessor : public QObject
    {
        Q_OBJECT
    public:
        MouseEventProcessor(GraphVisualizerWidget* gvw) : gvw(gvw) {}
    protected:
        GraphVisualizerWidget* gvw;
        bool eventFilter(QObject* obj, QEvent* event) override;
    };
}

