/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SnapshotControlWidgetController
 * @author     Manfred Kroehnert ( Manfred dot Kroehnert at kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <MemoryX/gui-plugins/SnapshotControl/ui_SnapshotControlWidget.h>

#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <MemoryX/interface/components/WorkingMemoryInterface.h>
#include <MemoryX/interface/components/LongtermMemoryInterface.h>

namespace armarx
{
    /**
    \page MemoryX-GuiPlugins-SnapshotControl SnapshotGui
    \brief SnapshotControlWidgetController provides a graphical interface for loading, storing, and removing Memory Snapshots

    \image html SnapshotControlGui.png

    A list of all available Snapshots will be fetched after a connection to LongtermMemory and WorkingMemory has been established.
    These Snapshot names are presented in a drop-down list and the selected entry
    can either be loaded into WorkingMemory or removed from the memory/database.

    Additionally, it is possible to save the current content of the WorkingMemory under a new Snapshot name.
    The new name is provided via the text input field and "Snapshot_" is prepended to the name if it has been omitted.

    SnapshotControl API Documentation \ref SnapshotControlWidgetController
    \see SnapshotControlGuiPlugin
    */

    /**
     * \class SnapshotControlWidgetController
     * \brief SnapshotControlWidgetController provides a graphical interface for loading, storing, and removing Memory Snapshots
     * \see SnapshotControlGuiPlugin
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        SnapshotControlWidgetController:
        public ArmarXComponentWidgetControllerTemplate<SnapshotControlWidgetController>
    {
        Q_OBJECT

    public:
        /**
         * Controller Constructor
         */
        explicit SnapshotControlWidgetController();

        /**
         * Controller destructor
         */
        ~SnapshotControlWidgetController() override;

        /**
         * @see ArmarXWidgetController::loadSettings()
         */
        void loadSettings(QSettings* settings) override;

        /**
         * @see ArmarXWidgetController::saveSettings()
         */
        void saveSettings(QSettings* settings) override;

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        static QString GetWidgetName()
        {
            return "MemoryX.SnapshotControl";
        }

        /**
         * @see armarx::Component::onInitComponent()
         *
         * Initializes waiting for proxies of LongtermMemory and WorkingMemory.
         */
        void onInitComponent() override;

        /**
         * @see armarx::Component::onConnectComponent()
         *
         * Fetches proxies and the list of all available snapshots.
         * Enables the main widget.
         */
        void onConnectComponent() override;

        /**
         * @see armarx::Component::onDisconnectComponent()
         *
         * Disables the main widget if the connection has been lost.
         */
        void onDisconnectComponent() override;

    public slots:
        /* QT slot declarations */
        /**
         * @brief loadSnapshotNames fetches the list of available Snapshot names from LongtermMemory
         */
        void loadSnapshotNames();

        /**
         * @brief loadSnapshot loads the Snapshot currently selected in the dropdown list into WorkingMemory
         */
        void loadSnapshot();
        /**
         * @brief removeSnapshot removes the Snapshot currently selected in the dropdown list from LongtermMemory
         */
        void removeSnapshot();
        /**
         * @brief saveSnapshot saves the current content of WorkingMemory in LongtermMemory under the name provided in the text input field
         */
        void saveSnapshot();
    private slots:
        /**
         * @brief snapshotNameChanged is called when text is typed into the input field (en-/disables the save button)
         * @param snapshotName
         */
        void snapshotNameChanged(const QString& snapshotName);
    signals:
        /* QT signal declarations */

    private:
        /**
         * @brief connectSlots does the initial wiring of signals to slots
         */
        void connectSlots();
        /**
         * Widget Form
         */
        Ui::SnapshotControlWidget widget;

        /**
         * @brief workingMemoryPrx proxy to WorkingMemory
         */
        memoryx::WorkingMemoryInterfacePrx workingMemoryPrx;
        /**
         * @brief longtermMemoryPrx proxy to LongtermMemory
         */
        memoryx::LongtermMemoryInterfacePrx longtermMemoryPrx;

        /**
         * @brief LONGTERM_SNAPSHOT_PREFIX is set to "Snapshot_" and needs to be the prefix of each Snapshot name
         */
        const std::string LONGTERM_SNAPSHOT_PREFIX;
    };
}

