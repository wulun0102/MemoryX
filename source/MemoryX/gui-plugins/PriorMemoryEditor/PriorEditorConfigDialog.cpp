/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::MemoryX::PriorMemoryEditorPlugin
* @author     Alexey Kozlov (kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "PriorEditorConfigDialog.h"
#include <MemoryX/gui-plugins/PriorMemoryEditor/ui_PriorEditorConfigDialog.h>
#include <IceUtil/UUID.h>


using namespace memoryx;

PriorEditorConfigDialog::PriorEditorConfigDialog(armarx::IceManagerPtr iceManager, QWidget* parent) :
    QDialog(parent),
    ui(new Ui::PriorEditorConfigDialog)
{
    uuid = IceUtil::generateUUID();
    ui->setupUi(this);
    proxyFinder = new armarx::IceProxyFinder<armarx::RobotStateComponentInterfacePrx>(this);
    //    proxyFinder->setIceManager(iceManager);
    proxyFinder->setSearchMask("RobotState*");
    ui->gridLayout->addWidget(proxyFinder, 5, 1);
    /*    fileDialog = new QFileDialog(parent);
        fileDialog->setModal(true);
        fileDialog->setFileMode(QFileDialog::ExistingFiles);
        fileDialog->setNameFilter(tr("XML (*.xml)"));
        connect(ui->btnSelectModelFile, SIGNAL(clicked()), fileDialog, SLOT(show()));
        connect(fileDialog, SIGNAL(accepted()), this, SLOT(modelFileSelected()));*/

    ;
}

PriorEditorConfigDialog::~PriorEditorConfigDialog()
{
    delete ui;
}

void PriorEditorConfigDialog::modelFileSelected()
{
    //    ui->editRobotFile->setText(fileDialog->selectedFiles()[0]);
}


