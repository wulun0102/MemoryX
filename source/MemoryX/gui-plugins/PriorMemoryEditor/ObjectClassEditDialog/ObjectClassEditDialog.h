/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::MemoryX::PriorMemoryEditorPlugin
* @author     Alexey Kozlov (kozlov at kit dot edu)
* @author     Kai Welke (welke at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include "../EntityEditDialog.h"

#include "ManipulationAttributesEditTab.h"
#include "RecognitionAttributesEditTab.h"
#include "MotionAttributesEditTab.h"
#include "GraspEditorDialog.h"

#include <MemoryX/core/entity/Entity.h>

#include <QFileDialog>

namespace Ui
{
    class ObjectClassEditDialog;
}

namespace memoryx
{

    class ObjectClassEditDialog : public EntityEditDialog
    {
        Q_OBJECT

    public:
        explicit ObjectClassEditDialog(armarx::RobotStateComponentInterfacePrx robotStateComponent, QWidget* parent = 0);
        ~ObjectClassEditDialog() override;

        void setClassNameEditable(bool editable);

        void updateGui(const EntityPtr& entity) override;
        void updateEntity(const EntityPtr& entity, std::string filesDBName) override;

    public slots:

    private:
        Ui::ObjectClassEditDialog* ui;

        // tabs
        ManipulationAttributesEditTab* manipulationAttributesEditTab;
        RecognitionAttributesEditTab* recognitionAttributesEditTab;
        MotionAttributesEditTab* motionAttributesEditTab;
        armarx::RobotStateComponentInterfacePrx robotStateComponent;

        friend class PriorEditorController;
    };
}

