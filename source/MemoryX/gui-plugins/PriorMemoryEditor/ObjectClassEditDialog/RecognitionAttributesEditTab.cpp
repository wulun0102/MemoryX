/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::MemoryX::PriorMemoryEditorPlugin
* @author     Kai Welke (welke at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "RecognitionAttributesEditTab.h"
#include <MemoryX/gui-plugins/PriorMemoryEditor/ui_RecognitionAttributesEditTab.h>

#include <MemoryX/libraries/helpers/ObjectRecognitionHelpers/ObjectRecognitionWrapper.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectClass.h>

// ArmarXCore
#include <ArmarXCore/core/logging/Logging.h>

using namespace memoryx;
using namespace memoryx::EntityWrappers;

RecognitionAttributesEditTab::RecognitionAttributesEditTab(QWidget* parent)
    : EntityAttributesEditTab(parent),
      ui(new Ui::RecognitionAttributesEditTab),
      texturedRecognition("TexturedObjectRecognition"),
      segmentableRecognition("SegmentableObjectRecognition"),
      handMarkerRecognition("HandMarkerLocalization"),
      arMarkerRecognition("ArMarkerLocalization"),
      otherRecognitionMethod("Other")
{
    ui->setupUi(this);

    // init feature file dialog
    connect(ui->comboBoxRecognitionMethod, SIGNAL(currentIndexChanged(const QString&)), this, SLOT(recognitionMethodChanged(const QString&)));

    texturedFeatureFileDialog = new QFileDialog(parent);
    texturedFeatureFileDialog->setModal(true);
    texturedFeatureFileDialog->setFileMode(QFileDialog::ExistingFiles);
    texturedFeatureFileDialog->setNameFilter(tr("IVT data files (*.dat)"));
    connect(ui->btnSelectTexturedFeatureFile, SIGNAL(clicked()), texturedFeatureFileDialog, SLOT(show()));
    connect(texturedFeatureFileDialog, SIGNAL(accepted()), this, SLOT(texturedFeatureFileSelected()));

    segmentableFeatureFileDialog = new QFileDialog(parent);
    segmentableFeatureFileDialog->setModal(true);
    segmentableFeatureFileDialog->setFileMode(QFileDialog::ExistingFiles);
    segmentableFeatureFileDialog->setNameFilter(tr("IVT config files (config.txt)"));
    connect(ui->btnSelectSegmentableFeatureFile, SIGNAL(clicked()), segmentableFeatureFileDialog, SLOT(show()));
    connect(segmentableFeatureFileDialog, SIGNAL(accepted()), this, SLOT(segmentableFeatureFileSelected()));

    recognitionMethodChanged("");
}

RecognitionAttributesEditTab::~RecognitionAttributesEditTab()
{
    delete ui;
}

void RecognitionAttributesEditTab::updateGui(const EntityPtr& entity)
{
    ObjectClassPtr objectClass = ObjectClassPtr::dynamicCast(entity);

    ObjectRecognitionWrapperPtr objectRecognitionWrapper = objectClass->getWrapper<ObjectRecognitionWrapper>();
    TexturedRecognitionWrapperPtr texturedRecognitionWrapper = objectClass->getWrapper<TexturedRecognitionWrapper>();
    SegmentableRecognitionWrapperPtr segmentableRecognitionWrapper = objectClass->getWrapper<SegmentableRecognitionWrapper>();
    HandMarkerBallWrapperPtr handMarkerBallWrapper = objectClass->getWrapper<HandMarkerBallWrapper>();
    ArMarkerWrapperPtr arMarkerWrapper = objectClass->getWrapper<ArMarkerWrapper>();

    // fill dialog (with ivt recognition wrapper attributes)
    std::string recognitionMethod = objectRecognitionWrapper->getRecognitionMethod();
    int index = ui->comboBoxRecognitionMethod->findText(QString(recognitionMethod.c_str()));

    if (index != -1)
    {
        ui->comboBoxRecognitionMethod->setCurrentIndex(index);
    }
    else if (recognitionMethod != "")
    {
        index = ui->comboBoxRecognitionMethod->findText(QString(otherRecognitionMethod.c_str()));

        if (index != -1)
        {
            ui->comboBoxRecognitionMethod->setCurrentIndex(index);
            recognitionMethod = otherRecognitionMethod;
        }
        else
        {
            ARMARX_WARNING_S << "Something is wrong with the ComboBox for the recognition methods";
        }
    }

    if (recognitionMethod == texturedRecognition)
    {
        ui->editTexturedFeatureFile->setText(QString(texturedRecognitionWrapper->getFeatureFile().c_str()));
    }
    else if (recognitionMethod == segmentableRecognition)
    {
        std::string configName = segmentableRecognitionWrapper->getDataFiles() + "/config.txt";
        ui->editSegmentableFeatureFile->setText(QString(configName.c_str()));
        ui->comboBoxObjectColor->setCurrentIndex(int(segmentableRecognitionWrapper->getObjectColor()));
    }
    else if (recognitionMethod == handMarkerRecognition)
    {
        ui->comboBoxMarkerColor->setCurrentIndex(int(handMarkerBallWrapper->getObjectColor()));
    }
    else if (recognitionMethod == arMarkerRecognition)
    {
        std::vector<int> ids = arMarkerWrapper->getArMarkerIDs();
        std::vector<float> sizes = arMarkerWrapper->getArMarkerSizes();
        std::vector<Eigen::Vector3f> translations = arMarkerWrapper->getTransformationToCenterTranslations();
        std::vector<Eigen::Vector3f> rotations = arMarkerWrapper->getTransformationToCenterRotationsRPY();

        if (ids.size() != 2 || sizes.size() != 2 || translations.size() != 2 || rotations.size() != 2)
        {
            ARMARX_WARNING_S << "ArMarker entity has an invalid number of markers";
        }
        else
        {
            ui->editArMarkerID->setText(QString(std::to_string(ids.at(0)).c_str()));
            ui->editArMarkerID_2->setText(QString(std::to_string(ids.at(1)).c_str()));

            ui->editArMarkerSize->setText(QString(std::to_string(sizes.at(0)).c_str()));
            ui->editArMarkerSize_2->setText(QString(std::to_string(sizes.at(1)).c_str()));

            ui->editArMarkerTrafoX->setText(QString(std::to_string(translations.at(0)(0)).c_str()));
            ui->editArMarkerTrafoY->setText(QString(std::to_string(translations.at(0)(1)).c_str()));
            ui->editArMarkerTrafoZ->setText(QString(std::to_string(translations.at(0)(2)).c_str()));
            ui->editArMarkerTrafoX_2->setText(QString(std::to_string(translations.at(1)(0)).c_str()));
            ui->editArMarkerTrafoY_2->setText(QString(std::to_string(translations.at(1)(1)).c_str()));
            ui->editArMarkerTrafoZ_2->setText(QString(std::to_string(translations.at(1)(2)).c_str()));

            ui->editArMarkerTrafoR->setText(QString(std::to_string(rotations.at(0)(0)).c_str()));
            ui->editArMarkerTrafoP->setText(QString(std::to_string(rotations.at(0)(1)).c_str()));
            ui->editArMarkerTrafoYaw->setText(QString(std::to_string(rotations.at(0)(2)).c_str()));
            ui->editArMarkerTrafoR_2->setText(QString(std::to_string(rotations.at(1)(0)).c_str()));
            ui->editArMarkerTrafoP_2->setText(QString(std::to_string(rotations.at(1)(1)).c_str()));
            ui->editArMarkerTrafoYaw_2->setText(QString(std::to_string(rotations.at(1)(2)).c_str()));
        }
    }
    else if (recognitionMethod == otherRecognitionMethod)
    {
        ui->editOtherRecogMethodName->setText(QString(objectRecognitionWrapper->getRecognitionMethod().c_str()));
    }
    else if (recognitionMethod != "")
    {
        ARMARX_WARNING_S << "Recognition method " << recognitionMethod << " not handled here";
    }
}


void RecognitionAttributesEditTab::updateEntity(const EntityPtr& entity, std::string filesDBName)
{
    ObjectClassPtr objectClass = ObjectClassPtr::dynamicCast(entity);

    ObjectRecognitionWrapperPtr objectRecognitionWrapper = objectClass->getWrapper<ObjectRecognitionWrapper>();
    TexturedRecognitionWrapperPtr texturedRecognitionWrapper = objectClass->getWrapper<TexturedRecognitionWrapper>();
    SegmentableRecognitionWrapperPtr segmentableRecognitionWrapper = objectClass->getWrapper<SegmentableRecognitionWrapper>();
    HandMarkerBallWrapperPtr handMarkerBallWrapper = objectClass->getWrapper<HandMarkerBallWrapper>();
    ArMarkerWrapperPtr arMarkerWrapper = objectClass->getWrapper<ArMarkerWrapper>();

    // update IVT recognition wrapper specific attributes
    std::string recognitionMethod = ui->comboBoxRecognitionMethod->currentText().toStdString();

    if (recognitionMethod == otherRecognitionMethod)
    {
        recognitionMethod = ui->editOtherRecogMethodName->text().toStdString();
    }
    else if (recognitionMethod == texturedRecognition)
    {
        texturedRecognitionWrapper->setFeatureFile(ui->editTexturedFeatureFile->text().toStdString(), filesDBName);
    }
    else if (recognitionMethod == segmentableRecognition)
    {
        std::string dataPath = ui->editSegmentableFeatureFile->text().toStdString();
        dataPath = dataPath.substr(0, dataPath.length() - 11);
        segmentableRecognitionWrapper->setDataFiles(dataPath, filesDBName);
        segmentableRecognitionWrapper->setObjectColor((ObjectColor) ui->comboBoxObjectColor->currentIndex());
    }
    else if (recognitionMethod == handMarkerRecognition)
    {
        handMarkerBallWrapper->setObjectColor((ObjectColor) ui->comboBoxMarkerColor->currentIndex());
    }
    else if (recognitionMethod == arMarkerRecognition)
    {
        std::vector<int> ids;
        ids.push_back(std::stoi(ui->editArMarkerID->text().toStdString()));
        ids.push_back(std::stoi(ui->editArMarkerID_2->text().toStdString()));
        arMarkerWrapper->setArMarkerIDs(ids);

        std::vector<float> sizes;
        sizes.push_back(std::stof(ui->editArMarkerSize->text().toStdString()));
        sizes.push_back(std::stof(ui->editArMarkerSize_2->text().toStdString()));
        arMarkerWrapper->setArMarkerSizes(sizes);

        Eigen::Vector3f tempVec;
        std::vector<Eigen::Vector3f> translations;
        tempVec(0) = std::stof(ui->editArMarkerTrafoX->text().toStdString());
        tempVec(1) = std::stof(ui->editArMarkerTrafoY->text().toStdString());
        tempVec(2) = std::stof(ui->editArMarkerTrafoZ->text().toStdString());
        translations.push_back(tempVec);
        tempVec(0) = std::stof(ui->editArMarkerTrafoX_2->text().toStdString());
        tempVec(1) = std::stof(ui->editArMarkerTrafoY_2->text().toStdString());
        tempVec(2) = std::stof(ui->editArMarkerTrafoZ_2->text().toStdString());
        translations.push_back(tempVec);
        arMarkerWrapper->setTransformationToCenterTranslations(translations);

        std::vector<Eigen::Vector3f> rotations;
        tempVec(0) = std::stof(ui->editArMarkerTrafoR->text().toStdString());
        tempVec(1) = std::stof(ui->editArMarkerTrafoP->text().toStdString());
        tempVec(2) = std::stof(ui->editArMarkerTrafoYaw->text().toStdString());
        rotations.push_back(tempVec);
        tempVec(0) = std::stof(ui->editArMarkerTrafoR_2->text().toStdString());
        tempVec(1) = std::stof(ui->editArMarkerTrafoP_2->text().toStdString());
        tempVec(2) = std::stof(ui->editArMarkerTrafoYaw_2->text().toStdString());
        rotations.push_back(tempVec);
        arMarkerWrapper->setTransformationToCenterRotationsRPY(rotations);
    }
    else
    {
        ARMARX_WARNING_S << "Recognition method " << recognitionMethod << " not handled here";
    }

    objectRecognitionWrapper->setRecognitionMethod(recognitionMethod);
}

void RecognitionAttributesEditTab::texturedFeatureFileSelected()
{
    ui->editTexturedFeatureFile->setText(texturedFeatureFileDialog->selectedFiles()[0]);
}

void RecognitionAttributesEditTab::segmentableFeatureFileSelected()
{
    ui->editSegmentableFeatureFile->setText(segmentableFeatureFileDialog->selectedFiles()[0]);
}


void RecognitionAttributesEditTab::recognitionMethodChanged(const QString& method)
{
    bool enableTextured = (method.toStdString() == texturedRecognition);
    bool enableSegmentable = (method.toStdString() == segmentableRecognition);
    bool enableHandMarker = (method.toStdString() == handMarkerRecognition);
    bool enableArMarker = (method.toStdString() == arMarkerRecognition);
    bool enableOtherRecogMethod = (method.toStdString() == otherRecognitionMethod);

    ui->btnSelectTexturedFeatureFile->setEnabled(enableTextured);
    ui->editTexturedFeatureFile->setEnabled(enableTextured);

    ui->btnSelectSegmentableFeatureFile->setEnabled(enableSegmentable);
    ui->editSegmentableFeatureFile->setEnabled(enableSegmentable);
    ui->comboBoxObjectColor->setEnabled(enableSegmentable);

    ui->comboBoxMarkerColor->setEnabled(enableHandMarker);

    ui->editArMarkerID->setEnabled(enableArMarker);
    ui->editArMarkerSize->setEnabled(enableArMarker);
    ui->editArMarkerTrafoX->setEnabled(enableArMarker);
    ui->editArMarkerTrafoY->setEnabled(enableArMarker);
    ui->editArMarkerTrafoZ->setEnabled(enableArMarker);
    ui->editArMarkerTrafoR->setEnabled(enableArMarker);
    ui->editArMarkerTrafoP->setEnabled(enableArMarker);
    ui->editArMarkerTrafoYaw->setEnabled(enableArMarker);
    ui->editArMarkerID_2->setEnabled(enableArMarker);
    ui->editArMarkerSize_2->setEnabled(enableArMarker);
    ui->editArMarkerTrafoX_2->setEnabled(enableArMarker);
    ui->editArMarkerTrafoY_2->setEnabled(enableArMarker);
    ui->editArMarkerTrafoZ_2->setEnabled(enableArMarker);
    ui->editArMarkerTrafoR_2->setEnabled(enableArMarker);
    ui->editArMarkerTrafoP_2->setEnabled(enableArMarker);
    ui->editArMarkerTrafoYaw_2->setEnabled(enableArMarker);

    ui->editOtherRecogMethodName->setEnabled(enableOtherRecogMethod);
}
