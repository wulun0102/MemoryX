/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::MemoryX::PriorMemoryEditorPlugin
* @author     Kai Welke (welke at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <QWidget>
#include <MemoryX/core/entity/Entity.h>
#include "../EntityAttributesEditTab.h"


namespace Ui
{
    class ManipulationAttributesEditTab;
}

namespace memoryx
{

    /**
     * This tab allows to change the simox attributes of an objectclass entity.
     **/
    class ManipulationAttributesEditTab : public EntityAttributesEditTab
    {
        Q_OBJECT

    public:
        explicit ManipulationAttributesEditTab(armarx::RobotStateComponentInterfacePrx robotStateComponent, QWidget* parent = 0);
        ~ManipulationAttributesEditTab() override;

        /**
         * Pure virtual method. Implement this in order to update the dialog with the information
         * contained in the entity provided as parameter.
         *
         * @param entity the entity used for updating the Gui
         **/
        void updateGui(const EntityPtr& entity) override;

        /**
         * Pure virtual method. Implement this in order to update the entity with information
         * edited in the gui
         *
         * @param entity the entity to update
         **/
        void updateEntity(const EntityPtr& entity, std::string filesDBName) override;

    public slots:
        void manipulationFileSelected();
        void visuFileSelected();
        void collisionFileSelected();
        void editGrasps();

        void update();

    private:
        Ui::ManipulationAttributesEditTab* ui;
        QFileDialog* manipulationFileDialog;
        QFileDialog* visuFileDialog;
        QFileDialog* collisionFileDialog;
        armarx::RobotStateComponentInterfacePrx robotStateComponent;
    };
}

