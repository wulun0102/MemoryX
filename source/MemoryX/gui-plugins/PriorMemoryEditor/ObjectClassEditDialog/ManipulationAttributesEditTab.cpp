/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::MemoryX::PriorMemoryEditorPlugin
* @author     Kai Welke (welke at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include <QMessageBox>

#include "ManipulationAttributesEditTab.h"
#include <MemoryX/gui-plugins/PriorMemoryEditor/ui_ManipulationAttributesEditTab.h>

#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <MemoryX/libraries/helpers/VirtualRobotHelpers/SimoxObjectWrapper.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectClass.h>

#include "GraspEditorDialog.h"


using namespace memoryx;
using namespace memoryx::EntityWrappers;

ManipulationAttributesEditTab::ManipulationAttributesEditTab(armarx::RobotStateComponentInterfacePrx robotStateComponent, QWidget* parent)
    : EntityAttributesEditTab(parent),
      ui(new Ui::ManipulationAttributesEditTab),
      robotStateComponent(robotStateComponent)
{
    ui->setupUi(this);

    // connect radiobuttons
    connect(ui->rbManipulationFile, SIGNAL(toggled(bool)), this, SLOT(update()));
    connect(ui->rbIVFiles, SIGNAL(toggled(bool)), this, SLOT(update()));
    update();

    // init manipulation file dialog
    manipulationFileDialog = new QFileDialog(parent);
    manipulationFileDialog->setModal(true);
    manipulationFileDialog->setFileMode(QFileDialog::ExistingFiles);
    manipulationFileDialog->setNameFilter(tr("Manipulation Object XML (*.xml *.moxml)"));
    connect(ui->btnSelectManipulationFile, SIGNAL(clicked()), manipulationFileDialog, SLOT(show()));
    connect(manipulationFileDialog, SIGNAL(accepted()), this, SLOT(manipulationFileSelected()));
    connect(ui->btnEditGrasps, SIGNAL(clicked()), this, SLOT(editGrasps()));

    // init visualization file dialog
    visuFileDialog = new QFileDialog(parent);
    visuFileDialog->setModal(true);
    visuFileDialog->setFileMode(QFileDialog::ExistingFiles);
    visuFileDialog->setNameFilter(tr("Inventor & VRML models (*.iv *.wrl)"));
    connect(ui->btnSelectVisuFile, SIGNAL(clicked()), visuFileDialog, SLOT(show()));
    connect(visuFileDialog, SIGNAL(accepted()), this, SLOT(visuFileSelected()));

    // init collision file dialog
    collisionFileDialog = new QFileDialog(parent);
    collisionFileDialog->setModal(true);
    collisionFileDialog->setFileMode(QFileDialog::ExistingFiles);
    collisionFileDialog->setNameFilter(tr("Inventor & VRML models (*.iv *.wrl)"));
    connect(ui->btnSelectCollisionFile, SIGNAL(clicked()), collisionFileDialog, SLOT(show()));
    connect(collisionFileDialog, SIGNAL(accepted()), this, SLOT(collisionFileSelected()));
}

ManipulationAttributesEditTab::~ManipulationAttributesEditTab()
{
    delete ui;
}

void ManipulationAttributesEditTab::updateGui(const EntityPtr& entity)
{
    ObjectClassPtr objectClass = ObjectClassPtr::dynamicCast(entity);

    SimoxObjectWrapperPtr simoxWrapper = objectClass->getWrapper<SimoxObjectWrapper>();

    // fill dialog (with simox wrapper attributes)
    ui->rbManipulationFile->setChecked(true);
    ui->editManipulationFile->setText(QString::fromStdString(simoxWrapper->getManipulationObjectFileName()));

    Eigen::Vector3f putdownOrientation = simoxWrapper->getPutdownOrientationRPY();
    ui->editPutdownOrientationR->setText(QString::number(putdownOrientation(0)));
    ui->editPutdownOrientationP->setText(QString::number(putdownOrientation(1)));
    ui->editPutdownOrientationY->setText(QString::number(putdownOrientation(2)));
}


void ManipulationAttributesEditTab::updateEntity(const EntityPtr& entity, std::string filesDBName)
{
    ObjectClassPtr objectClass = ObjectClassPtr::dynamicCast(entity);

    SimoxObjectWrapperPtr simoxWrapper = objectClass->getWrapper<SimoxObjectWrapper>();

    // update simox wrapper specific attributes
    if (ui->rbManipulationFile->isChecked())
    {
        simoxWrapper->setAndStoreManipulationFile(ui->editManipulationFile->text().toStdString(), filesDBName);
    }
    else
    {
        simoxWrapper->setAndStoreModelIVFiles(ui->editVisuFile->text().toStdString(),
                                              ui->editCollisionFile->text().toStdString(),
                                              filesDBName);
    }

    Eigen::Vector3f putdownOrientation;
    putdownOrientation(0) = std::stof(ui->editPutdownOrientationR->text().toStdString());
    putdownOrientation(1) = std::stof(ui->editPutdownOrientationP->text().toStdString());
    putdownOrientation(2) = std::stof(ui->editPutdownOrientationY->text().toStdString());
    simoxWrapper->setPutdownOrientationRPY(putdownOrientation);
}

void ManipulationAttributesEditTab::manipulationFileSelected()
{
    ui->editManipulationFile->setText(manipulationFileDialog->selectedFiles()[0]);
}

void ManipulationAttributesEditTab::visuFileSelected()
{
    ui->editVisuFile->setText(visuFileDialog->selectedFiles()[0]);
}

void ManipulationAttributesEditTab::collisionFileSelected()
{
    ui->editCollisionFile->setText(collisionFileDialog->selectedFiles()[0]);
}

void ManipulationAttributesEditTab::editGrasps()
{
    if (ui->editManipulationFile->text().isEmpty())
    {
        QMessageBox msgBox;
        msgBox.setText("Please select a manipulation file first.");
        msgBox.setIcon(QMessageBox::Information);
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.setDefaultButton(QMessageBox::Ok);
        msgBox.exec();
    }
    else
    {
        std::string robotFile("/usr/local/data/robots/ArmarIII/ArmarIII.xml");
        std::string localRobot = "RobotAPI/robots/Armar3/ArmarIII.xml";
        armarx::CMakePackageFinder finder("RobotAPI");
        if (armarx::ArmarXDataPath::getAbsolutePath(localRobot, localRobot, {finder.getDataDir()}))
        {
            robotFile = localRobot;
        }

        if (robotStateComponent)
        {
            for (auto package : robotStateComponent->getArmarXPackages())
            {
                auto path = robotStateComponent->getRobotFilename();
                armarx::CMakePackageFinder finder(package);
                if (finder.packageFound())
                {
                    if (armarx::ArmarXDataPath::getAbsolutePath(path, path, {finder.getDataDir()}, false))
                    {
                        robotFile = path;
                    }

                }
            }
        }


        std::string objectFile(ui->editManipulationFile->text().toStdString());
        GraspEditorDialog* dialog = new GraspEditorDialog(objectFile, robotFile, this);
        dialog->exec();
    }
}

void ManipulationAttributesEditTab::update()
{
    // update controls of object visulization and collision model
    const bool moFile = ui->rbManipulationFile->isChecked();
    ui->editManipulationFile->setEnabled(moFile);
    ui->lblManipulationFile->setEnabled(moFile);
    ui->btnSelectManipulationFile->setEnabled(moFile);
    ui->btnEditGrasps->setEnabled(moFile);

    ui->editVisuFile->setEnabled(!moFile);
    ui->lblVisuFile->setEnabled(!moFile);
    ui->btnSelectVisuFile->setEnabled(!moFile);

    ui->editCollisionFile->setEnabled(!moFile);
    ui->lblCollisionFile->setEnabled(!moFile);
    ui->btnSelectCollisionFile->setEnabled(!moFile);

}
