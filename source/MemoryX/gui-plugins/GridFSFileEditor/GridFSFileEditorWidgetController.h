/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::GridFSFileEditorWidgetController
 * @author     Mirko Waechter (waechter at kit dot edu)
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <MemoryX/gui-plugins/GridFSFileEditor/ui_GridFSFileEditorWidget.h>

#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <MemoryX/interface/components/WorkingMemoryInterface.h>
#include <MemoryX/interface/components/LongtermMemoryInterface.h>

namespace armarx
{
    /**
    \page MemoryX-GuiPlugins-GridFSFileEditor GridFS FileEditor
    \brief This widget allows viewing files that are referenced in the prior knowledge database.
    \image html GridFSFileEditorGui.png

    For details on GridFS see the \ref MemoryX-GridFilesystem "GridFS description"

    GridFS Editor API Documentation \ref GridFSFileEditorWidgetControlle
    \see GridFSFileEditorGuiPlugin
    */

    /**
     * \class GridFSFileEditorWidgetController
     * \brief This widget allows viewing files that are referenced in the prior knowledge database.
     * \see GridFSFileEditorGuiPlugin
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        GridFSFileEditorWidgetController:
        public ArmarXComponentWidgetControllerTemplate<GridFSFileEditorWidgetController>
    {
        Q_OBJECT

    public:
        /**
         * Controller Constructor
         */
        explicit GridFSFileEditorWidgetController();

        /**
         * Controller destructor
         */
        virtual ~GridFSFileEditorWidgetController();

        /**
         * \see ArmarXWidgetController::loadSettings()
         */
        void loadSettings(QSettings* settings) override;

        /**
         * \see ArmarXWidgetController::saveSettings()
         */
        void saveSettings(QSettings* settings) override;

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        static QString GetWidgetName()
        {
            return "MemoryX.GridFSFileEditor";
        }
        static QIcon GetWidgetIcon()
        {
            return QIcon {"://icons/server.svg"};
        }

        /**
         * \see armarx::Component::onInitComponent()
         *
         * Initializes waiting for proxies of LongtermMemory and WorkingMemory.
         */
        void onInitComponent() override;

        /**
         * \see armarx::Component::onConnectComponent()
         *
         * Fetches proxies and the list of all available snapshots.
         * Enables the main widget.
         */
        void onConnectComponent() override;

        /**
         * \see armarx::Component::onDisconnectComponent()
         *
         * Disables the main widget if the connection has been lost.
         */
        void onDisconnectComponent() override;

    public slots:
        /* QT slot declarations */

        void loadFileNames(QString dbName = "");
        void loadFileContent(const QString& fileName);
        void saveContentToFile();
        void deleteUnreferencedFiles();

    private slots:
    signals:
        /* QT signal declarations */
        void connected();
    private:
        /**
         * \brief connectSlots does the initial wiring of signals to slots
         */
        void connectSlots();

        void replaceFileDocID(QString fileId, const QString& newId);

        std::string whereQuery(std::string fileId) const;
        /**
         * Widget Form
         */
        Ui::GridFSFileEditorWidget widget;

        /**
         * \brief commonStoragePrx proxy to CommonStorage
         */
        memoryx::CommonStorageInterfacePrx commonStoragePrx;
        std::string dbName;
        QString currentId;
    };
}

