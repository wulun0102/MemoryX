/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    VisionX::gui-plugins::ObjectLocalizationRequesterWidgetController
 * \author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * \date       2015
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ObjectLocalizationRequesterWidgetController.h"
#include <ArmarXCore/observers/variant/ChannelRef.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>
#include <MemoryX/core/entity/Entity.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>
#include <string>

namespace armarx
{
    ObjectLocalizationRequesterWidgetController::ObjectLocalizationRequesterWidgetController() :
        customToolbar(NULL)
    {
        widget.setupUi(getWidget());
        connect(widget.btnRequest, SIGNAL(clicked()), this, SLOT(on_btnRequest_clicked()), Qt::UniqueConnection);
        connect(widget.btnRelease, SIGNAL(clicked()), this, SLOT(on_btnRelease_clicked()), Qt::UniqueConnection);
        enableMainWidget(false);
    }


    ObjectLocalizationRequesterWidgetController::~ObjectLocalizationRequesterWidgetController()
    {

    }


    void ObjectLocalizationRequesterWidgetController::loadSettings(QSettings* settings)
    {

    }

    void ObjectLocalizationRequesterWidgetController::saveSettings(QSettings* settings)
    {

    }


    void ObjectLocalizationRequesterWidgetController::onInitComponent()
    {
        usingProxy("ObjectMemoryObserver");
        usingProxy("PriorKnowledge");
    }


    void ObjectLocalizationRequesterWidgetController::onConnectComponent()
    {
        getProxy(omo, "ObjectMemoryObserver");
        getProxy(prior, "PriorKnowledge");
        updateAvailableObjects();
        on_toolButtonRefresh_clicked();
        enableMainWidgetAsync(true);

    }

    void ObjectLocalizationRequesterWidgetController::onDisconnectComponent()
    {
        enableMainWidgetAsync(false);
    }

    void ObjectLocalizationRequesterWidgetController::updateAvailableObjects()
    {
        auto entities = prior->getObjectClassesSegment()->getAllEntities();
        QString oldSelection = widget.comboBoxAvailableObjectClasses->currentText();
        widget.comboBoxAvailableObjectClasses->clear();
        QStringList objects;
        for (memoryx::EntityBasePtr base : entities)
        {
            memoryx::EntityPtr entity = memoryx::EntityPtr::dynamicCast(base);
            auto var = entity->getAttributeValue("recognitionMethod");
            if (var && !var->getString().empty() && var->getString() != "<none>")
            {
                objects << (QString::fromStdString(entity->getName()));
            }
        }
        objects.sort();
        widget.comboBoxAvailableObjectClasses->addItems(objects);
        int index = widget.comboBoxAvailableObjectClasses->findText(oldSelection);
        if (index > 0)
        {
            widget.comboBoxAvailableObjectClasses->setCurrentIndex(index);
        }
    }

    void ObjectLocalizationRequesterWidgetController::updateAvailableChannels()
    {
        ARMARX_INFO << "refreshing channels";
        channels = omo->getAvailableChannels(false);

        widget.comboBoxRequestedObjectClasses->clear();
        for (auto& channel : channels)
        {
            if (channel.first.find("_query_") != std::string::npos)
            {
                widget.comboBoxRequestedObjectClasses->addItem(QString::fromStdString(channel.first));
            }
        }
    }



    void armarx::ObjectLocalizationRequesterWidgetController::on_toolButtonRefresh_clicked()
    {

        updateAvailableChannels();
        updateAvailableObjects();
    }

    void armarx::ObjectLocalizationRequesterWidgetController::on_btnRequest_clicked()
    {
        ARMARX_INFO << "requesting";
        if (!widget.comboBoxAvailableObjectClasses->currentText().isEmpty())
        {
            omo->requestObjectClassRepeated(widget.comboBoxAvailableObjectClasses->currentText().toStdString(),
                                            widget.spinBoxInterval->value(), widget.spinBoxPriority->value());
        }
        updateAvailableChannels();
    }

    void armarx::ObjectLocalizationRequesterWidgetController::on_btnRelease_clicked()
    {
        ARMARX_INFO << "releasing";
        auto channelName = channels[widget.comboBoxRequestedObjectClasses->currentText().toStdString()].name;
        if (!channelName.empty())
        {
            armarx::ChannelRefPtr ref = new armarx::ChannelRef(omo, channelName);
            omo->releaseObjectClass(ref);
        }
        updateAvailableChannels();
    }

    QPointer<QWidget> ObjectLocalizationRequesterWidgetController::getCustomTitlebarWidget(QWidget* parent)
    {
        if (customToolbar)
        {
            if (parent != customToolbar->parent())
            {
                customToolbar->setParent(parent);
            }

            return customToolbar;
        }

        customToolbar = new QToolBar(parent);
        customToolbar->setIconSize(QSize(16, 16));
        customToolbar->addAction(QIcon("://icons/view-refresh-7.png"), "Refresh", this, SLOT(on_toolButtonRefresh_clicked()));

        return customToolbar;
    }
}
