/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// std
#include <exception>
#include <string>

// local
#include "CreateGroupOperation.h"
#include "DeleteGroupOperation.h"
#include "EmptyOperation.h"

controller::CreateGroupOperation::CreateGroupOperation(const std::shared_ptr<memoryxcontroller::MemoryXController>& memoryXController, const std::shared_ptr<scene3D::Scene>& scene, const std::string& groupName) :
    Operation(memoryXController, scene),
    groupName(groupName),
    failed(false)
{
    if (groupName.empty() || groupName == "")
    {
        failed = true;
    }
}

const controller::OperationPtr controller::CreateGroupOperation::createInverseOperation() const
{
    std::shared_ptr<controller::Operation> inverseOperation(new controller::DeleteGroupOperation(getMemoryXController(), getScene(), groupName));

    if (failed)
    {
        inverseOperation.reset(new controller::EmptyOperation(getMemoryXController(), getScene(), getObjectId()));
    }

    return inverseOperation;
}

void controller::CreateGroupOperation::executeOnWorkingMemory()
{
    // do nothing, because the WorkingMemory does not manage groups
}

void controller::CreateGroupOperation::executeOnScene()
{
    if (!failed)
    {
        std::shared_ptr<scene3D::Scene> scene = getScene();
        scene3D::SceneGroupPtr newSceneGroup(new scene3D::SceneGroup(groupName));
        scene->getGroupManager()->addGroup(newSceneGroup);
    }
}
