/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// std
#include <exception>
#include <string>

// scene3D
#include <Inventor/SbVec3f.h>
#include <Inventor/SbRotation.h>

// local
#include "../scene3D/Scene.h"
#include "Operation.h"
#include "../memoryxcontroller/MemoryXController.h"

namespace controller
{
    /**
    * A Operation to create a new object in the scene and the WorkingMemory.
    *
    * @see controller::Operation
    */
    class AddOperation : public Operation
    {
    public:
        /**
        * Creates a new operation, which is a copy of the given operation, but has a new random ID.
        *
        * @param operation The operation to copy;
        */
        AddOperation(const AddOperation& operation);

        /**
        * Creates a new operation, which adds a new object.
        *
        * @param memoryXController The MemoryXController to add the object at.
        * @param scene The scene to add the object at.
        * @param objectName The name of the class of the object. This is used to identify the class in PriorKnowledge.
        * @param objectCollection The name of the collection where the object is in. This is used to identify the class in PriorKnowledge.
        * @param objectPosition The position where the object should be added.
        * @param objectRotation The rotation to add the object with.
        * @param objectId The object id to create the object with.
        *
        * @see controller::Operation::Operation
        */
        AddOperation(const std::shared_ptr<memoryxcontroller::MemoryXController>& memoryXController, const std::shared_ptr<scene3D::Scene>& scene, const std::string& objectName, const std::string& objectCollection, const SbVec3f& objectPosition, const SbRotation& objectRotation, const std::string& objectId);

        /**
        * Creates a new operation, which adds a new object.
        * A new random object ID is created in this operation.
        *
        * @param memoryXController The MemoryXController to add the object at.
        * @param scene The scene to add the object at.
        * @param objectName The name of the class of the object. This is used to identify the class in PriorKnowledge.
        * @param objectCollection The name of the collection where the object is in. This is used to identify the class in PriorKnowledge.
        * @param objectPosition The position where the object should be added.
        * @param objectRotation The rotation to add the object with.
        *
        * @see controller::Operation::Operation
        */
        AddOperation(const std::shared_ptr<memoryxcontroller::MemoryXController>& memoryXController, const std::shared_ptr<scene3D::Scene>& scene, const std::string& objectName, const std::string& objectCollection, const SbVec3f& objectPosition, const SbRotation& objectRotation);

        /**
        * Creates a new operation, which adds a new object.
        * A new random object ID is created in this operation.
        * The object will be added at a default position with a default rotation.
        *
        * @param memoryXController The MemoryXController to add the object at.
        * @param scene The scene to add the object at.
        * @param objectName The name of the class of the object. This is used to identify the class in PriorKnowledge.
        * @param objectCollection The name of the collection where the object is in. This is used to identify the class in PriorKnowledge.
        *
        * @see controller::Operation::Operation
        */
        AddOperation(const std::shared_ptr<memoryxcontroller::MemoryXController>& memoryXController, const std::shared_ptr<scene3D::Scene>& scene, const std::string& objectName, const std::string& objectCollection);

        /**
        * Returns a new operation which removes the object, which will be added in this operation.
        *
        * @return The operation, which removes the object.
        *
        * @see controller::Operation::createInverseOperation
        */
        const OperationPtr createInverseOperation() const override;

    protected:
        /**
        * Adds the object to the WorkingMemory.
        *
        * @see controller::Operation::executeOnWorkingMemory
        */
        void executeOnWorkingMemory() override;

        /**
        * Adds the object to the local Scene.
        *
        * @see controller::Operation::executeOnScene
        */
        void executeOnScene() override;

    private:
        void createRandomId();

        std::string objectName;
        std::string objectCollection;
        SbVec3f objectPosition;
        SbRotation objectRotation;

    };
}

