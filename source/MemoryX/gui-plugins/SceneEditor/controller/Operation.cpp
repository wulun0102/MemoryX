/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Operation.h"

controller::Operation::Operation(const std::shared_ptr<memoryxcontroller::MemoryXController>& memoryXController, const std::shared_ptr<scene3D::Scene>& scene, const std::string& objectId) :
    scene(scene),
    memoryXController(memoryXController),
    objectId(objectId)
{

}

controller::Operation::Operation(const std::shared_ptr<memoryxcontroller::MemoryXController>& memoryXController, const std::shared_ptr<scene3D::Scene>& scene) :
    scene(scene),
    memoryXController(memoryXController),
    objectId()
{

}

const std::shared_ptr<scene3D::Scene> controller::Operation::getScene() const
{
    return scene.lock();
}

const std::shared_ptr<memoryxcontroller::MemoryXController> controller::Operation::getMemoryXController() const
{
    return memoryXController.lock();
}

std::string controller::Operation::getObjectId() const
{
    return objectId;
}

void controller::Operation::setObjectId(const std::string& objectId)
{
    Operation::objectId = objectId;
}

bool controller::Operation::isExecuteable()
{
    return true;
}
