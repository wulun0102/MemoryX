/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// std
#include <stdexcept>
#include <string>

// scene3D
#include <Inventor/SbVec3f.h>
#include <Inventor/SbRotation.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoCone.h>
#include <Inventor/nodes/SoMaterial.h>

// local
#include "AddOperation.h"
#include "RemoveOperation.h"


controller::AddOperation::AddOperation(const controller::AddOperation& operation) :
    Operation(operation.getMemoryXController(), operation.getScene()),
    objectName(operation.objectName),
    objectCollection(operation.objectCollection),
    objectPosition(operation.objectPosition),
    objectRotation(operation.objectRotation)
{
    while (getObjectId() == operation.getObjectId())
    {
        createRandomId();
    }
}

controller::AddOperation::AddOperation(const std::shared_ptr<memoryxcontroller::MemoryXController>& memoryXController, const std::shared_ptr<scene3D::Scene>& scene, const std::string& objectName, const std::string& objectCollection, const SbVec3f& objectPosition, const SbRotation& objectRotation, const std::string& objectId) :
    Operation(memoryXController, scene, objectId),
    objectName(objectName),
    objectCollection(objectCollection),
    objectPosition(objectPosition),
    objectRotation(objectRotation)
{

}

controller::AddOperation::AddOperation(const std::shared_ptr<memoryxcontroller::MemoryXController>& memoryXController, const std::shared_ptr<scene3D::Scene>& scene, const std::string& objectName, const std::string& objectCollection, const SbVec3f& objectPosition, const SbRotation& objectRotation) :
    Operation(memoryXController, scene),
    objectName(objectName),
    objectCollection(objectCollection),
    objectPosition(objectPosition),
    objectRotation(objectRotation)
{
    createRandomId();
}

controller::AddOperation::AddOperation(const std::shared_ptr<memoryxcontroller::MemoryXController>& memoryXController, const std::shared_ptr<scene3D::Scene>& scene, const std::string& objectName, const std::string& objectCollection) :
    Operation(memoryXController, scene),
    objectName(objectName),
    objectCollection(objectCollection),
    objectPosition(0, 0, 0),
    objectRotation(SbVec3f(1, 0, 0), 0)
{
    createRandomId();
}

void controller::AddOperation::createRandomId()
{
    static const char chars[] =
        "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    const int length = 20;

    std::shared_ptr<scene3D::Scene> scene = getScene();
    char random[length + 1];

    do
    {
        for (int i = 0; i < length; ++i)
        {
            random[i] = chars[rand() % (sizeof(chars) - 1)];
        }

        random[length] = 0;
    }
    while (scene->getObjectManager()->getObjectById(random));

    std::string id = random;
    setObjectId(id);
}

const controller::OperationPtr controller::AddOperation::createInverseOperation() const
{
    std::shared_ptr<controller::Operation> inverseOperation(new controller::RemoveOperation(getMemoryXController(), getScene(), objectName, objectCollection, objectPosition, objectRotation, getObjectId()));
    return inverseOperation;
}

void controller::AddOperation::executeOnWorkingMemory()
{

    memoryxcontroller::MemoryXControllerPtr memoryXController = getMemoryXController();
    ARMARX_CHECK_EXPRESSION(memoryXController);

    ARMARX_CHECK_EXPRESSION(memoryXController->getPriorKnowlegdeController());
    memoryx::ObjectClassPtr objectClass = memoryXController->getPriorKnowlegdeController()->getObjectClassPtr(objectName, objectCollection);
    ARMARX_CHECK_EXPRESSION(objectClass);


    std::string newObjectId = memoryXController->getWorkingMemoryController()->addObjectInstance(objectName, objectClass);
    setObjectId(newObjectId);
    ARMARX_CHECK_EXPRESSION(getMemoryXController()->getWorkingMemoryController());

    getMemoryXController()->getWorkingMemoryController()->rotateTranslateObject(newObjectId, objectRotation, objectPosition);
}

void controller::AddOperation::executeOnScene()
{
    std::shared_ptr<scene3D::Scene> scene = getScene();
    memoryxcontroller::PriorKnowledgeControllerPtr priorKnowledgeController = getMemoryXController()->getPriorKnowlegdeController();
    memoryx::ObjectClassPtr objectClass = priorKnowledgeController->getObjectClassPtr(objectName, objectCollection);
    SoNode* geometryNode = priorKnowledgeController->getCoinVisualisation(objectClass, false);
    SoNode* collisionNode = priorKnowledgeController->getCoinVisualisation(objectClass, true);

    if (!collisionNode)
    {
        ARMARX_WARNING_S << "object \"" << objectName << "\" has no collison model";
        collisionNode = new SoSeparator;
        SoMaterial* material = new SoMaterial;
        material->ambientColor.setValue(1.0f, 0.0f, 0.0f);
        material->diffuseColor.setValue(1.0f, 0.0f, 0.0f);
        SoCone* cone = new SoCone;
        cone->bottomRadius.setValue(0.03);
        cone->height.setValue(0.3);
        ((SoSeparator*)collisionNode)->addChild(material);
        ((SoSeparator*)collisionNode)->addChild(cone);
    }

    if (!geometryNode)
    {
        ARMARX_WARNING_S << "object \"" << objectName << "\" has no visualisation model";
        geometryNode = new SoSeparator;
        SoMaterial* material = new SoMaterial;
        material->ambientColor.setValue(1.0f, 0.0f, 0.0f);
        material->diffuseColor.setValue(1.0f, 0.0f, 0.0f);
        SoCone* cone = new SoCone;
        cone->bottomRadius.setValue(0.03);
        cone->height.setValue(0.3);
        ((SoSeparator*)geometryNode)->addChild(material);
        ((SoSeparator*)geometryNode)->addChild(cone);
    }

    SoSeparator* geometry = new SoSeparator();
    geometry->addChild(geometryNode);
    SoSeparator* collision = new SoSeparator();
    collision->addChild(collisionNode);

    scene3D::SceneObjectPtr object(new scene3D::SceneObject(getObjectId(), objectName, objectCollection, geometry, collision));
    object->setRotation(objectRotation);
    object->setTranslation(objectPosition);
    scene->getObjectManager()->addObject(object);

}
