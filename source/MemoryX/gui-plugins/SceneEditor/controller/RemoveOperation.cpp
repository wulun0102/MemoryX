/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// std
#include <stdexcept>
#include <string>

// scene3D
#include <Inventor/SbVec3f.h>
#include <Inventor/SbRotation.h>
#include <QtCore/qstatemachine.h>

// local
#include "RemoveOperation.h"

controller::RemoveOperation::RemoveOperation(const std::shared_ptr<memoryxcontroller::MemoryXController>& memoryXController, const std::shared_ptr<scene3D::Scene>& scene, const std::string& objectName, const std::string& objectCollection, const SbVec3f& objectPosition, const SbRotation& objectRotation, const std::string& objectId) :
    Operation(memoryXController, scene, objectId),
    objectName(objectName),
    objectCollection(objectCollection),
    objectPosition(objectPosition),
    objectRotation(objectRotation)
{
}

controller::RemoveOperation::RemoveOperation(const std::shared_ptr<memoryxcontroller::MemoryXController>& memoryXController, const std::shared_ptr<scene3D::Scene>& scene, const std::string& objectId) :
    Operation(memoryXController, scene, objectId),
    objectName(),
    objectCollection(),
    objectPosition(),
    objectRotation()
{
}

const controller::OperationPtr controller::RemoveOperation::createInverseOperation() const
{
    if (!(objectName.empty() || objectCollection.empty()))
    {
        std::shared_ptr<controller::Operation> inverseOperation(new controller::AddOperation(getMemoryXController(), getScene(), objectName, objectCollection, objectPosition, objectRotation, getObjectId()));
        return inverseOperation;
    }
    else
    {
        throw std::runtime_error("Not yet executed");
    }
}

void controller::RemoveOperation::executeOnWorkingMemory()
{
    memoryxcontroller::MemoryXControllerPtr memoryXController = getMemoryXController();
    /* currently we can not get these informations here; we should not need it, because we get them from the local scene
    if(!(objectName.empty() || objectCollection.empty()))
    {
        objectName = "";
        objectCollection = "";
        objectPosition = "";
        objectRotation = "";
    }
    */
    memoryXController->getWorkingMemoryController()->removeObjectInstance(getObjectId());
}

void controller::RemoveOperation::executeOnScene()
{
    std::shared_ptr<scene3D::Scene> scene = getScene();
    scene3D::SceneObjectPtr object = scene->getObjectManager()->getObjectById(getObjectId());

    if (!object)
    {
        throw std::logic_error("Object does not exist");
    }

    objectName = object->getClassId();
    objectCollection = object->getCollection();
    objectPosition = object->getTranslation();
    objectRotation = object->getRotation();
    scene->getObjectManager()->removeObject(object);
}

bool controller::RemoveOperation::isExecuteable()
{
    std::shared_ptr<scene3D::Scene> scene = getScene();
    scene3D::SceneObjectPtr object = scene->getObjectManager()->getObjectById(getObjectId());

    if (!object)
    {
        return true;
    }

    return object->isMutable();
}
