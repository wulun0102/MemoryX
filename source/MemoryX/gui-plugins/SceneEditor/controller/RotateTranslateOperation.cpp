/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// std
#include <stdexcept>
#include <string>

// scene3D
#include <Inventor/SbVec3f.h>
#include <Inventor/SbRotation.h>

// local
#include "RotateTranslateOperation.h"

controller::RotateTranslateOperation::RotateTranslateOperation(const std::shared_ptr<memoryxcontroller::MemoryXController>& memoryXController, const std::shared_ptr<scene3D::Scene>& scene, const std::string& objectId, const SbRotation& oldRotation, const SbRotation& newRotation, const SbVec3f& oldPosition, const SbVec3f& newPosition) :
    Operation(memoryXController, scene, objectId),
    oldRotation(oldRotation),
    newRotation(newRotation),
    oldPosition(oldPosition),
    newPosition(newPosition)
{
}

const controller::OperationPtr controller::RotateTranslateOperation::createInverseOperation() const
{
    std::shared_ptr<controller::Operation> inverseOperation(new controller::RotateTranslateOperation(getMemoryXController(), getScene(), getObjectId(), newRotation, oldRotation, newPosition, oldPosition));
    return inverseOperation;
}

void controller::RotateTranslateOperation::executeOnWorkingMemory()
{
    getMemoryXController()->getWorkingMemoryController()->rotateTranslateObject(getObjectId(), newRotation, newPosition);
}

void controller::RotateTranslateOperation::executeOnScene()
{
    std::shared_ptr<scene3D::Scene> scene = getScene();
    scene3D::SceneObjectPtr object = scene->getObjectManager()->getObjectById(getObjectId());

    if (!object)
    {
        throw std::logic_error("Object does not exist");
    }

    object->setRotation(newRotation);
    object->setTranslation(newPosition);

    //Reset manipulator to apply new position and translation
    scene->getManipulatorManager()->applyManipulator(object);

    if (scene->getSelectionManager()->isSelected(object))
    {
        scene->getManipulatorManager()->addManipulator(object);
    }
}

bool controller::RotateTranslateOperation::isExecuteable()
{
    std::shared_ptr<scene3D::Scene> scene = getScene();
    scene3D::SceneObjectPtr object = scene->getObjectManager()->getObjectById(getObjectId());

    if (!object)
    {
        return true;
    }

    return object->isMutable();
}
