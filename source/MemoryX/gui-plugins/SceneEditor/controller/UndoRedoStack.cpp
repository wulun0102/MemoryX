/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// std
#include <exception>
#include <qevent.h>

// local
#include "UndoRedoStack.h"
#include "UndoAction.h"
#include "Controller.h"

controller::UndoRedoStack::UndoRedoStack() :
    undoStack(new std::vector<UndoActionPtr>()),
    redoStack(new std::vector<UndoActionPtr>())
{
}

bool controller::UndoRedoStack::canUndo()
{
    return !undoStack->empty();
}

bool controller::UndoRedoStack::canRedo()
{
    return !redoStack->empty();
}

std::shared_ptr<std::vector<controller::OperationPtr> > controller::UndoRedoStack::undo()
{
    std::shared_ptr<std::vector<controller::OperationPtr> > toUndo;

    if (canUndo())
    {
        toUndo = undoStack->back()->undo();
        redoStack->push_back(undoStack->back());
        undoStack->pop_back();
        return toUndo;
    }
    else
    {
        toUndo.reset(new std::vector<controller::OperationPtr>());
    }

    return toUndo;
}

std::shared_ptr<std::vector<controller::OperationPtr> > controller::UndoRedoStack::redo()
{
    std::shared_ptr<std::vector<controller::OperationPtr> > toRedo;

    if (canRedo())
    {
        toRedo = redoStack->back()->redo();
        undoStack->push_back(redoStack->back());
        redoStack->pop_back();
    }
    else
    {
        toRedo.reset(new std::vector<controller::OperationPtr>());
    }

    return toRedo;
}

void controller::UndoRedoStack::push(const std::shared_ptr<std::vector<controller::OperationPtr> >& operations)
{
    controller::UndoActionPtr action(new controller::UndoAction(operations));
    undoStack->push_back(action);

    if (canRedo())
    {
        redoStack.reset(new std::vector<UndoActionPtr>());
    }
}

void controller::UndoRedoStack::updateObjectId(std::string oldId, std::string newId)
{
    for (auto it = undoStack->begin(); it != undoStack->end(); it++)
    {
        for (auto ito = it->get()->getOperations()->begin(); ito != it->get()->getOperations()->end(); ito++)
        {
            if (ito->get()->getObjectId() == oldId)
            {
                ito->get()->setObjectId(newId);
            }
            else if (ito->get()->getObjectId() == newId)
            {
                ito->get()->setObjectId(oldId);
            }
        }
    }

    for (auto it = redoStack->begin(); it != redoStack->end(); it++)
    {
        for (auto ito = it->get()->getOperations()->begin(); ito != it->get()->getOperations()->end(); ito++)
        {
            if (ito->get()->getObjectId() == oldId)
            {
                ito->get()->setObjectId(newId);
            }
            else if (ito->get()->getObjectId() == newId)
            {
                ito->get()->setObjectId(oldId);
            }
        }
    }
}

void controller::UndoRedoStack::clear()
{
    redoStack.reset(new std::vector<UndoActionPtr>());
    undoStack.reset(new std::vector<UndoActionPtr>());
}

