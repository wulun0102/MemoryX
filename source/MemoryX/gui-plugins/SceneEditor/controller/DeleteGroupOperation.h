/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// std
#include <exception>
#include <string>

// local
#include "../scene3D/Scene.h"
#include "Operation.h"

namespace controller
{
    /**
    * A operation to delete a group.
    *
    * @see controller::Operation
    */
    class DeleteGroupOperation : public Operation
    {
    public:
        /**
        * Creates a new operation, which deletes a group.
        *
        * @param memoryXController The MemoryXController to delete the group from. (This is currently not used.)
        * @param scene The scene to delete the group from.
        * @param groupName The name of the group to delete.
        *
        * @see controller::Operation::Operation
        */
        DeleteGroupOperation(const std::shared_ptr<memoryxcontroller::MemoryXController>& memoryXController, const std::shared_ptr<scene3D::Scene>& scene, const std::string& groupName);

        /**
        * Returns a new operation, which creates the deleted group again.
        *
        * @return The operation, which creates the deleted group.
        *
        * @see controller::Operation::createInverseOperation
        */
        const OperationPtr createInverseOperation() const override;

    protected:
        /**
        * Because the WorkingMemory does not support groups, nothing is done here.
        *
        * @see controller::Operation::executeOnWorkingMemory
        */
        void executeOnWorkingMemory() override;

        /**
        * Deletes the group.
        *
        * @see controller::Operation::executeOnScene
        */
        void executeOnScene() override;

    private:
        std::string groupName;

    };
}

