/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// std
#include <exception>
#include <vector>

// local
#include "UndoAction.h"

controller::UndoAction::UndoAction(const std::shared_ptr<std::vector<controller::OperationPtr> >& operations) :
    operations(operations)
{
}

std::shared_ptr<std::vector<controller::OperationPtr> > controller::UndoAction::redo()
{
    return operations;
}

std::shared_ptr<std::vector<controller::OperationPtr> > controller::UndoAction::undo()
{
    std::shared_ptr<std::vector<controller::OperationPtr> > inverted(new std::vector<controller::OperationPtr>());

    for (std::vector<controller::OperationPtr>::reverse_iterator it = operations->rbegin(); it != operations->rend(); ++it)
    {
        inverted->push_back(it->get()->createInverseOperation());
    }

    return inverted;
}

const std::shared_ptr<std::vector<controller::OperationPtr> > controller::UndoAction::getOperations() const
{
    return operations;
}

