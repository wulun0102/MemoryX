/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <algorithm>

#include "SceneGroupManager.h"

std::vector<scene3D::SceneGroupPtr> scene3D::SceneGroupManager::getAllGroups()
{
    return sceneGroups;
}

scene3D::SceneGroupPtr scene3D::SceneGroupManager::getGroupById(std::string groupId)
{
    for (SceneGroupPtr group : sceneGroups)
    {
        if (group->getGroupId().compare(groupId) == 0)
        {
            return group;
        }
    }

    return scene3D::SceneGroupPtr();
}

void scene3D::SceneGroupManager::addGroup(SceneGroupPtr group)
{
    //Check whether an object with same ID already exists
    if (this->getGroupById(group->getGroupId()) != NULL)
    {
        throw std::logic_error("Group already exists!");
    }

    //Otherwise push object in our list
    sceneGroups.push_back(group);
}

void scene3D::SceneGroupManager::removeGroup(scene3D::SceneGroupPtr group)
{
    if (group != NULL)
    {
        if (std::find(sceneGroups.begin(), sceneGroups.end(), group) != sceneGroups.end())
        {
            //Remove object from list
            sceneGroups.erase(std::remove(sceneGroups.begin(), sceneGroups.end(), group), sceneGroups.end());
        }
        else
        {
            throw std::runtime_error("This group is not registered in the sceneGroupManager!");
        }
    }
    else
    {
        throw std::runtime_error("Group is null!");
    }
}

void scene3D::SceneGroupManager::renameGroup(scene3D::SceneGroupPtr group, std::string newGroupId)
{
    if (this->getGroupById(newGroupId) != NULL)
    {
        throw std::invalid_argument("New ID of Group already exists!");
    }

    //Otherwise change ID of group
    group->setGroupId(newGroupId);
}
