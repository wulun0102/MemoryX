/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <Inventor/engines/SoElapsedTime.h>
#include <Inventor/nodes/SoPerspectiveCamera.h>
#include <Inventor/SoOffscreenRenderer.h>
#include <Inventor/nodes/SoDirectionalLight.h>
#include <Inventor/nodes/SoRotationXYZ.h>

#include "PreviewGenerator.h"

QImage scene3D::PreviewGenerator::createPreview(SoNode* node, int width, int height)
{
    SoSeparator* root = new SoSeparator();
    root->ref();

    SoPerspectiveCamera* camera = new SoPerspectiveCamera();
    root->addChild(camera);

    SoDirectionalLight* light = new SoDirectionalLight();
    root->addChild(light);

    root->addChild(node);

    SbViewportRegion const region;
    camera->viewAll(node, region);

    SoOffscreenRenderer offscreenRenderer(region);
    offscreenRenderer.setComponents(SoOffscreenRenderer::Components::RGB_TRANSPARENCY);
    offscreenRenderer.render(root);

    QImage img(offscreenRenderer.getBuffer(), width,
               height, QImage::Format_ARGB32);

    root->unref();

    // Important!
    return img.rgbSwapped();
}

SoSeparator* scene3D::PreviewGenerator::createAnimatedPreview(SoNode* node)
{
    SoSeparator* root = new SoSeparator;

    SoRotationXYZ* myRotXYZ = new SoRotationXYZ;
    root->addChild(myRotXYZ);

    root->addChild(node);

    myRotXYZ->axis = SoRotationXYZ::Y; // rotate about X axis
    SoElapsedTime* myCounter = new SoElapsedTime;
    myRotXYZ->angle.connectFrom(&myCounter->timeOut);


    return root;
}

