/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <string>
#include <vector>
#include <algorithm>

#include "SceneGroup.h"

std::string scene3D::SceneGroup::getGroupId()
{
    return this->groupID;
}

std::vector<scene3D::SceneObjectPtr> scene3D::SceneGroup::getAllObjects()
{
    return this->objects;
}

bool scene3D::SceneGroup::contains(scene3D::SceneObjectPtr object)
{
    return std::find(objects.begin(), objects.end(), object) != objects.end();
}

void scene3D::SceneGroup::addObject(scene3D::SceneObjectPtr object)
{
    //Check if object already exists in group
    if (object == NULL || this->contains(object))
    {
        return;
    }

    //Otherwise push object in our list
    objects.push_back(object);
}

void scene3D::SceneGroup::removeObject(scene3D::SceneObjectPtr object)
{
    //Parameter is not null and object is contained in group
    if (object != NULL && this->contains(object))
    {
        //Remove object from list
        objects.erase(std::remove(objects.begin(), objects.end(), object), objects.end());
    }
}

void scene3D::SceneGroup::clearGroup()
{
    this->objects.clear();
}

scene3D::SceneGroup::SceneGroup(const std::string& groupID) : groupID(groupID)
{

}

void scene3D::SceneGroup::setGroupId(std::string groupId)
{
    this->groupID = groupId;
}
