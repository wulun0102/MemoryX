/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SceneSelectionManager.h"
#include "../controller/SelectOperation.h"
#include "../controller/DeselectOperation.h"

bool scene3D::SceneSelectionManager::createOperations = true;

void scene3D::SceneSelectionManager::selectionCallback(void* userData, SoPath* selectionPath)
{
    Scene* scene = (Scene*) userData;
    SceneObject* object = (SceneObject*)selectionPath->getTail();

    scene->getManipulatorManager()->addManipulator(object);

    if (controller::ControllerPtr controller = scene->getController().lock())
    {
        if (SceneSelectionManager::createOperations)
        {
            std::shared_ptr<std::vector<controller::OperationPtr> > operations(new std::vector<controller::OperationPtr>());

            controller::OperationPtr operation(new controller::SelectOperation(
                                                   controller->getMemoryXController(),
                                                   controller->getScene(),
                                                   object->getObjectId()));
            operations->push_back(operation);
            controller->execute(controller::Controller::EXECUTE_ON_WM | controller::Controller::UNDOABLE, operations);
        }
        else
        {
            std::shared_ptr<std::vector<controller::OperationPtr> > operations(new std::vector<controller::OperationPtr>());
            //To make sure the selected object gets displayed in preview when in viewer mode
            controller->execute(0, operations);
        }
    }
}

void scene3D::SceneSelectionManager::deselectionCallback(void* userData, SoPath* selectionPath)
{
    Scene* scene = (Scene*) userData;
    SceneObject* object = (SceneObject*)selectionPath->getTail();

    scene->getManipulatorManager()->applyManipulator(object);

    if (controller::ControllerPtr controller = scene->getController().lock())
    {
        if (SceneSelectionManager::createOperations)
        {
            std::shared_ptr<std::vector<controller::OperationPtr> > operations(new std::vector<controller::OperationPtr>());

            controller::OperationPtr operation(new controller::DeselectOperation(
                                                   controller->getMemoryXController(),
                                                   controller->getScene(),
                                                   object->getObjectId()));
            operations->push_back(operation);
            controller->execute(controller::Controller::EXECUTE_ON_WM | controller::Controller::UNDOABLE, operations, false);
        }
        else
        {
            std::shared_ptr<std::vector<controller::OperationPtr> > operations(new std::vector<controller::OperationPtr>());
            //To make sure the selected object gets displayed in preview when in viewer mode
            controller->execute(0, operations, false);
        }
    }
}

SoPath* scene3D::SceneSelectionManager::pickFilterCB(void* userData, const SoPickedPoint* pick)
{
    SoFullPath* p = (SoFullPath*)pick->getPath();

    //Make sure we didn't select a manipulator
    for (int i = 0; i < p->getLength(); i++)
    {
        SoNode* n = p->getNode(i);

        if (n->isOfType(SoTransformManip::getClassTypeId()))
        {
            //Path includes a manipulator, so just ignore it
            //returning NULL would lead to a deselection of all objects
            //therefore return an empty path
            return new SoPath;
        }
    }

    /*Try to adapt the path to select our Object and truncate rest of path.
     This is not really necessary, because rest of class only uses SoPath
     and not SoFullPath, and therefore only sees Object at Tail of path anyway.
     It seems to be a good idea to keep everything tidy though.*/
    for (int i = 0; i < p->getLength(); i++)
    {
        SoNode* n = p->getNode(i);

        if (n->isOfType(SceneObject::getClassTypeId()))
        {
            p->truncate(i + 1);
        }
    }

    //Return new path
    return p;
}

scene3D::SceneSelectionManager::SceneSelectionManager(ScenePtr scene) : scene(scene)
{
    scene->selectionRootNode->setPickFilterCallback(pickFilterCB, scene.get());
    scene->selectionRootNode->addSelectionCallback(selectionCallback, scene.get());
    scene->selectionRootNode->addDeselectionCallback(deselectionCallback, scene.get());
}

std::vector<scene3D::SceneObjectPtr> scene3D::SceneSelectionManager::getAllSelected()
{
    std::vector<scene3D::SceneObjectPtr> selectedObjects;

    if (ScenePtr scene = this->scene.lock())
    {
        for (int i = 0; i < scene->selectionRootNode->getNumSelected(); i++)
        {
            SoPath* objectPath = scene->selectionRootNode->getPath(i);

            //SceneObject is a BaseKit, so we can assume its the tail
            //of a normal SoPath.
            SoNode* objectNode = objectPath->getNodeFromTail(0);

            if (objectNode->isOfType(SceneObject::getClassTypeId()))
            {
                selectedObjects.push_back(SceneObjectPtr((SceneObject*) objectNode));
            }
        }
    }

    return selectedObjects;
}

bool scene3D::SceneSelectionManager::isSelected(scene3D::SceneObjectPtr object)
{
    if (ScenePtr scene = this->scene.lock())
    {
        //Try to find a path that contains our object.
        for (int i = 0; i < scene->selectionRootNode->getNumSelected(); i++)
        {
            SoPath* objectPath = scene->selectionRootNode->getPath(i);

            if (objectPath->containsNode(object.get()))
            {
                return true;
            }
        }
    }

    return false;
}

void scene3D::SceneSelectionManager::addToSelection(scene3D::SceneObjectPtr object)
{
    if (ScenePtr scene = this->scene.lock())
    {
        //Temporarily disable throwing operations
        //Otherwise we would end in a deadlock in controller
        if (SceneSelectionManager::createOperations)
        {
            SceneSelectionManager::createOperations = false;
            scene->selectionRootNode->select(object.get());
            //Enable again
            SceneSelectionManager::createOperations = true;
        }
        else
        {
            scene->selectionRootNode->select(object.get());
        }
    }
}

void scene3D::SceneSelectionManager::removeFromSelection(scene3D::SceneObjectPtr object)
{
    if (ScenePtr scene = this->scene.lock())
    {
        //Temporarily disable throwing operations
        //Otherwise we would end in a deadlock in controller
        if (SceneSelectionManager::createOperations)
        {
            SceneSelectionManager::createOperations = false;
            scene->selectionRootNode->deselect(object.get());
            //Enable again
            SceneSelectionManager::createOperations = true;
        }
        else
        {
            scene->selectionRootNode->select(object.get());
        }
    }
}

void scene3D::SceneSelectionManager::addToSelection(scene3D::SceneGroupPtr group)
{
    for (SceneObjectPtr object : group->getAllObjects())
    {
        this->addToSelection(object);
    }
}

void scene3D::SceneSelectionManager::removeFromSelection(scene3D::SceneGroupPtr group)
{
    for (SceneObjectPtr object : group->getAllObjects())
    {
        this->removeFromSelection(object);
    }
}

void scene3D::SceneSelectionManager::storeHistory()
{
    this->historySelected = this->getAllSelected();
}

void scene3D::SceneSelectionManager::restoreHistory()
{
    for (SceneObjectPtr object : historySelected)
    {
        this->addToSelection(object);
    }
}

void scene3D::SceneSelectionManager::deselectAll()
{
    if (ScenePtr scene = this->scene.lock())
    {
        scene->selectionRootNode->deselectAll();
    }
}

scene3D::SceneObjectPtr scene3D::SceneSelectionManager::getLastSelected()
{
    if (ScenePtr scene = this->scene.lock())
    {
        //Try to find a path that contains our object.
        if (scene->selectionRootNode->getNumSelected() > 0)
        {
            SoPath* objectPath = scene->selectionRootNode->getPath(scene->selectionRootNode->getNumSelected() - 1);

            //SceneObject is a BaseKit, so we can assume its the tail
            //of a normal SoPath.
            SoNode* objectNode = objectPath->getNodeFromTail(0);
            return (SceneObjectPtr((SceneObject*)objectNode));
        }
    }

    return NULL;
}


void scene3D::SceneSelectionManager::setCreateOperations(bool enable)
{
    SceneSelectionManager::createOperations = enable;
}

bool scene3D::SceneSelectionManager::getCreateOperations()
{
    return SceneSelectionManager::createOperations;
}
