/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <Inventor/draggers/SoDragger.h>

#include "Scene.h"
#include "ManipulatorMode.h"


namespace scene3D
{
    class SceneManipulatorManager
    {
        friend class SceneSelectionManager;

    public:
        /**
         * Constructor
         * Creates an Instance of the Class.
         *
         * @param scene Existing Scene
         */
        SceneManipulatorManager(ScenePtr scene);

        /**
         * Sets a specific ManipulatorMode.
         *
         * @param mode ManipulatorMode to be set
         */
        void setManipulatorMode(scene3D::ManipulatorMode mode);

        /**
         * Returns the set ManipulatorMode.
         *
         * @return ManipulatorMode ManipulatorMode
         */
        ManipulatorMode getManipulatorMode();


        /**
         * Adds the selected Manipulator to a SceneObject.
         *
         * @param object SceneObject the Manipulator is added to
         */
        void addManipulator(SceneObjectPtr object);

        /**
         * Applys the selected Manipulator to a SceneObject.
         *
         * @param object SceneObject the Manipulator is applied to
         */
        void applyManipulator(SceneObjectPtr object);
    private:
        SceneWeakPtr scene;

        scene3D::ManipulatorMode manipulatorMode;

        static void manipulatorStartCallback(void* userdata, SoDragger* dragger);

        static void manipulatorFinishCallback(void* userdata, SoDragger* dragger);

        static void manipulatorValueChangedCallback(void* userdata, SoDragger* dragger);
    };
}

