/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <string>
#include <vector>

#include "SceneObject.h"
#include "Scene.h"

namespace scene3D
{
    class SceneObjectManager
    {
    public:
        /**
         * Constructor
         * Creates an Instance of the Class.
         *
         * @param scene Existing Scene
         */
        SceneObjectManager(ScenePtr scene);

        /**
         * Returns all Existing Objects.
         *
         * @return vector<scene3D::SceneObjectPtr > All Objects as Vector
         */
        std::vector<scene3D::SceneObjectPtr> getAllObjects() const;

        /**
         * Returns a SceneObject specified by Id.
         *
         * @param objectId Id specifying SceneObject
         * @return scene3D::SceneObject Specified SceneObject
         */
        scene3D::SceneObjectPtr getObjectById(const std::string& objectId) const;

        /**
         * Adds a SceneObject.
         *
         * @param object SceneObject
         */
        void addObject(scene3D::SceneObjectPtr object);

        /**
         * Removes a SceneObject.
         *
         * @param object SceneObject
         */
        void removeObject(scene3D::SceneObjectPtr object);

    private:
        std::vector<scene3D::SceneObjectPtr> sceneObjects;
        SceneWeakPtr scene;
    };
}

