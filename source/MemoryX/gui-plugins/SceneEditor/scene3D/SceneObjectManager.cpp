/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <algorithm>

#include "SceneObjectManager.h"

scene3D::SceneObjectManager::SceneObjectManager(ScenePtr scene) : scene(scene)
{

}

std::vector<scene3D::SceneObjectPtr> scene3D::SceneObjectManager::getAllObjects() const
{
    return this->sceneObjects;
}

scene3D::SceneObjectPtr scene3D::SceneObjectManager::getObjectById(const std::string& objectId) const
{
    for (SceneObjectPtr object : sceneObjects)
    {
        if (object->getObjectId().compare(objectId) == 0)
        {
            return object;
        }
    }

    return NULL;
}

void scene3D::SceneObjectManager::addObject(scene3D::SceneObjectPtr object)
{
    if (ScenePtr scene = this->scene.lock())
    {
        /* Lock scene, prevent rendering*/
        std::unique_lock lock(scene->execute_mutex);

        //Check whether an object with same ID already exists
        if (this->getObjectById(object->getObjectId()) != NULL)
        {
            throw std::logic_error("Object with this ID is already contained in scene!");
        }

        //Otherwise push object in our list
        sceneObjects.push_back(object);

        //Place in scene
        scene->objectRootNode->insertChild(object.get(), 0);
    }
}

void scene3D::SceneObjectManager::removeObject(scene3D::SceneObjectPtr object)
{
    if (ScenePtr scene = this->scene.lock())
    {
        /* Lock scene, prevent rendering*/
        std::unique_lock lock(scene->execute_mutex);

        if (object != NULL)
        {
            if (std::find(sceneObjects.begin(), sceneObjects.end(), object) != sceneObjects.end())
            {
                if (scene->getSelectionManager()->isSelected(object))
                {
                    std::cerr << "Warning: Remove selected item, whis is currently selected." << std::endl;
                }

                //Remove object from list
                sceneObjects.erase(std::remove(sceneObjects.begin(), sceneObjects.end(), object), sceneObjects.end());

                scene->getSelectionManager()->removeFromSelection(object);
                scene->objectRootNode->removeChild(object.get());
            }
            else
            {
                throw std::runtime_error("This object is not registered in the sceneObjectManager!");
            }
        }
        else
        {
            throw std::runtime_error("Object is null!");
        }
    }
}
