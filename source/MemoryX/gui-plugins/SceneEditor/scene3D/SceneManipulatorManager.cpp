/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <Inventor/manips/SoTransformerManip.h>

#include "SceneManipulatorManager.h"
#include "../controller/RotateTranslateOperation.h"

void scene3D::SceneManipulatorManager::setManipulatorMode(scene3D::ManipulatorMode mode)
{
    if (ScenePtr scene = this->scene.lock())
    {
        this->manipulatorMode = mode;

        for (SceneObjectPtr object : scene->getSelectionManager()->getAllSelected())
        {
            object->applyManipulator();
            this->addManipulator(object);
        }
    }
}

void scene3D::SceneManipulatorManager::manipulatorStartCallback(void* userdata, SoDragger* dragger)
{
    std::pair<SceneObject*, Scene*>* data = (std::pair<SceneObject*, Scene*>*) userdata;
    SceneObject* object = data->first;
    Scene* scene = data->second;

    for (SceneObjectPtr sceneobject : scene->sceneSelectionManager->getAllSelected())
    {
        sceneobject->pushHistory();

        if (sceneobject != object)
        {
            sceneobject->trackThisTransformation(object->manipNode);
        }

        sceneobject->isManipulated = true;
    }
}

void scene3D::SceneManipulatorManager::manipulatorValueChangedCallback(void* userdata, SoDragger* dragger)
{
    std::pair<SceneObject*, Scene*>* data = (std::pair<SceneObject*, Scene*>*) userdata;
    SceneObject* object = data->first;
    Scene* scene = data->second;

    if (controller::ControllerPtr controller = scene->getController().lock())
    {
        std::vector<std::string> changed;
        changed.push_back(object->getObjectId());
        controller->triggerObjectsChanged(changed);
    }
}

void scene3D::SceneManipulatorManager::manipulatorFinishCallback(void* userdata, SoDragger* dragger)
{
    Scene* scene = (Scene*)userdata;

    if (controller::ControllerPtr controller = scene->getController().lock())
    {
        std::shared_ptr<std::vector<controller::OperationPtr> > operations(new std::vector<controller::OperationPtr>());

        for (SceneObjectPtr sceneobject : scene->sceneSelectionManager->getAllSelected())
        {

            controller::OperationPtr operation(new controller::RotateTranslateOperation(
                                                   controller->getMemoryXController(),
                                                   controller->getScene(), sceneobject->getObjectId(),
                                                   sceneobject->getHistoryRotation(), sceneobject->getRotation(),
                                                   sceneobject->getHistoryTranslation(), sceneobject->getTranslation()));

            operations->push_back(operation);

            sceneobject->untrackTransformations();
            sceneobject->isManipulated = false;
        }

        controller->execute(controller::Controller::EXECUTE_ON_WM | controller::Controller::UNDOABLE, operations, false);
        controller->executeQueuedOperations(false);
    }
}

scene3D::SceneManipulatorManager::SceneManipulatorManager(ScenePtr scene) : scene(scene)
{
    this->manipulatorMode = ManipulatorMode::NONE;
}

void scene3D::SceneManipulatorManager::addManipulator(scene3D::SceneObjectPtr object)
{
    //Create new manipulator
    SoTransformerManip* manip = new SoTransformerManip;
    SoSeparator* nullSep = new SoSeparator;

    //Make all scale knobs disappear
    manip->getDragger()->setPart("scale1", nullSep);
    manip->getDragger()->setPart("scale2", nullSep);
    manip->getDragger()->setPart("scale3", nullSep);
    manip->getDragger()->setPart("scale4", nullSep);
    manip->getDragger()->setPart("scale5", nullSep);
    manip->getDragger()->setPart("scale6", nullSep);
    manip->getDragger()->setPart("scale7", nullSep);
    manip->getDragger()->setPart("scale8", nullSep);


    if (this->manipulatorMode != ManipulatorMode::ALL)
    {
        if (this->manipulatorMode == ManipulatorMode::ROTATION
            || this->manipulatorMode == ManipulatorMode::NONE)
        {
            //Make all translation knobs disappear
            manip->getDragger()->setPart("translator1", nullSep);
            manip->getDragger()->setPart("translator2", nullSep);
            manip->getDragger()->setPart("translator3", nullSep);
            manip->getDragger()->setPart("translator4", nullSep);
            manip->getDragger()->setPart("translator5", nullSep);
            manip->getDragger()->setPart("translator6", nullSep);
        }

        if (this->manipulatorMode == ManipulatorMode::TRANSLATION
            || this->manipulatorMode == ManipulatorMode::NONE)
        {
            //Make all translation knobs disappear
            manip->getDragger()->setPart("rotator1", nullSep);
            manip->getDragger()->setPart("rotator2", nullSep);
            manip->getDragger()->setPart("rotator3", nullSep);
            manip->getDragger()->setPart("rotator4", nullSep);
            manip->getDragger()->setPart("rotator5", nullSep);
            manip->getDragger()->setPart("rotator6", nullSep);
        }
    }

    if (ScenePtr scene = this->scene.lock())
    {
        manip->getDragger()->addStartCallback(manipulatorStartCallback, new std::pair<SceneObject*, Scene*>(object.get(), scene.get()));
        manip->getDragger()->addValueChangedCallback(manipulatorValueChangedCallback, new std::pair<SceneObject*, Scene*>(object.get(), scene.get()));
        manip->getDragger()->addFinishCallback(manipulatorFinishCallback, scene.get());
    }

    object->addManipulator(manip);
}

void scene3D::SceneManipulatorManager::applyManipulator(scene3D::SceneObjectPtr object)
{
    object->applyManipulator();
}

scene3D::ManipulatorMode scene3D::SceneManipulatorManager::getManipulatorMode()
{
    return this->manipulatorMode;
}
