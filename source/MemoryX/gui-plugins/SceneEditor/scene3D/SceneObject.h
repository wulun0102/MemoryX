/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/nodes/SoTransform.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodekits/SoBaseKit.h>
#include <Inventor/SbRotation.h>
#include <Inventor/manips/SoTransformManip.h>

#include <string>
#include <map>

namespace scene3D
{
    class SceneGroup;
}

namespace scene3D
{
    class SceneObject : public SoBaseKit
    {

        friend class SceneManipulatorManager;

        SO_KIT_HEADER(SceneObject);

        SO_KIT_CATALOG_ENTRY_HEADER(root);
        SO_KIT_CATALOG_ENTRY_HEADER(transform);
        SO_KIT_CATALOG_ENTRY_HEADER(transformManip);
        SO_KIT_CATALOG_ENTRY_HEADER(modelswitch);
        SO_KIT_CATALOG_ENTRY_HEADER(visual);
        SO_KIT_CATALOG_ENTRY_HEADER(collision);

    public:
        /**
         * @brief
         *
         * @param objectId
         * @param classId
         * @param collection
         * @param geometry
         * @param collision
         */
        SceneObject(std::string objectId, std::string classId, std::string collection, SoSeparator* geometry, SoSeparator* collision);

        /**
         * Returns the Translation of the SceneObject.
         *
         * @return SbVec3f Translation of SceneObject
         */
        SbVec3f getTranslation();

        /**
         * Sets the Translation of the SceneObject.
         *
         * @param translation Translation of SceneObject
         */
        void setTranslation(SbVec3f translation);

        /**
         * Returns the Rotation of the SceneObject.
         *
         * @return SbRotation Rotation of SceneObject
         */
        SbRotation getRotation();

        /**
         * Sets the Rotation of the SceneObject.
         *
         * @param rotation Rotation of SceneObject
         */
        void setRotation(SbRotation rotation);

        /**
         * Decides, whether CollisionMesh is beeing shown.
         *
         * @param show Show Collision Mesh
         */
        void showCollisionMesh(bool show);

        /**
         * Initializes Class.
         *
         */
        static void initClass();

        /**
         * Returns the ObjectId of the SceneObject.
         *
         * @return std::string ObjectId of the SceneObject
         */
        std::string getObjectId() const;
        /**
         * Sets the ObjectId of the SceneObject.
         *
         * @param objectId ObjectId of the SceneObject
         */
        void setObjectId(std::string& objectId);
        /**
         * Returns the ClassId of the SceneObject.
         *
         * @return std::string ClassId of the SceneObject
         */
        std::string getClassId() const;
        /**
         * Sets the ClassId of the SceneObject.
         *
         * @param classId ClassId of the SceneObject
         */
        void setClassId(std::string& classId);
        /**
         * Returns the Collection of the SceneObject.
         *
         * @return std::string Collection of the SceneObject
         */
        std::string getCollection() const;
        /**
         * Sets the Collection of the SceneObject.
         *
         * @param collection Collection of the SceneObject
         */
        void setCollection(std::string& collection);

        /**
         * Returns all Attributes of the SceneObject as Map.
         *
         * @return std::map<std::string, std::string> All Attributes as Map
         */
        std::map<std::string, std::string> getAllAttributes();

        /**
         * Returns if you are allowed to change this object.
         *
         * @return true, if you are allowed; false otherwise
         */
        bool isMutable();

    private:
        static bool isInitialized;
        std::string classId;
        std::string collection;
        std::string objectId;

        SceneObject();

        void addManipulator(SoTransformManip* manip);

        void applyManipulator();

        bool hasManipulator();

        void trackThisTransformation(SoTransform* transformation);
        void untrackTransformations();

        SbVec3f lastTranslation;
        SbRotation lastRotation;
        void pushHistory();

        SbRotation getHistoryRotation();
        SbVec3f getHistoryTranslation();

        SoTransformManip* manipNode;
        SoTransform* transformNode;
        SoSwitch* switchNode;

        bool isManipulated;
    };
}

