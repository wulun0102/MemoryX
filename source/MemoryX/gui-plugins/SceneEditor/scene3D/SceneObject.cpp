/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <Inventor/nodes/SoTransform.h>
#include "SceneObject.h"
#include "Scene.h"
#include <boost/format.hpp>

SO_KIT_SOURCE(scene3D::SceneObject)

void scene3D::SceneObject::initClass()
{
    SO_KIT_INIT_CLASS(SceneObject, SoBaseKit, "BaseKit");
}

bool scene3D::SceneObject::isInitialized = false;

scene3D::SceneObject::SceneObject(std::string objectId, std::string classId, std::string collection,
                                  SoSeparator* geometry, SoSeparator* collision) : classId(classId), collection(collection), manipNode(nullptr), isManipulated(false)
{

    if (!SceneObject::isInitialized)
    {
        SceneObject::initClass();
        SceneObject::isInitialized = true;
    }

    //Initializer for our BaseKit
    SO_KIT_CONSTRUCTOR(SceneObject);
    SO_KIT_ADD_CATALOG_ENTRY(root, SoSeparator, false, this, "", TRUE);
    SO_KIT_ADD_CATALOG_ENTRY(transformManip, SoTransformManip,
                             TRUE, root, "", TRUE);
    SO_KIT_ADD_CATALOG_ENTRY(transform, SoTransform,
                             TRUE, root, "", TRUE);
    SO_KIT_ADD_CATALOG_ENTRY(modelswitch, SoSwitch,
                             TRUE, root, "", TRUE);
    SO_KIT_ADD_CATALOG_ENTRY(visual, SoSeparator,
                             TRUE, modelswitch, "", TRUE);
    SO_KIT_ADD_CATALOG_ENTRY(collision, SoSeparator,
                             TRUE, modelswitch, "", TRUE);
    SO_KIT_INIT_INSTANCE();

    //Use given Ids as our node name and object class
    this->setObjectId(objectId);

    //Initialize root node
    this->setPart("root", new SoSeparator);

    //Add an empty transform node, manipulator is still null and not displayed
    this->transformNode = new SoTransform;
    this->setPart("transform", this->transformNode);

    //Add switch for visual and collision model
    this->switchNode = new SoSwitch;
    this->setPart("modelswitch", this->switchNode);
    //Set switch to show visual model
    this->switchNode->whichChild = 0;

    //Insert visual model
    this->setPart("visual", geometry);

    //Insert collision model
    this->setPart("collision", collision);
}

scene3D::SceneObject::SceneObject() : isManipulated(false)
{
    //Initializer for our BaseKit
    SO_KIT_CONSTRUCTOR(SceneObject);
    SO_KIT_ADD_CATALOG_ENTRY(root, SoSeparator, false, this, "", TRUE);
    SO_KIT_ADD_CATALOG_ENTRY(transform, SoTransform,
                             TRUE, this, nullptr, TRUE);
    SO_KIT_ADD_CATALOG_ENTRY(transformManip, SoTransformManip,
                             TRUE, root, "", TRUE);
    SO_KIT_ADD_CATALOG_ENTRY(modelswitch, SoSwitch,
                             TRUE, this, nullptr, TRUE);
    SO_KIT_ADD_CATALOG_ENTRY(visual, SoSeparator,
                             TRUE, this, modelswitch, TRUE);
    SO_KIT_ADD_CATALOG_ENTRY(collision, SoSeparator,
                             TRUE, this, modelswitch, TRUE);
    SO_KIT_INIT_INSTANCE();
}

SbVec3f scene3D::SceneObject::getTranslation()
{
    if (this->hasManipulator())
    {
        SoTransform* tempTransform = new SoTransform;
        tempTransform->ref();

        tempTransform->combineRight(transformNode);
        tempTransform->combineRight(manipNode);

        SbVec3f result = tempTransform->translation.getValue();
        tempTransform->unref();
        return result;
    }

    return this->transformNode->translation.getValue();
}

void scene3D::SceneObject::setTranslation(SbVec3f translation)
{
    this->transformNode->translation.setValue(translation);

    //Reset manipulator
    if (this->hasManipulator())
    {
        this->manipNode->setToDefaults();
    }
}

SbRotation scene3D::SceneObject::getRotation()
{
    if (this->hasManipulator())
    {
        SoTransform* tempTransform = new SoTransform;
        tempTransform->ref();

        tempTransform->combineRight(transformNode);
        tempTransform->combineRight(manipNode);


        SbRotation result = tempTransform->rotation.getValue();
        tempTransform->unref();
        return result;
    }

    return this->transformNode->rotation.getValue();
}

void scene3D::SceneObject::setRotation(SbRotation rotation)
{
    this->transformNode->rotation.setValue(rotation);

    //Reset manipulator
    if (this->hasManipulator())
    {
        this->manipNode->setToDefaults();
    }
}

void scene3D::SceneObject::showCollisionMesh(bool show)
{
    this->switchNode->whichChild = show ? 1 : 0;
}

void scene3D::SceneObject::addManipulator(SoTransformManip* manip)
{
    this->manipNode = manip;
    this->setPart("transformManip", manipNode);
}

void scene3D::SceneObject::applyManipulator()
{
    if (this->hasManipulator())
    {
        transformNode->combineRight(manipNode);
        this->setPart("transformManip", NULL);
        this->manipNode = nullptr;
    }
}

bool scene3D::SceneObject::hasManipulator()
{
    return this->manipNode != nullptr;
}

std::string scene3D::SceneObject::getObjectId() const
{
    return this->objectId;
}

void scene3D::SceneObject::setObjectId(std::string& objectId)
{
    this->objectId = objectId;
}

std::string scene3D::SceneObject::getClassId() const
{
    return classId;
}

void scene3D::SceneObject::setClassId(std::string& classId)
{
    this->classId = classId;
}

std::string scene3D::SceneObject::getCollection() const
{
    return collection;
}

void scene3D::SceneObject::setCollection(std::string& collection)
{
    this->collection = collection;
}

std::map<std::string, std::string> scene3D::SceneObject::getAllAttributes()
{
    std::map<std::string, std::string> allAttributes;

    //Translation
    float x, y, z;
    this->getTranslation().getValue(x, y, z);
    allAttributes.insert(std::pair<std::string, std::string>("Translation X", std::to_string(x)));
    allAttributes.insert(std::pair<std::string, std::string>("Translation Y", std::to_string(y)));
    allAttributes.insert(std::pair<std::string, std::string>("Translation Z", std::to_string(z)));

    //Rotation
    float pw, px, py, pz;
    this->getRotation().getValue(px, py, pz, pw);
    allAttributes.insert(std::pair<std::string, std::string>("Rotation X", std::to_string(px)));
    allAttributes.insert(std::pair<std::string, std::string>("Rotation Y", std::to_string(py)));
    allAttributes.insert(std::pair<std::string, std::string>("Rotation Z", std::to_string(pz)));
    allAttributes.insert(std::pair<std::string, std::string>("Angle", std::to_string(pw)));

    //Matrix
    SbMatrix matrix;
    matrix.setRotate(this->getRotation());
    matrix.setTranslate(this->getTranslation());
    std::string out;

    for (int i = 0; i < 4; i++)
    {
        out.append(boost::str(boost::format("%3.2g %3.2g %3.2g %3.2g") %
                              matrix[i][0] % matrix[i][1] %
                              matrix[i][2] % matrix[i][3]));

        if (i < 3)
        {
            out.append("\n");
        }
    }

    allAttributes.insert(std::pair<std::string, std::string>("Matrix", out));

    return allAttributes;
}
void scene3D::SceneObject::pushHistory()
{
    this->lastRotation = this->getRotation();
    this->lastTranslation = this->getTranslation();
}

SbRotation scene3D::SceneObject::getHistoryRotation()
{
    return this->lastRotation;
}

SbVec3f scene3D::SceneObject::getHistoryTranslation()
{
    return lastTranslation;
}

void scene3D::SceneObject::trackThisTransformation(SoTransform* transformation)
{
    this->manipNode->rotation.connectFrom(&transformation->rotation);
    this->manipNode->translation.connectFrom(&transformation->translation);
}

void scene3D::SceneObject::untrackTransformations()
{
    this->manipNode->rotation.disconnect();
    this->manipNode->translation.disconnect();
}

bool scene3D::SceneObject::isMutable()
{
    return !isManipulated;
}
