/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

//Typedefs
#include "PointerDefinitions.h"

//Manager
#include "SceneObjectManager.h"
#include "SceneGroupManager.h"
#include "SceneSelectionManager.h"
#include "SceneManipulatorManager.h"
#include "PreviewGenerator.h"

//Other classes
#include "../controller/Controller.h"
#include "../gui/ScenegraphWidget.h"
#include "../gui/MinimapWidget.h"

//Inventor
#include <Inventor/nodes/SoSelection.h>
#include <Inventor/nodes/SoCamera.h>

#include <mutex>


namespace controller
{
    class Controller;
}

namespace gui
{
    class ScenegraphWidget;
    class MinimapWidget;
}

namespace scene3D
{
    class Scene
    {

        friend class SceneObjectManager;

        friend class SceneSelectionManager;

        friend class SceneManipulatorManager;

        friend class gui::ScenegraphWidget;

        friend class gui::MinimapWidget;

    public:
        /**
        * Creates a new instance of the Scene
        */
        static ScenePtr create(controller::ControllerPtr controller);

        ~Scene();

        /**
         * Registers a new camera to the scene. Basically adds it on top of selection root node, which is head of
         * our scene.
         *
         * @param camera The Camera to add to the scene.
         * @return SoSeparator A Seperator which can now be used as root in scene in, for example, a viewer.
         */
        SoSeparator* registerCamera(SoCamera* camera);

        /**
         * Returns the SceneObjectManager, which handles all objects displayed in scene.
         *
         * @see scene3D::SceneObjectManager
         * @return SceneObjectManager The current SceneObjectManager
         */
        SceneObjectManagerPtr getObjectManager();

        /**
         * Returns the SceneGroupManager, which handles all groups saved in scene.
         *
         * @see scene3D::SceneGroupManager
         * @return SceneGroupManager The SceneGroupManager
         */
        SceneGroupManagerPtr getGroupManager();

        /**
         * Returns the SelectionManager, which handles all selection actions in scene.
         *
         * @see scene3D::SceneSelectionManager
         * @return SceneSelectionManager The SceneSelectionManager
         */
        SceneSelectionManagerPtr getSelectionManager();

        /**
         * Returns the ManipulatorManager, which handles all manipulators and their callbacks.
         *
         * @see scene3D::SceneManipulationManager
         * @return SceneManipulatorManager The SceneManipulationManager
         */
        SceneManipulatorManagerPtr getManipulatorManager();

        /**
         * Returns the PreviewGenerator, which can generate preview images of objects.
         *
         * @see scene3D::PreviewGenerator
         * @return PreviewGenerator The PreviewGenerator
         */
        PreviewGeneratorPtr getPreviewGenerator();

        /**
         * Returns the Controller for this scene.
         *
         * @see controller::controller
         * @return controller::Controller The Controller
         */
        controller::ControllerWeakPtr getController();

        /**
         * Switch back to ViewerMode.
         *
         */
        void enterViewerMode();

        /**
         * Switch to EditorMode.
         *
         */
        void enterEditorMode();


        /**
         * Any changes to the scene have to be synchronized. It is not allowed to change scenegraph
         * while rendering
         */
        std::mutex execute_mutex;

        /**
          * Make camera view all. Makes sure grid floor is not taken into account.
         */
        void makeCameraViewAll(SoCamera* camera, const SbViewportRegion& region);

    private:
        /**
         * Constructor, called by static create()
         * Creates a new Scene. Also initializes all managers needed to manage the objects, the selection,
         * the manipulators and groups. Adds a gridfloor to the scene.
         *
         * @param controller The controller used to execute operations and get access to other components of tool.
         */
        Scene(controller::ControllerPtr controller);
        bool viewerMode;

        SoSelection* selectionRootNode;
        SoSeparator* objectRootNode;

        SceneObjectManagerPtr sceneObjectManager;
        SceneGroupManagerPtr sceneGroupManager;
        SceneSelectionManagerPtr sceneSelectionManager;
        SceneManipulatorManagerPtr sceneManipulatorManager;
        PreviewGeneratorPtr scenePreviewGenerator;
        controller::ControllerWeakPtr controller;
    };
}

