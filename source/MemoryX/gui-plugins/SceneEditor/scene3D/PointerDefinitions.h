/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "SceneObject.h"

#include <boost/smart_ptr/intrusive_ptr.hpp>

#include <iostream>
#include <memory>

namespace scene3D
{
    class Scene;
    using ScenePtr = std::shared_ptr<Scene>;
    using SceneWeakPtr = std::weak_ptr<Scene>;

    class SceneObject;
    using SceneObjectPtr = boost::intrusive_ptr<SceneObject>;

    inline void intrusive_ptr_add_ref(scene3D::SceneObject* obj)
    {
        obj->ref();
    }

    inline void intrusive_ptr_release(scene3D::SceneObject* obj)
    {
        obj->unref();
    }

    class SceneGroup;
    using SceneGroupPtr = std::shared_ptr<SceneGroup>;

    class SceneObjectManager;
    using SceneObjectManagerPtr = std::shared_ptr<SceneObjectManager>;

    class SceneGroupManager;
    using SceneGroupManagerPtr = std::shared_ptr<SceneGroupManager>;

    class SceneSelectionManager;
    using SceneSelectionManagerPtr = std::shared_ptr<SceneSelectionManager>;

    class SceneManipulatorManager;
    using SceneManipulatorManagerPtr = std::shared_ptr<SceneManipulatorManager>;

    class PreviewGenerator;
    using PreviewGeneratorPtr = std::shared_ptr<PreviewGenerator>;
}


