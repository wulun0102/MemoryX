/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <exception>
#include <string>
#include <map>
#include <vector>
#include <mutex>

// Coin3D
#include <Inventor/nodes/SoNode.h>

// MemoryXInterface
#include <MemoryX/interface/components/PriorKnowledgeInterface.h>
#include <MemoryX/interface/components/CommonStorageInterface.h>

// MemoryX
#include <MemoryX/core/MemoryXCoreObjectFactories.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectInstance.h>

#include "MemoryXController.h"

namespace memoryxcontroller
{
    class MemoryXController;

    /**
     * @brief The controller for the communication with the priorknowledge.
     *
     * This class provides methods to get information from the priorknowledge.
     * It is not possible to change the content of the priorknowledge.
     */
    class PriorKnowledgeController
    {
    public:
        /**
         * @brief Constructor.
         *
         * Constructs a new instance of this class.
         *
         * @param priorKnowledgePrx the proxy to the priorknowledge where the object classes are stored
         */
        PriorKnowledgeController(const memoryx::PriorKnowledgeInterfacePrx& priorKnowledgePrx);
        /**
         * @brief Copy-constructor.
         *
         * This constructor can be used if a copy of the given PriorknowledgeController is needed.
         * It prevents that problems with mutex:scoped_lock occure.
         *
         * @param other the priorknowledgecontroller that is about to be copied
         */
        PriorKnowledgeController(const PriorKnowledgeController& other);
        /**
         * @brief Destructor.
         *
         */
        ~PriorKnowledgeController();
        /**
         * @brief Returns a list of the names of all collections in the database.
         *
         * @return std::vector<std::string> the names of all availiable collections
         */
        std::vector<std::string> getCollectionNames() const;
        /**
         * @brief Returns a list of pointers to all object classes in the given collection.
         *
         * @param collection the name of the collection
         * @return std::vector<memoryx::ObjectClassPtr> the pointers to all object classes
         */
        std::vector<memoryx::ObjectClassPtr> getAllObjectClassesPtr(const std::string& collection) const;
        /**
         * @brief Returns a pointer to the object class with the given name in the give collection.
         *
         * If there is no object class with the given name, a null pointer is returned.
         *
         * @param className the name of the object class
         * @param collection the name of the collection, where the object class belongs to
         * @return memoryx::ObjectClassPtr the pointer to the object class with the given name
         */
        memoryx::ObjectClassPtr getObjectClassPtr(const std::string& className, const std::string& collection) const;
        /**
         * @brief Returns a coin node that contains a visual model of the given object class.
         *
         * If the parameter collisionModel is true, the a collision model of the object class is returned, otherwise its visualisation model.
         *
         * @param objectClass the pointer to object class
         * @param collisionModel true if the collision model should be returned
         * @return SoNode the coin node that contains the requested visual model
         */
        SoNode* getCoinVisualisation(const memoryx::ObjectClassPtr& objectClass, const bool& collisionModel) const;
        /**
         * @brief Returns a map with every attribute and its value of the given object class.
         *
         * The values of the attributes are partially converted in strings that are easy to display.
         *
         * @param objectClass the pointer to the given object class
         * @return std::map<std::string, std::string> the map with the attributes
         */
        std::map<std::string, std::string> getAllAttributes(const memoryx::ObjectClassPtr& objectClass) const;
        /**
         * @brief Returns the name of the collection the object class of the given object instance belongs to.
         *
         * @param objectInstance the given object instance
         * @return std::string the name of the collection the object class belongs to
         */
        std::string getCollection(const memoryx::ObjectInstancePtr& objectInstance) const;

    private:
        memoryx::PriorKnowledgeInterfacePrx priorKnowledgePrx;
        memoryx::CommonStorageInterfacePrx databasePrx;
        memoryx::PersistentObjectClassSegmentBasePrx classesSegmentPrx;
        MemoryXController* memoryXController;

        memoryx::GridFileManagerPtr fileManager;

        mutable std::recursive_mutex mutexEntities;
    };
    using PriorKnowledgeControllerPtr = std::shared_ptr<PriorKnowledgeController>;
}

