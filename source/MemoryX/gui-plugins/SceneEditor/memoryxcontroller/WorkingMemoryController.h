/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <exception>
#include <string>
#include <vector>

// Coin3D
#include <Inventor/SbVec3f.h>
#include <Inventor/SbRotation.h>

// MemoryXInterface
#include <MemoryX/interface/components/WorkingMemoryInterface.h>
#include <MemoryX/interface/memorytypes/MemorySegments.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectClass.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectInstance.h>

// Core
#include <ArmarXCore/core/ManagedIceObject.h>

#include "MemoryXController.h"

#include <mutex>

namespace memoryxcontroller
{
    class MemoryXController;

    /**
     * @brief The controller for the communication with the working memory.
     *
     * This class provides methods to add, change and remove object instances in the working memory.
     * Furthermore it is possible to get the content of the memory and save its content in snapshots.
     */
    class WorkingMemoryController :
        public armarx::ManagedIceObject,
        virtual public memoryx::WorkingMemoryListenerInterface
    {
    public:

        /**
         * @brief Constructor.
         *
         * Creates a new instance of this class and registers this managed ice object.
         *
         * @param workingMemoryPrx the proxy to the used working memory
         * @param workingMemoryUpdatesTopic the name of the topic this ice object will subscribe to recieve events from the working memory
         * @param objectInstancesSegmentName the name of the segment in the data base where the object instances are stored
         * @param memoryXController the MemoryXController
         *
         * @see memoryxcontroller::MemoryXController
         */
        WorkingMemoryController(const memoryx::WorkingMemoryInterfacePrx& workingMemoryPrx,
                                const std::string& workingMemoryUpdatesTopic,
                                const std::string& objectInstancesSegmentName,
                                const std::shared_ptr<MemoryXController>& memoryXController);
        /**
         * @brief Destructor.
         *
         * Destructs this instance of this class.
         */
        ~WorkingMemoryController() override;
        /**
         * @brief Adds a new object instance to the working memory.
         *
         * Returns the id of the new object instance.
         *
         * @param objectName the name of the added object instance
         * @param objectClassPtr a pointer to the object class of the new object instance
         * @return std::string the unique id of this object instance that is set by the working memory
         */
        std::string addObjectInstance(const std::string& objectName, const memoryx::ObjectClassPtr& objectClassPtr);
        /**
         * @brief Adds all given object instances to the working memory.
         *
         * This method will also add the given object instances to the local scene
         *
         * @param instancePtrs object instances to add to the working memory
         * @return std::vector<std::string>
         */
        std::vector<std::string> addObjectInstances(const std::vector<memoryx::ObjectInstancePtr>& instancePtrs);
        /**
         * @brief Removes the object instance with the given id.
         *
         * @throws Exception when there is no object instance with the given id
         * @param id the id of the object instance that should be removed from the working memory
         */
        void removeObjectInstance(const std::string& id);
        /**
         * @brief Applies the given orientation and position to the object instance with the given id.
         *
         * Important: The values of the position vector will be multiplied by 1000 because the working memory uses another scale
         *
         * @param id the id of the object that gets the new orientation and position
         * @param newOrientation the new orientation
         * @param newPosition the new position
         */
        void rotateTranslateObject(const std::string& id, const SbRotation& newOrientation, const SbVec3f& newPosition);
        /**
         * @brief Returns all object ids that are currently in the working memory.
         *
         * @return std::vector<std::string> all object ids of the objects in the working memory
         */
        std::vector<std::string> getAllObjectInstances() const;

        /**
         * @brief Adds all object instances of the working memory to the local scene.
         *
         */
        void addAllInstancesToLocalScene() const;

        /**
         * @brief Saves the whole scene in a snapshot with the given name.
         *
         * Retruns true if the saving was sucessful.
         *
         * @param snapshotName the name of the snapshot where all object instances of the working memory are saved
         * @param longtermMemoryPrx a proxy for the longterm-memory where the snapshot will be stored
         * @return bool returns true if the saving was succesful
         */
        bool saveSceneInSnapshot(const std::string& snapshotName, const memoryx::LongtermMemoryInterfacePrx& longtermMemoryPrx) const;
        /**
         * @brief Saves the object instances with the given ids in a snapshot with the given name.
         *
         * Retruns true if the saving was sucessful.
         *
         * @param snapshotName the name of the snapshot where the object instances are saved
         * @param longtermMemoryPrx a proxy for the longterm-memory where the snapshot will be stored
         * @param objectIds the list of ids
         * @return bool returns true if the saving was succesful
         */
        bool saveObjectsInSnapshot(const std::string& snapshotName, const memoryx::LongtermMemoryInterfacePrx& longtermMemoryPrx, const std::vector<std::string>& objectIds) const;

        /**
        * Returns the rotation of a object instance. The rotation is translated to the format used in the Scene.
        *
        * @return The rotation.
        */
        static SbRotation getSbRotationFromInstance(const memoryx::ObjectInstancePtr& objectInstance);

        /**
        * Returns the position of a object instance. The position is translated to the format used in the Scene.
        *
        * @return The position.
        */
        static SbVec3f getSbVec3fFromInstance(const memoryx::ObjectInstancePtr& objectInstance);

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         *
         */
        void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         *
         */
        void onConnectComponent() override;

        /**
         * @brief Returns the default name of this managed ice object.
         *
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override;

        /**
         * @brief Recognises when an entity is created in the working memory
         *
         * @see armarx::ManagedIceObject::reportEntityCreated()
         */
        void reportEntityCreated(const std::string& segmentName, const ::memoryx::EntityBasePtr& entity, const ::Ice::Current& = Ice::emptyCurrent) override;
        /**
         * @brief Recognises when an entity is updated in the working memory
         *
         * @see armarx::ManagedIceObject::reportEntityUpdated()
         */
        void reportEntityUpdated(const std::string& segmentName, const ::memoryx::EntityBasePtr& entityOld, const ::memoryx::EntityBasePtr& entityNew, const ::Ice::Current& = Ice::emptyCurrent) override;
        /**
         * @brief Recognises when an entity is removed from the working memory
         *
         * @see armarx::ManagedIceObject::reportEntityRemoved()
         */
        void reportEntityRemoved(const std::string& segmentName, const ::memoryx::EntityBasePtr& entity, const ::Ice::Current& = Ice::emptyCurrent) override;
        /**
         * @brief Recognises when a segment was loaded from a snapshot
         *
         * @see armarx::ManagedIceObject::reportSnapshotLoaded()
         */
        void reportSnapshotLoaded(const std::string& segmentName, const ::Ice::Current& = Ice::emptyCurrent) override;
        void reportSnapshotCompletelyLoaded(const Ice::Current& c = Ice::emptyCurrent) override { }

        /**
         * @brief Recognises when an entity is created on the working memory
         *
         * @see armarx::ManagedIceObject::reportMemoryCleared()
         */
        void reportMemoryCleared(const std::string& segmentName, const ::Ice::Current& = Ice::emptyCurrent) override;

    private:
        mutable std::recursive_mutex mutexEntities;

        bool findAndRemoveInstanceFromList(std::list<memoryx::ObjectInstancePtr>& list, const memoryx::ObjectInstancePtr& instance) const;

        memoryx::WorkingMemoryInterfacePrx workingMemoryPrx;
        memoryx::ObjectInstanceMemorySegmentBasePrx objectInstancesPrx;
        std::weak_ptr<memoryxcontroller::MemoryXController> memoryXController;

        std::string workingMemoryUpdatesTopic;
        std::list<memoryx::ObjectInstancePtr> currentChangedInstances;
    };
    using WorkingMemoryControllerPtr = IceInternal::Handle<WorkingMemoryController>;
}

