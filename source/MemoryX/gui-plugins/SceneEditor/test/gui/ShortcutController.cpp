/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE ArmarX::SceneEditor::gui::ShortcutController
#define ARMARX_BOOST_TEST

#include <MemoryX/Test.h>

#include "../../gui/ShortcutController.h"

#include <QMenu>
#include <QApplication>

static QApplication* app = new QApplication(boost::unit_test::framework::master_test_suite().argc, boost::unit_test::framework::master_test_suite().argv);

BOOST_AUTO_TEST_CASE(initEmptyShortcutController) // works!
{
    // test values

    // action done
    gui::ShortcutController* testShortcutController  = new gui::ShortcutController();

    // compare values

    // result
    BOOST_CHECK(testShortcutController);
}

BOOST_AUTO_TEST_CASE(initFilledShortcutController) // works!
{
    // test values
    QString testName1 = "test value !§$%&/()=? 1";
    QString testName2 = "test value !§$%&/()=? 2";
    QKeySequence testKeySequence1(Qt::Key_X);
    QKeySequence testKeySequence2(Qt::Key_Y);
    int countName1 = 0, countName2 = 0, countOthers = 0;

    QHash<QString, QKeySequence> paramHash;
    paramHash.insert(testName1, testKeySequence1);
    paramHash.insert(testName2, testKeySequence2);

    // action done
    gui::ShortcutController* testShortcutController = new gui::ShortcutController(paramHash);

    // compare values
    QHash<QString, QKeySequence>::iterator i;
    QHash<QString, QKeySequence> allShortcuts = testShortcutController->getAllShortcuts();

    // test
    BOOST_CHECK(testShortcutController);

    for (i = allShortcuts.begin(); i != allShortcuts.end(); ++i)
    {
        if (i.key() == testName1 && i.value() == testKeySequence1)
        {
            countName1++;
        }
        else if (i.key() == testName2 && i.value() == testKeySequence2)
        {
            countName2++;
        }
        else
        {
            countOthers++;
        }
    }

    BOOST_CHECK_EQUAL(countName1, 1);
    BOOST_CHECK_EQUAL(countName2, 1);
    BOOST_CHECK_EQUAL(countOthers, 0);
}

BOOST_AUTO_TEST_CASE(updateShortcut) // works!
{
    // test values
    gui::ShortcutController testShortcutController;
    QString testName1 = "test value !§$%&/()=? 1";
    QKeySequence testKeySequence1(Qt::Key_X);
    int countName1 = 0, countOthers = 0;

    // action done
    testShortcutController.updateShortcut(testName1, testKeySequence1);

    // compare values
    QHash<QString, QKeySequence>::iterator i;
    QHash<QString, QKeySequence> allShortcuts = testShortcutController.getAllShortcuts();

    // test
    for (i = allShortcuts.begin(); i != allShortcuts.end(); ++i)
    {
        if (i.key() == testName1 && i.value() == testKeySequence1)
        {
            countName1++;
        }
        else
        {
            countOthers++;
        }
    }

    BOOST_CHECK_EQUAL(countName1, 1);
    BOOST_CHECK_EQUAL(countOthers, 0);
}

BOOST_AUTO_TEST_CASE(registerNewAction) // works!
{
    // test values
    gui::ShortcutController testShortcutController;
    QString testName1 = "test value !§$%&/()=? 1";
    QPointer<QAction> testAction1(new QAction(NULL));
    int countName1 = 0, countOthers = 0;

    // action done
    testShortcutController.registerAction(testName1, testAction1);

    // compare values
    QHash<QString, QPointer<QAction> >::iterator i;
    QHash<QString, QPointer<QAction> > allActions = testShortcutController.getAllActions();

    // test
    for (i = allActions.begin(); i != allActions.end(); ++i)
    {
        if (i.key() == testName1 && i.value() == testAction1)
        {
            countName1++;
        }
        else
        {
            countOthers++;
        }
    }

    BOOST_CHECK_EQUAL(countName1, 1);
    BOOST_CHECK_EQUAL(countOthers, 0);
}

BOOST_AUTO_TEST_CASE(returnAllShortcuts) // works!
{
    // test values
    gui::ShortcutController testShortcutController;
    QString testName1 = "test value !§$%&/()=? 1";
    QString testName2 = "test value !§$%&/()=? 2";
    QKeySequence testKeySequence1(Qt::Key_X);
    QKeySequence testKeySequence2(Qt::Key_Y);
    int countName1 = 0, countName2 = 0, countOthers = 0;

    // action done
    testShortcutController.updateShortcut(testName1, testKeySequence1);
    testShortcutController.updateShortcut(testName2, testKeySequence2);

    // compare values
    QHash<QString, QKeySequence> allShortcuts = testShortcutController.getAllShortcuts();

    // test
    for (auto i = allShortcuts.begin(); i != allShortcuts.end(); ++i)
    {
        if (i.key() == testName1 && i.value() == testKeySequence1)
        {
            countName1++;
        }
        else if (i.key() == testName2 && i.value() == testKeySequence2)
        {
            countName2++;
        }
        else
        {
            countOthers++;
        }
    }

    BOOST_CHECK_EQUAL(countName1, 1);
    BOOST_CHECK_EQUAL(countName2, 1);
    BOOST_CHECK_EQUAL(countOthers, 0);
}

BOOST_AUTO_TEST_CASE(registerExistingAction) // works!
{
    // test values
    gui::ShortcutController testShortcutController;
    QString testName1 = "test value !§$%&/()=? 1";
    QPointer<QAction> testAction1(new QAction(NULL));

    // action done
    testShortcutController.registerAction(testName1, testAction1);

    // compare values

    // result
    BOOST_CHECK_THROW(testShortcutController.registerAction(testName1, testAction1), std::exception);
}

BOOST_AUTO_TEST_CASE(registerShortcutThenAction)
{
    // test values
    gui::ShortcutController testShortcutController;
    QString testName1 = "test value !§$%&/()=? 1";
    QString testName2 = "test value !§$%&/()=? 1";
    QKeySequence testKeySequence1(Qt::Key_X);
    QPointer<QAction> testAction1(new QAction(NULL));
    int countName1 = 0, countName2 = 0, countOthers = 0;

    // action done
    testShortcutController.updateShortcut(testName1, testKeySequence1);
    testShortcutController.registerAction(testName2, testAction1);

    // compare values
    QHash<QString, QKeySequence> Shortcuts = testShortcutController.getAllShortcuts();
    QHash<QString, QPointer<QAction> > Actions = testShortcutController.getAllActions();

    // test
    for (auto i = Shortcuts.begin(); i != Shortcuts.end(); ++i)
    {
        if (i.key() == testName1 && i.value() == testKeySequence1)
        {
            countName1++;
        }
        else
        {
            countOthers++;
        }
    }

    for (auto j = Actions.begin(); j != Actions.end(); ++j)
    {
        if (j.key() == testName2 && j.value() == testAction1)
        {
            countName2++;
        }
        else
        {
            countOthers++;
        }
    }

    BOOST_CHECK_EQUAL(countName1, 1);
    BOOST_CHECK_EQUAL(countName2, 1);
    BOOST_CHECK_EQUAL(countOthers, 0);
    BOOST_CHECK_EQUAL(testAction1->shortcut(), testKeySequence1);
}

BOOST_AUTO_TEST_CASE(registerActionThenShortcut)
{
    // test values
    gui::ShortcutController testShortcutController;
    QString testName1 = "test value !§$%&/()=? 1";
    QString testName2 = "test value !§$%&/()=? 1";
    QKeySequence testKeySequence1(Qt::Key_X);
    QPointer<QAction> testAction1(new QAction(NULL));
    int countName1 = 0, countName2 = 0, countOthers = 0;

    // action done
    testShortcutController.registerAction(testName1, testAction1);
    testShortcutController.updateShortcut(testName2, testKeySequence1);

    // compare values
    QHash<QString, QKeySequence> Shortcuts = testShortcutController.getAllShortcuts();
    QHash<QString, QPointer<QAction> > Actions = testShortcutController.getAllActions();

    // test
    for (auto i = Shortcuts.begin(); i != Shortcuts.end(); ++i)
    {
        if (i.key() == testName1 && i.value() == testKeySequence1)
        {
            countName1++;
        }
        else
        {
            countOthers++;
        }
    }

    for (auto j = Actions.begin(); j != Actions.end(); ++j)
    {
        if (j.key() == testName2 && j.value() == testAction1)
        {
            countName2++;
        }
        else
        {
            countOthers++;
        }
    }

    BOOST_CHECK_EQUAL(countName1, 1);
    BOOST_CHECK_EQUAL(countName2, 1);
    BOOST_CHECK_EQUAL(countOthers, 0);
    BOOST_CHECK_EQUAL(testAction1->shortcut(), testKeySequence1);
}

BOOST_AUTO_TEST_CASE(registerExistingShortcut)
{
    // test values
    gui::ShortcutController testShortcutController;
    QString testName1 = "test value !§$%&/()=? 1";
    QString testName2 = "test value !§$%&/()=? 2";
    QKeySequence testKeySequence1 = QKeySequence();
    QKeySequence testKeySequence2 = QKeySequence(Qt::Key_X);
    int countName1 = 0, countName2 = 0, countOthers = 0;

    // action done
    testShortcutController.updateShortcut(testName1, testKeySequence2);
    testShortcutController.updateShortcut(testName2, testKeySequence2);

    // compare values
    QHash<QString, QKeySequence> Shortcuts = testShortcutController.getAllShortcuts();

    // test
    for (auto i = Shortcuts.begin(); i != Shortcuts.end(); ++i)
    {
        if (i.key() == testName1 && i.value() == testKeySequence1)
        {
            countName1++;
        }
        else if (i.key() == testName2 && i.value() == testKeySequence2)
        {
            countName2++;
        }
        else
        {
            countOthers++;
        }
    }

    BOOST_CHECK_EQUAL(countName1, 1);
    BOOST_CHECK_EQUAL(countName2, 1);
    BOOST_CHECK_EQUAL(countOthers, 0);
}

BOOST_AUTO_TEST_CASE(returnRegisteredShortcuts)
{
    // test values
    gui::ShortcutController testShortcutController;
    QString testName1 = "test value !§$%&/()=? 1";
    QString testName2 = "test value !§$%&/()=? 2";
    QKeySequence testKeySequence1(Qt::Key_X);
    QKeySequence testKeySequence2(Qt::Key_Y);
    QPointer<QAction> testAction1(new QAction(NULL));
    int countName1 = 0, countName2 = 0, countOthers = 0;

    // action done
    testShortcutController.updateShortcut(testName1, testKeySequence1);
    testShortcutController.updateShortcut(testName2, testKeySequence2);
    testShortcutController.registerAction(testName2, testAction1);

    // compare values
    QHash<QString, QKeySequence> allShortcuts = testShortcutController.getAllRegisteredShortcuts();

    // test
    for (auto i = allShortcuts.begin(); i != allShortcuts.end(); ++i)
    {
        if (i.key() == testName1 && i.value() == testKeySequence1)
        {
            countName1++;
        }
        else if (i.key() == testName2 && i.value() == testKeySequence2)
        {
            countName2++;
        }
        else
        {
            countOthers++;
        }
    }

    BOOST_CHECK_EQUAL(countName1, 0);
    BOOST_CHECK_EQUAL(countName2, 1);
    BOOST_CHECK_EQUAL(countOthers, 0);
}
