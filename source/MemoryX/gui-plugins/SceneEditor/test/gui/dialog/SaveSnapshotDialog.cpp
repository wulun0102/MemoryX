/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE ArmarX::SceneEditor::gui::SaveSnapshotDialog
#define ARMARX_BOOST_TEST

/* Qt headers */
#include <QApplication>

#include <MemoryX/Test.h>

#include "../../InitializeCoin.h"
#include <MemoryX/gui-plugins/SceneEditor/controller/Controller.h>

#include <MemoryX/gui-plugins/SceneEditor/gui/dialog/SaveSnapshotDialog.h>
#include <MemoryX/gui-plugins/SceneEditor/test/MemoryXControllerTestEnvironment.h>

#include <QRadioButton>
#include <QLineEdit>
#include <QDialogButtonBox>
#include <QComboBox>
#include <QPushButton>
#include <QListWidget>
#include <QtCore/qprocess.h>

static QApplication* app = new QApplication(boost::unit_test::framework::master_test_suite().argc, boost::unit_test::framework::master_test_suite().argv);

BOOST_AUTO_TEST_CASE(constructor)
{
    controller::ControllerPtr control = controller::Controller::create();
    gui::dialog::SaveSnapshotDialog* saveSnapshotDialog = new gui::dialog::SaveSnapshotDialog(control);
    BOOST_CHECK(saveSnapshotDialog);
    delete saveSnapshotDialog;
}

BOOST_AUTO_TEST_CASE(checkLineEditStatus)
{
    controller::ControllerPtr control = controller::Controller::create();
    gui::dialog::SaveSnapshotDialog* saveSnapshotDialog = new gui::dialog::SaveSnapshotDialog(control);

    saveSnapshotDialog->findChild<QRadioButton*>("saveAllRadioButton")->setChecked(true);
    BOOST_CHECK_EQUAL(saveSnapshotDialog->findChild<QListWidget*>("groupsListWidget")->isEnabled(), false);
    saveSnapshotDialog->findChild<QRadioButton*>("saveGroupsRadioButton")->setChecked(true);
    BOOST_CHECK_EQUAL(saveSnapshotDialog->findChild<QListWidget*>("groupsListWidget")->isEnabled(), true);
    saveSnapshotDialog->findChild<QRadioButton*>("newSnapshotRadioButton")->setChecked(true);
    BOOST_CHECK_EQUAL(saveSnapshotDialog->findChild<QLineEdit*>("snapshotNameLineEdit")->isEnabled(), true);
    BOOST_CHECK_EQUAL(saveSnapshotDialog->findChild<QComboBox*>("existingSnapshotsComboBox")->isEnabled(), false);
    saveSnapshotDialog->findChild<QRadioButton*>("replaceSnapshotRadioButton")->setChecked(true);
    BOOST_CHECK_EQUAL(saveSnapshotDialog->findChild<QLineEdit*>("snapshotNameLineEdit")->isEnabled(), false);
    BOOST_CHECK_EQUAL(saveSnapshotDialog->findChild<QComboBox*>("existingSnapshotsComboBox")->isEnabled(), true);

    delete saveSnapshotDialog;
}

BOOST_AUTO_TEST_CASE(checkOKButtonStatus)
{
    controller::ControllerPtr control = controller::Controller::create();
    gui::dialog::SaveSnapshotDialog* saveSnapshotDialog = new gui::dialog::SaveSnapshotDialog(control);

    saveSnapshotDialog->findChild<QRadioButton*>("newSnapshotRadioButton")->setChecked(true);

    saveSnapshotDialog->findChild<QLineEdit*>("snapshotNameLineEdit")->setText("not empty");
    BOOST_CHECK_EQUAL(saveSnapshotDialog->findChild<QDialogButtonBox*>("buttonBox")->button(QDialogButtonBox::Ok)->isEnabled(), true);

    saveSnapshotDialog->findChild<QLineEdit*>("snapshotNameLineEdit")->setText("");
    BOOST_CHECK_EQUAL(saveSnapshotDialog->findChild<QDialogButtonBox*>("buttonBox")->button(QDialogButtonBox::Ok)->isEnabled(), false);

    delete saveSnapshotDialog;
}

BOOST_AUTO_TEST_CASE(saveSnapshot)
{
    MemoryXEnvironment environment;

    for (std::string snapshotName : ENVIRONMENT(environment).longtermMemory->getSnapshotNames())
    {
        ENVIRONMENT(environment).longtermMemory->removeWorkingMemorySnapshot(snapshotName);
    }

    controller::ControllerPtr control = CONTROLLER(environment);
    int previousSnapshotNumber = control->getMemoryXController()->getAllSnapshots().size();
    gui::dialog::SaveSnapshotDialog* saveSnapshotDialog = new gui::dialog::SaveSnapshotDialog(control);

    saveSnapshotDialog->findChild<QRadioButton*>("saveAllRadioButton")->setChecked(true);
    saveSnapshotDialog->findChild<QRadioButton*>("newSnapshotRadioButton")->setChecked(true);
    saveSnapshotDialog->findChild<QLineEdit*>("snapshotNameLineEdit")->setText("testsnapshot 1");

    saveSnapshotDialog->findChild<QDialogButtonBox*>("buttonBox")->button(QDialogButtonBox::Ok)->click();

    BOOST_CHECK_EQUAL(control->getMemoryXController()->getAllSnapshots().size(), previousSnapshotNumber + 1);
}

BOOST_AUTO_TEST_CASE(showDialog)
{
    MemoryXEnvironment environment;
    controller::ControllerPtr control = CONTROLLER(environment);
    gui::dialog::SaveSnapshotDialog* saveSnapshotDialog = new gui::dialog::SaveSnapshotDialog(control);
    saveSnapshotDialog->show();
    BOOST_CHECK(saveSnapshotDialog);
    delete saveSnapshotDialog;
}
