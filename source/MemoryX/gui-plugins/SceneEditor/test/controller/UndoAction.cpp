/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE ArmarX::SceneEditor::controller::UndoAction
#define ARMARX_BOOST_TEST

#include <MemoryX/Test.h>

#include <MemoryX/gui-plugins/SceneEditor/controller/UndoAction.h>
#include <MemoryX/gui-plugins/SceneEditor/controller/Operation.h>

namespace controller
{
    class Operation;
}

class TestOperation1Inverse : public controller::Operation
{
public:
    TestOperation1Inverse() :
        controller::Operation(boost::shared_ptr<memoryxcontroller::MemoryXController>(), boost::shared_ptr<scene3D::Scene>())
    {
    }

    const controller::OperationPtr createInverseOperation() const
    {
        controller::OperationPtr p(new TestOperation1Inverse());
        return p;
    }

    virtual void executeOnWorkingMemory()
    {

    }

    virtual void executeOnScene()
    {

    }
};

class TestOperation1 : public controller::Operation
{
public:
    TestOperation1() :
        controller::Operation(boost::shared_ptr<memoryxcontroller::MemoryXController>(), boost::shared_ptr<scene3D::Scene>())
    {
    }

    const controller::OperationPtr createInverseOperation() const
    {
        controller::OperationPtr p(new TestOperation1Inverse());
        return p;
    }

    virtual void executeOnWorkingMemory()
    {

    }

    virtual void executeOnScene()
    {

    }
};

class TestOperation2Inverse : public controller::Operation
{
public:
    TestOperation2Inverse() :
        controller::Operation(boost::shared_ptr<memoryxcontroller::MemoryXController>(), boost::shared_ptr<scene3D::Scene>())
    {
    }

    const controller::OperationPtr createInverseOperation() const
    {
        controller::OperationPtr p(new TestOperation2Inverse());
        return p;
    }

    virtual void executeOnWorkingMemory()
    {

    }

    virtual void executeOnScene()
    {

    }
};

class TestOperation2 : public controller::Operation
{
public:
    TestOperation2() :
        controller::Operation(boost::shared_ptr<memoryxcontroller::MemoryXController>(), boost::shared_ptr<scene3D::Scene>())
    {
    }

    const controller::OperationPtr createInverseOperation() const
    {
        controller::OperationPtr p(new TestOperation2Inverse());
        return p;
    }

    virtual void executeOnWorkingMemory()
    {

    }

    virtual void executeOnScene()
    {

    }
};

BOOST_AUTO_TEST_CASE(getOperationsEmpty)
{
    boost::shared_ptr<std::vector<controller::OperationPtr>> operations(new std::vector<controller::OperationPtr>());
    controller::UndoAction action(operations);
    BOOST_CHECK_EQUAL(operations->size(), action.getOperations()->size());
}

BOOST_AUTO_TEST_CASE(getOperations)
{
    boost::shared_ptr<std::vector<controller::OperationPtr>> operations(new std::vector<controller::OperationPtr>());

    for (int i = 0; i < 17; ++i)
    {
        controller::OperationPtr c;

        if (i % 5 == 0)
        {
            c = boost::shared_ptr<TestOperation1>(new TestOperation1());
        }
        else
        {
            c = boost::shared_ptr<TestOperation2>(new TestOperation2());
        }

        operations->push_back(c);
    }

    controller::UndoAction action(operations);
    BOOST_CHECK_EQUAL(operations->size(), action.getOperations()->size());
    boost::shared_ptr<std::vector<controller::OperationPtr>> operationsFromAction = action.getOperations();

    for (std::vector<controller::OperationPtr>::size_type i = 0; i != operationsFromAction->size(); i++)
    {
        BOOST_CHECK_EQUAL((*operations)[i].get(), (*operationsFromAction)[i].get());
    }
}

BOOST_AUTO_TEST_CASE(undoEmpty)
{
    boost::shared_ptr<std::vector<controller::OperationPtr>> operations(new std::vector<controller::OperationPtr>());
    controller::UndoAction action(operations);
    BOOST_CHECK_EQUAL(operations->size(), action.undo()->size());
}

BOOST_AUTO_TEST_CASE(undo)
{
    boost::shared_ptr<std::vector<controller::OperationPtr>> operations(new std::vector<controller::OperationPtr>());

    for (int i = 0; i < 17; ++i)
    {
        controller::OperationPtr c;

        if (i % 5 == 0)
        {
            c = boost::shared_ptr<TestOperation1>(new TestOperation1());
        }
        else
        {
            c = boost::shared_ptr<TestOperation2>(new TestOperation2());
        }

        operations->push_back(c);
    }

    controller::UndoAction action(operations);
    BOOST_CHECK_EQUAL(operations->size(), action.undo()->size());
    boost::shared_ptr<std::vector<controller::OperationPtr>> operationsFromAction = action.undo();

    for (std::vector<controller::OperationPtr>::size_type i = 0; i != operationsFromAction->size(); i++)
    {
        if (i % 5 == 0)
        {
            controller::Operation* o = ((*operationsFromAction)[operationsFromAction->size() - i - 1]).get();
            BOOST_CHECK(dynamic_cast<TestOperation1Inverse*>(o) != NULL);
        }
        else
        {
            controller::Operation* o = ((*operationsFromAction)[operationsFromAction->size() - i - 1]).get();
            BOOST_CHECK(dynamic_cast<TestOperation2Inverse*>(o) != NULL);
        }
    }
}

BOOST_AUTO_TEST_CASE(redoEmpty)
{
    boost::shared_ptr<std::vector<controller::OperationPtr>> operations(new std::vector<controller::OperationPtr>());
    controller::UndoAction action(operations);
    BOOST_CHECK_EQUAL(operations->size(), action.redo()->size());
}

BOOST_AUTO_TEST_CASE(redo)
{
    boost::shared_ptr<std::vector<controller::OperationPtr>> operations(new std::vector<controller::OperationPtr>());

    for (int i = 0; i < 17; ++i)
    {
        controller::OperationPtr c;

        if (i % 5 == 0)
        {
            c = boost::shared_ptr<TestOperation1>(new TestOperation1());
        }
        else
        {
            c = boost::shared_ptr<TestOperation2>(new TestOperation2());
        }

        operations->push_back(c);
    }

    controller::UndoAction action(operations);
    BOOST_CHECK_EQUAL(operations->size(), action.redo()->size());
    boost::shared_ptr<std::vector<controller::OperationPtr>> operationsFromAction = action.redo();

    for (std::vector<controller::OperationPtr>::size_type i = 0; i != operationsFromAction->size(); i++)
    {
        if (i % 5 == 0)
        {
            controller::Operation* o = ((*operationsFromAction)[i]).get();
            BOOST_CHECK(dynamic_cast<TestOperation1*>(o) != NULL);
        }
        else
        {
            controller::Operation* o = ((*operationsFromAction)[i]).get();
            BOOST_CHECK(dynamic_cast<TestOperation2*>(o) != NULL);
        }
    }
}