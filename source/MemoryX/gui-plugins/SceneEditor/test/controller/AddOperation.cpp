/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE ArmarX::SceneEditor::controller::AddOperation
#define ARMARX_BOOST_TEST

#include <MemoryX/Test.h>

#include "../InitializeCoin.h"

#include <MemoryX/gui-plugins/SceneEditor/controller/AddOperation.h>
#include <MemoryX/gui-plugins/SceneEditor/test/MemoryXControllerTestEnvironment.h>

class PublicAddOperation : public controller::AddOperation
{
public:
    PublicAddOperation(const AddOperation& operation) :
        AddOperation(operation)
    {
    }

    PublicAddOperation(const boost::shared_ptr<memoryxcontroller::MemoryXController>& memoryXController, const boost::shared_ptr<scene3D::Scene>& scene, const std::string& objectName, const std::string& objectCollection, const SbVec3f& objectPosition, const SbRotation& objectRotation, const std::string& objectId) :
        AddOperation(memoryXController, scene, objectName, objectCollection, objectPosition, objectRotation, objectId)
    {
    }

    PublicAddOperation(const boost::shared_ptr<memoryxcontroller::MemoryXController>& memoryXController, const boost::shared_ptr<scene3D::Scene>& scene, const std::string& objectName, const std::string& objectCollection, const SbVec3f& objectPosition, const SbRotation& objectRotation) :
        AddOperation(memoryXController, scene, objectName, objectCollection, objectPosition, objectRotation)
    {
    }

    PublicAddOperation(const boost::shared_ptr<memoryxcontroller::MemoryXController>& memoryXController, const boost::shared_ptr<scene3D::Scene>& scene, const std::string& objectName, const std::string& objectCollection) :
        AddOperation(memoryXController, scene, objectName, objectCollection)
    {
    }

    virtual void executeOnWorkingMemory()
    {
        controller::AddOperation::executeOnWorkingMemory();
    }

    virtual void executeOnScene()
    {
        controller::AddOperation::executeOnScene();
    }
};

static inline bool floatEqual(float f1, float f2)
{
    return (f1 < f2 ? f2 - f1 : f1 - f2) < 0.00001f;
}

static inline bool sbRotationEqual(SbRotation r1, SbRotation r2)
{
    float v1x, v1y, v1z, v1w, v2x, v2y, v2z, v2w;
    r1.getValue(v1x, v1y, v1z, v1w);
    r2.getValue(v2x, v2y, v2z, v2w);
    return floatEqual(v1x, v2x) && floatEqual(v1y, v2y) && floatEqual(v1z, v2z) && floatEqual(v1w, v2w);
}

static inline bool sbVec3fEqual(SbVec3f v1, SbVec3f v2)
{
    float v1x, v1y, v1z, v2x, v2y, v2z;
    v1.getValue(v1x, v1y, v1z);
    v2.getValue(v2x, v2y, v2z);
    return floatEqual(v1x, v2x) && floatEqual(v1y, v2y) && floatEqual(v1z, v2z);
}

BOOST_AUTO_TEST_CASE(constructor1)
{
    controller::ControllerPtr c = controller::Controller::create();
    controller::OperationPtr operation(new controller::AddOperation(c->getMemoryXController(), c->getScene(), "testClass", "testCollection"));
    BOOST_CHECK(operation);
    BOOST_CHECK(operation.get());
    BOOST_CHECK(operation->getObjectId() != "");
}

BOOST_AUTO_TEST_CASE(constructor2)
{
    controller::ControllerPtr c = controller::Controller::create();
    controller::OperationPtr operation(new controller::AddOperation(c->getMemoryXController(), c->getScene(), "testClass", "testCollection", SbVec3f(), SbRotation()));
    BOOST_CHECK(operation);
    BOOST_CHECK(operation.get());
    BOOST_CHECK(operation->getObjectId() != "");
}

BOOST_AUTO_TEST_CASE(constructor3)
{
    controller::ControllerPtr c = controller::Controller::create();
    controller::OperationPtr operation(new controller::AddOperation(c->getMemoryXController(), c->getScene(), "testClass", "testCollection", SbVec3f(), SbRotation(), "Object 1"));
    BOOST_CHECK(operation);
    BOOST_CHECK(operation.get());
    BOOST_CHECK(operation->getObjectId() == "Object 1");
}

BOOST_AUTO_TEST_CASE(constructor4)
{
    controller::ControllerPtr c = controller::Controller::create();
    controller::AddOperation add(c->getMemoryXController(), c->getScene(), "testClass", "testCollection", SbVec3f(), SbRotation(), "Object 1");
    controller::OperationPtr operation(new controller::AddOperation(add));
    BOOST_CHECK(operation);
    BOOST_CHECK(operation.get());
    BOOST_CHECK(operation->getObjectId() != "Object 1");
}
