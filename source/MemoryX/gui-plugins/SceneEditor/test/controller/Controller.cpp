/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE ArmarX::SceneEditor::controller::Controller
#define ARMARX_BOOST_TEST

#include <MemoryX/Test.h>

#include <stdexcept>
#include <QObject>

#include "../InitializeCoin.h"
#include "../TestSlot.h"

#include <MemoryX/gui-plugins/SceneEditor/controller/Controller.h>
#include <MemoryX/gui-plugins/SceneEditor/test/MemoryXControllerTestEnvironment.h>

int timesInverseOperationCreated = 0;
int timesInverseOperationExecuteOnWorkingMemoryExecuted = 0;
int timesInverseOperationExecuteOnSceneExecuted = 0;
std::vector<std::string> usedIds;

std::string createRandomId()
{
    static const char chars[] =
        "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    const int length = 20;

    char random[length + 1];

    for (int i = 0; i < length; ++i)
    {
        random[i] = chars[rand() % (sizeof(chars) - 1)];
    }

    random[length] = 0;

    return random;
}

typedef struct OperationsInit
{
    OperationsInit()
    {
        timesInverseOperationCreated = 0;
        timesInverseOperationExecuteOnWorkingMemoryExecuted = 0;
        timesInverseOperationExecuteOnSceneExecuted = 0;
        usedIds.clear();
    };

    ~OperationsInit()
    {
        usedIds.clear();
        timesInverseOperationCreated = 0;
        timesInverseOperationExecuteOnWorkingMemoryExecuted = 0;
        timesInverseOperationExecuteOnSceneExecuted = 0;
    }
} OperationsInit;

class TestOperationInverse : public controller::Operation
{
public:
    TestOperationInverse(const boost::shared_ptr<memoryxcontroller::MemoryXController>& memoryXController, const boost::shared_ptr<scene3D::Scene>& scene) :
        controller::Operation(memoryXController, scene)
    {
        ++timesInverseOperationCreated;
    }

    const boost::shared_ptr<controller::Operation> createInverseOperation() const
    {
        boost::shared_ptr<controller::Operation> p(new TestOperationInverse(getMemoryXController(), getScene()));
        return p;
    }

    virtual void executeOnWorkingMemory()
    {
        ++timesInverseOperationExecuteOnWorkingMemoryExecuted;
    }

    virtual void executeOnScene()
    {
        ++timesInverseOperationExecuteOnSceneExecuted;
    }
};

class TestOperation : public controller::Operation
{
public:
    TestOperation(const boost::shared_ptr<memoryxcontroller::MemoryXController>& memoryXController, const boost::shared_ptr<scene3D::Scene>& scene) :
        controller::Operation(memoryXController, scene),
        timesExecuteOnWorkingMemoryExecuted(0),
        timesExecuteOnSceneExecuted(0),
        idChangedOnScene(false),
        idChangedOnWorkingMemory(false),
        throwsOnScene(false),
        throwsOnWorkingMemory(false)
    {
        do
        {
            std::string id = createRandomId();
            setObjectId(id);
        }
        while (std::find(usedIds.begin(), usedIds.end(), getObjectId()) == usedIds.end());
    }

    const boost::shared_ptr<controller::Operation> createInverseOperation() const
    {
        boost::shared_ptr<controller::Operation> p(new TestOperationInverse(getMemoryXController(), getScene()));
        return p;
    }

    virtual void executeOnWorkingMemory()
    {
        std::string oldId = getObjectId();

        while (idChangedOnWorkingMemory && (oldId == getObjectId() || std::find(usedIds.begin(), usedIds.end(), getObjectId()) != usedIds.end()))
        {
            std::string id = createRandomId();
            Operation::setObjectId(id);
        }

        if (std::find(usedIds.begin(), usedIds.end(), getObjectId()) == usedIds.end())
        {
            usedIds.push_back(getObjectId());
        }

        if (throwsOnWorkingMemory)
        {
            throw "test";
        }

        ++timesExecuteOnWorkingMemoryExecuted;
    }

    virtual void executeOnScene()
    {
        std::string oldId = getObjectId();

        while (idChangedOnScene && (oldId == getObjectId() || std::find(usedIds.begin(), usedIds.end(), getObjectId()) != usedIds.end()))
        {
            std::string id = createRandomId();
            Operation::setObjectId(id);
        }

        if (std::find(usedIds.begin(), usedIds.end(), getObjectId()) == usedIds.end())
        {
            usedIds.push_back(getObjectId());
        }

        if (throwsOnScene)
        {
            throw "test";
        }

        ++timesExecuteOnSceneExecuted;
    }

    int getTimesExecuteOnWorkingMemoryExecuted()
    {
        return timesExecuteOnWorkingMemoryExecuted;
    }

    int getTimesExecuteOnSceneExecuted()
    {
        return timesExecuteOnSceneExecuted;
    }


    void setIdChangedOnScene(bool idChangedOnScene)
    {
        TestOperation::idChangedOnScene = idChangedOnScene;
    }

    void setIdChangedOnWorkingMemory(bool idChangedOnWorkingMemory)
    {
        TestOperation::idChangedOnWorkingMemory = idChangedOnWorkingMemory;
    }

    void setThrowsOnScene(bool throwsOnScene)
    {
        TestOperation::throwsOnScene = throwsOnScene;
    }

    void setThrowsOnWorkingMemory(bool throwsOnWorkingMemory)
    {
        TestOperation::throwsOnWorkingMemory = throwsOnWorkingMemory;
    }

    void setObjectId(const std::string& id)
    {
        if (std::find(usedIds.begin(), usedIds.end(), id) == usedIds.end())
        {
            usedIds.push_back(id);
        }

        Operation::setObjectId(id);
    }

private:
    int timesExecuteOnWorkingMemoryExecuted;
    int timesExecuteOnSceneExecuted;
    bool idChangedOnScene;
    bool idChangedOnWorkingMemory;
    bool throwsOnScene;
    bool throwsOnWorkingMemory;
};

static std::string addObjectToEnvironment(MemoryXEnvironment& environment, SbRotation& rotation, SbVec3f& position)
{
    memoryx::ObjectClassPtr objectClass = new memoryx::ObjectClass();
    objectClass->setName("testObject1");
    objectClass->putAttribute("IvFile", "");
    objectClass->putAttribute("IvFileCollision", "");
    objectClass->putAttribute("ManipulationFile", "");
    objectClass->putAttribute("parentClasses", "");
    ENVIRONMENT(environment).priorKnowledgePrx->getObjectClassesSegment()->addEntity(objectClass);

    std::string objectId = CONTROLLER(environment)->getMemoryXController()->getWorkingMemoryController()->addObjectInstance("testObject1", objectClass);

    memoryx::ObjectInstanceMemorySegmentBasePrx objectInstancesPrx = memoryx::ObjectInstanceMemorySegmentBasePrx::uncheckedCast(ENVIRONMENT(environment).workingMemoryPrx->getSegment("objectInstances"));
    memoryx::ObjectInstancePtr object = memoryx::ObjectInstancePtr::dynamicCast(objectInstancesPrx->getEntityById(objectId));

    CONTROLLER(environment)->getMemoryXController()->getWorkingMemoryController()->rotateTranslateObject(objectId, rotation, position);

    scene3D::SceneObjectPtr sceneObject = scene3D::SceneObjectPtr(new scene3D::SceneObject(objectId, "testObject1", CONTROLLER(environment)->getMemoryXController()->getPriorKnowlegdeController()->getCollection(object), new SoSeparator, new SoSeparator));
    CONTROLLER(environment)->getScene()->getObjectManager()->addObject(sceneObject);
    sceneObject->setRotation(rotation);
    sceneObject->setTranslation(position);
    scene3D::SceneGroupPtr group(new scene3D::SceneGroup(objectId + "_group"));
    CONTROLLER(environment)->getScene()->getGroupManager()->addGroup(group);
    group->addObject(sceneObject);
    return objectId;
}

BOOST_AUTO_TEST_CASE(createController)
{
    controller::ControllerPtr c = controller::Controller::create();
    BOOST_CHECK(c);
    BOOST_CHECK(c->getScene());
    BOOST_CHECK(c->getMemoryXController());
    BOOST_CHECK(c->getShortcutController());
}

BOOST_AUTO_TEST_CASE(executeNoFlags)
{
    OperationsInit o;

    controller::ControllerPtr c = controller::Controller::create();
    boost::shared_ptr<std::vector<controller::OperationPtr>> operations(new std::vector<controller::OperationPtr>());
    TestOperation* t = new TestOperation(boost::shared_ptr<memoryxcontroller::MemoryXController>(), boost::shared_ptr<scene3D::Scene>());
    controller::OperationPtr t1(t);
    operations->push_back(t1);
    BOOST_CHECK_EQUAL(timesInverseOperationCreated, 0);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnWorkingMemoryExecuted, 0);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnSceneExecuted, 0);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnSceneExecuted(), 0);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnWorkingMemoryExecuted(), 0);
    c->execute(0, operations);
    BOOST_CHECK_EQUAL(timesInverseOperationCreated, 0);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnWorkingMemoryExecuted, 0);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnSceneExecuted, 0);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnSceneExecuted(), 0);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnWorkingMemoryExecuted(), 0);
    c->undo();
    BOOST_CHECK_EQUAL(timesInverseOperationCreated, 0);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnWorkingMemoryExecuted, 0);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnSceneExecuted, 0);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnSceneExecuted(), 0);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnWorkingMemoryExecuted(), 0);
    c->undo();
    BOOST_CHECK_EQUAL(timesInverseOperationCreated, 0);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnWorkingMemoryExecuted, 0);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnSceneExecuted, 0);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnSceneExecuted(), 0);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnWorkingMemoryExecuted(), 0);
    c->redo();
    BOOST_CHECK_EQUAL(timesInverseOperationCreated, 0);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnWorkingMemoryExecuted, 0);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnSceneExecuted, 0);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnSceneExecuted(), 0);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnWorkingMemoryExecuted(), 0);
}

BOOST_AUTO_TEST_CASE(executeUndoable)
{
    OperationsInit o;

    controller::ControllerPtr c = controller::Controller::create();
    boost::shared_ptr<std::vector<controller::OperationPtr>> operations(new std::vector<controller::OperationPtr>());
    TestOperation* t = new TestOperation(boost::shared_ptr<memoryxcontroller::MemoryXController>(), boost::shared_ptr<scene3D::Scene>());
    controller::OperationPtr t1(t);
    operations->push_back(t1);
    BOOST_CHECK_EQUAL(timesInverseOperationCreated, 0);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnWorkingMemoryExecuted, 0);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnSceneExecuted, 0);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnSceneExecuted(), 0);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnWorkingMemoryExecuted(), 0);
    c->execute(controller::Controller::UNDOABLE, operations);
    BOOST_CHECK_EQUAL(timesInverseOperationCreated, 0);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnWorkingMemoryExecuted, 0);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnSceneExecuted, 0);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnSceneExecuted(), 0);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnWorkingMemoryExecuted(), 0);
    c->undo();
    BOOST_CHECK_EQUAL(timesInverseOperationCreated, 1);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnWorkingMemoryExecuted, 1);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnSceneExecuted, 1);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnSceneExecuted(), 0);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnWorkingMemoryExecuted(), 0);
    c->undo();
    BOOST_CHECK_EQUAL(timesInverseOperationCreated, 1);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnWorkingMemoryExecuted, 1);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnSceneExecuted, 1);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnSceneExecuted(), 0);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnWorkingMemoryExecuted(), 0);
    c->redo();
    BOOST_CHECK_EQUAL(timesInverseOperationCreated, 1);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnWorkingMemoryExecuted, 1);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnSceneExecuted, 1);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnSceneExecuted(), 1);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnWorkingMemoryExecuted(), 1);
}

BOOST_AUTO_TEST_CASE(executeScene)
{
    OperationsInit o;

    controller::ControllerPtr c = controller::Controller::create();
    boost::shared_ptr<std::vector<controller::OperationPtr>> operations(new std::vector<controller::OperationPtr>());
    TestOperation* t = new TestOperation(boost::shared_ptr<memoryxcontroller::MemoryXController>(), boost::shared_ptr<scene3D::Scene>());
    controller::OperationPtr t1(t);
    operations->push_back(t1);
    BOOST_CHECK_EQUAL(timesInverseOperationCreated, 0);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnWorkingMemoryExecuted, 0);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnSceneExecuted, 0);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnSceneExecuted(), 0);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnWorkingMemoryExecuted(), 0);
    c->execute(controller::Controller::EXECUTE_ON_SCENE, operations);
    BOOST_CHECK_EQUAL(timesInverseOperationCreated, 0);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnWorkingMemoryExecuted, 0);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnSceneExecuted, 0);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnSceneExecuted(), 1);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnWorkingMemoryExecuted(), 0);
    c->undo();
    BOOST_CHECK_EQUAL(timesInverseOperationCreated, 0);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnWorkingMemoryExecuted, 0);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnSceneExecuted, 0);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnSceneExecuted(), 1);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnWorkingMemoryExecuted(), 0);
    c->undo();
    BOOST_CHECK_EQUAL(timesInverseOperationCreated, 0);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnWorkingMemoryExecuted, 0);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnSceneExecuted, 0);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnSceneExecuted(), 1);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnWorkingMemoryExecuted(), 0);
    c->redo();
    BOOST_CHECK_EQUAL(timesInverseOperationCreated, 0);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnWorkingMemoryExecuted, 0);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnSceneExecuted, 0);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnSceneExecuted(), 1);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnWorkingMemoryExecuted(), 0);
}

BOOST_AUTO_TEST_CASE(executeWorkingMemory)
{
    OperationsInit o;

    controller::ControllerPtr c = controller::Controller::create();
    boost::shared_ptr<std::vector<controller::OperationPtr>> operations(new std::vector<controller::OperationPtr>());
    TestOperation* t = new TestOperation(boost::shared_ptr<memoryxcontroller::MemoryXController>(), boost::shared_ptr<scene3D::Scene>());
    controller::OperationPtr t1(t);
    operations->push_back(t1);
    BOOST_CHECK_EQUAL(timesInverseOperationCreated, 0);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnWorkingMemoryExecuted, 0);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnSceneExecuted, 0);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnSceneExecuted(), 0);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnWorkingMemoryExecuted(), 0);
    c->execute(controller::Controller::EXECUTE_ON_WM, operations);
    BOOST_CHECK_EQUAL(timesInverseOperationCreated, 0);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnWorkingMemoryExecuted, 0);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnSceneExecuted, 0);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnSceneExecuted(), 0);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnWorkingMemoryExecuted(), 1);
    c->undo();
    BOOST_CHECK_EQUAL(timesInverseOperationCreated, 0);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnWorkingMemoryExecuted, 0);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnSceneExecuted, 0);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnSceneExecuted(), 0);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnWorkingMemoryExecuted(), 1);
    c->undo();
    BOOST_CHECK_EQUAL(timesInverseOperationCreated, 0);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnWorkingMemoryExecuted, 0);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnSceneExecuted, 0);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnSceneExecuted(), 0);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnWorkingMemoryExecuted(), 1);
    c->redo();
    BOOST_CHECK_EQUAL(timesInverseOperationCreated, 0);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnWorkingMemoryExecuted, 0);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnSceneExecuted, 0);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnSceneExecuted(), 0);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnWorkingMemoryExecuted(), 1);
}

BOOST_AUTO_TEST_CASE(executeSceneWorkingMemoryUndoable)
{
    OperationsInit o;

    controller::ControllerPtr c = controller::Controller::create();
    boost::shared_ptr<std::vector<controller::OperationPtr>> operations(new std::vector<controller::OperationPtr>());
    TestOperation* t = new TestOperation(boost::shared_ptr<memoryxcontroller::MemoryXController>(), boost::shared_ptr<scene3D::Scene>());
    controller::OperationPtr t1(t);
    operations->push_back(t1);
    BOOST_CHECK_EQUAL(timesInverseOperationCreated, 0);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnWorkingMemoryExecuted, 0);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnSceneExecuted, 0);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnSceneExecuted(), 0);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnWorkingMemoryExecuted(), 0);
    c->execute(controller::Controller::EXECUTE_ON_SCENE | controller::Controller::EXECUTE_ON_WM | controller::Controller::UNDOABLE, operations);
    BOOST_CHECK_EQUAL(timesInverseOperationCreated, 0);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnWorkingMemoryExecuted, 0);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnSceneExecuted, 0);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnSceneExecuted(), 1);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnWorkingMemoryExecuted(), 1);
    c->undo();
    BOOST_CHECK_EQUAL(timesInverseOperationCreated, 1);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnWorkingMemoryExecuted, 1);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnSceneExecuted, 1);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnSceneExecuted(), 1);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnWorkingMemoryExecuted(), 1);
    c->undo();
    BOOST_CHECK_EQUAL(timesInverseOperationCreated, 1);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnWorkingMemoryExecuted, 1);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnSceneExecuted, 1);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnSceneExecuted(), 1);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnWorkingMemoryExecuted(), 1);
    c->redo();
    BOOST_CHECK_EQUAL(timesInverseOperationCreated, 1);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnWorkingMemoryExecuted, 1);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnSceneExecuted, 1);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnSceneExecuted(), 2);
    BOOST_CHECK_EQUAL(t->getTimesExecuteOnWorkingMemoryExecuted(), 2);
}

BOOST_AUTO_TEST_CASE(executeIdOnWorkingMemoryChanged)
{
    OperationsInit o;

    controller::ControllerPtr c = controller::Controller::create();

    boost::shared_ptr<std::vector<controller::OperationPtr>> operations1(new std::vector<controller::OperationPtr>());

    TestOperation* t1 = new TestOperation(boost::shared_ptr<memoryxcontroller::MemoryXController>(), boost::shared_ptr<scene3D::Scene>());
    std::string t1Id = t1->getObjectId();
    t1->setIdChangedOnWorkingMemory(true);
    controller::OperationPtr o1(t1);
    operations1->push_back(o1);

    TestOperation* t2 = new TestOperation(boost::shared_ptr<memoryxcontroller::MemoryXController>(), boost::shared_ptr<scene3D::Scene>());
    std::string t2Id = t2->getObjectId();
    t2->setIdChangedOnWorkingMemory(true);
    controller::OperationPtr o2(t2);
    operations1->push_back(o2);

    TestOperation* t3 = new TestOperation(boost::shared_ptr<memoryxcontroller::MemoryXController>(), boost::shared_ptr<scene3D::Scene>());
    t3->setObjectId(t1Id);
    std::string t3Id = t3->getObjectId();
    controller::OperationPtr o3(t3);
    operations1->push_back(o3);

    TestOperation* t4 = new TestOperation(boost::shared_ptr<memoryxcontroller::MemoryXController>(), boost::shared_ptr<scene3D::Scene>());
    std::string t4Id = t4->getObjectId();
    t4->setIdChangedOnWorkingMemory(true);
    controller::OperationPtr o4(t4);
    operations1->push_back(o4);

    c->execute(controller::Controller::EXECUTE_ON_SCENE | controller::Controller::EXECUTE_ON_WM | controller::Controller::UNDOABLE, operations1);
    BOOST_CHECK(t1->getObjectId() != t1Id);
    BOOST_CHECK(t2->getObjectId() != t2Id);
    BOOST_CHECK(t3->getObjectId() == t1->getObjectId());
    BOOST_CHECK(t4->getObjectId() != t4Id);
    t1Id = t1->getObjectId();
    t2Id = t2->getObjectId();
    t3Id = t3->getObjectId();
    t4Id = t4->getObjectId();

    boost::shared_ptr<std::vector<controller::OperationPtr>> operations2(new std::vector<controller::OperationPtr>());

    TestOperation* t5 = new TestOperation(boost::shared_ptr<memoryxcontroller::MemoryXController>(), boost::shared_ptr<scene3D::Scene>());
    t5->setObjectId(t1Id);
    std::string t5Id = t5->getObjectId();
    t5->setIdChangedOnWorkingMemory(true);
    controller::OperationPtr o5(t5);
    operations2->push_back(o5);

    c->execute(controller::Controller::EXECUTE_ON_SCENE | controller::Controller::EXECUTE_ON_WM | controller::Controller::UNDOABLE, operations2);
    BOOST_CHECK(t1->getObjectId() == t5->getObjectId());
    BOOST_CHECK(t2->getObjectId() == t2Id);
    BOOST_CHECK(t3->getObjectId() == t5->getObjectId());
    BOOST_CHECK(t4->getObjectId() == t4Id);
    BOOST_CHECK(t5->getObjectId() != t5Id);
    t1Id = t1->getObjectId();
    t2Id = t2->getObjectId();
    t3Id = t3->getObjectId();
    t4Id = t4->getObjectId();
    t5Id = t5->getObjectId();

    boost::shared_ptr<std::vector<controller::OperationPtr>> operations3(new std::vector<controller::OperationPtr>());

    TestOperation* t6 = new TestOperation(boost::shared_ptr<memoryxcontroller::MemoryXController>(), boost::shared_ptr<scene3D::Scene>());
    t6->setObjectId(t1Id);
    std::string t6Id = t6->getObjectId();
    controller::OperationPtr o6(t6);
    operations3->push_back(o6);

    c->execute(controller::Controller::EXECUTE_ON_SCENE | controller::Controller::EXECUTE_ON_WM | controller::Controller::UNDOABLE, operations3);
    BOOST_CHECK(t1->getObjectId() == t1Id);
    BOOST_CHECK(t2->getObjectId() == t2Id);
    BOOST_CHECK(t3->getObjectId() == t3Id);
    BOOST_CHECK(t4->getObjectId() == t4Id);
    BOOST_CHECK(t5->getObjectId() == t5Id);
    BOOST_CHECK(t6->getObjectId() == t6Id);

    c->undo();
    // nothing happens, because we execute the inverse operation
    BOOST_CHECK(t1->getObjectId() == t1Id);
    BOOST_CHECK(t2->getObjectId() == t2Id);
    BOOST_CHECK(t3->getObjectId() == t3Id);
    BOOST_CHECK(t4->getObjectId() == t4Id);
    BOOST_CHECK(t5->getObjectId() == t5Id);
    BOOST_CHECK(t6->getObjectId() == t6Id);

    c->undo();
    // nothing happens, because we execute the inverse operation
    BOOST_CHECK(t1->getObjectId() == t1Id);
    BOOST_CHECK(t2->getObjectId() == t2Id);
    BOOST_CHECK(t3->getObjectId() == t3Id);
    BOOST_CHECK(t4->getObjectId() == t4Id);
    BOOST_CHECK(t5->getObjectId() == t5Id);
    BOOST_CHECK(t6->getObjectId() == t6Id);

    c->redo();
    BOOST_CHECK(t1->getObjectId() == t5->getObjectId());
    BOOST_CHECK(t2->getObjectId() == t2Id);
    BOOST_CHECK(t3->getObjectId() == t5->getObjectId());
    BOOST_CHECK(t4->getObjectId() == t4Id);
    BOOST_CHECK(t5->getObjectId() != t5Id);
    BOOST_CHECK(t6->getObjectId() == t5->getObjectId());
}

BOOST_AUTO_TEST_CASE(executeIdOnSceneChanged)
{
    OperationsInit o;

    controller::ControllerPtr c = controller::Controller::create();

    boost::shared_ptr<std::vector<controller::OperationPtr>> operations1(new std::vector<controller::OperationPtr>());

    TestOperation* t1 = new TestOperation(boost::shared_ptr<memoryxcontroller::MemoryXController>(), boost::shared_ptr<scene3D::Scene>());
    std::string t1Id = t1->getObjectId();
    t1->setIdChangedOnScene(true);
    controller::OperationPtr o1(t1);
    operations1->push_back(o1);

    BOOST_CHECK_THROW(c->execute(controller::Controller::EXECUTE_ON_SCENE | controller::Controller::EXECUTE_ON_WM | controller::Controller::UNDOABLE, operations1), std::runtime_error);
}

BOOST_AUTO_TEST_CASE(executeOperationFailedOnWorkingMemory)
{
    OperationsInit o;

    controller::ControllerPtr c = controller::Controller::create();

    boost::shared_ptr<std::vector<controller::OperationPtr>> operations(new std::vector<controller::OperationPtr>());

    TestOperation* t1 = new TestOperation(boost::shared_ptr<memoryxcontroller::MemoryXController>(), boost::shared_ptr<scene3D::Scene>());
    controller::OperationPtr o1(t1);
    operations->push_back(o1);

    TestOperation* t2 = new TestOperation(boost::shared_ptr<memoryxcontroller::MemoryXController>(), boost::shared_ptr<scene3D::Scene>());
    t2->setThrowsOnWorkingMemory(true);
    controller::OperationPtr o2(t2);
    operations->push_back(o2);

    BOOST_CHECK_EQUAL(timesInverseOperationCreated, 0);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnWorkingMemoryExecuted, 0);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnSceneExecuted, 0);
    BOOST_CHECK_EQUAL(t1->getTimesExecuteOnSceneExecuted(), 0);
    BOOST_CHECK_EQUAL(t1->getTimesExecuteOnWorkingMemoryExecuted(), 0);
    BOOST_CHECK_EQUAL(t2->getTimesExecuteOnSceneExecuted(), 0);
    BOOST_CHECK_EQUAL(t2->getTimesExecuteOnWorkingMemoryExecuted(), 0);

    c->execute(controller::Controller::EXECUTE_ON_WM, operations);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnWorkingMemoryExecuted, 1);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnSceneExecuted, 0);
    BOOST_CHECK_EQUAL(t1->getTimesExecuteOnSceneExecuted(), 0);
    BOOST_CHECK_EQUAL(t1->getTimesExecuteOnWorkingMemoryExecuted(), 1);
    BOOST_CHECK_EQUAL(t2->getTimesExecuteOnSceneExecuted(), 0);
    BOOST_CHECK_EQUAL(t2->getTimesExecuteOnWorkingMemoryExecuted(), 0);
}

BOOST_AUTO_TEST_CASE(executeOperationFailedOnScene)
{
    OperationsInit o;

    controller::ControllerPtr c = controller::Controller::create();

    boost::shared_ptr<std::vector<controller::OperationPtr>> operations(new std::vector<controller::OperationPtr>());

    TestOperation* t1 = new TestOperation(boost::shared_ptr<memoryxcontroller::MemoryXController>(), boost::shared_ptr<scene3D::Scene>());
    controller::OperationPtr o1(t1);
    operations->push_back(o1);

    TestOperation* t2 = new TestOperation(boost::shared_ptr<memoryxcontroller::MemoryXController>(), boost::shared_ptr<scene3D::Scene>());
    t2->setThrowsOnScene(true);
    controller::OperationPtr o2(t2);
    operations->push_back(o2);

    BOOST_CHECK_EQUAL(timesInverseOperationCreated, 0);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnWorkingMemoryExecuted, 0);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnSceneExecuted, 0);
    BOOST_CHECK_EQUAL(t1->getTimesExecuteOnSceneExecuted(), 0);
    BOOST_CHECK_EQUAL(t1->getTimesExecuteOnWorkingMemoryExecuted(), 0);
    BOOST_CHECK_EQUAL(t2->getTimesExecuteOnSceneExecuted(), 0);
    BOOST_CHECK_EQUAL(t2->getTimesExecuteOnWorkingMemoryExecuted(), 0);

    c->execute(controller::Controller::EXECUTE_ON_SCENE, operations);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnWorkingMemoryExecuted, 0);
    BOOST_CHECK_EQUAL(timesInverseOperationExecuteOnSceneExecuted, 1);
    BOOST_CHECK_EQUAL(t1->getTimesExecuteOnSceneExecuted(), 1);
    BOOST_CHECK_EQUAL(t1->getTimesExecuteOnWorkingMemoryExecuted(), 0);
    BOOST_CHECK_EQUAL(t2->getTimesExecuteOnSceneExecuted(), 0);
    BOOST_CHECK_EQUAL(t2->getTimesExecuteOnWorkingMemoryExecuted(), 0);
}

BOOST_AUTO_TEST_CASE(executeSignal)
{
    controller::ControllerPtr c = controller::Controller::create();

    TestSlot* s = new TestSlot();
    QObject::connect(c.get(), SIGNAL(operationExecuted(controller::vector_string)), s, SLOT(testSlot()));

    BOOST_CHECK_EQUAL(s->getTimesSlotExecuted(), 0);

    boost::shared_ptr<std::vector<controller::OperationPtr>> operations(new std::vector<controller::OperationPtr>());
    TestOperation* t = new TestOperation(boost::shared_ptr<memoryxcontroller::MemoryXController>(), boost::shared_ptr<scene3D::Scene>());
    controller::OperationPtr t1(t);
    operations->push_back(t1);

    c->execute(0, operations);
    BOOST_CHECK_EQUAL(s->getTimesSlotExecuted(), 1);

    delete s;
}

BOOST_AUTO_TEST_CASE(triggerObjectClassSelected)
{
    controller::ControllerPtr c = controller::Controller::create();

    TestSlot* s = new TestSlot();
    QObject::connect(c.get(), SIGNAL(objectClassSelected(const std::string&, const std::string&)), s, SLOT(testSlot()));

    BOOST_CHECK_EQUAL(s->getTimesSlotExecuted(), 0);
    c->triggerObjectClassSelected(std::string(), std::string());
    BOOST_CHECK_EQUAL(s->getTimesSlotExecuted(), 1);

    delete s;
}

BOOST_AUTO_TEST_CASE(triggerSceneObjectSelected)
{
    controller::ControllerPtr c = controller::Controller::create();

    TestSlot* s = new TestSlot();
    QObject::connect(c.get(), SIGNAL(sceneObjectSelected(scene3D::SceneObjectPtr)), s, SLOT(testSlot()));

    BOOST_CHECK_EQUAL(s->getTimesSlotExecuted(), 0);
    c->triggerSceneObjectSelected(scene3D::SceneObjectPtr(new scene3D::SceneObject("", "", "", new SoSeparator(), new SoSeparator())));
    BOOST_CHECK_EQUAL(s->getTimesSlotExecuted(), 1);

    delete s;
}

BOOST_AUTO_TEST_CASE(triggerMinimapClicked)
{
    controller::ControllerPtr c = controller::Controller::create();

    TestSlot* s = new TestSlot();
    QObject::connect(c.get(), SIGNAL(minimapClicked()), s, SLOT(testSlot()));

    BOOST_CHECK_EQUAL(s->getTimesSlotExecuted(), 0);
    c->triggerMinimapClicked();
    BOOST_CHECK_EQUAL(s->getTimesSlotExecuted(), 1);

    delete s;
}

BOOST_AUTO_TEST_CASE(triggerObjectsChanged)
{
    controller::ControllerPtr c = controller::Controller::create();

    TestSlot* s = new TestSlot();
    QObject::connect(c.get(), SIGNAL(objectsChanged(controller::vector_string)), s, SLOT(testSlot()));

    controller::vector_string objectIds;
    BOOST_CHECK_EQUAL(s->getTimesSlotExecuted(), 0);
    c->triggerObjectsChanged(objectIds);
    BOOST_CHECK_EQUAL(s->getTimesSlotExecuted(), 1);

    delete s;
}

BOOST_AUTO_TEST_CASE(reloadLocalSceneOnEmptyWorkingMemory)
{
    MemoryXEnvironment environment;
    ENVIRONMENT(environment).priorKnowledgePrx->getObjectClassesSegment()->removeAllEntities();
    ENVIRONMENT(environment).workingMemoryPrx->getSegment("objectInstances")->clear();

    SbRotation rotation = SbRotation(1, 1, 1, 1);
    SbVec3f position = SbVec3f(2, 3, 4);

    memoryx::ObjectClassPtr objectClass = new memoryx::ObjectClass();
    objectClass->setName("testObject1");
    objectClass->putAttribute("IvFile", "");
    objectClass->putAttribute("IvFileCollision", "");
    objectClass->putAttribute("ManipulationFile", "");
    objectClass->putAttribute("parentClasses", "");
    ENVIRONMENT(environment).priorKnowledgePrx->getObjectClassesSegment()->addEntity(objectClass);

    memoryx::ObjectClassPtr objectClass2 = new memoryx::ObjectClass();
    objectClass2->setName("testObject2");
    objectClass2->putAttribute("IvFile", "");
    objectClass2->putAttribute("IvFileCollision", "");
    objectClass2->putAttribute("ManipulationFile", "");
    objectClass2->putAttribute("parentClasses", "");
    ENVIRONMENT(environment).priorKnowledgePrx->getObjectClassesSegment()->addEntity(objectClass2);

    scene3D::SceneObjectPtr sceneObject = scene3D::SceneObjectPtr(new scene3D::SceneObject("Object 1", "testObject2", "", new SoSeparator, new SoSeparator));
    CONTROLLER(environment)->getScene()->getObjectManager()->addObject(sceneObject);
    sceneObject->setRotation(rotation);
    sceneObject->setTranslation(position);

    TestSlot* s = new TestSlot();
    QObject::connect(CONTROLLER(environment).get(), SIGNAL(reloadScene()), s, SLOT(testSlot()));

    BOOST_CHECK_EQUAL(s->getTimesSlotExecuted(), 0);
    CONTROLLER(environment)->reloadLocalScene();
    {
        scene3D::SceneObjectPtr sceneObject = CONTROLLER(environment)->getScene()->getObjectManager()->getObjectById("Object 1");
        BOOST_CHECK(!sceneObject);
    }
    BOOST_CHECK_EQUAL(s->getTimesSlotExecuted(), 1);
}

BOOST_AUTO_TEST_CASE(clearScene)
{
    MemoryXEnvironment environment;
    memoryx::ObjectInstanceMemorySegmentBasePrx objectInstancesPrx = memoryx::ObjectInstanceMemorySegmentBasePrx::uncheckedCast(ENVIRONMENT(environment).workingMemoryPrx->getSegment("objectInstances"));
    ENVIRONMENT(environment).priorKnowledgePrx->getObjectClassesSegment()->removeAllEntities();
    ENVIRONMENT(environment).workingMemoryPrx->getSegment("objectInstances")->clear();

    SbRotation rotation = SbRotation(1, 1, 1, 1);
    SbVec3f position = SbVec3f(2, 3, 4);

    std::vector<std::string> ids;
    ids.push_back(addObjectToEnvironment(environment, rotation, position));
    ids.push_back(addObjectToEnvironment(environment, rotation, position));
    ids.push_back(addObjectToEnvironment(environment, rotation, position));
    ids.push_back(addObjectToEnvironment(environment, rotation, position));

    CONTROLLER(environment)->clearScene();

    for (std::string objectId : ids)
    {
        memoryx::ObjectInstancePtr object = memoryx::ObjectInstancePtr::dynamicCast(objectInstancesPrx->getEntityById(objectId));
        BOOST_CHECK(!object);
        scene3D::SceneObjectPtr sceneObject = CONTROLLER(environment)->getScene()->getObjectManager()->getObjectById(objectId);
        BOOST_CHECK(!sceneObject);
        scene3D::SceneGroupPtr sceneGroup = CONTROLLER(environment)->getScene()->getGroupManager()->getGroupById(objectId + "_group");
        BOOST_CHECK(!sceneGroup);
    }
}

