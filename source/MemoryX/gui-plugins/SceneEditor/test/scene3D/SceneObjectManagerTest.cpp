/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE ArmarX::SceneEditor::controller::Controller
#define ARMARX_BOOST_TEST

#include <MemoryX/Test.h>
#include "../InitializeCoin.h"
#include "../TestSlot.h"

#include <MemoryX/gui-plugins/SceneEditor/scene3D/PointerDefinitions.h>
#include <MemoryX/gui-plugins/SceneEditor/controller/Controller.h>
#include <MemoryX/gui-plugins/SceneEditor/scene3D/SceneObject.h>
#include <MemoryX/gui-plugins/SceneEditor/scene3D/SceneObjectManager.h>

BOOST_AUTO_TEST_CASE(addObject)
{
    controller::ControllerPtr controller = controller::Controller::create();

    controller->getScene()->getObjectManager()->addObject(scene3D::SceneObjectPtr(new scene3D::SceneObject("testObject", "testClass", "testCollection", new SoSeparator, new SoSeparator)));
}

BOOST_AUTO_TEST_CASE(addObjectTwice)
{
    controller::ControllerPtr controller = controller::Controller::create();

    controller->getScene()->getObjectManager()->addObject(scene3D::SceneObjectPtr(new scene3D::SceneObject("testObject", "testClass", "testCollection", new SoSeparator, new SoSeparator)));
    BOOST_CHECK_THROW(controller->getScene()->getObjectManager()->addObject(scene3D::SceneObjectPtr(new scene3D::SceneObject("testObject", "testClass", "testCollection", new SoSeparator, new SoSeparator))), std::logic_error);
}

BOOST_AUTO_TEST_CASE(getObjectByIdExist)
{
    controller::ControllerPtr controller = controller::Controller::create();

    scene3D::SceneObjectPtr object(new scene3D::SceneObject("testObject", "testClass", "testCollection", new SoSeparator, new SoSeparator));

    controller->getScene()->getObjectManager()->addObject(object);

    BOOST_CHECK(controller->getScene()->getObjectManager()->getObjectById("testObject"));
}

BOOST_AUTO_TEST_CASE(getObjectByIdNotExist)
{
    controller::ControllerPtr controller = controller::Controller::create();

    BOOST_CHECK(!controller->getScene()->getObjectManager()->getObjectById("testObject"));
}

BOOST_AUTO_TEST_CASE(removeObject)
{
    controller::ControllerPtr controller = controller::Controller::create();

    scene3D::SceneObjectPtr object(new scene3D::SceneObject("testObject", "testClass", "testCollection", new SoSeparator, new SoSeparator));

    controller->getScene()->getObjectManager()->addObject(object);

    controller->getScene()->getObjectManager()->removeObject(object);

    BOOST_CHECK(!controller->getScene()->getObjectManager()->getObjectById("testObject"));
}

BOOST_AUTO_TEST_CASE(removeObjectNotExist)
{
    controller::ControllerPtr controller = controller::Controller::create();

    BOOST_CHECK_THROW(controller->getScene()->getObjectManager()->removeObject(scene3D::SceneObjectPtr(new scene3D::SceneObject("testObject", "testClass", "testCollection", new SoSeparator, new SoSeparator))), std::runtime_error);
}

BOOST_AUTO_TEST_CASE(removeObjectNull)
{
    controller::ControllerPtr controller = controller::Controller::create();

    BOOST_CHECK_THROW(controller->getScene()->getObjectManager()->removeObject(scene3D::SceneObjectPtr()), std::runtime_error);
}
