/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

/* Qt headers */
#include <QModelIndex>
#include <QMenu>
#include <QStringList>
#include <QTableWidgetItem>
#include <QHeaderView>


#include "../controller/Controller.h"
#include "../controller/AddToGroupOperation.h"
#include "../controller/CreateGroupOperation.h"
#include "../controller/SelectOperation.h"
#include "../controller/RemoveFromGroupOperation.h"
#include "../controller/DeleteGroupOperation.h"
#include "../scene3D/SceneObject.h"
#include "ShortcutController.h"
#include "GroupExplorerWidget.h"
#include <MemoryX/gui-plugins/SceneEditor/ui_GroupExplorerWidget.h>
#include "OverrideAction.h"

gui::GroupExplorerWidget::GroupExplorerWidget(const controller::ControllerPtr& control, QPointer<dialog::GroupExplorerDialog> groupEditorDialog, QWidget* parent) :
    QWidget(parent),
    ui(new Ui::GroupExplorerWidget),
    control(control),
    groupEditorDialog(groupEditorDialog)
{
    ui->setupUi(this);
    ui->newGroupButton->setIcon(QIcon(QString::fromStdString(":/icons/edit-add.ico")));

    // Register shortcuts
    std::shared_ptr<gui::ShortcutController> shortcutController = control->getShortcutController();
    QPointer<QAction> addToNewGroup(new OverrideAction("Group Explorer: Add Selection to new group", this));
    this->addAction(addToNewGroup);
    shortcutController->registerAction(addToNewGroup->text(), addToNewGroup);

    setMinimumWidth(200);

    // Connections to add selection to new group
    connect(ui->newGroupButton, SIGNAL(clicked()), this, SLOT(addSelectionToNewGroup()));
    connect(addToNewGroup, SIGNAL(triggered()), this, SLOT(addSelectionToNewGroup()));

    // Connections for context menu
    connect(ui->groupsTableWidget, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(customContextMenuRequested(QPoint)));

    // Connections to open group editor
    connect(ui->groupsTableWidget, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(triggerOpenGroupEditor(QModelIndex)));
    connect(this, SIGNAL(groupNameClicked()), this, SLOT(openGroupEditorDialog()));

    // Connections to update group list
    connect(control.get(), SIGNAL(operationExecuted(controller::vector_string)), this, SLOT(listAllGroups()));

    // Connections for buttons in table
    connect(ui->groupsTableWidget, SIGNAL(itemClicked(QTableWidgetItem*)), this, SLOT(triggerOperation(QTableWidgetItem*)));
    connect(this, SIGNAL(addToGroupClicked()), this, SLOT(addSelectionToGroup()));
    connect(this, SIGNAL(addToSelectionClicked()), this, SLOT(addGroupToSelection()));


}


gui::GroupExplorerWidget::~GroupExplorerWidget()
{
    delete ui;
}

void gui::GroupExplorerWidget::customContextMenuRequested(const QPoint& position)
{
    currentItem = ui->groupsTableWidget->itemAt(position);
    QModelIndex index = ui->groupsTableWidget->indexAt(position);

    // Position has to be in the first column where the group names are listed
    if (index.isValid() && index.column() == 0)
    {
        QMenu* menu = new QMenu(this);
        menu->addAction(tr("Add selection to group"), this, SLOT(addSelectionToGroup()));
        menu->addAction(tr("Add group to selection"), this, SLOT(addGroupToSelection()));
        menu->addAction(tr("Remove group"), this, SLOT(groupRemoveClicked()));
        menu->addAction(tr("Show in Group Editor..."), this, SLOT(openGroupEditorDialog()));
        menu->popup(ui->groupsTableWidget->viewport()->mapToGlobal(position));
    }

}

void gui::GroupExplorerWidget::addSelectionToGroup()
{
    if (currentItem != NULL)
    {
        std::string groupName = currentItem->text().toStdString();
        createAddToGroupOperations(groupName);
    }

}

void gui::GroupExplorerWidget::addGroupToSelection()
{
    if (currentItem != NULL)
    {
        std::string groupName = currentItem->text().toStdString();
        std::vector<scene3D::SceneObjectPtr> objectsInGroup;

        if (controller::ControllerPtr controller = control.lock())
        {
            objectsInGroup = controller->getScene()->getGroupManager()->getGroupById(groupName)->getAllObjects();
            std::shared_ptr<std::vector<controller::OperationPtr> > operations(new std::vector<controller::OperationPtr>());

            // Create SelectOperation for each object
            for (std::size_t i = 0; i < objectsInGroup.size(); ++i)
            {
                controller::OperationPtr operation(new controller::SelectOperation(controller->getMemoryXController(),
                                                   controller->getScene(),
                                                   objectsInGroup[i]->getObjectId()));
                operations->push_back(operation);
            }

            controller->execute(controller::Controller::EXECUTE_ON_SCENE | controller::Controller::EXECUTE_ON_WM | controller::Controller::UNDOABLE, operations);
        }
    }
}

void gui::GroupExplorerWidget::groupRemoveClicked()
{
    if (currentItem != NULL)
    {
        if (controller::ControllerPtr controller = control.lock())
        {
            std::string groupName = currentItem->text().toStdString();

            if (!controller->getScene()->getGroupManager()->getGroupById(groupName))
            {
                return;
            }

            std::shared_ptr<std::vector<controller::OperationPtr> > operations(new std::vector<controller::OperationPtr>());

            for (scene3D::SceneObjectPtr object : controller->getScene()->getGroupManager()->getGroupById(groupName)->getAllObjects())
            {
                controller::OperationPtr operation(new controller::RemoveFromGroupOperation(controller->getMemoryXController(),
                                                   controller->getScene(),
                                                   groupName,
                                                   object->getObjectId()));
                operations->push_back(operation);
            }

            controller::OperationPtr operation(new controller::DeleteGroupOperation(controller->getMemoryXController(),
                                               controller->getScene(),
                                               groupName));
            operations->push_back(operation);
            controller->execute(controller::Controller::EXECUTE_ON_SCENE | controller::Controller::UNDOABLE, operations);
        }
    }
}

void gui::GroupExplorerWidget::triggerOperation(QTableWidgetItem* item)
{
    ui->groupsTableWidget->setCurrentItem(item);
    currentItem = ui->groupsTableWidget->item(ui->groupsTableWidget->currentRow(), 0);

    if (currentItem != NULL)
    {
        // Icon to add selection to group has been clicked
        if (item->column() == 1)
        {
            emit addToGroupClicked();
        }
        // Icon to add group to selection has been clicked
        else if (item->column() == 2)
        {
            emit addToSelectionClicked();
        }
    }
}

void gui::GroupExplorerWidget::triggerOpenGroupEditor(QModelIndex index)
{
    if (index.column() == 0)
    {
        emit groupNameClicked();
    }
}

void gui::GroupExplorerWidget::createAddToGroupOperations(std::string groupName)
{
    std::vector<scene3D::SceneObjectPtr> selectedObjects;

    if (controller::ControllerPtr controller = control.lock())
    {
        selectedObjects = controller->getScene()->getSelectionManager()->getAllSelected();
        std::shared_ptr<std::vector<controller::OperationPtr> > operations(new std::vector<controller::OperationPtr>());

        // Create SelectOperation for each object
        for (std::size_t i = 0; i < selectedObjects.size(); ++i)
        {
            controller::OperationPtr operation(new controller::AddToGroupOperation(controller->getMemoryXController(),
                                               controller->getScene(),
                                               groupName, selectedObjects[i]->getObjectId()));
            operations->push_back(operation);
        }

        controller->execute(controller::Controller::EXECUTE_ON_SCENE | controller::Controller::EXECUTE_ON_WM | controller::Controller::UNDOABLE, operations);
    }
}

void gui::GroupExplorerWidget::openGroupEditorDialog()
{
    if (groupEditorDialog)
    {
        groupEditorDialog->setCurrentGroup(currentItem->text());
        groupEditorDialog->exec();
    }
}

void gui::GroupExplorerWidget::listAllGroups()
{
    ui->groupsTableWidget->clear();

    std::vector<scene3D::SceneGroupPtr> allGroups;

    if (controller::ControllerPtr controller = control.lock())
    {
        allGroups = controller->getScene()->getGroupManager()->getAllGroups();
    }

    ui->groupsTableWidget->setColumnCount(3);
    ui->groupsTableWidget->setRowCount(allGroups.size());

    int index = 0;

    for (std::size_t i = 0; i < allGroups.size(); ++i)
    {
        scene3D::SceneGroupPtr group = allGroups.at(i);
        QString groupName = QString::fromStdString(group->getGroupId());
        QTableWidgetItem* item = new QTableWidgetItem(groupName);
        QTableWidgetItem* addIcon = new QTableWidgetItem(QIcon(QString::fromStdString(":/icons/edit-add.ico")), "");
        QTableWidgetItem* selectIcon = new QTableWidgetItem(QIcon(QString::fromStdString(":/images/Select.png")), "");
        addIcon->setToolTip(tr("Add selection to group"));
        selectIcon->setToolTip(tr("Add group to selection"));
        ui->groupsTableWidget->setItem(index, 0, item);
        ui->groupsTableWidget->setItem(index, 1, addIcon);
        ui->groupsTableWidget->setItem(index, 2, selectIcon);
        ++index;
    }

    // To align the button columns on the right
    QHeaderView* headerView = ui->groupsTableWidget->horizontalHeader();
    headerView->setResizeMode(headerView->logicalIndexAt(0, 0), QHeaderView::Stretch);
    headerView->setResizeMode(headerView->logicalIndexAt(1, 0), QHeaderView::Fixed);
    headerView->setResizeMode(headerView->logicalIndexAt(2, 0), QHeaderView::Fixed);
    headerView->resizeSection(1, 20);
    headerView->resizeSection(2, 20);
}

void gui::GroupExplorerWidget::addSelectionToNewGroup()
{
    if (controller::ControllerPtr controller = control.lock())
    {
        int nameIndex = 0;

        do
        {
            nameIndex++;
        }
        while (controller->getScene()->getGroupManager()->getGroupById(tr("New Group ").toStdString() + std::to_string(nameIndex)) != NULL);

        std::string groupName = tr("New Group ").toStdString() + std::to_string(nameIndex);

        // Create CreateGroup operation
        std::shared_ptr<std::vector<controller::OperationPtr> > operations(new std::vector<controller::OperationPtr>());
        controller::OperationPtr operation(new controller::CreateGroupOperation(controller->getMemoryXController(),
                                           controller->getScene(),
                                           groupName));
        operations->push_back(operation);
        controller->execute(controller::Controller::EXECUTE_ON_SCENE | controller::Controller::UNDOABLE, operations);

        createAddToGroupOperations(groupName);
    }
}

void gui::GroupExplorerWidget::retranslate()
{
    ui->retranslateUi(this);
}
