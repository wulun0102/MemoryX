/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

/* Qt headers */
#include <QWidget>
#include <QPointer>
#include <QSortFilterProxyModel>

/* C++ headers */
#include <string>
#include <vector>

#include "../controller/Controller.h"
#include "ObjectInspectorWidget.h"
#include "ObjectExplorerModel.h"

namespace Ui
{
    class ObjectExplorerWidget;
}

namespace gui
{
    /**
    * This class provides a QWidget which displays all object classes from PriorKnowledge in their collections in a tree view.
    * The object class can be filtered via a line edit.
    *
    */
    class ObjectExplorerWidget : public QWidget
    {
        Q_OBJECT

    public:

        /**
        * Constructor.
        * Constructs a object explorer widget.
        * Expects a controller::ControllerPtr to get the data to display and a <i>parent</i> Qt::QWidget.
        *
        * @param    control     shared pointer to controller::Controller
        * @param    parent      parent widget
        *
        */
        explicit ObjectExplorerWidget(const controller::ControllerPtr& control, QWidget* parent = 0);
        ~ObjectExplorerWidget() override;

        /**
        * Populates the gui::ObjectExplorerModel with data and sets up all child widgets.
        *
        * @see  gui::ObjectExplorerModel
        *
        */
        void onConnect();

        /**
        * Translates all translatable strings in this dialog.
        */
        void retranslate();

    private Q_SLOTS:
        void filterFixedString();
        void getSelectedObject(const QModelIndex& index);

    private:
        Ui::ObjectExplorerWidget* ui;
        controller::ControllerWeakPtr control;
        QPointer<gui::ObjectExplorerModel> sourceModel;

    };
}

