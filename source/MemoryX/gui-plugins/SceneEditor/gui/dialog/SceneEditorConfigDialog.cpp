/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SceneEditorConfigDialog.h"
#include <MemoryX/gui-plugins/SceneEditor/ui_SceneEditorConfigDialog.h>

#include <filesystem>
#include <filesystem>

#include <IceUtil/UUID.h>

#include <QComboBox>
#include <QMessageBox>
#include <QDialogButtonBox>
#include <QPushButton>
#include <QFileDialog>

#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/IceProxyFinder.h>

#include <MemoryX/interface/components/PriorKnowledgeInterface.h>
#include <MemoryX/interface/components/WorkingMemoryInterface.h>
//#include <QtCore/qfsfileengine.h>


gui::dialog::SceneEditorConfigDialog::SceneEditorConfigDialog(QWidget* parent) :
    QDialog(parent),
    ui(new Ui::SceneEditorConfigDialog)
{
    ui->setupUi(this);

    workingMemoryProxyFinder = new armarx::IceProxyFinder<memoryx::WorkingMemoryInterfacePrx>(this);
    workingMemoryProxyFinder->setSearchMask("WorkingMemory");
    workingMemoryProxyFinder->showSearchMaskField(true);
    workingMemoryProxyFinder->showLabels(false);
    ui->workingMemoryGridLayout->addWidget(workingMemoryProxyFinder, 0, 1, 1, 2);

    workingMemoryUpdatesTopicFinder = new armarx::IceTopicFinder(this);
    workingMemoryUpdatesTopicFinder->setSearchMask("WorkingMemoryUpdates");
    workingMemoryUpdatesTopicFinder->showSearchMaskField(false);
    workingMemoryUpdatesTopicFinder->showLabels(false);
    ui->workingMemoryGridLayout->addWidget(workingMemoryUpdatesTopicFinder, 1, 1, 1, 2);

    priorKnowledgeProxyFinder = new armarx::IceProxyFinder<memoryx::PriorKnowledgeInterfacePrx>(this);
    priorKnowledgeProxyFinder->setSearchMask("PriorKnowledge");
    priorKnowledgeProxyFinder->showSearchMaskField(true);
    priorKnowledgeProxyFinder->showLabels(false);
    ui->priorKnowledgeGridLayout->addWidget(priorKnowledgeProxyFinder, 0, 1, 1, 2);

    connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(verifyConfiguration()));
    ui->buttonBox->button(QDialogButtonBox::Ok)->setDefault(false);

    std::filesystem::path currentPath(std::filesystem::current_path());
    std::filesystem::path defaultFile("SceneEditor.ini");
    this->ui->settingsFileLineEdit->setText(QString::fromStdString((currentPath / defaultFile).string()));
    connect(ui->fileBrowserToolButton, SIGNAL(released()), this, SLOT(fileBrowserToolButtonReleased()));
}

gui::dialog::SceneEditorConfigDialog::~SceneEditorConfigDialog()
{
    delete ui;
}

std::string gui::dialog::SceneEditorConfigDialog::getDefaultName() const
{
    return "SceneEditorConfigDialog" + IceUtil::generateUUID();
}

void gui::dialog::SceneEditorConfigDialog::onInitComponent()
{
    workingMemoryProxyFinder->setIceManager(this->getIceManager());
    workingMemoryUpdatesTopicFinder->setIceManager(this->getIceManager());
    priorKnowledgeProxyFinder->setIceManager(this->getIceManager());
}

void gui::dialog::SceneEditorConfigDialog::onConnectComponent()
{

}

void gui::dialog::SceneEditorConfigDialog::onExitComponent()
{
    QObject::disconnect();
}

void gui::dialog::SceneEditorConfigDialog::setIceObjectName()
{
    this->setName(this->getDefaultName());
}

std::string gui::dialog::SceneEditorConfigDialog::getWorkingMemoryName()
{
    return this->workingMemoryProxyFinder->getSelectedProxyName().toStdString();
}

std::string gui::dialog::SceneEditorConfigDialog::getWorkingMemoryUpdatesTopic()
{
    return this->workingMemoryUpdatesTopicFinder->getSelectedProxyName().toStdString();
}

std::string gui::dialog::SceneEditorConfigDialog::getPriorMemoryName()
{
    return this->priorKnowledgeProxyFinder->getSelectedProxyName().toStdString();
}

void gui::dialog::SceneEditorConfigDialog::verifyConfiguration()
{
    if (this->getWorkingMemoryName().length() == 0)
    {
        QMessageBox::critical(this, tr("Invalid Configuration"), tr("The WorkingMemory name must not be empty."));
    }
    else if (this->getWorkingMemoryUpdatesTopic().length() == 0)
    {
        QMessageBox::critical(this, tr("Invalid Configuration"), tr("The WorkingMemory updates topic name must not be empty."));
    }
    else if (!this->isTopicAvailable(this->getWorkingMemoryUpdatesTopic()))
    {
        QMessageBox::critical(this, tr("Invalid Configuration"), tr("The WorkingMemory updates topic name does not name an avaiable topic."));
    }
    else if (this->getPriorMemoryName().length() == 0)
    {
        QMessageBox::critical(this, tr("Invalid Configuration"), tr("The PriorMemory name must not be empty."));
    }
    else if (this->ui->settingsFileLineEdit->text().length() == 0)
    {
        QMessageBox::critical(this, tr("Invalid Configuration"), tr("The settings file path name must not be empty."));
    }
    else
    {
        this->accept();
    }
}

void gui::dialog::SceneEditorConfigDialog::retranslate()
{
    this->ui->retranslateUi(this);
}

QString gui::dialog::SceneEditorConfigDialog::getSettingsFilePath()
{
    return this->ui->settingsFileLineEdit->text();
}

void gui::dialog::SceneEditorConfigDialog::fileBrowserToolButtonReleased()
{
    std::filesystem::path currentPath(this->ui->settingsFileLineEdit->text().toStdString());
    QString filePath = QFileDialog::getSaveFileName(this, tr("Open settings file"), QString::fromStdString(currentPath.remove_filename().string()), tr("Settings (*.ini)"), new QString(), QFileDialog::DontConfirmOverwrite);

    if (!filePath.endsWith(".ini", Qt::CaseSensitivity::CaseInsensitive) && !filePath.isEmpty())
    {
        filePath.append(".ini");
    }

    this->ui->settingsFileLineEdit->setText(filePath);
}

bool gui::dialog::SceneEditorConfigDialog::isTopicAvailable(std::string topicName)
{
    return this->workingMemoryUpdatesTopicFinder->getProxyNameList("*").contains(QString::fromStdString(topicName));
}
