/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SaveSnapshotDialog.h"
#include <MemoryX/gui-plugins/SceneEditor/ui_SaveSnapshotDialog.h>
#include <QPushButton>

gui::dialog::SaveSnapshotDialog::SaveSnapshotDialog(controller::ControllerPtr control, QWidget* parent) :
    QDialog(parent),
    ui(new Ui::SaveSnapshotDialog),
    control(control)
{
    ui->setupUi(this);
    connectSlots();
    newSnapshot = false;
    allObjects = true;
}

gui::dialog::SaveSnapshotDialog::~SaveSnapshotDialog()
{
    delete ui;
}


void gui::dialog::SaveSnapshotDialog::connectSlots()
{
    connect(ui->newSnapshotRadioButton, SIGNAL(toggled(bool)), this, SLOT(enableSaveNewSnapshot(bool)));
    connect(ui->replaceSnapshotRadioButton, SIGNAL(toggled(bool)), this, SLOT(enableReplaceSnapshot(bool)));
    connect(ui->saveAllRadioButton, SIGNAL(toggled(bool)), this, SLOT(enableSaveAllObjects(bool)));
    connect(ui->saveGroupsRadioButton, SIGNAL(toggled(bool)), this, SLOT(enableSaveGroups(bool)));
    connect(ui->snapshotNameLineEdit, SIGNAL(textChanged(QString)), this, SLOT(checkOkButtonStatus()));
    connect(ui->groupsListWidget, SIGNAL(itemSelectionChanged()), this, SLOT(checkOkButtonStatus()));
}

void gui::dialog::SaveSnapshotDialog::showEvent(QShowEvent*)
{
    if (controller::ControllerPtr controller = control.lock())
    {
        // Load Existing Snapshots
        ui->existingSnapshotsComboBox->clear();
        std::vector<std::string> allSnapshotsVector = controller->getMemoryXController()->getAllSnapshots();

        for (std::vector<std::string>::iterator it = allSnapshotsVector.begin(); it != allSnapshotsVector.end(); ++it)
        {
            ui->existingSnapshotsComboBox->insertItem(ui->existingSnapshotsComboBox->count(), QString::fromStdString(*it));
        }

        // Load Existing Groups
        ui->groupsListWidget->clear();
        std::vector<scene3D::SceneGroupPtr> allGroupsVector = controller->getScene()->getGroupManager()->getAllGroups();

        for (std::vector<scene3D::SceneGroupPtr>::iterator it = allGroupsVector.begin(); it != allGroupsVector.end(); ++it)
        {
            ui->groupsListWidget->insertItem(ui->groupsListWidget->count(), QString::fromStdString((*it)->getGroupId()));
        }

        checkOkButtonStatus();
    }
}

void gui::dialog::SaveSnapshotDialog::accept()
{
    if (controller::ControllerPtr controller = control.lock())
    {
        QString snapshotName = newSnapshot ? ui->snapshotNameLineEdit->text() : ui->existingSnapshotsComboBox->currentText();
        std::string snapshotNameStr = snapshotName.toStdString();

        if (allObjects)
        {
            controller->saveSnapshotAsJSON(snapshotNameStr);
            controller->getMemoryXController()->saveSceneInSnapshot(snapshotNameStr);
        }
        //std::cout << "Saving all Objects as snapshot " << snapshotName.toStdString() << std::endl;
        else
        {
            std::vector<std::string> allObjects;
            QList<QListWidgetItem*> allSelectedGroups = ui->groupsListWidget->selectedItems();

            for (QList<QListWidgetItem*>::iterator it = allSelectedGroups.begin(); it != allSelectedGroups.end(); ++it)
            {
                scene3D::SceneGroupPtr selectedGroup = controller->getScene()->getGroupManager()->getGroupById((*it)->text().toStdString());
                std::vector<scene3D::SceneObjectPtr> allObjectsInGroup = selectedGroup->getAllObjects();

                for (std::vector<scene3D::SceneObjectPtr>::iterator it = allObjectsInGroup.begin(); it != allObjectsInGroup.end(); ++it)
                {
                    allObjects.insert(allObjects.end(), (*it)->getObjectId());
                }
            }

            controller->getMemoryXController()->saveObjectsInSnapshot(snapshotNameStr, allObjects);
            //std::cout << "Saving " << allObjects.size() << " Objects as snapshot " << snapshotName.toStdString() << std::endl;
        }

        close();
    }
}

void gui::dialog::SaveSnapshotDialog::checkOkButtonStatus()
{
    bool inputIsValid = (!ui->snapshotNameLineEdit->text().isEmpty() || !newSnapshot) && (ui->groupsListWidget->selectedItems().count() > 0 || allObjects);
    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(inputIsValid);
}

void gui::dialog::SaveSnapshotDialog::enableSaveNewSnapshot(bool enabled)
{
    if (enabled)
    {
        ui->snapshotNameLineEdit->setEnabled(enabled);
        ui->existingSnapshotsComboBox->setDisabled(enabled);
        newSnapshot = true;
        checkOkButtonStatus();
    }

}

void gui::dialog::SaveSnapshotDialog::enableReplaceSnapshot(bool enabled)
{
    if (enabled)
    {
        ui->existingSnapshotsComboBox->setEnabled(enabled);
        ui->snapshotNameLineEdit->setDisabled(enabled);
        newSnapshot = false;
        checkOkButtonStatus();
    }

}

void gui::dialog::SaveSnapshotDialog::enableSaveAllObjects(bool enabled)
{
    if (enabled)
    {
        ui->groupsListWidget->setDisabled(enabled);
        allObjects = true;
        checkOkButtonStatus();
    }
}

void gui::dialog::SaveSnapshotDialog::enableSaveGroups(bool enabled)
{
    if (enabled)
    {
        ui->groupsListWidget->setEnabled(enabled);
        allObjects = false;
        checkOkButtonStatus();
    }
}

void gui::dialog::SaveSnapshotDialog::retranslate()
{
    this->ui->retranslateUi(this);
}
