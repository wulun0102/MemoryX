/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

/* Qt headers */
#include <QDialog>
#include <QPointer>

/* ArmarX headers */
#include <ArmarXCore/core/services/tasks/RunningTask.h>
#include <ArmarXCore/core/IceManager.h>
#include <ArmarXCore/core/ManagedIceObject.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/IceProxyFinder.h>

namespace Ui
{
    class SceneEditorConfigDialog;
}

namespace gui::dialog
{
    /**
    * This class provides a dialog derived from Qt::QDialog and armarx::ManagedIceObject.
    * In this dialog the user has to set the necessary settings to start the plugin.
    *
    * @see  armarx::ManagedIceObject
    *
    */
    class SceneEditorConfigDialog :
        public QDialog,
        virtual public armarx::ManagedIceObject
    {
        Q_OBJECT

    public:
        /**
        * Constructor.
        * Creates an instance of SceneEditorConfigDialog.
        *
        * @param    parent   parent widget
        *
        */
        explicit SceneEditorConfigDialog(QWidget* parent = 0);

        /**
        * Destructor.
        * Destroys the initial config dialog.
        *
        */
        ~SceneEditorConfigDialog() override;

        /**
        * Reimplemented armarx::ManagedIceObject:getDefaultName().
        *
        * @see armarx::ManagedIceObject:getDefaultName()
        *
        */
        std::string getDefaultName() const override;

        /**
        * Reimplemented armarx::ManagedIceObject:onInitComponent().
        *
        * @see armarx::ManagedIceObject:onInitComponent()
        *
        */
        void onInitComponent() override;

        /**
        * Reimplemented armarx::ManagedIceObject:onConnectComponent().
        *
        * @see armarx::ManagedIceObject:onConnectComponent()
        *
        */
        void onConnectComponent() override;

        /**
        * Reimplemented armarx::ManagedIceObject:onExitComponent().
        *
        * @see armarx::ManagedIceObject:onExitComponent()
        *
        */
        void onExitComponent() override;

        /**
        * Sets unic name for this widget.
        *
        */
        void setIceObjectName();

        /**
        * Returns selected WorkingMemory name
        *
        * @return WorkingMemory name
        *
        */
        std::string getWorkingMemoryName();

        /**
        * Returns selected WorkingMemory updates topic
        *
        * @return WorkingMemory updates topic
        *
        */
        std::string getWorkingMemoryUpdatesTopic();

        /**
        * Returns selected PriorKnowledge name.
        *
        * @return   PriorMemory name
        *
        */
        std::string getPriorMemoryName();

        /**
        * Returns the full path to the selected settings file.
        *
        * @return   file path to settings file
        *
        */
        QString getSettingsFilePath();

        /**
          * Translates all translatable strings in this dialog.
          */
        void retranslate();

    private slots:
        void verifyConfiguration();
        void fileBrowserToolButtonReleased();

        bool isTopicAvailable(std::string topicName);

    private:
        Ui::SceneEditorConfigDialog* ui;

        QPointer<armarx::IceProxyFinderBase> workingMemoryProxyFinder;
        QPointer<armarx::IceTopicFinder> workingMemoryUpdatesTopicFinder;
        QPointer<armarx::IceProxyFinderBase> priorKnowledgeProxyFinder;
    };
}
