/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <QDialog>
#include <QLineEdit>

#include "../ShortcutTableModel.h"
#include "../ShortcutController.h"
#include "../../controller/Controller.h"
#include <QSortFilterProxyModel>

namespace Ui
{
    class ShortcutDialog;
}

namespace gui::dialog
{
    class ShortcutDialog : public QDialog
    {
        Q_OBJECT

    public:
        /**
        * Constructor
        *
        * Creates a new Instance of this class.
        *
        * @param control Pointer to Main Controller
        * @param parent Pointer to parent Widget
        */
        explicit ShortcutDialog(controller::ControllerPtr control, QWidget* parent = 0);

        /**
         * Destructor.
         *
         */
        ~ShortcutDialog() override;

        /**
          * Translates all translatable strings in this dialog.
          */
        void retranslate();

    private Q_SLOTS:
        void shortcutClicked(QModelIndex index);
        void shortcutChanged();

    private:
        void showEvent(QShowEvent* event) override;
        Ui::ShortcutDialog* ui;
        QPointer<ShortcutTableModel> tableModel;
        QPointer<QSortFilterProxyModel> proxyModel;
        std::shared_ptr<gui::ShortcutController> shortcutController;
        QString activeAction;
    };
}
