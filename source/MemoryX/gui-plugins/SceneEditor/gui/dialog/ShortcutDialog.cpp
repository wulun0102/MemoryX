/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ShortcutDialog.h"
#include <MemoryX/gui-plugins/SceneEditor/ui_ShortcutDialog.h>

#include <QHash>
#include <QKeySequence>

gui::dialog::ShortcutDialog::ShortcutDialog(controller::ControllerPtr control, QWidget* parent) :
    QDialog(parent),
    ui(new Ui::ShortcutDialog)
{
    ui->setupUi(this);
    shortcutController = control->getShortcutController();

    tableModel = new ShortcutTableModel();
    proxyModel = new QSortFilterProxyModel();
    proxyModel->setSourceModel(tableModel);

    ui->shortcutsTableView->setModel(proxyModel);
    ui->shortcutsTableView->horizontalHeader()->setResizeMode(0, QHeaderView::Stretch);
    ui->shortcutsTableView->horizontalHeader()->resizeSection(1, 150);

    // Connect Slots and Signals
    connect(ui->buttonBox, SIGNAL(rejected()), this, SLOT(close()));
    connect(ui->shortcutsTableView, SIGNAL(clicked(QModelIndex)), this, SLOT(shortcutClicked(QModelIndex)));
    connect(ui->shortcutLineEdit, SIGNAL(textChanged(QString)), this, SLOT(shortcutChanged()));
}

gui::dialog::ShortcutDialog::~ShortcutDialog()
{
    delete ui;
}

void gui::dialog::ShortcutDialog::showEvent(QShowEvent* event)
{
    tableModel->setShortcutHashTable(shortcutController->getAllRegisteredShortcuts());
    proxyModel = new QSortFilterProxyModel();
    proxyModel->setSourceModel(tableModel);
    ui->shortcutsTableView->setModel(proxyModel);
    ui->shortcutsTableView->sortByColumn(0, Qt::AscendingOrder);
}

void gui::dialog::ShortcutDialog::shortcutClicked(QModelIndex index)
{
    ui->keySequenceGroupBox->setEnabled(true);
    activeAction = tableModel->actionName(proxyModel->mapToSource(index), Qt::DisplayRole).toString();
    ui->shortcutLineEdit->setText(shortcutController->getAllRegisteredShortcuts().value(activeAction));
    ui->shortcutLineEdit->setFocus();
}

void gui::dialog::ShortcutDialog::shortcutChanged()
{
    shortcutController->updateShortcut(activeAction, ui->shortcutLineEdit->text());
    tableModel->setShortcutHashTable(shortcutController->getAllRegisteredShortcuts());
    proxyModel = new QSortFilterProxyModel();
    proxyModel->setSourceModel(tableModel);
    ui->shortcutsTableView->setModel(proxyModel);
    ui->shortcutsTableView->sortByColumn(0, Qt::AscendingOrder);
}

void gui::dialog::ShortcutDialog::retranslate()
{
    this->ui->retranslateUi(this);
}
