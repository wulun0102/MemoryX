/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MinimapWidget.h"
#include <QGridLayout>

gui::MinimapWidget::MinimapWidget(const controller::ControllerPtr control, QWidget* parent) :
    QWidget(parent),
    control(control),
    camera(new SoOrthographicCamera)
{
    //setPalette(Qt::transparent);
    //setAttribute(Qt::WA_TransparentForMouseEvents);

    this->setContentsMargins(1, 1, 1, 1);

    QGridLayout* grid = new QGridLayout();
    grid->setContentsMargins(0, 0, 0, 0);
    this->setLayout(grid);
    this->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);

    QWidget* view1 = new QWidget(this);
    view1->setMinimumSize(100, 100);
    view1->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);

    viewer.reset(new scene3D::MinimapViewer(control, view1));
    viewer->setSceneGraph(control->getScene()->registerCamera(camera));

    //View whole scene from top
    camera->position.setValue(0, 0, 0);
    camera->pointAt(SbVec3f(0, 0, -1));
    control->getScene()->makeCameraViewAll(camera, viewer->getViewportRegion());

    //Apply custom highlighting and make viewer redraw on selection changes
    viewer->setGLRenderAction(new scene3D::SoGLHighlightRenderAction(viewer->getViewportRegion(), control->getScene().get()));
    viewer->redrawOnSelectionChange(control->getScene()->selectionRootNode);

    // show everything
    // Show the viewer causes, that the gui show a window on loading the plugin. This window does not close.
    // However everything works if this method is not called.
    //    viewer->show();

    grid->addWidget(view1, 0, 0, 1, 2);

    setMinimumWidth(200);

    connect(control.get(), SIGNAL(reloadScene()), this, SLOT(updateMinimap()));
    connect(control.get(), SIGNAL(operationExecuted(controller::vector_string)), this, SLOT(updateMinimap()));
}

void gui::MinimapWidget::updateMinimap()
{
    camera->position.setValue(0, 0, 0);
    camera->pointAt(SbVec3f(0, 0, -1));

    if (controller::ControllerPtr controller = control.lock())
    {
        controller->getScene()->makeCameraViewAll(camera, viewer->getViewportRegion());
    }
}
