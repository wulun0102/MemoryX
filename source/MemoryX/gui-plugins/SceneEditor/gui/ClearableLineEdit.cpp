/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

/* Qt-headers */
#include <QStyle>

#include "ClearableLineEdit.h"

gui::ClearableLineEdit::ClearableLineEdit(QWidget* parent) :
    QLineEdit(parent),
    clearButton(new QToolButton(this))
{
    QIcon icon = QIcon::fromTheme("edit-clear", QIcon(":/icon/dialog-close.ico"));
    clearButton->setIcon(icon);
    clearButton->setCursor(Qt::ArrowCursor);
    clearButton->setStyleSheet("QToolButton { border: none; padding: 0px; }");
    clearButton->hide();

    int frameWidth = style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
    setStyleSheet(QString("QLineEdit { padding-right: %1px; } ").arg(clearButton->sizeHint().width() + frameWidth + 1));
    QSize minSize = minimumSizeHint();
    setMinimumSize(qMax(minSize.width(), clearButton->sizeHint().height() + frameWidth * 2 + 2), qMax(minSize.height(), clearButton->sizeHint().height() + frameWidth * 2 + 2));

    connect(clearButton, SIGNAL(clicked()), this, SLOT(clear()));
    connect(this, SIGNAL(textChanged(const QString&)), this, SLOT(updateClearButton(const QString&)));
}

void gui::ClearableLineEdit::resizeEvent(QResizeEvent*)
{
    QSize size = clearButton->sizeHint();
    int frameWidth = style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
    clearButton->move(rect().right() - frameWidth - size.width(),
                      (rect().bottom() + 1 - size.height()) / 2);
}

void gui::ClearableLineEdit::updateClearButton(const QString& text)
{
    clearButton->setVisible(!text.isEmpty());
}


gui::ClearableLineEdit::~ClearableLineEdit()
{
}
