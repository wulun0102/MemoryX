/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

/* Qt headers */
#include <QTreeView>

namespace gui
{
    /**
    * This class derived from QTreeView supports custom drag and drop behavior.
    * The QTreeView class provides a default model/view implementation of a tree view.
    *
    * @see Qt::QTreeView
    *
    */
    class CollectionsTreeView :
        public QTreeView
    {
        Q_OBJECT

    public:

        /**
        * Constructor.
        * Constructs a tree view with a parent to represent a model's data.
        *
        * @param parent parent widget
        */
        explicit CollectionsTreeView(QWidget* parent = 0);

        /**
        * Starts a drag with a dragable QPixmap using the given <i>supportedActions</i>.
        *
        * @param supportedActions supported Qt::DropActions
        * @see Qt::QAbstractItemView
        * @see Qt::QDrag
        *
        */
        void startDrag(Qt::DropActions supportedActions) override;
    };
}

