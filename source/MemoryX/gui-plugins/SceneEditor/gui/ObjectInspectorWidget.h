/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

/* Qt headers */
#include <QWidget>
#include <QPointer>

#include "../controller/Controller.h"
#include "../scene3D/SceneObject.h"
#include "../scene3D/ObjectPreviewViewer.h"
#include "PropertyBrowserWidget.h"

namespace Ui
{
    class ObjectInspectorWidget;
}

using ObjectPreviewViewerPtr = std::shared_ptr<scene3D::ObjectPreviewViewer>;

namespace gui
{
    /**
    * This class provides a widget which displays all class and instance attributes of a currently selected scene3D::SceneObject or a memoryx::ObjectClassPtr in a tree view.
    *
    */
    class ObjectInspectorWidget : public QWidget
    {
        Q_OBJECT

    public:

        /**
        * Constructor.
        * Constructs an instance of ObjectInspectorWidget.
        * Expects a controller::ControllerPtr to get information about the scene and the MemoryX.
        *
        */
        explicit ObjectInspectorWidget(const controller::ControllerPtr& control, QWidget* parent = 0);

        /**
        * Destructor.
        *
        */
        ~ObjectInspectorWidget() override;

        /**
          * Translates all translatable strings in this dialog.
          */
        void retranslate();

    private Q_SLOTS:
        void setAllAttributes(scene3D::SceneObjectPtr sceneObject);
        void setClassAttributes(const std::string& objectClass, const std::string& collection);
        void sceneObjectsUpdated(controller::vector_string objectIds);

    private:
        controller::ControllerWeakPtr control;
        QPointer<gui::PropertyBrowserWidget> propertyBrowser;
        ObjectPreviewViewerPtr viewer;
        std::string currentObjectId;

        void showPreviewImage(const memoryx::ObjectClassPtr& objectClass);
        memoryx::ObjectClassPtr getObjectClass(const std::string& objectClass, const std::string& collection);
    };
}

