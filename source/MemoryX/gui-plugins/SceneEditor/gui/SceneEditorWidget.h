/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

/* Qt headers */
#include <QWidget>
#include <QPushButton>
#include <QVBoxLayout>
#include <QResizeEvent>
#include <QButtonGroup>
#include <QPointer>
#include <QLabel>

#include "../controller/Controller.h"
#include "ScenegraphWidget.h"

namespace Ui
{
    class SceneEditorWidget;
}

namespace gui
{
    /**
    * This class provides a widget which contains all buttons for the scene.
    *
    * @see gui::SceneGraphWidget
    *
    */
    class SceneEditorWidget : public QWidget
    {
        Q_OBJECT

    public:
        /**
        * Constructor.
        * Constructs a scene editor widget.
        * Expects controller::ControllerPtr to trigger operations.
        *
        * @param    control     shared pointer to controller::Controller
        * @param    parent      parent widget
        *
        */
        explicit SceneEditorWidget(const controller::ControllerPtr& control, QWidget* parent = 0);

        ~SceneEditorWidget() override;

        /**
          * Translates all translatable strings in this dialog.
          */
        void retranslate();

        void postDocking();

    Q_SIGNALS:

        /**
        * Signal emitted when the size of the widget has changed.
        *
        */
        void sizeChangedSignal(QSize newSize);

    private Q_SLOTS:
        void sizeChanged(QSize newSize);
        void undoButtonReleased();
        void redoButtonReleased();
        void removeSelectedObjectsButtonReleased();
        void collisionMeshToggleButtonToggled(bool checked);
        void rotateManipulatorToggleButtonToggled(bool checked);
        void translateManipulatorToggleButtonToggled(bool checked);
        void editorViewerButtonGroupButtonClicked(int id);
        void toggleEditorViewerMode();

    private:
        void resizeEvent(QResizeEvent* event) override;

        controller::ControllerWeakPtr control;
        QPointer<ScenegraphWidget> sceneGraph;
        QPointer<QPushButton> collisionMeshToggleButton;
        QPointer<QPushButton> rotateManipulatorToggleButton;
        QPointer<QPushButton> translateManipulatorToggleButton;
        QPointer<QPushButton> undoButton;
        QPointer<QPushButton> redoButton;
        QPointer<QPushButton> removeSelectedObjectsButton;
        QPointer<QPushButton> editorToggleButton;
        QPointer<QPushButton> viewerToggleButton;
        QPointer<QButtonGroup> editorViewerButtonGroup;
        QPointer<QVBoxLayout> mainLayout;
        QSize leftButtonsSize;
        QString buttonColor;
        int margin;
        int spacing;
        static const int VIEWER_BUTTON_ID = 0;
        static const int EDITOR_BUTTON_ID = 1;
        bool showCollisionMesh;
        bool showTranslateManipulator;
        bool showRotateManipulator;

    };
}

