/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditorMainWindow
 * @author     Adrian Knobloch ( adrian dot knobloch at student dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <string>

/* Qt headers */
#include <QComboBox>
#include <QDir>
#include <QHash>
#include <QString>
#include <QKeySequence>

#include <ArmarXCore/core/ArmarXManager.h>
//#include <qinputcontext.h>

/* SceneEditor plugin headers */
#include "SceneEditorMainWindowController.h"
#include <MemoryX/gui-plugins/SceneEditor/gui/dialog/ui_SceneEditorConfigDialog.h>
#include "OverrideAction.h"

gui::SceneEditorMainWindowController::SceneEditorMainWindowController() :
    mainWindow(NULL),
    loadSnapshotAction(new OverrideAction(QIcon(":/icons/document-open-folder.png"), tr("Load Snapshot"), this)),
    saveSnapshotAction(new OverrideAction(QIcon(":/icons/document-save-5.ico"), tr("Save Snapshot"), this)),
    openShortcutDialogAction(new OverrideAction(QIcon(":/images/keys.png"), tr("Keyboard Shortcuts"), this)),
    openGroupEditorAction(new OverrideAction(QIcon(":/images/chain-pencil.png"), tr("Group Editor"), this)),
    clearWorkingMemoryAction(new OverrideAction(QIcon(":/icons/dialog-close.ico"), tr("Clear Scene"), this)),
    control(controller::Controller::create()),
    settingsPath()
{
    shortcutDialog = new dialog::ShortcutDialog(control);
    groupEditorDialog = new dialog::GroupExplorerDialog(control);
    loadSnapshotDialog = new gui::dialog::LoadSnapshotDialog(control);
    saveSnapshotDialog = new gui::dialog::SaveSnapshotDialog(control);
    configDialog = NULL;
    customToolbar = NULL;
    workingMemoryName = "";
    workingMemoryUpdatesTopic = "";
    objectInstancesSegmentName = "";
    priorMemoryName = "";

    connect(this, SIGNAL(disconnectComponent()), this, SLOT(onDisconnectComponentQt()));
    connect(this, SIGNAL(connectComponent()), this, SLOT(onConnectComponentQt()));

    std::shared_ptr<gui::ShortcutController> shortcutController = control->getShortcutController();

    connect(loadSnapshotAction, SIGNAL(triggered()), this, SLOT(openLoadSnapshotDialog()));
    shortcutController->registerAction("General: Load Snapshot", loadSnapshotAction);

    connect(saveSnapshotAction, SIGNAL(triggered()), this, SLOT(openSaveSnapshotDialog()));
    shortcutController->registerAction("General: Save Snapshot", saveSnapshotAction);

    connect(openShortcutDialogAction, SIGNAL(triggered()), this, SLOT(openShortcutDialog()));
    shortcutController->registerAction("General: Edit Shortcuts", openShortcutDialogAction);

    connect(openGroupEditorAction, SIGNAL(triggered()), this, SLOT(openGroupEditorDialog()));
    shortcutController->registerAction("General: Edit Groups", openGroupEditorAction);

    connect(clearWorkingMemoryAction, SIGNAL(triggered()), this, SLOT(clearScene()));
    shortcutController->registerAction("General: Clear Scene", clearWorkingMemoryAction);

    this->createLanguageMenu();
}

void gui::SceneEditorMainWindowController::postDocking()
{
    qobject_cast<gui::SceneEditorMainWindow*>(getWidget())->postDocking();
}


gui::SceneEditorMainWindowController::~SceneEditorMainWindowController()
{
    if (!settingsPath.isEmpty())
    {
        QSettings settings(settingsPath, QSettings::IniFormat);
        settings.clear();
        QHash<QString, QKeySequence> shortcuts = control->getShortcutController()->getAllShortcuts();

        for (auto it = shortcuts.begin(); it != shortcuts.end(); ++it)
        {
            settings.setValue("ShortcutController/shortcut/" + it.key(), it.value().toString());
        }

        settings.setValue("SceneEditorMainWindowController/language", languageSelection->itemData(languageSelection->currentIndex()));
        settings.sync();
    }
}


void gui::SceneEditorMainWindowController::loadSettings(QSettings* settings)
{

}

void gui::SceneEditorMainWindowController::saveSettings(QSettings* settings)
{

}


void gui::SceneEditorMainWindowController::onInitComponent()
{
    usingProxy(workingMemoryName);
    usingProxy("LongtermMemory");
    usingProxy(priorMemoryName);
    usingProxy("CommonStorage");
}


void gui::SceneEditorMainWindowController::onConnectComponent()
{
    memoryx::LongtermMemoryInterfacePrx longtermMemoryPrx = getProxy<memoryx::LongtermMemoryInterfacePrx>("LongtermMemory");
    memoryx::WorkingMemoryInterfacePrx workingMemoryPrx = getProxy<memoryx::WorkingMemoryInterfacePrx>(workingMemoryName);
    memoryx::PriorKnowledgeInterfacePrx priorKnowledgePrx = getProxy<memoryx::PriorKnowledgeInterfacePrx>(priorMemoryName);
    this->control->getMemoryXController()->init(priorKnowledgePrx,
            workingMemoryPrx,
            workingMemoryUpdatesTopic,
            objectInstancesSegmentName,
            longtermMemoryPrx,
            this->control);
    getArmarXManager()->addObject(control->getMemoryXController()->getWorkingMemoryController());
    control->reloadLocalScene();
    enableMainWidgetAsync(true);
    emit connectComponent();
}

void gui::SceneEditorMainWindowController::onDisconnectComponent()
{
    shortcutDialog->close();
    groupEditorDialog->close();
    loadSnapshotDialog->close();
    saveSnapshotDialog->close();
    enableMainWidgetAsync(false);
    emit disconnectComponent();
}

void gui::SceneEditorMainWindowController::onConnectComponentQt()
{
    qobject_cast<gui::SceneEditorMainWindow*>(getWidget())->onConnect();
    customToolbar->setEnabled(true);
}

void gui::SceneEditorMainWindowController::onDisconnectComponentQt()
{
    qobject_cast<gui::SceneEditorMainWindow*>(getWidget())->onDisconnect();
    customToolbar->setDisabled(true);
}

QPointer<QWidget> gui::SceneEditorMainWindowController::getWidget()
{
    if (!mainWindow)
    {
        mainWindow = new SceneEditorMainWindow(control, groupEditorDialog);
        connect(mainWindow, SIGNAL(languageChangedByUser()), this, SLOT(slotLanguageChangedByUser()));
    }

    return qobject_cast<QWidget*>(mainWindow);
}

QPointer<QWidget> gui::SceneEditorMainWindowController::getCustomTitlebarWidget(QWidget* parent)
{
    if (customToolbar)
    {
        if (parent != customToolbar->parent())
        {
            customToolbar->setParent(parent);
        }

        return qobject_cast<QWidget*>(customToolbar);
    }

    customToolbar = new QToolBar(parent);
    customToolbar->setIconSize(QSize(16, 16));

    customToolbar->addAction(loadSnapshotAction);
    customToolbar->addAction(saveSnapshotAction);
    customToolbar->addAction(openShortcutDialogAction);
    customToolbar->addAction(openGroupEditorAction);
    customToolbar->addAction(clearWorkingMemoryAction);
    customToolbar->addWidget(languageSelection);
    customToolbar->setDisabled(true);

    return qobject_cast<QWidget*>(customToolbar);
}

QPointer<QDialog> gui::SceneEditorMainWindowController::getConfigDialog(QWidget* parent)
{
    if (!configDialog)
    {
        configDialog = new dialog::SceneEditorConfigDialog(parent);
        configDialog->setIceObjectName();
    }

    return qobject_cast<dialog::SceneEditorConfigDialog*>(configDialog);
}

void gui::SceneEditorMainWindowController::configured()
{
    workingMemoryName = configDialog->getWorkingMemoryName();
    workingMemoryUpdatesTopic = configDialog->getWorkingMemoryUpdatesTopic();
    objectInstancesSegmentName = "objectInstances";
    priorMemoryName = configDialog->getPriorMemoryName();

    settingsPath = configDialog->getSettingsFilePath();
    QSettings settings(settingsPath, QSettings::IniFormat);
    QHash<QString, QKeySequence> shortcuts;
    QStringList allKeys = settings.allKeys();

    for (auto it = allKeys.begin(); it != allKeys.end(); ++it)
    {
        if ((*it).startsWith("ShortcutController/shortcut/", Qt::CaseSensitivity::CaseSensitive))
        {
            shortcuts.insert((*it).replace("ShortcutController/shortcut/", "", Qt::CaseSensitivity::CaseSensitive), QKeySequence(settings.value(*it).value<QString>()));
        }
    }

    QString language = settings.value("SceneEditorMainWindowController/language", QString()).value<QString>();

    for (int i = 0; i < languageSelection->count(); ++i)
    {
        if (languageSelection->itemData(i) == language)
        {
            loadLanguage(language);
            languageSelection->setCurrentIndex(i);
            break;
        }
    }

    if (shortcuts.isEmpty())
    {
        shortcuts.insert("General: Clear Scene", QKeySequence("Ctrl+Shift+Del"));
        shortcuts.insert("General: Edit Groups", QKeySequence("Ctrl+G"));
        shortcuts.insert("General: Edit Shortcuts", QKeySequence("Ctrl+K"));
        shortcuts.insert("General: Load Snapshot", QKeySequence("Ctrl+O"));
        shortcuts.insert("General: Save Snapshot", QKeySequence("Ctrl+S"));
        shortcuts.insert("Group Explorer: Add Selection to new group", QKeySequence("Ctrl+Shift++"));
        shortcuts.insert("Object Explorer: Set Focus on Search Field", QKeySequence("Ctrl+Space"));
        shortcuts.insert("Scene: Camera view all", QKeySequence("Shift+5"));
        shortcuts.insert("Scene: Camera view from X", QKeySequence("Shift+3"));
        shortcuts.insert("Scene: Camera view from Y", QKeySequence("Shift+7"));
        shortcuts.insert("Scene: Camera view from Z", QKeySequence("Shift+1"));
        shortcuts.insert("Scene: Camera view from negative X", QKeySequence("Ctrl+Shift+3"));
        shortcuts.insert("Scene: Camera view from negative Y", QKeySequence("Ctrl+Shift+7"));
        shortcuts.insert("Scene: Camera view from negative Z", QKeySequence("Ctrl+Shift+1"));
        shortcuts.insert("Scene: Copy selection", QKeySequence("Ctrl+C"));
        shortcuts.insert("Scene: Duplicate selection", QKeySequence("Ctrl+D"));
        shortcuts.insert("Scene: Paste selection", QKeySequence("Ctrl+V"));
        shortcuts.insert("Scene: Redo last Action", QKeySequence("Ctrl+Shift+Z"));
        shortcuts.insert("Scene: Remove the selected Objects", QKeySequence("Del"));
        shortcuts.insert("Scene: Select all or deselect all", QKeySequence("Ctrl+A"));
        shortcuts.insert("Scene: Switch between Editor and Viewer Mode", QKeySequence("Z"));
        shortcuts.insert("Scene: Switch to Editor Mode", QKeySequence("Shift+E"));
        shortcuts.insert("Scene: Switch to Viewer Mode", QKeySequence("Shift+V"));
        shortcuts.insert("Scene: Toggle Collision Meshes", QKeySequence("Shift+C"));
        shortcuts.insert("Scene: Toggle Rotation Manipulator", QKeySequence("Shift+R"));
        shortcuts.insert("Scene: Toggle Translation Manipulator", QKeySequence("Shift+T"));
        shortcuts.insert("Scene: Undo last Action", QKeySequence("Ctrl+Z"));
        shortcuts.insert("Scene: Reset rotation", QKeySequence("Alt+R"));
        shortcuts.insert("Scene: Reset translation", QKeySequence("Alt+T"));
    }

    for (auto it = shortcuts.begin(); it != shortcuts.end(); ++it)
    {
        control->getShortcutController()->updateShortcut(it.key(), it.value());
    }

    configDialog->getArmarXManager()->removeObjectBlocking(configDialog->getName());
}

void gui::SceneEditorMainWindowController::clearScene()
{
    control->clearScene();
}

void gui::SceneEditorMainWindowController::openLoadSnapshotDialog()
{
    loadSnapshotDialog->exec();
}

void gui::SceneEditorMainWindowController::openSaveSnapshotDialog()
{
    saveSnapshotDialog->exec();
}

void gui::SceneEditorMainWindowController::openShortcutDialog()
{
    shortcutDialog->exec();
}

void gui::SceneEditorMainWindowController::openGroupEditorDialog()
{
    groupEditorDialog->exec();
}

void gui::SceneEditorMainWindowController::createLanguageMenu()
{
    languageSelection = new QComboBox();

    connect(languageSelection, SIGNAL(currentIndexChanged(int)), this, SLOT(slotLanguageChanged(int)));

    QDir dir(":/languages/");
    QStringList fileNames = dir.entryList(QStringList("SceneEditorLanguages_*.qm"));

    for (int i = 0; i < fileNames.size(); ++i)
    {
        // get locale extracted by filename
        QString language;
        language = fileNames[i];                  // "SceneEditorLanguages_de.qm"
        language.truncate(language.lastIndexOf('.'));   // "SceneEditorLanguages_de"
        language.remove(0, language.indexOf('_') + 1);   // "de"

        QString languageName = QLocale(language).nativeLanguageName();

        languageSelection->addItem(languageName, language);

        // Gets the local language and checks whether the added language is the same. If so it is set as the active language.
        QString defaultLocale = QLocale::system().name();       // e.g. "de_DE"
        defaultLocale.truncate(defaultLocale.lastIndexOf('_')); // e.g. "de"

        if (defaultLocale == language)
        {
            languageSelection->setCurrentIndex(languageSelection->count() - 1);
        }
    }
}

void gui::SceneEditorMainWindowController::switchTranslator(QTranslator& translator, const QString& filename, const QString& directory)
{
    // remove the old translator
    qApp->removeTranslator(&translator);

    // load the new translator
    if (translator.load(filename, directory))
    {
        qApp->installTranslator(&translator);
    }
}

void gui::SceneEditorMainWindowController::loadLanguage(const QString& rLanguage)
{
    if (m_currLang != rLanguage)
    {
        m_currLang = rLanguage;
        QString languageFilePath = QString::fromStdString(":/languages/");

        switchTranslator(m_translator, QString("SceneEditorLanguages_%1.qm").arg(rLanguage), languageFilePath);
        //        switchTranslator(m_translatorQt, QString("qt_%1.qm").arg(rLanguage), languageFilePath);
    }
}

// Called every time, when a menu entry of the language menu is called
void gui::SceneEditorMainWindowController::slotLanguageChanged(const int languageIndex)
{
    if (languageIndex != -1)
    {
        QString language = this->languageSelection->itemData(languageIndex).toString();
        this->loadLanguage(language);
    }
}

// Called every time, when the main window gets a language change event
void gui::SceneEditorMainWindowController::slotLanguageChangedByUser()
{
    this->saveSnapshotDialog->retranslate();
    this->loadSnapshotDialog->retranslate();
    this->groupEditorDialog->retranslate();
    this->shortcutDialog->retranslate();

    this->openGroupEditorAction->setText(tr("Group Editor"));
    this->openShortcutDialogAction->setText(tr("Keyboard Shortcuts"));
    this->saveSnapshotAction->setText(tr("Save Snapshot"));
    this->loadSnapshotAction->setText(tr("Load Snapshot"));
    this->clearWorkingMemoryAction->setText(tr("Clear Scene"));
}
