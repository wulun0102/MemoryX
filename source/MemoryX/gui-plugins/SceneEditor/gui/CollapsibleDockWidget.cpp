/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <iostream>
#include <assert.h>
#include <QPointer>
#include <QSizePolicy>
#include <qtreeview.h>
#include <QTimer>

#include "CollapsibleDockWidget.h"
#include "SceneEditorMainWindow.h"

gui::CollapsibleDockWidget::CollapsibleDockWidget(const QString& title, QWidget* parent, Qt::WindowFlags flags) :
    QDockWidget(title, parent, flags)
{
    gui::CollapsibleDockWidget::TitleBar* titleBar = new gui::CollapsibleDockWidget::TitleBar(this);
    setTitleBarWidget(titleBar);
}

gui::CollapsibleDockWidget::CollapsibleDockWidget(QWidget* parent, Qt::WindowFlags flags) :
    QDockWidget(parent, flags)
{
    gui::CollapsibleDockWidget::TitleBar* titleBar = new gui::CollapsibleDockWidget::TitleBar(this);
    setTitleBarWidget(titleBar);
}

void gui::CollapsibleDockWidget::setCollapsibleWidget(QWidget* w)
{
    gui::CollapsibleDockWidget::InnerWidgetWrapper* wid = new gui::CollapsibleDockWidget::InnerWidgetWrapper(this);
    wid->setWidget(w);
    QDockWidget::setWidget(wid);

    if (isVisible())
    {
        widget()->show();
    }
}

void gui::CollapsibleDockWidget::setCollapsed(bool collapsed)
{
    gui::CollapsibleDockWidget::InnerWidgetWrapper* innerWidget = dynamic_cast<gui::CollapsibleDockWidget::InnerWidgetWrapper*>(widget());

    if (innerWidget != NULL)
    {
        innerWidget->setCollapsed(collapsed);
    }
    else
    {
        std::cerr << "DockWidget is not collapsible" << std::endl;
    }
}

bool gui::CollapsibleDockWidget::isCollapsed()
{
    gui::CollapsibleDockWidget::InnerWidgetWrapper* innerWidget = dynamic_cast<gui::CollapsibleDockWidget::InnerWidgetWrapper*>(widget());
    return innerWidget != NULL ? innerWidget->isCollapsed() : false;
}

void gui::CollapsibleDockWidget::toggleCollapsed()
{
    setCollapsed(!isCollapsed());
}

void gui::CollapsibleDockWidget::windowTitleChanged()
{
    gui::CollapsibleDockWidget::TitleBar* titleBar = dynamic_cast<gui::CollapsibleDockWidget::TitleBar*>(this->titleBarWidget());

    if (titleBar)
    {
        titleBar->windowTitleChanged();
    }
}

gui::CollapsibleDockWidget::InnerWidgetWrapper::InnerWidgetWrapper(QDockWidget* parent) :
    QWidget(parent),
    widget(NULL),
    hlayout(new QHBoxLayout(this)),
    widget_height(0),
    oldSize(0, 0)
{
    this->hlayout->setSpacing(0);
    this->hlayout->setContentsMargins(0, 0, 0, 0);
    this->setLayout(this->hlayout);
    QDockWidget* parentDockWidget = dynamic_cast<QDockWidget*>(parent);
    assert(parentDockWidget != NULL);
    oldMinimumSizeParent = parentDockWidget->minimumSize();
    oldMaximumSizeParent = parentDockWidget->maximumSize();
    oldMinimumSize = minimumSize();
    oldMaximumSize = maximumSize();
}

void gui::CollapsibleDockWidget::InnerWidgetWrapper::setWidget(QWidget* widget)
{
    this->widget = widget;
    this->widget_height = widget->height();
    this->layout()->addWidget(widget);
    this->oldSize = this->size();
    QDockWidget* parentDockWidget = dynamic_cast<QDockWidget*>(this->parent());
    assert(parentDockWidget != NULL);
    oldMinimumSizeParent = parentDockWidget->minimumSize();
    oldMaximumSizeParent = parentDockWidget->maximumSize();
    oldMinimumSize = minimumSize();
    oldMaximumSize = maximumSize();
}

bool gui::CollapsibleDockWidget::InnerWidgetWrapper::isCollapsed()
{
    return !this->widget->isVisible();
}

void gui::CollapsibleDockWidget::InnerWidgetWrapper::setCollapsed(bool collapsed)
{
    QDockWidget* parentDockWidget = dynamic_cast<QDockWidget*>(this->parent());
    assert(parentDockWidget != NULL);
    gui::CollapsibleDockWidget::TitleBar* parentDockWidgetTitleBar = dynamic_cast<gui::CollapsibleDockWidget::TitleBar*>(parentDockWidget->titleBarWidget());
    assert(parentDockWidgetTitleBar != NULL);

    if (!collapsed)
    {
        parentDockWidget->setMinimumSize(oldMinimumSizeParent);
        parentDockWidget->setMaximumSize(oldMaximumSizeParent);
        this->widget->show();
        parentDockWidgetTitleBar->showTitle(true);
        this->layout()->addWidget(this->widget);
        this->setMinimumSize(oldMinimumSize);
        this->setMaximumSize(oldMaximumSize);
        this->setBaseSize(this->oldSize);
        this->resize(this->oldSize);
    }
    else
    {
        this->oldSize = this->size();
        oldMinimumSizeParent = parentDockWidget->minimumSize();
        oldMaximumSizeParent = parentDockWidget->maximumSize();
        oldMinimumSize = minimumSize();
        oldMaximumSize = maximumSize();
        this->layout()->removeWidget(this->widget);
        this->widget->hide();
        parentDockWidgetTitleBar->showTitle(false);
        parentDockWidget->setMinimumSize(25, 25);
        parentDockWidget->setMaximumSize(25, 25);
        QTimer::singleShot(1, parentDockWidget, SLOT(setCollapsedSizes()));
    }
}

void gui::CollapsibleDockWidget::setCollapsedSizes()
{
    gui::CollapsibleDockWidget::InnerWidgetWrapper* innerWidget = dynamic_cast<gui::CollapsibleDockWidget::InnerWidgetWrapper*>(widget());
    assert(innerWidget != NULL);
    setMinimumSize(25, 25);

    if (features() & QDockWidget::DockWidgetVerticalTitleBar)
    {
        setMaximumSize(25, innerWidget->getOldMaximumSizeParent().height());
    }
    else
    {
        setMaximumSize(innerWidget->getOldMaximumSizeParent().width(), 25);
    }
}

gui::CollapsibleDockWidget::TitleBar::TitleBar(QWidget* parent) :
    QWidget(parent),
    hlayout(new QHBoxLayout(this)),
    collapse(new QPushButton(this)),
    title(new QLabel(parent->windowTitle()))
{
    this->hlayout->setDirection(QBoxLayout::Direction::RightToLeft);
    this->hlayout->setSpacing(0);
    this->hlayout->setContentsMargins(0, 0, 0, 0);
    this->setLayout(this->hlayout);
    this->hlayout->addWidget(collapse);
    collapse->setIcon(QIcon(":/images/branch-open.png"));
    collapse->setCheckable(false);
    collapse->setFixedSize(20, 20);
    connect(collapse, SIGNAL(released()), parent, SLOT(toggleCollapsed()));
    this->hlayout->addStretch();
    this->hlayout->addWidget(title);
}

void gui::CollapsibleDockWidget::TitleBar::windowTitleChanged()
{
    QDockWidget* parentDockWidget = dynamic_cast<QDockWidget*>(this->parent());
    title->setText(parentDockWidget->windowTitle());
}

void gui::CollapsibleDockWidget::TitleBar::showTitle(bool show)
{
    title->setVisible(show);

    if (show)
    {
        collapse->setIcon(QIcon(":/images/branch-open.png"));
    }
    else
    {
        collapse->setIcon(QIcon(":/images/branch-closed.png"));
    }
}

QSize const& gui::CollapsibleDockWidget::InnerWidgetWrapper::getOldMaximumSizeParent() const
{
    return oldMaximumSizeParent;
}
