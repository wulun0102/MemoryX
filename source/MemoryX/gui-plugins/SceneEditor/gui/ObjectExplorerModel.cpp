/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <QStandardItem>
#include <QDataStream>
#include <QByteArray>
#include <QtDesigner/abstractmetadatabase.h>
#include "ObjectExplorerModel.h"

gui::ObjectExplorerModel::ObjectExplorerModel(const controller::ControllerPtr& control, QObject* parent) :
    QSortFilterProxyModel(parent),
    control(control),
    sModel(new gui::ObjectExplorerModel::ObjectExplorerModelPrivate(control, 0, 1, this))
{
    setSourceModel(sModel);

    connect(this, SIGNAL(reload()), this, SLOT(onReload()));
}

void gui::ObjectExplorerModel::onConnect()
{
    emit reload();
}

void gui::ObjectExplorerModel::onReload()
{
    sModel->onReload();
}

void gui::ObjectExplorerModel::setFilterFixedString(QString searchPattern)
{
    setFilterCaseSensitivity(Qt::CaseInsensitive);
    QSortFilterProxyModel::setFilterFixedString(searchPattern);
}

std::pair<std::string, std::string> gui::ObjectExplorerModel::getItemInformation(const QModelIndex& index)
{
    return sModel->getItemInformation(mapToSource(index));
}

bool gui::ObjectExplorerModel::filterAcceptsRow(int sourceRow, const QModelIndex& sourceParent) const
{
    QModelIndex index = sourceModel()->index(sourceRow, 0, sourceParent);
    return showThis(index);
}

bool gui::ObjectExplorerModel::showThis(const QModelIndex& index) const
{
    bool acceptRow = false;

    //Gives you the info for number of childs with a parent
    if (sourceModel()->rowCount(index) > 0)
    {
        for (int nChild = 0; nChild < sourceModel()->rowCount(index); ++nChild)
        {
            QModelIndex childIndex = sourceModel()->index(nChild, 0, index);

            if (!childIndex.isValid())
            {
                break;
            }

            acceptRow = showThis(childIndex);

            if (acceptRow)
            {
                break;
            }
        }
    }
    else
    {
        QModelIndex useIndex = sourceModel()->index(index.row(), 0, index.parent());
        QString name = sourceModel()->data(useIndex, Qt::DisplayRole).toString();

        if (!name.contains(filterRegExp()) || name.isEmpty())
        {
            acceptRow = false;
        }
        else
        {
            acceptRow = true;
        }
    }

    return acceptRow;
}

QPixmap gui::ObjectExplorerModel::getItemPixmap(const QModelIndex& index) const
{
    return sModel->getItemPixmap(mapToSource(index));
}


/* methods for private class */

gui::ObjectExplorerModel::ObjectExplorerModelPrivate::ObjectExplorerModelPrivate(const controller::ControllerPtr& control, int rows, int columns, QObject* parent) :
    QStandardItemModel(rows, columns, parent),
    control(control),
    sizeHint(50, 50)
{
}

void gui::ObjectExplorerModel::ObjectExplorerModelPrivate::onReload()
{
    if (controller::ControllerPtr controller = control.lock())
    {
        std::vector<std::string> collections = controller->getMemoryXController()->getPriorKnowlegdeController()->getCollectionNames();
        this->clear();
        this->setRowCount(collections.size());
        std::vector<memoryx::ObjectClassPtr> objects;
        std::string objectName;

        for (int row = 0; row < (int)collections.size(); ++row)
        {
            objects = controller->getMemoryXController()->getPriorKnowlegdeController()->getAllObjectClassesPtr(collections[row]);

            // check if collection is empty
            // in this case the collection will not be added to the model
            if (objects.size() < 1)
            {
                ARMARX_WARNING_S << "No objects found in collection " << collections.at(row);
                continue;
            }

            QStandardItem* item = new QStandardItem(QString::fromStdString(collections[row]));

            for (int i = 0; i < (int)objects.size(); ++i)
            {
                objectName = objects[i]->getName();

                if (controller->getMemoryXController()->getPriorKnowlegdeController()->getCoinVisualisation(objects[i], true) && controller->getMemoryXController()->getPriorKnowlegdeController()->getCoinVisualisation(objects[i], false))
                {
                    QStandardItem* child = new QStandardItem(QString::fromStdString(objectName));
                    child->setSizeHint(sizeHint);
                    child->setIcon(this->getPreviewImageOfObject(controller, objects[i]));
                    child->setEditable(false);
                    item->setChild(i, child);
                }
                else
                {
                    ARMARX_WARNING_S << "No model found for: " << objectName;
                }
            }

            item->setSizeHint(QSize(30, 30));
            item->setDragEnabled(false);
            this->appendRow(item);
        }
    }
}

std::pair<std::string, std::string> gui::ObjectExplorerModel::ObjectExplorerModelPrivate::getItemInformation(const QModelIndex& index)
{
    std::pair<std::string, std::string> toReturn;

    if (index.isValid())
    {
        controller::ControllerPtr controller = control.lock();
        QStandardItem* item = itemFromIndex(index);

        if (controller && item->parent())
        {
            toReturn.first = item->text().toStdString(); // objectClass
            toReturn.second = item->parent()->text().toStdString(); // collection
        }
    }


    return toReturn;

}

QPixmap gui::ObjectExplorerModel::ObjectExplorerModelPrivate::getItemPixmap(const QModelIndex& index) const
{
    QPixmap pixmap;

    if (index.isValid())
    {
        QStandardItem* item = itemFromIndex(index);
        pixmap = item->icon().pixmap(item->icon().actualSize(sizeHint));
    }
    else
    {
        QIcon i = QIcon(QPixmap::fromImage(QImage(":/icons/armarx-gui.png")));
        pixmap = i.pixmap(i.actualSize(sizeHint));
    }

    return pixmap;
}

QIcon gui::ObjectExplorerModel::ObjectExplorerModelPrivate::getPreviewImageOfObject(const controller::ControllerPtr& controller, const memoryx::ObjectClassPtr& object)
{
    //SoNode* visualisation = controller->getMemoryXController()->getPriorKnowlegdeController()->getCoinVisualisation(object, false);
    QImage preview;
    preview = QImage(":/icons/armarx-gui.png");

    /*if (visualisation)
    {
        preview = controller->getScene()->getPreviewGenerator()->createPreview(visualisation, 50, 50);
    }
    if (!visualisation || preview.isNull())
    {
        preview = QImage("/home/h2t/armarx/Gui/source/Gui/ArmarXGui/icons/armarx-gui.png", "PNG");
        std::cout << "QImage does not exist for " + object->getName() << std::endl;
    }*/

    return QIcon(QPixmap::fromImage(preview));
}

QStringList gui::ObjectExplorerModel::ObjectExplorerModelPrivate::mimeTypes() const
{
    QStringList types;
    types << "application/vnd.text.list";
    return types;
}

QMimeData* gui::ObjectExplorerModel::ObjectExplorerModelPrivate::mimeData(const QModelIndexList& indexes) const
{
    QMimeData* mimeData = new QMimeData();
    QByteArray encodedData;

    QDataStream stream(&encodedData, QIODevice::WriteOnly);

    foreach (QModelIndex index, indexes)
    {
        if (index.isValid())
        {
            QStandardItem* item = itemFromIndex(index);

            if (item->parent())
            {
                stream << item->parent()->text();
                stream << item->text();
            }
        }
    }

    mimeData->setData("application/vnd.text.list", encodedData);
    return mimeData;
}
