/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

/* Qt-headers */
#include <QLineEdit>
#include <QToolButton>
#include <QPointer>

namespace gui
{

    /**
    * This class is a custom QLineEdit which provides a button to clear the text of the line edit.
    * Inherited from QLineEdit.
    *
    * @see Qt::QLineEdit
    *
    */
    class ClearableLineEdit : public QLineEdit
    {
        Q_OBJECT
    public:

        /**
        * Constructor.
        * Creates an instance of ClearableLineEdit by a given QWidget as parent.
        * The <i>parent</i> argument is sent to the QWidget constructor.
        * If no parent is given, the default parent is 0.
        *
        * @param    parent  parent widget
        *
        */
        explicit ClearableLineEdit(QWidget* parent = 0);

        /**
        * Destructor.
        * Destroys the clearable line edit.
        *
        */
        ~ClearableLineEdit() override;

    protected:
        /**
        * This event handler is implemented to receive widget resize events which are passed in the <i>event</i> parameter.
        * When <i>resizeEvent()</i> is called, the widget already has its new geometry.
        * The old size is accessible through Qt::QResizeEvent:oldSize().
        *
        * @param    Qt::QResizeEvent
        * @see      Qt::QResizeEvent
        * @see      Qt::QWidget:resizeEvent();
        *
        */
        void resizeEvent(QResizeEvent*) override;

    private:
        QPointer<QToolButton> clearButton;

    private Q_SLOTS:
        void updateClearButton(const QString& text);

    };
}

