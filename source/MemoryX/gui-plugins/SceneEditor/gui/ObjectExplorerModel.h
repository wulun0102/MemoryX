/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <QStandardItemModel>
#include <QSortFilterProxyModel>
#include <QStringList>
#include <QModelIndexList>
#include <QMimeData>
#include <QPointer>
#include <QSize>
#include <QBrush>

#include "../controller/Controller.h"

namespace gui
{
    /**
    * This class provides a model for storing classes from memoryxcontroller::PriorKnowledgeController and allows to filter them.
    *
    * @see memoryxcontroller::PriorKnowledgeController
    */
    class ObjectExplorerModel : public QSortFilterProxyModel
    {
        Q_OBJECT

    private:
        class ObjectExplorerModelPrivate : public QStandardItemModel
        {
        public:
            explicit ObjectExplorerModelPrivate(const controller::ControllerPtr& control, int rows, int columns, QObject* parent = 0);

            QStringList mimeTypes() const override;

            QMimeData* mimeData(const QModelIndexList& indexes) const override;

            void onReload();

            std::pair<std::string, std::string> getItemInformation(const QModelIndex& index);

            QPixmap getItemPixmap(const QModelIndex& index) const;

        private:
            controller::ControllerWeakPtr control;
            QSize sizeHint;

            QIcon getPreviewImageOfObject(const controller::ControllerPtr& controller, const memoryx::ObjectClassPtr& object);

        };

    public:
        /**
        * Creates a new instance of this class.
        *
        * @param control The main controller of the whole plugin.
        *
        * @see controller::Controller
        */
        explicit ObjectExplorerModel(const controller::ControllerPtr& control, QObject* parent = 0);

        /**
        * Reloads the model.
        * Should be called after the plugin is connected.
        */
        void onConnect();

        /**
        * Sets the string to filter all classes. Only the classes, which contain this string are shown after this.
        * The string is caseinsensitive.
        *
        * @param searchPattern The string to filter with.
        */
        void setFilterFixedString(QString searchPattern);

        /**
        * Returns a tupel containing information about the class at the given index.
        * The tupel contains the class name at first and class collection at second position.
        *
        * @param index The index in this model of the class.
        *
        * @return The tupel containing the informations.
        */
        std::pair<std::string, std::string> getItemInformation(const QModelIndex& index);

        /**
        * Returns the Icon of the class at index.
        *
        * @param index The index in this model of the class.
        *
        * @return The pixmap containing the image.
        */
        QPixmap getItemPixmap(const QModelIndex& index) const;

    signals:
        void reload();

    private slots:
        void onReload();

    private:
        controller::ControllerWeakPtr control;
        QPointer<gui::ObjectExplorerModel::ObjectExplorerModelPrivate> sModel;

        bool filterAcceptsRow(int sourceRow, const QModelIndex& sourceParent) const override;
        bool showThis(const QModelIndex& index) const;

    };
}

