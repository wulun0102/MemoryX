/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::gui-plugins::SceneEditor
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <QWidget>
#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>
#include <Inventor/nodes/SoOrthographicCamera.h>

#include "../controller/Controller.h"
#include "../scene3D/MinimapViewer.h"
#include "../scene3D/SoGLHighlightRenderAction.h"

using MinimapViewerPtr = std::shared_ptr<scene3D::MinimapViewer>;

namespace gui
{
    /**
    * This class is a widget which provides a scene3D::MinimapViewer to render the scene graph in bird perspective.
    *
    */
    class MinimapWidget : public QWidget
    {
        Q_OBJECT
    public:

        /**
        * Constructor.
        * Constructs a minimap widget.
        * Expects a controller::ControllerPtr.
        *
        * @param    control     shared pointer to controller::Controller
        * @param    parent      parent widget
        * @see      scene3D::MinimapViewer
        *
        */
        explicit MinimapWidget(const controller::ControllerPtr control, QWidget* parent = 0);

    private Q_SLOTS:
        void updateMinimap();

    private:
        controller::ControllerWeakPtr control;
        MinimapViewerPtr viewer;
        SoOrthographicCamera* camera;
    };
}

