armarx_component_set_name(PriorKnowledgeImporter)
set(COMPONENT_LIBS MemoryXImporters)
set(SOURCES main.cpp PriorKnowledgeImporterApp.h)
armarx_add_component_executable("${SOURCES}")
