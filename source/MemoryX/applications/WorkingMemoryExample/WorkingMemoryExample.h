/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::WorkingMemoryExample
* @author     (kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/Component.h>
#include <RobotAPI/interface/core/RobotState.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <MemoryX/interface/components/WorkingMemoryInterface.h>
#include <MemoryX/interface/components/LongtermMemoryInterface.h>

namespace memoryx
{

    class WorkingMemoryExamplePropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        WorkingMemoryExamplePropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("TestToRun", "UpdateNotifications", "Name of test that should be executed");
            defineOptionalProperty<std::string>("SnapshotName", "", "Name of WM snapshot to load (if TestToRun=SnapshotLoad)");
            defineOptionalProperty<std::string>("CommonPlacesSnapshot", "dataset1_1205", "Name of secondary WM snapshot to load (if TestToRun=CommonPlaces)");
        }
    };

    class ARMARXCOMPONENT_IMPORT_EXPORT WorkingMemoryExample :
        virtual public armarx::Component,
        virtual public WorkingMemoryListenerInterface
    {
    public:
        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return armarx::PropertyDefinitionsPtr(
                       new WorkingMemoryExamplePropertyDefinitions(
                           getConfigIdentifier()));
        }

        // inherited from Component
        std::string getDefaultName() const override
        {
            return "WorkingMemoryExample";
        }
        void onInitComponent() override;
        void onConnectComponent() override;

        void reportEntityCreated(const std::string& segmentName, const ::memoryx::EntityBasePtr& entity, const ::Ice::Current& = Ice::emptyCurrent) override;
        void reportEntityUpdated(const std::string& segmentName, const ::memoryx::EntityBasePtr& entityOld, const ::memoryx::EntityBasePtr& entityNew, const ::Ice::Current& = Ice::emptyCurrent) override;
        void reportEntityRemoved(const std::string& segmentName, const ::memoryx::EntityBasePtr& entity, const ::Ice::Current& = Ice::emptyCurrent) override;
        void reportSnapshotLoaded(const std::string& segmentName, const ::Ice::Current& = Ice::emptyCurrent) override;
        void reportSnapshotCompletelyLoaded(const Ice::Current& c = Ice::emptyCurrent) override;
        void reportMemoryCleared(const std::string& segmentName, const ::Ice::Current& = Ice::emptyCurrent) override;

    private:
        WorkingMemoryInterfacePrx memoryPrx;
        LongtermMemoryInterfacePrx longtermMemoryPrx;
        armarx::RobotStateComponentInterfacePrx robotStateComponentPrx;
        std::string testToRun;

        void testFileTransfer();
        void testUpdateObserver();
        void testObjectMovement();
        void testSnapshots();
        void testCommonPlaces();
        void testAgent();
    };


}

