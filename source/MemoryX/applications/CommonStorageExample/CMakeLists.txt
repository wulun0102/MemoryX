armarx_component_set_name(CommonStorageExample)


set(COMPONENT_LIBS ArmarXCore MemoryXInterfaces)

set(SOURCES main.cpp
    CommonStorageExample.cpp
    CommonStorageExample.h
)

armarx_add_component_executable("${SOURCES}")
