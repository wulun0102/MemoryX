/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::CommonPlacesTester
* @author     Alexey Kozlov (kozlov at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <MemoryX/interface/components/WorkingMemoryInterface.h>
#include <MemoryX/interface/components/LongtermMemoryInterface.h>
#include <MemoryX/interface/components/CommonPlacesLearnerInterface.h>

namespace memoryx
{

    class CommonPlacesTesterPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        CommonPlacesTesterPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("TestToRun", "LearnFromSnapshot", "Name of test that should be executed (LearnFromSnapshot, LoadLTM)");
            defineOptionalProperty<std::string>("SnapshotToLoad", "", "Name of snapshot to be loaded into WM on startup");
            defineOptionalProperty<std::string>("SnapshotsToLearnFrom", "", "Comma-separated list of snapshot to be processed by CommonPlacesLearner");
            defineOptionalProperty<bool>("LoadToWM", false, "Load LTM contents to WM after each learned snapshot (e.g. for visualization)");

            defineOptionalProperty<int>("InitialDelay", 0, "Timeout before starting the learning process (in ms)");
            defineOptionalProperty<int>("DelayBetweenSnapshots", 0, "Timeout after each learned snapshot (in ms)");
            defineOptionalProperty<std::string>("ObjectToLoad", "", "Name of object to load from LTM ");
            defineOptionalProperty<std::string>("LTMSegment", "objects", "Name of segment in LTM to store learned CommonPlaces / to run queries on");
            defineOptionalProperty<std::string>("ClusterDeviationType", "AABB", "Type of deviation: AABB, OrientedBBox or EqualSphere");
            defineOptionalProperty<float>("ClusterMaxDeviation", 100., "Max. intra-cluster deviation (meanings depends of ClusterDeviationType)");

            defineOptionalProperty<int>("SampleSize", 10, "");
            defineOptionalProperty<int>("IterationCount", 1, "");

        }
    };

    class ARMARXCOMPONENT_IMPORT_EXPORT CommonPlacesTester :
        virtual public armarx::Component
    {
    public:
        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return armarx::PropertyDefinitionsPtr(
                       new CommonPlacesTesterPropertyDefinitions(
                           getConfigIdentifier()));
        }

        // inherited from Component
        std::string getDefaultName() const override
        {
            return "CommonPlacesTester";
        }
        void onInitComponent() override;
        void onConnectComponent() override;

    private:
        WorkingMemoryInterfacePrx memoryPrx;
        LongtermMemoryInterfacePrx longtermMemoryPrx;
        CommonPlacesLearnerInterfacePrx cpLearnerPrx;
        std::string testToRun;
        std::string objectToLoad;
        std::string ltmSegmentName;

        void testLearnFromSnapshot();
        void testLearnFromSingleSnapshot();
        void testLoadLTM();
        void testClustering();

        void testMerging();
        void testClusteringBatch();
        void testAging();
        void testAging2();
        void testGetClusterLocations();

        void getRandomIds(std::vector<std::string>& allIds, std::vector<std::string>& ids, int sampleSize);

        DeviationType getDeviationTypeFromString(const std::string& devTypeStr);

        struct SimpleTimer
        {
            struct timeval tv1, tv2, dtv;
            struct timezone tz;
            void time_start()
            {
                gettimeofday(&tv1, &tz);
            }
            long time_stop()
            {
                gettimeofday(&tv2, &tz);
                dtv.tv_sec = tv2.tv_sec - tv1.tv_sec;
                dtv.tv_usec = tv2.tv_usec - tv1.tv_usec;

                if (dtv.tv_usec < 0)
                {
                    dtv.tv_sec--;
                    dtv.tv_usec += 1000000;
                }

                return dtv.tv_sec * 1000 + dtv.tv_usec / 1000;
            }
        };

        SimpleTimer mstimer;
    };
}
