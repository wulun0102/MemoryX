armarx_component_set_name(SimoxSceneImporter)
set(COMPONENT_LIBS MemoryXCore MemoryXImporters)
set(SOURCES main.cpp SimoxSceneImporterApp.h)
armarx_add_component_executable("${SOURCES}")
