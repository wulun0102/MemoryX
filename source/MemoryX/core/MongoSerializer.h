/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::CommonStorage
* @author     Alexey Kozlov ( kozlov at kit dot edu)
* @date       Sep 19, 2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/interface/observers/Serialization.h>

#include <MemoryX/interface/components/CommonStorageInterface.h>

namespace armarx
{
    class JSONObject;
    using JSONObjectPtr = IceInternal::Handle<JSONObject>;
}

namespace memoryx
{
    class MongoSerializer;
    using MongoSerializerPtr = IceUtil::Handle<MongoSerializer>;

    class MongoSerializer :
        public DBSerializerBase
    {
    public:
        MongoSerializer(const Ice::CommunicatorPtr ic = Ice::CommunicatorPtr(), bool useMongoObjectIds = false);
        ~MongoSerializer() override;

        /**
         * Takes a SerializablePtr and transforms it into a JSON object using the JSONObject serialization mechanism.
         * The result is returned in a new DBStorableData which contains the result in its JSON field.
         */
        DBStorableData serialize(const armarx::SerializablePtr& obj, const ::Ice::Current& = Ice::emptyCurrent) override;
        /**
         * Uses the JSON content of DBStorableDate \p objData and deserializes it into the \p obj pointer.
         */
        void deserialize(const DBStorableData& objData, const armarx::SerializablePtr& obj, const ::Ice::Current& = Ice::emptyCurrent) override;

        /**
         * Serialize an Ice object passed in the \p obj parameter.
         * The result is stored in the JSON field of a new DBStorableData
         *  instance and returned.
         */
        DBStorableData serializeIceObject(const armarx::SerializablePtr& obj, const ::Ice::Current& = Ice::emptyCurrent);
        /**
         * Deserializes the \p objData DBStorableData object into an instance of
         * Serializable and returns it.
         */
        armarx::SerializablePtr deserializeIceObject(const DBStorableData& objData, const ::Ice::Current& = Ice::emptyCurrent);

    private:
        bool useMongoIds;
        /**
         * jsonSerializer contains a JSONObject instance which is used to create copies for (de)serialization.
         * Creating copies is better scalable since it would otherwise be necessary to lock the access with mutexes.
         */
        armarx::JSONObjectPtr jsonSerializer;

        void serializeMongoId(const armarx::JSONObjectPtr& serializer);
        void deserializeMongoId(const armarx::JSONObjectPtr& deserializer);
    };

}

