/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::CommonStorage
* @author     Alexey Kozlov ( kozlov at kit dot edu)
* @date       Sep 19, 2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "MongoSerializer.h"

#include <ArmarXCore/util/json/JSONObject.h>


using namespace memoryx;

// NOTE: Mongo uses special datatype, "ObjectID" to store auto-generated document ids in collection
// It has a JSON representation which consists of 2 enclosed fields: _id : { $oid: { "<idstring>" } }
// For "natural" or user-defined ids this is not the case, they're stored as just _id: "idstring"
// Since MongoSerializer needs to support both id flavors and, more important, we store ids of either type
// as plain strings in C++ objects, some special processing is required
// See serializeMongoId()/deserializeMongoId functions for details
const std::string NATURAL_ID_FIELD_NAME = "_id";
const std::string MONGO_OBJECTID_FIELD_NAME = "_id.$oid";

MongoSerializer::MongoSerializer(const Ice::CommunicatorPtr ic, bool useMongoObjectIds /* = false */) :
    useMongoIds(useMongoObjectIds)
{
    jsonSerializer = new armarx::JSONObject(ic);
    jsonSerializer->setIdField(NATURAL_ID_FIELD_NAME);
}

MongoSerializer::~MongoSerializer()
    = default;

DBStorableData MongoSerializer::serialize(const armarx::SerializablePtr& obj, const ::Ice::Current& c)
{
    DBStorableData result;
    armarx::JSONObjectPtr serializer = new armarx::JSONObject(*jsonSerializer);
    serializer->reset();
    obj->serialize(serializer);

    // special handling for mongo ids, since they need to be saved as  _id : { $oid: { "<idstring>" } }
    serializeMongoId(serializer);

    result.JSON = serializer->toString();
    return result;
}

void MongoSerializer::deserialize(const DBStorableData& objData, const armarx::SerializablePtr& obj, const ::Ice::Current& c)
{
    armarx::JSONObjectPtr deserializer = new armarx::JSONObject(*jsonSerializer);

    deserializer->reset();
    deserializer->fromString(objData.JSON);

    // special handling for mongo ids, since they need to be saved as  _id : { $oid: { "<idstring>" } }
    deserializeMongoId(deserializer);

    if (obj)
    {
        obj->deserialize(deserializer);
    }
}

DBStorableData MongoSerializer::serializeIceObject(const armarx::SerializablePtr& obj, const ::Ice::Current&)
{
    DBStorableData result;
    armarx::JSONObjectPtr serializer = new armarx::JSONObject(*jsonSerializer);
    serializer->reset();
    serializer->serializeIceObject(obj);

    // special handling for mongo ids, since they need to be saved as  _id : { $oid: { "<idstring>" } }
    serializeMongoId(serializer);

    result.JSON = serializer->toString();
    return result;
}

armarx::SerializablePtr MongoSerializer::deserializeIceObject(const DBStorableData& objData, const ::Ice::Current&)
{
    if (objData.JSON.empty())
    {
        return armarx::SerializablePtr();
    }

    armarx::JSONObjectPtr deserializer = new armarx::JSONObject(*jsonSerializer);
    deserializer->reset();
    deserializer->fromString(objData.JSON);

    // special handling for mongo ids, since they need to be saved as  _id : { $oid: { "<idstring>" } }
    deserializeMongoId(deserializer);

    return deserializer->deserializeIceObject();
}

void MongoSerializer::serializeMongoId(const armarx::JSONObjectPtr& serializer)
{
    if (useMongoIds && serializer->hasElement(serializer->getIdField()))
    {
        armarx::AbstractObjectSerializerPtr idElem = serializer->createElement();
        idElem->setString("$oid", serializer->getStringId());
        serializer->setElement(NATURAL_ID_FIELD_NAME, idElem);
    }
}

void MongoSerializer::deserializeMongoId(const armarx::JSONObjectPtr& deserializer)
{
    if (useMongoIds && deserializer->hasElement(NATURAL_ID_FIELD_NAME))
    {
        deserializer->setString(NATURAL_ID_FIELD_NAME, deserializer->getString(MONGO_OBJECTID_FIELD_NAME));
    }
}

