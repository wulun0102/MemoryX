/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::WorkingMemory
* @author     ALexey Kozlov ( kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <Ice/Ice.h>

#include <MemoryX/core/entity/ProbabilityMeasures.h>
#include <MemoryX/core/entity/EntityAttribute.h>
#include <MemoryX/core/entity/EntityRef.h>
#include <MemoryX/core/entity/Entity.h>
#include <MemoryX/core/MongoDBRef.h>
#include <ArmarXCore/core/system/FactoryCollectionBase.h>
#include <MemoryX/core/memory/SegmentUtilImplementations.h>

namespace memoryx::ObjectFactories
{
    class MemoryXCoreObjectFactories : public armarx::FactoryCollectionBase
    {
    public:
        armarx::ObjectFactoryMap getFactories() override
        {
            armarx::ObjectFactoryMap map;

            add<memoryx::EntityAttributeBase, EntityAttribute>(map);
            add<memoryx::MongoDBRefBase, MongoDBRef>(map);

            add<memoryx::DiscreteProbabilityBase, DiscreteProbability>(map);
            add<memoryx::UnivariateNormalDistributionBase, memoryx::UnivariateNormalDistribution>(map);
            add<memoryx::IsotropicNormalDistributionBase, memoryx::IsotropicNormalDistribution>(map);
            add<memoryx::MultivariateNormalDistributionBase, memoryx::MultivariateNormalDistribution>(map);
            add<memoryx::GaussianMixtureDistributionBase, memoryx::GaussianMixtureDistribution>(map);

            add<EntityRefBase, EntityRef>(map);
            add<EntityBase, Entity>(map);
            add<SegmentLockBase, SegmentLock>(map);
            return map;
        }

        static bool FactoryRegistrationDummyVar;
    };
    const armarx::FactoryCollectionBaseCleanUp MemoryXCoreObjectFactoriesVar = armarx::FactoryCollectionBase::addToPreregistration(new MemoryXCoreObjectFactories());
}
