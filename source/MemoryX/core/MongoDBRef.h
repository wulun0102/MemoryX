/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Core
* @author     Alexey Kozlov ( kozlov at kit dot edu)
* @date       Sep 23, 2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <MemoryX/interface/core/MongoDBRefBase.h>

#include <ArmarXCore/observers/variant/Variant.h>
#include <ArmarXCore/observers/AbstractObjectSerializer.h>

namespace armarx::VariantType
{
    const VariantTypeId MongoDBRef = Variant::addTypeName("::memoryx::MongoDBRefBase");
}

namespace memoryx
{
    /**
     * @class MongoDBRef
     * @brief Represents a cross-database reference to a document in MongoDB.
     * @ingroup Core
     *
     * The MongoDBRef holds a pair of (database_name, document_id). It is currently used to store
     * references to GridFS files in EntityAttribute.
     */
    class MongoDBRef :
        public MongoDBRefBase
    {
    public:
        /**
         * Construct an empty MongoDBRef
         *
         */
        MongoDBRef();

        /**
         * Construct a MongoDBRef
         *
         * @param dbName  mongo database name
         * @param docId   document ID
         *
         */
        MongoDBRef(const ::std::string& dbName, const ::std::string& docId);
        ~MongoDBRef() override;

        // inherited from VariantDataClass
    public:
        Ice::ObjectPtr ice_clone() const override
        {
            return this->clone();
        }
        armarx::VariantDataClassPtr clone(const Ice::Current& c = Ice::emptyCurrent) const override
        {
            return new MongoDBRef(*this);
        }
        std::string output(const Ice::Current& c = Ice::emptyCurrent) const override
        {
            std::stringstream s;
            s << dbName << ":" << docId;
            return s.str();
        }
        armarx::VariantTypeId getType(const Ice::Current& c = Ice::emptyCurrent) const override
        {
            return armarx::VariantType::MongoDBRef;
        }
        bool validate(const Ice::Current& c = Ice::emptyCurrent) override
        {
            return true;
        }

        /**
         * @brief Write this DBRef into serializer.
         *
         *
         */
        void serialize(const armarx::ObjectSerializerBasePtr& serializer, const ::Ice::Current& c = Ice::emptyCurrent) const override;

        /**
         * @brief Read DBRef from serializer.
         *
         */
        void deserialize(const armarx::ObjectSerializerBasePtr& serializer, const ::Ice::Current& c = Ice::emptyCurrent) override;
    };
    using MongoDBRefPtr = IceInternal::Handle<MongoDBRef>;

}

