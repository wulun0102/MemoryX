/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::Core
* @author     ALexey Kozlov ( kozlov at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

// Ice
#include <Ice/ObjectAdapter.h>

#include <MemoryX/interface/core/MemoryInterface.h>

#include <shared_mutex>

namespace memoryx
{

    struct MemorySegmentEntry
    {
        AbstractMemorySegmentPrx proxy;
        AbstractMemorySegmentPtr pointer;
    };
    using MemorySegmentMap = std::map<std::string, MemorySegmentEntry>;

    /*!
     * \brief The SegmentedMemory class provides an interface for organizing memories with segmented data structures.
     */
    class SegmentedMemory:
        virtual public MemoryInterface
    {
    protected:
        ~SegmentedMemory() override;
    public:
        SegmentedMemory();


        AbstractMemorySegmentPrx addSegment(const std::string& segmentName, const AbstractMemorySegmentPtr& segment, const ::Ice::Current& = Ice::emptyCurrent) override;
        bool hasSegment(const std::string& segmentName, const ::Ice::Current& = Ice::emptyCurrent) const override;
        AbstractMemorySegmentPrx getSegment(const std::string& segmentName, const ::Ice::Current& = Ice::emptyCurrent) const override;
        void removeSegment(const std::string& segmentName, const ::Ice::Current& = Ice::emptyCurrent) override;
        NameList getSegmentNames(const ::Ice::Current& = Ice::emptyCurrent) const override;

        EntityBasePtr findEntityById(const std::string& entityId, const Ice::Current& c = Ice::emptyCurrent) override;
        EntityRefBasePtr findEntityRefById(const std::string& entityId, const Ice::Current& c = Ice::emptyCurrent) override;

        AbstractMemorySegmentPtr getSegmentPtr(const std::string& segmentName);

        virtual Ice::ObjectAdapterPtr getObjectAdapter() const = 0;

    protected:
        MemorySegmentMap segments;
        mutable std::shared_mutex segmentsMutex;

    };


    using SegmentedMemoryPtr = IceInternal::Handle<SegmentedMemory>;

}

