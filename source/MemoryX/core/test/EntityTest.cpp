/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::Core
 * @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE MemoryX::EntityTests
#define ARMARX_BOOST_TEST
#include <MemoryX/Test.h>

#include "../entity/Entity.h"

#include <ArmarXCore/observers/variant/StringValueMap.h>

class TestEntity : public memoryx::Entity
{
public:
    TestEntity() :
        memoryx::Entity()
    {
    }
};

using TestEntityPtr = IceInternal::Handle<TestEntity>;


BOOST_AUTO_TEST_SUITE(PersistentProfilerSegmentTest)

BOOST_AUTO_TEST_CASE(emptyEntityEquality)
{
    TestEntityPtr entity1 = new TestEntity();
    TestEntityPtr entity2 = new TestEntity();
    BOOST_CHECK(entity1->equals(entity2));
    BOOST_CHECK(entity2->equals(entity1));
}

BOOST_AUTO_TEST_CASE(entityIdEquality)
{
    TestEntityPtr entity1 = new TestEntity();
    TestEntityPtr entity2 = new TestEntity();
    entity1->setId("Id1");
    BOOST_CHECK(!entity1->equals(entity2));
    BOOST_CHECK(!entity2->equals(entity1));

    entity2->setId("Id1");
    BOOST_CHECK(entity1->equals(entity2));
    BOOST_CHECK(entity2->equals(entity1));
}

BOOST_AUTO_TEST_CASE(entityIdAndNameEquality)
{
    TestEntityPtr entity1 = new TestEntity();
    TestEntityPtr entity2 = new TestEntity();
    entity1->setId("Id1");
    entity2->setId("Id1");

    entity1->setName("Name1");
    BOOST_CHECK(!entity1->equals(entity2));

    entity2->setName("Name1");
    BOOST_CHECK(entity1->equals(entity2));
}

BOOST_AUTO_TEST_CASE(entityIdAndNameAndAttributeEquality)
{
    TestEntityPtr entity1 = new TestEntity();
    TestEntityPtr entity2 = new TestEntity();
    entity1->setId("Id1");
    entity2->setId("Id1");
    entity1->setName("Name1");
    entity2->setName("Name1");

    entity1->putAttribute<std::string>("attribute1", "value1");
    BOOST_CHECK(!entity1->equals(entity2));

    entity2->putAttribute<std::string>("attribute1", "value1");
    BOOST_CHECK(entity1->equals(entity2));

    entity1->putAttribute<std::string>("attribute2", "value1");
    entity2->putAttribute<std::string>("attribute2", "value2");
    BOOST_CHECK(!entity1->equals(entity2));
}

BOOST_AUTO_TEST_CASE(entityNameAndAttributeEquality)
{
    TestEntityPtr entity1 = new TestEntity();
    TestEntityPtr entity2 = new TestEntity();
    entity1->setId("Id1");
    entity2->setId("Id2");
    entity1->setName("Name1");
    entity2->setName("Name1");

    entity1->putAttribute<std::string>("attribute1", "value1");
    BOOST_CHECK(!entity1->equalsAttributes(entity2));

    entity2->putAttribute<std::string>("attribute1", "value1");
    BOOST_CHECK(entity1->equalsAttributes(entity2));

    entity1->putAttribute<std::string>("attribute2", "value1");
    entity2->putAttribute<std::string>("attribute2", "value2");
    BOOST_CHECK(!entity1->equalsAttributes(entity2));
}

BOOST_AUTO_TEST_CASE(entityComplexAttributeEquality)
{
    TestEntityPtr entity1 = new TestEntity();
    TestEntityPtr entity2 = new TestEntity();
    entity1->setId("Id1");
    entity2->setId("Id2");
    entity1->setName("Name1");
    entity2->setName("Name1");

    armarx::StringValueMapPtr attributeMap1 = armarx::StringValueMap::FromStdMap<std::string>({});
    armarx::StringValueMapPtr attributeMap2 = armarx::StringValueMap::FromStdMap<std::string>({{"key1", "value1"}});

    armarx::StringValueMapPtr attributeMap3a = armarx::StringValueMap::FromStdMap<std::string>({{"key2", "value1"}});
    armarx::StringValueMapPtr attributeMap3b = armarx::StringValueMap::FromStdMap<std::string>({{"key2", "value2"}});

    entity1->putAttribute<armarx::StringValueMapPtr>("attribute1", attributeMap1);
    BOOST_CHECK(!entity1->equalsAttributes(entity2));

    entity2->putAttribute<armarx::StringValueMapPtr>("attribute1", attributeMap1);
    BOOST_CHECK(entity1->equalsAttributes(entity2));

    entity1->putAttribute<armarx::StringValueMapPtr>("attribute1", attributeMap2);
    BOOST_CHECK(!entity1->equalsAttributes(entity2));

    entity2->putAttribute<armarx::StringValueMapPtr>("attribute1", attributeMap2);
    BOOST_CHECK(entity1->equalsAttributes(entity2));

    entity1->putAttribute<armarx::StringValueMapPtr>("attribute2", attributeMap3a);
    entity2->putAttribute<armarx::StringValueMapPtr>("attribute2", attributeMap3b);
    BOOST_CHECK(!entity1->equalsAttributes(entity2));
}

BOOST_AUTO_TEST_SUITE_END()
