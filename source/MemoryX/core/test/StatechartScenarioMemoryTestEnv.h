/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include <ArmarXCore/statechart/test/StatechartScenarioTestEnv.h>

#include <MemoryX/interface/components/CommonStorageInterface.h>
#include <MemoryX/interface/components/WorkingMemoryInterface.h>
#include <MemoryX/interface/components/LongtermMemoryInterface.h>
#include <MemoryX/interface/components/PriorKnowledgeInterface.h>
#include <MemoryX/interface/memorytypes/MemorySegments.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectClass.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectInstance.h>

namespace armarx
{

    struct MemoryAccessObject : armarx::ManagedIceObject
    {
    public:
        std::string getDefaultName() const override
        {
            return "MemoryAccessObject";
        }

        memoryx::CommonStorageInterfacePrx commonStorage;
        memoryx::PriorKnowledgeInterfacePrx priorKnowledge;
        memoryx::WorkingMemoryInterfacePrx workingMemory;
        memoryx::LongtermMemoryInterfacePrx longtermMemory;


        // ManagedIceObject interface
    protected:
        void onInitComponent() override
        {
            usingProxy("CommonStorage");
            usingProxy("WorkingMemory");
            usingProxy("LongtermMemory");
            usingProxy("PriorKnowledge");
        }

        void onConnectComponent() override
        {
            assignProxy(commonStorage, "CommonStorage");
            assignProxy(priorKnowledge, "PriorKnowledge");
            assignProxy(workingMemory, "WorkingMemory");
            assignProxy(longtermMemory, "LongtermMemory");
        }
    };

    using MemoryAccessObjectPtr = IceInternal::Handle<MemoryAccessObject>;


    class StatechartScenarioMemoryTestEnvironment : public armarx::StatechartScenarioTestEnvironment
    {
    public:
        StatechartScenarioMemoryTestEnvironment(const std::string& testName, const std::string& dbPackage = "ArmarXDB", const std::string pathToDatabase = "ArmarXDB/dbexport/memdb",  int registryPort = 11220)
            : armarx::StatechartScenarioTestEnvironment(testName, registryPort)
        {
            memoryAccess = new MemoryAccessObject;
            manager->addObject(memoryAccess, "MemoryAccessObject" + IceUtil::generateUUID());

            armarx::CMakePackageFinder memoryXFinder("MemoryX");
            this->pathToDB = pathToDatabase;
            std::filesystem::path dbbasedir;
            armarx::CMakePackageFinder dbpackFinder(dbPackage);
            dbbasedir = dbpackFinder.getDataDir();
            if (dbPackage == "ArmarXDB" && !dbpackFinder.packageFound()) // handle special case of ArmarXDB, because nobody cmakes the ArmarXDB package...
            {
                std::filesystem::path memoryDir(memoryXFinder.getPackageDir());
                memoryDir.parent_path();

                dbbasedir = memoryDir.parent_path() / dbPackage / "data";
                if (!std::filesystem::exists(dbbasedir))
                {
                    dbbasedir = "";
                }
            }
            if (dbbasedir.empty())
            {
                throw armarx::LocalException() << "Could not find DB package: " << dbPackage;
            }
            std::string command = memoryXFinder.getBinaryDir() + "/mongod.sh stop";
            [[maybe_unused]] auto discard = system(command.c_str());
            discard = system("killall mongod");
            mongoDataPath = armarx::test::getCmakeValue("ARMARX_USER_CONFIG_DIR_ABSOLUT") + "/mongo/data/db_test_" + testName;
            std::string startcommand = memoryXFinder.getBinaryDir() + "/mongod.sh start " + mongoDataPath;
            stopcommand = memoryXFinder.getBinaryDir() + "/mongod.sh stop " + mongoDataPath;
            ARMARX_INFO_S << VAROUT(startcommand);
            int res = system(startcommand.c_str());
            res = WEXITSTATUS(res);

            ARMARX_INFO_S << "Start result: " << res;
            if (res == 100)
            {
                command = memoryXFinder.getBinaryDir() + "/mongod.sh repair " + mongoDataPath;
                discard = system(command.c_str());
                res = system(startcommand.c_str());
                res = WEXITSTATUS(res);
                if (res != 0)
                {
                    ARMARX_ERROR_S << ("Could not start mongodb - not even with repairing - exit code was: ")  << res;
                    throw armarx::LocalException("Could not start mongodb - not even with repairing - exit code was: ")  << res;
                }
            }
            std::string command2 = memoryXFinder.getBinaryDir() + "/mongoimport.sh " + dbbasedir.string() + "/" + this->pathToDB + "";
            res = system(command2.c_str());

        }
        ~StatechartScenarioMemoryTestEnvironment()
        {
            if (memoryAccess->commonStorage)
            {
                for (std::string collection : memoryAccess->commonStorage->getCollectionNames("testdb_" + this->testName))
                {
                    memoryAccess->commonStorage->dropCollection(collection);
                }
            }

            manager->removeObjectNonBlocking(memoryAccess->getName());
            [[maybe_unused]] auto discard = system(stopcommand.c_str());
        }

        MemoryAccessObjectPtr memoryAccess;
        std::string pathToDB;
        std::string mongoDataPath;
        std::string stopcommand;
    };

    using StatechartScenarioMemoryTestEnvironmentPtr = std::shared_ptr<StatechartScenarioMemoryTestEnvironment>;
}
