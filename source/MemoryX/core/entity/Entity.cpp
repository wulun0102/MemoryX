/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::WorkingMemory
* @author     ALexey Kozlov ( kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "Entity.h"
#include "ProbabilityMeasures.h"

#include <ArmarXCore/observers/AbstractObjectSerializer.h>

#include <ArmarXCore/util/json/JSONObject.h>
#include <ArmarXCore/util/CPPUtility/trace.h>

namespace memoryx
{

    Entity::Entity(const Entity& source) :
        IceUtil::Shared(source),
        ::armarx::Serializable(source),
        EntityBase()
    {
        deepCopy(source);
    }

    EntityPtr Entity::CreateGenericEntity()
    {
        return EntityPtr(new Entity());
    }

    void Entity::deepCopy(const Entity& source)
    {
        {
            //            boost::shared_lock<boost::shared_mutex> lock(source.entityMutex);
            std::shared_lock lock2(entityMutex);
            id = source.id;
            name = source.name;
        }
        //        ARMARX_INFO_S << "Entity copy const - " << VAROUT(source.wrappers.size());
        // clone attributes
        {
            //            armarx::ScopedLock lock(source.attributesMutex);
            std::scoped_lock lock2(attributesMutex);
            attributes.clear();

            for (AttributeMap::const_iterator iter = source.attributes.begin() ; iter != source.attributes.end() ; iter++)
            {
                EntityAttributeBasePtr attrBase = iter->second;
                EntityAttributeBasePtr attr = EntityAttributeBasePtr::dynamicCast(attrBase->ice_clone());
                attributes[iter->first] = attr;
            }
        }
        // clone wrappers
        {
            //            RecursiveMutex::scoped_lock  lock(source.wrappersMutex);
            std::scoped_lock lock2(wrappersMutex);
            wrappers.clear();

            for (AbstractEntityWrapperBaseList::const_iterator iter = source.wrappers.begin() ; iter != source.wrappers.end() ; iter++)
            {
                if ((*iter))
                {
                    EntityWrappers::AbstractEntityWrapperPtr wrapper = EntityWrappers::AbstractEntityWrapperPtr::dynamicCast((*iter)->ice_clone());

                    if (wrapper)
                    {
                        wrapper->setEntity(this);
                        wrappers.push_back(wrapper);
                    }
                }
            }
        }
    }

    void Entity::__decRef()
    {
        int wrapperCount;
        {
            std::scoped_lock  lock(wrappersMutex);
            wrapperCount = wrappers.size();
        }
        int remainingRefs = __getRef() - 1;
        if (
            remainingRefs <= wrapperCount &&
            remainingRefs > 0 &&
            wrapperCount > 0 &&

#if ICE_INT_VERSION >= 30603
            !__hasFlag(NoDelete)
#else
            !_noDelete
#endif
        )
        {
            std::scoped_lock  lock(wrappersMutex);
            int counter = 0;
            for (const AbstractEntityWrapperBasePtr& wrapper : wrappers)
            {
                EntityWrappers::AbstractEntityWrapperPtr p = EntityWrappers::AbstractEntityWrapperPtr::dynamicCast(wrapper);
                if (p)
                {
                    if (p->getEntity().get() == this)
                    {
                        counter++;
                    }
                }

            }
            if (counter == remainingRefs)
            {
                // only the wrappers point on this object
                // first make a temporal copy of the wrapper list, then delete the wrappers list
                // so that when the temp wrapper list is deleted and the ref count of the entity decreased, the wrappers.size() check is zero and this part is not run again
                auto tmpWrappers = wrappers;
                wrappers.clear();
                for (const AbstractEntityWrapperBasePtr& wrapper : tmpWrappers)
                {
                    EntityWrappers::AbstractEntityWrapperPtr p = EntityWrappers::AbstractEntityWrapperPtr::dynamicCast(wrapper);
                    p->setEntity(NULL);
                }
            }
        }
        Shared::__decRef();
    }

    Entity::~Entity()
    {
        //        ARMARX_INFO_S << "Deleting entity with id " << id;
        __setNoDelete(true);
        //        ARMARX_INFO_S << "~Entity() " << getName();
        // Remove entity ptr in wrapper, because it becomes invalid
        // it is not a shared ptr because it would introduce a cycle of shared ptrs
        //        auto tmpWrapper = wrappers;
        //        wrappers.clear();
        //        for (const AbstractEntityWrapperBasePtr& wrapper : tmpWrapper)
        //        {
        //            auto p = EntityWrappers::AbstractEntityWrapperPtr::dynamicCast(wrapper);
        //            if (p)
        //            {
        //                p->setEntity(NULL);
        //            }
        //        }
    }

    ::std::string Entity::getId(const ::Ice::Current&) const
    {
        std::shared_lock lock(entityMutex);
        return id;
    }

    void Entity::setId(const ::std::string& id, const ::Ice::Current&)
    {
        std::unique_lock lock(entityMutex);
        this->id = id;
    }

    ::std::string Entity::getName(const ::Ice::Current&) const
    {
        std::shared_lock lock(entityMutex);
        return name;
    }

    void Entity::setName(const ::std::string& name, const ::Ice::Current&)
    {
        std::unique_lock lock(entityMutex);
        this->name = name;
    }

    bool Entity::isMetaEntity(const Ice::Current&) const
    {
        if (hasAttribute("isMetaEntity"))
        {
            auto attr = getAttribute("isMetaEntity");
            return attr->getValue()->getBool();
        }

        return false;
    }

    void Entity::setMetaEntity(bool isMetaEntity, const Ice::Current&)
    {
        if (hasAttribute("isMetaEntity"))
        {
            removeAttribute("isMetaEntity");
        }

        EntityAttributePtr attr = new EntityAttribute("isMetaEntity");
        attr->addValue(armarx::VariantPtr(new armarx::Variant(isMetaEntity)));
        putAttribute(attr);
    }

    EntityRefBaseList Entity::getDirectParentRefs() const
    {
        EntityRefBaseList result;

        if (!hasAttribute("parentEntityRefs"))
        {
            return result;
        }

        auto attr = getAttribute("parentEntityRefs");

        for (int i = 0; i < attr->size(); i++)
        {
            result.push_back(armarx::VariantPtr::dynamicCast(attr->getValueAt(i))->get<EntityRefBase>());
        }

        return result;
    }

    EntityRefBaseList Entity::getAllParentRefs(bool includeMetaEntities) const
    {
        EntityRefBaseList result;
        EntityRefBaseList explored;
        EntityRefBaseList unexplored = getDirectParentRefs();

        while (!unexplored.empty())
        {
            EntityRefBasePtr entityRef = unexplored.back();
            EntityPtr next = EntityPtr::dynamicCast(entityRef->getEntity());
            unexplored.pop_back();

            bool isExplored = std::find_if(explored.cbegin(), explored.cend(), [&](const EntityRefBasePtr & entityRef)
            {
                return entityRef->entityId == next->getId();
            }) != explored.cend();

            if (isExplored)
            {
                continue;
            }

            EntityRefBaseList newParents = next->getDirectParentRefs();
            explored.push_back(entityRef);
            unexplored.insert(unexplored.end(), newParents.begin(), newParents.end());

            if (next->isMetaEntity() && !includeMetaEntities)
            {
                continue;
            }

            result.push_back(entityRef);
        }

        return result;
    }

    std::vector<std::string> Entity::getAllParentsAsStringList() const
    {
        Ice::StringSeq result;
        const auto& parents = getAllParentRefs();
        std::transform(parents.cbegin(), parents.cend(), std::back_inserter(result), [](const EntityRefBasePtr & e)
        {
            return e->entityName;
        });

        return result;
    }

    void Entity::setDirectParentRefs(const EntityRefBaseList& entityRefs)
    {
        if (hasAttribute("parentEntityRefs"))
        {
            removeAttribute("parentEntityRefs");
        }

        EntityAttributePtr attr = new EntityAttribute("parentEntityRefs");

        for (const auto& entity : entityRefs)
        {
            attr->addValue(new armarx::Variant(entity));
        }

        putAttribute(attr);
    }

    EntityAttributeBasePtr Entity::getAttribute(const ::std::string& attrName, const ::Ice::Current&) const
    {
        std::scoped_lock lock(attributesMutex);
        AttributeMap::const_iterator it = attributes.find(attrName);

        if (it != attributes.end())
        {
            return it->second;
        }
        else
        {
            return EntityAttributeBasePtr();
        }
    }

    armarx::VariantPtr Entity::getAttributeValue(const ::std::string& attrName) const
    {
        EntityAttributeBasePtr attr = getAttribute(attrName);

        // check if attribute exists
        if (!attr)
        {
            return armarx::VariantPtr();
        }

        // check if attr has exactly one value (ie not empty and not multimodal)
        if (attr->size() != 1)
        {
            return armarx::VariantPtr();
        }

        return armarx::VariantPtr::dynamicCast(attr->getValue());
    }

    void Entity::putAttribute(const ::memoryx::EntityAttributeBasePtr& attr, const ::Ice::Current&)
    {
        std::scoped_lock lock(attributesMutex);
        attributes[attr->getName()] = attr;
    }

    void Entity::removeAttribute(const ::std::string& attrName, const ::Ice::Current&)
    {
        std::scoped_lock lock(attributesMutex);
        attributes.erase(attrName);
    }

    bool Entity::hasAttribute(const std::string& attrName, const Ice::Current& c) const
    {
        ARMARX_TRACE;
        std::scoped_lock lock(attributesMutex);
        return attributes.find(attrName) != attributes.end();
    }

    memoryx::NameList Entity::getAttributeNames(const Ice::Current&) const
    {
        memoryx::NameList result;
        std::scoped_lock lock(attributesMutex);

        for (AttributeMap::const_iterator it = attributes.begin(); it != attributes.end(); ++it)
        {
            result.push_back(it->first);
        }

        return result;
    }


    bool Entity::equals(const EntityBasePtr& otherEntity, const Ice::Current&) const
    {
        if (getId() != otherEntity->getId())
        {
            return false;
        }
        return equalsAttributes(otherEntity);
    }

    bool Entity::equalsAttributes(const EntityBasePtr& otherEntity, const Ice::Current&) const
    {
        if (getName() != otherEntity->getName())
        {
            return false;
        }
        NameList attributeNames = getAttributeNames();
        if (otherEntity->getAttributeNames().size() != attributeNames.size())
        {
            return false;
        }

        for (std::string& attributeName : attributeNames)
        {
            // check if attribute values are equal
            armarx::JSONObjectPtr thisSerializer = new armarx::JSONObject();
            armarx::JSONObjectPtr otherSerializer = new armarx::JSONObject();
            getAttribute(attributeName)->serialize(thisSerializer);
            otherEntity->getAttribute(attributeName)->serialize(otherSerializer);
            if (otherSerializer->toString() != thisSerializer->toString())
            {
                return false;
            }
        }

        return true;
    }

    Ice::ObjectPtr Entity::ice_clone() const
    {
        return this->clone();
    }

    EntityPtr Entity::clone(const Ice::Current& c) const
    {
        EntityPtr ret = new Entity();
        ret->deepCopy(*this);
        return ret;
    }

    void Entity::output(std::ostream& stream) const
    {
        std::scoped_lock lock(attributesMutex);
        stream << "[" << id << "] " << name << std::endl;
        stream << "Attributes: (" << attributes.size() << ")" << std::endl;

        for (AttributeMap::const_iterator it = attributes.begin(); it != attributes.end(); ++it)
        {
            stream << it->first << ": " << EntityAttributePtr::dynamicCast(it->second) << std::endl;
        }
    }

    void Entity::ice_preMarshal()
    {
        // wrappers are only locally available -> clear them before sending (they would be broken on the other side)
        //    armarx::RecursiveMutex::scoped_lock lock(wrappersMutex);
        //    wrappers.clear();
        //    for(auto& elem : wrappers)
        //    {
        //        elem = NULL;
        //    }
        //    ARMARX_INFO_S << getName() << " size: " << wrappers.size();
    }

    void Entity::ice_postUnmarshal()
    {
        //    ARMARX_INFO_S << getName() << " size: " << wrappers.size();
        std::scoped_lock  lock(wrappersMutex);
        wrappers.clear();
    }


    void Entity::serialize(const armarx::ObjectSerializerBasePtr& serializer, const ::Ice::Current& c) const
    {
        armarx::AbstractObjectSerializerPtr obj = armarx::AbstractObjectSerializerPtr::dynamicCast(serializer);
        {
            std::shared_lock lock(entityMutex);

            // empty ids shouldn't be added to allow them to be auto-generated
            if (!id.empty())
            {
                obj->setId(id);
            }

            obj->setString("name", name);
        }

        armarx::AbstractObjectSerializerPtr objAttrs = obj->createElement();
        {
            std::scoped_lock lock(attributesMutex);

            for (AttributeMap::const_iterator it = attributes.begin(); it != attributes.end(); ++it)
            {
                armarx::AbstractObjectSerializerPtr attr = obj->createElement();
                it->second->serialize(attr, c);
                objAttrs->setElement(it->first, attr);
            }
        }
        obj->setElement("attrs", objAttrs);
    }

    void Entity::deserialize(const armarx::ObjectSerializerBasePtr& serializer, const ::Ice::Current& c)
    {
        armarx::AbstractObjectSerializerPtr obj = armarx::AbstractObjectSerializerPtr::dynamicCast(serializer);
        {
            std::unique_lock lock(entityMutex);

            if (!obj->getIdField().empty() && obj->hasElement(obj->getIdField()))
            {
                id = obj->getStringId();
            }

            name = obj->getString("name");
        }

        const armarx::AbstractObjectSerializerPtr objAttrs = obj->getElement("attrs");
        const std::vector<std::string> attrNames = objAttrs->getElementNames();
        {
            // putAttribute() also locks the datastructure!
            std::scoped_lock lock(attributesMutex);
            attributes.clear();
        }

        for (std::vector<std::string>::const_iterator it = attrNames.begin(); it != attrNames.end(); ++it)
        {
            EntityAttributePtr attr = new EntityAttribute(*it);
            attr->deserialize(objAttrs->getElement(*it));
            putAttribute(attr);
        }
    }

}
